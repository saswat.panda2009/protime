﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class master_staff
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtcaste.Text = ""
        txtcitycd.Text = ""
        txtcyclesl.Text = ""
        txtdeptcd.Text = ""
        txtdesgcd.Text = ""
        txtdevicecode.Text = ""
        txtdivsl.Text = ""
        txtdob.Text = Format(Now.AddYears(-18), "dd/MM/yyyy")
        txtemail.Text = ""
        txtempid.Text = ""
        txtguardian.Text = ""
        txtjoindate.Text = Format(Now, "dd/MM/yyyy")
        txtnal.Text = ""
        txtperadd.Text = ""
        txtperdistcd.Text = ""
        txtperph1.Text = ""
        txtperph2.Text = ""
        txtpreadd.Text = ""
        txtperstate.Text = ""
        txtpredistcd.Text = ""
        txtpreph1.Text = ""
        txtpreph2.Text = ""
        txtprestate.Text = ""
        txtquali.Text = ""
        txtreligion.Text = ""
        txtrfid.Text = ""
        txtshortname.Text = ""
        txtshiftsl.Text = ""
        txtstaff.Text = ""
        txtstatcd.Text = ""
        txtcatsl.Text = ""
        cmbempstatus.SelectedIndex = 0
        cmbemptp.SelectedIndex = 1
        cmbgender.SelectedIndex = 0
        cmbmaried.SelectedIndex = 0
        cmbshifttp.SelectedIndex = 0
        cmbpaypayment.SelectedIndex = 0
        cmbwagestype.SelectedIndex = 0
        chkapp.Checked = True
        chkconveyances.Checked = False
        txtwages.Text = ""
        txtpacno.Text = ""
        txtpbanknm.Text = ""
        txtpfno.Text = ""
        txtppanno.Text = ""
        txtifsc.Text = ""
        txtesic.Text = ""
        txtpfno.Text = ""
        txtuno.Text = ""
        txtgroupsl.Text = ""
        txtmother.Text = ""
        txtspouse.Text = ""
        txtchildren.Text = ""
        txtbloodgroup.Text = ""
        txtemergencynm.Text = ""
        txtrelation.Text = ""
        txtemergencycontact.Text = ""
        txtemergencyaddr.Text = ""
        txtdriving.Text = ""
        txtvoterid.Text = ""
        txtaadhar.Text = ""
        txtresign.Text = "01/01/1900"
        txtreason.Text = ""
        chkweekcompoff.Checked = False
        chkholidayoff.Checked = False
        'dvstaf.DataSource = Nothing
        'dvstaf.DataBind()
        Me.shift()
        Me.paygroup()
        rdglobal.Checked = True
        Me.deptdisp()
        Me.desgdisp()
        Me.per_distdisp()
        Me.pre_distdisp()
        Me.divisiondisp()
        Me.categorydisp()
        Me.payrollheaddisp()
        divmsg.Visible = False
        divmsg.Attributes("class") = "alert bg-success"
        If txtmode.Text = "E" Then
            TabContainer1.ActiveTabIndex = 0
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Employee Master (Entry Mode)"
            txtstaff.Focus()
        ElseIf txtmode.Text = "M" Then
            TabContainer1.ActiveTabIndex = 0
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Employee Master (Edit Mode)"
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Staff Master (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub payrollheaddisp()
        'Dim ds As DataSet = get_dataset("select head_nm,0 as no_day from payroll_component")
        'dvstaf.DataSource = ds.Tables(0)
        'dvstaf.DataBind()
    End Sub

    Private Sub dvdisp()
        Dim ds1 As DataSet = get_dataset("SELECT row_number() over(ORDER BY staf_nm) as 'sl',staf.staf_nm, staf.short_nm, staf.emp_code, dept_mst.dept_nm, desg_mst.desg_nm FROM staf LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl WHERE staf.loc_cd=" & CType(Session("loc_cd"), Integer) & " ORDER BY staf_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    'Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim chkhdr As CheckBox = dvstaf.HeaderRow.FindControl("chk1")
    '    If chkhdr.Checked = True Then
    '        For i As Integer = 0 To dvstaf.Rows.Count - 1
    '            Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
    '            chk.Checked = True
    '        Next
    '    ElseIf chkhdr.Checked = False Then
    '        For i As Integer = 0 To dvstaf.Rows.Count - 1
    '            Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
    '            chk.Checked = False
    '        Next
    '    End If
    'End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub paygroup()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbgroup.Items.Clear()
        cmbgroup.Items.Add("Please Select A Group")
        Dim ds As DataSet = get_dataset("select group_nm from payroll_group  WHERE loc_cd=" & loc_cd & " ORDER BY group_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbgroup.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub categorydisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbempcat.Items.Clear()
        cmbempcat.Items.Add("Please Select A Category")
        Dim ds As DataSet = get_dataset("select cat_nm from emp_category  WHERE loc_cd=" & loc_cd & " ORDER BY cat_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbempcat.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub desgdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdesg.Items.Clear()
        cmbdesg.Items.Add("Please Select A Desgination")
        Dim ds As DataSet = get_dataset("SELECT desg_nm FROM desg_mst WHERE loc_cd=" & loc_cd & " ORDER BY desg_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdesg.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub deptdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdept.Items.Clear()
        cmbdept.Items.Add("Please Select A Department")
        Dim ds As DataSet = get_dataset("SELECT dept_nm FROM dept_mst WHERE loc_cd=" & loc_cd & " ORDER BY dept_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdept.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub per_distdisp()
        cmbperdist.Items.Clear()
        cmbperdist.Items.Add("Please Select A District")
        Dim ds As DataSet = get_dataset("SELECT city_name FROM city ORDER BY city_name")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbperdist.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Sub pre_distdisp()
        cmbpredist.Items.Clear()
        cmbpredist.Items.Add("Please Select A District")
        Dim ds As DataSet = get_dataset("SELECT city_name FROM city ORDER BY city_name")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbpredist.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Protected Sub cmbdept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdept.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdeptcd.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT dept_sl FROM dept_mst WHERE dept_nm='" & Trim(cmbdept.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdeptcd.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub

    Protected Sub valid_dept_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles valid_dept.ServerValidate
        If Val(txtdeptcd.Text) = 0 Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If
    End Sub

    Protected Sub valid_desg_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles valid_desg.ServerValidate
        If Val(txtdesgcd.Text) = 0 Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If
    End Sub

    Protected Sub cmbdesg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdesg.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdesgcd.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT desg_sl FROM desg_mst WHERE desg_nm='" & Trim(cmbdesg.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdesgcd.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub

    Protected Sub cmbperdist_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbperdist.SelectedIndexChanged
        txtperdistcd.Text = ""
        txtperstate.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT city.city_cd, stat.stat_name FROM city LEFT OUTER JOIN stat ON city.stat_cd = stat.stat_cd WHERE city_name='" & Trim(cmbperdist.Text) & "'")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtperdistcd.Text = ds1.Tables(0).Rows(0).Item(0)
            txtperstate.Text = ds1.Tables(0).Rows(0).Item(1)
        End If
    End Sub


    Protected Sub cmbpredist_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbpredist.SelectedIndexChanged
        txtpredistcd.Text = ""
        txtprestate.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT city.city_cd, stat.stat_name FROM city LEFT OUTER JOIN stat ON city.stat_cd = stat.stat_cd WHERE city_name='" & Trim(cmbpredist.Text) & "'")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtpredistcd.Text = ds1.Tables(0).Rows(0).Item(0)
            txtprestate.Text = ds1.Tables(0).Rows(0).Item(1)
        End If
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If Val(txtdivsl.Text) = 0 Then
            TabContainer1.ActiveTabIndex = 0
            Exit Sub
        End If
        If Val(txtcatsl.Text) = 0 Then
            TabContainer1.ActiveTabIndex = 0
            Exit Sub
        End If
        If Val(txtdesgcd.Text) = 0 Then
            TabContainer1.ActiveTabIndex = 0
            Exit Sub
        End If
        If Val(txtdeptcd.Text) = 0 Then
            TabContainer1.ActiveTabIndex = 0
            Exit Sub
        End If
        If Val(txtdeptcd.Text) = 0 Then
            TabContainer1.ActiveTabIndex = 0
            Exit Sub
        End If
        Dim week_coff, holi_coff As Integer
        week_coff = IIf(chkweekcompoff.Checked, 1, 0)
        holi_coff = IIf(chkholidayoff.Checked, 1, 0)
        Dim block_app As String = "N"
        block_app = IIf(chkapp.Checked, "N", "Y")
        Dim allowconveyances As Integer = 0
        allowconveyances = IIf(chkconveyances.Checked, 1, 0)
        txtgroupsl.Text = ""
        Dim dsgroupsl As DataSet = get_dataset("SELECT group_sl FROM payroll_group WHERE group_nm='" & Trim(cmbgroup.Text) & "' AND loc_cd=" & loc_cd & "")
        If dsgroupsl.Tables(0).Rows.Count <> 0 Then
            txtgroupsl.Text = dsgroupsl.Tables(0).Rows(0).Item(0)
        End If
        Dim weekofftp As String = IIf(rdglobal.Checked, 1, 2)
        If cmbshifttp.SelectedIndex = 2 Or cmbshifttp.SelectedIndex = 3 Then
            txtcyclesl.Text = ""
            Dim ds1 As DataSet = get_dataset("SELECT cycle_sl FROM cycle1 WHERE cycle_nm='" & Trim(cmbshiftcycle.Text) & "' AND loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtcyclesl.Text = ds1.Tables(0).Rows(0).Item(0)
            End If
            If Val(txtcyclesl.Text) = 0 Then
                divmsg.Visible = True
                divmsg.Attributes("class") = "alert bg-danger"
                lblmsg.Text = "Please Select A Valid Shift Cycle."
                cmbshiftcycle.Focus()
                Exit Sub
            End If
        ElseIf cmbshifttp.SelectedIndex = 0 Then
            txtshiftsl.Text = ""
            Dim ds1 As DataSet = get_dataset("SELECT shift_sl FROM shift_mst  WHERE shift_nm='" & Trim(cmbshiftcycle.Text) & "' AND loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtshiftsl.Text = ds1.Tables(0).Rows(0).Item(0)
            End If
            If Val(txtshiftsl.Text) = 0 Then
                divmsg.Visible = True
                divmsg.Attributes("class") = "alert bg-danger"
                lblmsg.Text = "Please Select A Valid Shift."
                cmbshiftcycle.Focus()
                Exit Sub
            End If
        End If
        If txtmode.Text = "E" Then
            Dim dsname As DataSet = get_dataset("SELECT emp_code FROM staf WHERE emp_code='" & UCase(Trim(txtstaff.Text)) & "'")
            If dsname.Tables(0).Rows.Count <> 0 Then
                TabContainer1.ActiveTabIndex = 0
                divmsg.Visible = True
                divmsg.Attributes("class") = "alert bg-danger"
                lblmsg.Text = "Employee Employee Code Already Exits."
                txtempid.Focus()
                Exit Sub
            End If
            Dim dssname As DataSet = get_dataset("SELECT device_code FROM staf WHERE device_code='" & Trim(txtdevicecode.Text) & "'")
            If dssname.Tables(0).Rows.Count <> 0 Then
                TabContainer1.ActiveTabIndex = 0
                divmsg.Visible = True
                divmsg.Attributes("class") = "alert bg-danger"
                lblmsg.Text = "Device Code Already Exits."
                txtdevicecode.Focus()
                Exit Sub
            End If
            txtstatcd.Text = "1"
            Dim dsmax As DataSet = get_dataset("SELECT max(staf_sl) FROM staf")
            If Not IsDBNull(dsmax.Tables(0).Rows(0).Item(0)) Then
                txtstatcd.Text = dsmax.Tables(0).Rows(0).Item(0) + 1
            End If

            start1()
            SQLInsert("INSERT INTO staf(staf_sl,staf_nm,short_nm,emp_code,device_code,emp_status,emp_tp,desg_sl,dept_sl,doj," & _
            "add1,city_cd1,state1,ph1,ph2,add2,city_cd2,state2,ph3,ph4,email,dob,gndr,married,nation,religion,caste,fgh_nm,quali," & _
            "staf_image,loc_cd,div_sl,shift_tp,pswd,last_login,cat_sl,shift_sl,cycle_sl,weekly_off_tp,RFID_Cardno," & _
            "pay_mode,pan_no,bank_name,ac_no,ifsc_code,esi_no,pf_no,uan_no,group_sl,wages_type,wages,moth_nm,spouse_name,children_name," & _
            "blood_grp,emergency_name,relation,emergency_contact,emergency_addr,driving_license,voter_id,adhar_card,dor,reason,week_coff,holi_coff,allow_conveyances) VALUES(" & Val(txtstatcd.Text) & ",'" & Trim(txtstaff.Text) & "','" & Trim(txtshortname.Text) & _
            "','" & UCase(Trim(txtempid.Text)) & "','" & txtdevicecode.Text & "','" & vb.Left(cmbempstatus.Text, 1) & "','" & _
            vb.Left(cmbemptp.Text, 1) & "'," & Val(txtdesgcd.Text) & "," & Val(txtdeptcd.Text) & ",'" & Format(stringtodate(txtjoindate.Text), "dd/MMM/yyyy") & _
            "','" & UCase(Trim(txtpreadd.Text)) & "'," & Val(txtpredistcd.Text) & ",'" & UCase(Trim(txtprestate.Text)) & "','" & _
            Trim(txtpreph1.Text) & "','" & Trim(txtpreph2.Text) & "','" & UCase(Trim(txtperadd.Text)) & "'," & Val(txtperdistcd.Text) & _
            ",'" & UCase(Trim(txtperstate.Text)) & "','" & Trim(txtperph1.Text) & "','" & Trim(txtperph2.Text) & "','" & LCase(Trim(txtemail.Text)) & _
            "','" & Format(stringtodate(txtdob.Text), "dd/MMM/yyyy") & "','" & vb.Left(cmbgender.Text, 1) & "','" & vb.Left(cmbmaried.Text, 1) & "','" & UCase(Trim(txtnal.Text)) & _
            "','" & UCase(Trim(txtreligion.Text)) & "','" & UCase(Trim(txtcaste.Text)) & "','" & UCase(Trim(txtguardian.Text)) & "','" & UCase(Trim(txtquali.Text)) & _
            "',''," & loc_cd & "," & Val(txtdivsl.Text) & "," & Val(cmbshifttp.SelectedIndex) + 1 & ",'PASSWORD',''," & Val(txtcatsl.Text) & "," & Val(txtshiftsl.Text) & _
            "," & Val(txtcyclesl.Text) & ",'" & weekofftp & "','" & Trim(txtrfid.Text) & "','" & vb.Left(cmbpaypayment.Text, 1) & "','" & txtppanno.Text & "','" & _
            txtpbanknm.Text & "','" & txtpacno.Text & "','" & txtifsc.Text & "','" & txtesic.Text & "','" & txtpfno.Text & "','" & txtuno.Text & "'," & Val(txtgroupsl.Text) & _
            ",'" & vb.Left(cmbwagestype.Text, 1) & "'," & Val(txtwages.Text) & ",'" & txtmother.Text & "','" & txtspouse.Text & "','" & txtchildren.Text & "','" & txtbloodgroup.Text & _
            "','" & txtemergencynm.Text & "','" & txtrelation.Text & "','" & txtemergencycontact.Text & "','" & txtemergencyaddr.Text & "','" & txtdriving.Text & "','" & _
            txtvoterid.Text & "','" & txtaadhar.Text & "','" & Format(stringtodate(txtresign.Text), "dd/MMM/yyyy") & _
            "','" & Trim(txtreason.Text) & "'," & week_coff & "," & holi_coff & "," & allowconveyances & ")")

            SQLInsert("Update staf Set staf_image=isnull((select staff_img from temp_staff where usr_sl=" & CType(Session("usr_sl"), Integer) & "),'') WHERE staf_sl=" & Val(txtstatcd.Text) & "")

            SQLInsert("INSERT INTO Employees(EmployeeName,EmployeeCode,StringCode,NumericCode,Gender,CompanyId,DepartmentId,CategoryId,EmployeeCodeInDevice," & _
            "EmployeeRFIDNumber,EmployementType,Status)  VALUES('" & Trim(txtstaff.Text) & "','" & Trim(txtempid.Text) & "','',0,'M'," & loc_cd & _
            "," & Val(txtdeptcd.Text) & "," & Val(txtcatsl.Text) & ",'" & txtdevicecode.Text & "','','Permanent','Working')")


            Dim android_sl As Integer = 1
            Dim dsmax1 As DataSet = get_dataset("SELECT max(android_sl) FROM android_login")
            If Not IsDBNull(dsmax1.Tables(0).Rows(0).Item(0)) Then
                android_sl = dsmax1.Tables(0).Rows(0).Item(0) + 1
            End If
            SQLInsert("INSERT INTO android_login(android_sl,usr_id,usr_pwd,name,last_login,block,usr_sl,lvl,loc_cd,div_sl) VALUES(" & android_sl & ",'" & _
            UCase(Trim(txtempid.Text)) & "','PASSWORD','" & Trim(txtshortname.Text) & "','','" & block_app & "'," & Val(txtstatcd.Text) & ",'E'," & loc_cd & "," & _
            Val(txtdivsl.Text) & ")")



            SQLInsert("UPDATE elog SET loc_cd =(SELECT loc_cd FROM Devices WHERE SerialNumber=elog.Serialno),staf_sl=(SELECT staf_sl FROM staf WHERE device_code=elog.device_code), device_no=(SELECT deviceid FROM Devices WHERE SerialNumber=elog.Serialno) WHERE log_tp='P'")

            'For i As Integer = 0 To dvstaf.Rows.Count - 1
            '    Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
            '    Dim lblhead As Label = dvstaf.Rows(i).FindControl("lblhead")
            '    Dim lblvalue As TextBox = dvstaf.Rows(i).FindControl("lblvalue")
            '    If chk.Checked = True Then
            '        SQLInsert("INSERT INTO emp_payroll_setup(staf_sl,loc_cd,head_nm,amt) VALUES(" & Val(txtstatcd.Text) & "," & loc_cd & ",'" & lblhead.Text & "'," & Val(lblvalue.Text) & ")")
            '    End If
            'Next
            close1()
            Me.clr()
            divmsg.Visible = True
            divmsg.Attributes("class") = "alert bg-success"
            lblmsg.Text = "Record Added Succesffuly."
        ElseIf txtmode.Text = "M" Then
            Dim ds As DataSet = get_dataset("SELECT device_code,emp_code FROM staf WHERE staf_sl=" & Val(txtstatcd.Text) & "")
            If ds.Tables(0).Rows.Count <> 0 Then
                If UCase(Trim(txtempid.Text)) <> ds.Tables(0).Rows(0).Item("emp_code") Then
                    Dim dsname As DataSet = get_dataset("SELECT emp_code FROM staf WHERE emp_code='" & UCase(Trim(txtstaff.Text)) & "'")
                    If dsname.Tables(0).Rows.Count <> 0 Then
                        TabContainer1.ActiveTabIndex = 0
                        divmsg.Visible = True
                        divmsg.Attributes("class") = "alert bg-danger"
                        lblmsg.Text = "Employee Code Already Exits."
                        txtempid.Focus()
                        Exit Sub
                    End If
                End If
                If Trim(txtdevicecode.Text) <> ds.Tables(0).Rows(0).Item("device_code") Then
                    Dim dssname As DataSet = get_dataset("SELECT device_code FROM staf WHERE device_code='" & Trim(txtdevicecode.Text) & "'")
                    If dssname.Tables(0).Rows.Count <> 0 Then
                        TabContainer1.ActiveTabIndex = 0
                        divmsg.Visible = True
                        divmsg.Attributes("class") = "alert bg-danger"
                        lblmsg.Text = "Device Code Already Exits."
                        txtdevicecode.Focus()
                        Exit Sub
                    End If
                End If
                start1()
                SQLInsert("UPDATE staf SET staf_nm='" & Trim(txtstaff.Text) & "',short_nm='" & Trim(txtshortname.Text) & _
                "',emp_code='" & UCase(Trim(txtempid.Text)) & "',device_code='" & Trim(txtdevicecode.Text) & "',emp_status='" & _
                vb.Left(cmbempstatus.Text, 1) & "',emp_tp='" & vb.Left(cmbemptp.Text, 1) & "',desg_sl=" & Val(txtdesgcd.Text) & _
                ",dept_sl=" & Val(txtdeptcd.Text) & ",doj='" & Format(stringtodate(txtjoindate.Text), "dd/MMM/yyyy") & "',add1='" & UCase(Trim(txtpreadd.Text)) & _
                "',city_cd1=" & Val(txtpredistcd.Text) & ",state1='" & UCase(Trim(txtprestate.Text)) & "',ph1='" & Trim(txtpreph1.Text) & _
                "',ph2='" & Trim(txtpreph2.Text) & "',add2='" & UCase(Trim(txtperadd.Text)) & "',city_cd2=" & Val(txtperdistcd.Text) & _
                ",state2='" & UCase(Trim(txtperstate.Text)) & "',ph3='" & Trim(txtperph1.Text) & "',ph4='" & Trim(txtperph2.Text) & _
                "',email='" & LCase(Trim(txtemail.Text)) & "',dob='" & Format(stringtodate(txtdob.Text), "dd/MMM/yyyy") & "',gndr='" & vb.Left(cmbgender.Text, 1) & _
                "',married='" & vb.Left(cmbmaried.Text, 1) & "',nation='" & UCase(Trim(txtnal.Text)) & "',religion='" & _
                UCase(Trim(txtreligion.Text)) & "',caste='" & UCase(Trim(txtcaste.Text)) & "',fgh_nm='" & UCase(Trim(txtguardian.Text)) & _
                "',quali='" & UCase(Trim(txtquali.Text)) & "',div_sl=" & Val(txtdivsl.Text) & ",cat_sl=" & Val(txtcatsl.Text) & ",weekly_off_tp='" & _
                weekofftp & "',shift_sl=" & Val(txtshiftsl.Text) & ",RFID_Cardno='" & Trim(txtrfid.Text) & "',shift_tp=" & Val(cmbshifttp.SelectedIndex) + 1 & _
                ",cycle_sl=" & Val(txtcyclesl.Text) & ",pay_mode='" & vb.Left(cmbpaypayment.Text, 1) & "',pan_no='" & txtppanno.Text & "',bank_name='" & _
                txtpbanknm.Text & "',ac_no='" & txtpacno.Text & "',ifsc_code='" & txtifsc.Text & "',esi_no='" & txtesic.Text & "',pf_no='" & txtpfno.Text & _
                "',uan_no='" & txtuno.Text & "',group_sl=" & Val(txtgroupsl.Text) & ",wages_type='" & vb.Left(cmbwagestype.Text, 1) & "',wages=" & Val(txtwages.Text) & _
                ",moth_nm='" & txtmother.Text & "',spouse_name='" & txtspouse.Text & "',children_name='" & txtchildren.Text & "',blood_grp='" & txtbloodgroup.Text & _
                "',emergency_name='" & txtemergencynm.Text & "',relation='" & txtrelation.Text & "',emergency_contact='" & txtemergencycontact.Text & "',emergency_addr='" & _
                txtemergencyaddr.Text & "',driving_license='" & txtdriving.Text & "',voter_id='" & txtvoterid.Text & "',adhar_card='" & txtaadhar.Text & _
                "',dor='" & Format(stringtodate(txtresign.Text), "dd/MMM/yyyy") & "',reason='" & Trim(txtreason.Text) & "',week_coff=" & week_coff & _
                ",holi_coff=" & holi_coff & ",allow_conveyances=" & allowconveyances & " WHERE staf_sl=" & Val(txtstatcd.Text) & "")

                SQLInsert("Update staf Set staf_image=(select staff_img from temp_staff where usr_sl=" & CType(Session("usr_sl"), Integer) & ") WHERE staf_sl=" & Val(txtstatcd.Text) & "")

                SQLInsert("Update Employees SET EmployeeName='" & Trim(txtstaff.Text) & "',EmployeeCode='" & UCase(Trim(txtempid.Text)) & _
                "',DepartmentId=" & Val(txtdeptcd.Text) & ",CategoryId=" & Val(txtcatsl.Text) & ",EmployeeCodeInDevice='" & Trim(txtdevicecode.Text) & _
                "',EmployeeRFIDNumber='" & txtrfid.Text & "' WHERE EmployeeCode='" & UCase(Trim(txtempid.Text)) & "'")


                SQLInsert("UPDATE android_login SET name='" & Trim(txtstaff.Text) & "',block='" & block_app & "' WHERE usr_sl=" & Val(txtstatcd.Text) & "")


                'SQLInsert("DELETE FROM emp_payroll_setup WHERE staf_sl=" & Val(txtstatcd.Text) & "")
                'For i As Integer = 0 To dvstaf.Rows.Count - 1
                '    Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
                '    Dim lblhead As Label = dvstaf.Rows(i).FindControl("lblhead")
                '    Dim lblvalue As TextBox = dvstaf.Rows(i).FindControl("lblvalue")
                '    If chk.Checked = True Then
                '        SQLInsert("INSERT INTO emp_payroll_setup(staf_sl,loc_cd,head_nm,amt) VALUES(" & Val(txtstatcd.Text) & "," & loc_cd & ",'" & lblhead.Text & "'," & Val(lblvalue.Text) & ")")
                '    End If
                'Next
                close1()
                Me.clr()
                divmsg.Visible = True
                divmsg.Attributes("class") = "alert bg-success"
                lblmsg.Text = "Record Modified Succesffuly."
            End If
        End If
    End Sub

    Protected Sub cmdview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdview.Click
        start1()
        SQLInsert("DELETE FROM temp_staff WHERE usr_sl=" & Val(CType(Session("usr_sl"), Integer)) & "")
        close1()
        Dim filePath As String = FileUpload1.PostedFile.FileName
        Dim filename As String = Path.GetFileName(filePath)
        Dim ext As String = Path.GetExtension(filename)
        Dim contenttype As String = String.Empty

        'Set the contenttype based on File Extension
        Select Case LCase(ext)
            Case ".jpg"
                contenttype = "image/jpg"
                Exit Select
            Case ".jpeg"
                contenttype = "image/jpg"
                Exit Select
            Case ".png"
                contenttype = "image/png"
                Exit Select
            Case ".gif"
                contenttype = "image/gif"
                Exit Select
        End Select
        If contenttype <> String.Empty Then
            Dim fs As Stream = FileUpload1.PostedFile.InputStream
            Dim br As New BinaryReader(fs)
            Dim bytes As Byte() = br.ReadBytes(fs.Length)

            'insert the file into database 
            Dim strQuery As String = "INSERT INTO temp_staff (staff_img,usr_sl) VALUES(@Data,@usr_sl)"
            Dim cmd As New SqlCommand(strQuery)
            cmd.Parameters.Add("@Data", SqlDbType.Binary).Value = bytes
            cmd.Parameters.Add("@usr_sl", SqlDbType.Int).Value = CType(Session("usr_sl"), Integer)
            InsertUpdateData(cmd)
        Else
            divmsg.Visible = True
            divmsg.Attributes("class") = "alert bg-danger"
            lblmsg.Text = "File format not recognised. Upload Image formats"
            Exit Sub
        End If
        Image1.ImageUrl = "~/ShowStaffImage.ashx?id=" & CType(Session("usr_sl"), Integer)
    End Sub

    Public Function InsertUpdateData(ByVal cmd As SqlCommand) As Boolean
        Dim strConnString As String = System.Configuration.ConfigurationManager.ConnectionStrings("dbnm").ConnectionString
        Dim con As New SqlConnection(strConnString)
        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Response.Write(ex.Message)
            Return False
        Finally
            con.Close()
            con.Dispose()
        End Try
    End Function

    Protected Sub valid_empid_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles valid_empid.ServerValidate
        Dim fnd As Integer = 0
        If txtmode.Text = "E" Then
            Dim ds1 As DataSet = get_dataset("SELECT emp_code FROM staf WHERE emp_code='" & UCase(Trim(txtempid.Text)) & "' and loc_cd=" & CType(Session("loc_cd"), Integer) & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                fnd = 1
            Else
                fnd = 0
            End If
        ElseIf txtmode.Text = "M" Then
            Dim ds2 As DataSet = get_dataset("SELECT emp_code FROM staf WHERE staf_sl=" & Val(txtstatcd.Text) & "")
            If ds2.Tables(0).Rows(0).Item(0) <> UCase(Trim(txtempid.Text)) Then
                Dim ds1 As DataSet = get_dataset("SELECT emp_code FROM staf WHERE emp_code='" & UCase(Trim(txtempid.Text)) & "' and loc_cd=" & CType(Session("loc_cd"), Integer) & "")
                If ds1.Tables(0).Rows.Count <> 0 Then
                    fnd = 1
                Else
                    fnd = 0
                End If
            Else
                fnd = 0
            End If
        End If
        If fnd = 1 Then
            args.IsValid = False
        Else
            args.IsValid = True
        End If
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Dim ds1 As DataSet = get_dataset("SELECT row_number() over(ORDER BY staf_nm) as 'sl',staf.staf_nm, staf.short_nm, staf.emp_code, dept_mst.dept_nm, desg_mst.desg_nm FROM staf LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl WHERE staf.loc_cd=" & CType(Session("loc_cd"), Integer) & " AND staf_nm like '%" & UCase(Trim(txtfilterstate.Text)) & "%' ORDER BY staf_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT staf_sl FROM staf WHERE emp_code='" & GridView1.Rows(rw).Cells(3).Text & "' and loc_cd=" & CType(Session("loc_cd"), Integer) & " ")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtmode.Text = "M"
                Me.clr()
                txtstatcd.Text = Val(ds1.Tables(0).Rows(0).Item("staf_sl"))
                Me.dvsel()
            End If
        ElseIf e.CommandName = "reset_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT staf_sl FROM staf WHERE emp_code='" & GridView1.Rows(rw).Cells(3).Text & "' and loc_cd=" & CType(Session("loc_cd"), Integer) & " ")
            If ds1.Tables(0).Rows.Count <> 0 Then
                start1()
                SQLInsert("UPDATE staf SET pswd='PASSWORD' WHERE staf_sl=" & Val(ds1.Tables(0).Rows(0).Item("staf_sl")) & "")
                SQLInsert("UPDATE android_login SET usr_pwd='PASSWORD' WHERE usr_sl=" & Val(ds1.Tables(0).Rows(0).Item("staf_sl")) & "")
                close1()
            End If
        End If
    End Sub

    Private Sub dvsel()
        Dim ds1 As DataSet = get_dataset("SELECT (select block from android_login where usr_sl=staf.staf_sl) as 'al',dept_mst.dept_nm, desg_mst.desg_nm, city.city_name AS pre_city, city_1.city_name AS per_city, division_mst.div_nm, emp_category.cat_nm,payroll_group.group_nm, staf.* FROM city AS city_1 RIGHT OUTER JOIN division_mst RIGHT OUTER JOIN staf LEFT OUTER JOIN payroll_group ON staf.group_sl = payroll_group.group_sl LEFT OUTER JOIN emp_category ON staf.cat_sl = emp_category.cat_sl ON division_mst.div_sl = staf.div_sl ON city_1.city_cd = staf.city_cd2 LEFT OUTER JOIN city ON staf.city_cd1 = city.city_cd LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl  WHERE staf_sl=" & Val(txtstatcd.Text) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtstaff.Text = ds1.Tables(0).Rows(0).Item("staf_nm")
            txtshortname.Text = ds1.Tables(0).Rows(0).Item("short_nm")
            txtempid.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            txtdevicecode.Text = ds1.Tables(0).Rows(0).Item("device_code")
            txtrfid.Text = ds1.Tables(0).Rows(0).Item("RFID_Cardno")
            If ds1.Tables(0).Rows(0).Item("emp_status") = "I" Then
                cmbempstatus.SelectedIndex = 0
            Else
                cmbempstatus.SelectedIndex = 1
            End If
            If ds1.Tables(0).Rows(0).Item("al") = "N" Then
                chkapp.Checked = True
            Else
                chkapp.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("allow_conveyances") = 1 Then
                chkconveyances.Checked = True
            Else
                chkconveyances.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("emp_tp") = "T" Then
                cmbemptp.SelectedIndex = 0
            ElseIf ds1.Tables(0).Rows(0).Item("emp_tp") = "P" Then
                cmbemptp.SelectedIndex = 1
            Else
                cmbemptp.SelectedIndex = 2
            End If
            cmbdivsion.Text = ds1.Tables(0).Rows(0).Item("div_nm")
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item("div_sl")
            cmbdept.Text = ds1.Tables(0).Rows(0).Item("dept_nm")
            cmbdesg.Text = ds1.Tables(0).Rows(0).Item("desg_nm")
            cmbempcat.Text = ds1.Tables(0).Rows(0).Item("cat_nm")
            txtdeptcd.Text = ds1.Tables(0).Rows(0).Item("dept_sl")
            txtdesgcd.Text = ds1.Tables(0).Rows(0).Item("desg_sl")
            txtcatsl.Text = ds1.Tables(0).Rows(0).Item("cat_sl")
            txtjoindate.Text = Format(ds1.Tables(0).Rows(0).Item("doj"), "dd/MM/yyyy")
            txtresign.Text = Format(ds1.Tables(0).Rows(0).Item("dor"), "dd/MM/yyyy")
            txtreason.Text = ds1.Tables(0).Rows(0).Item("reason")
            txtpreadd.Text = ds1.Tables(0).Rows(0).Item("add1")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item("pre_city")) Then
                cmbpredist.Text = ds1.Tables(0).Rows(0).Item("pre_city")
            End If
            cmbshifttp.SelectedIndex = ds1.Tables(0).Rows(0).Item("shift_tp") - 1
            Me.shift()
            txtshiftsl.Text = ds1.Tables(0).Rows(0).Item("shift_sl")
            txtcyclesl.Text = ds1.Tables(0).Rows(0).Item("cycle_sl")
            If cmbshifttp.SelectedIndex = 0 Then
                Dim ds As DataSet = get_dataset("SELECT shift_nm FROM shift_mst  WHERE shift_sl=" & Val(txtshiftsl.Text) & "")
                If ds.Tables(0).Rows.Count <> 0 Then
                    cmbshiftcycle.Text = ds.Tables(0).Rows(0).Item(0)
                End If
            ElseIf cmbshifttp.SelectedIndex = 2 Then
                Dim ds As DataSet = get_dataset("SELECT cycle_nm FROM cycle1  WHERE cycle_sl=" & Val(txtcyclesl.Text) & "")
                If ds.Tables(0).Rows.Count <> 0 Then
                    cmbshiftcycle.Text = ds.Tables(0).Rows(0).Item(0)
                End If
            ElseIf cmbshifttp.SelectedIndex = 3 Then
                Dim ds As DataSet = get_dataset("SELECT cycle_nm FROM cycle1  WHERE cycle_sl=" & Val(txtcyclesl.Text) & "")
                If ds.Tables(0).Rows.Count <> 0 Then
                    cmbshiftcycle.Text = ds.Tables(0).Rows(0).Item(0)
                End If
            End If
            txtpredistcd.Text = ds1.Tables(0).Rows(0).Item("city_cd1")
            txtprestate.Text = ds1.Tables(0).Rows(0).Item("state1")
            txtpreph1.Text = ds1.Tables(0).Rows(0).Item("ph1")
            txtpreph2.Text = ds1.Tables(0).Rows(0).Item("ph2")
            txtperadd.Text = ds1.Tables(0).Rows(0).Item("add2")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item("per_city")) Then
                cmbperdist.Text = ds1.Tables(0).Rows(0).Item("per_city")
            End If
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item("pre_city")) Then
                cmbpredist.Text = ds1.Tables(0).Rows(0).Item("pre_city")
            End If
            txtperdistcd.Text = ds1.Tables(0).Rows(0).Item("city_cd2")
            txtperstate.Text = ds1.Tables(0).Rows(0).Item("state2")
            txtperph1.Text = ds1.Tables(0).Rows(0).Item("ph3")
            txtperph2.Text = ds1.Tables(0).Rows(0).Item("ph4")
            txtemail.Text = ds1.Tables(0).Rows(0).Item("email")
            txtdob.Text = Format(ds1.Tables(0).Rows(0).Item("dob"), "dd/MM/yyyy")
            If ds1.Tables(0).Rows(0).Item("gndr") = "M" Then
                cmbgender.SelectedIndex = 0
            Else
                cmbgender.SelectedIndex = 1
            End If
            If ds1.Tables(0).Rows(0).Item("married") = "M" Then
                cmbmaried.SelectedIndex = 0
            Else
                cmbmaried.SelectedIndex = 1
            End If
            If ds1.Tables(0).Rows(0).Item("weekly_off_tp") = "1" Then
                rdglobal.Checked = True
            Else
                rdmanual.Checked = True
            End If
            If ds1.Tables(0).Rows(0).Item("week_coff") = "1" Then
                chkweekcompoff.Checked = True
            Else
                chkweekcompoff.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("holi_coff") = "1" Then
                chkholidayoff.Checked = True
            Else
                chkholidayoff.Checked = False
            End If
            txtnal.Text = ds1.Tables(0).Rows(0).Item("nation")
            txtreligion.Text = ds1.Tables(0).Rows(0).Item("religion")
            txtcaste.Text = ds1.Tables(0).Rows(0).Item("caste")
            txtguardian.Text = ds1.Tables(0).Rows(0).Item("fgh_nm")
            txtquali.Text = ds1.Tables(0).Rows(0).Item("quali")
            cmbpaypayment.SelectedIndex = Val(ds1.Tables(0).Rows(0).Item("pay_mode")) - 1
            cmbwagestype.SelectedIndex = Val(ds1.Tables(0).Rows(0).Item("wages_type")) - 1
            txtwages.Text = Format(Val(ds1.Tables(0).Rows(0).Item("wages")), "###0.00")
            txtpacno.Text = ds1.Tables(0).Rows(0).Item("ac_no")
            txtpbanknm.Text = ds1.Tables(0).Rows(0).Item("bank_name")
            txtppanno.Text = ds1.Tables(0).Rows(0).Item("pan_no")
            txtifsc.Text = ds1.Tables(0).Rows(0).Item("ifsc_code")
            txtesic.Text = ds1.Tables(0).Rows(0).Item("esi_no")
            txtpfno.Text = ds1.Tables(0).Rows(0).Item("pf_no")
            txtuno.Text = ds1.Tables(0).Rows(0).Item("uan_no")
            txtgroupsl.Text = ds1.Tables(0).Rows(0).Item("group_sl")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item("group_nm")) Then
                cmbgroup.Text = ds1.Tables(0).Rows(0).Item("group_nm")
            End If
            txtmother.Text = ds1.Tables(0).Rows(0).Item("moth_nm")
            txtspouse.Text = ds1.Tables(0).Rows(0).Item("spouse_name")
            txtchildren.Text = ds1.Tables(0).Rows(0).Item("children_name")
            txtbloodgroup.Text = ds1.Tables(0).Rows(0).Item("blood_grp")
            txtemergencynm.Text = ds1.Tables(0).Rows(0).Item("emergency_name")
            txtrelation.Text = ds1.Tables(0).Rows(0).Item("relation")
            txtemergencycontact.Text = ds1.Tables(0).Rows(0).Item("emergency_contact")
            txtemergencyaddr.Text = ds1.Tables(0).Rows(0).Item("emergency_addr")
            txtdriving.Text = ds1.Tables(0).Rows(0).Item("driving_license")
            txtvoterid.Text = ds1.Tables(0).Rows(0).Item("voter_id")
            txtaadhar.Text = ds1.Tables(0).Rows(0).Item("adhar_card")
            'Dim dspay As DataSet = get_dataset("SELECT * FROM emp_payroll_setup WHERE staf_sl=" & Val(txtstatcd.Text) & "")
            'For i As Integer = 0 To dvstaf.Rows.Count - 1
            '    Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
            '    Dim lblhead As Label = dvstaf.Rows(i).FindControl("lblhead")
            '    Dim lblvalue As TextBox = dvstaf.Rows(i).FindControl("lblvalue")
            '    Dim dr As DataRow()
            '    dr = dspay.Tables(0).Select("head_nm='" & lblhead.Text & "'")
            '    If dr.Length <> 0 Then
            '        chk.Checked = True
            '        lblvalue.Text = Format(Val(dr(0).Item("amt")), "######0.00")
            '    End If
            'Next
            Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
            start1()
            SQLInsert("DELETE FROM temp_staff WHERE usr_sl=" & usr_sl & "")
            SQLInsert("INSERT INTO temp_staff(staff_img,usr_sl) VALUES(''," & usr_sl & ")")
            SQLInsert("UPDATE temp_staff SET staff_img=(SELECT staf_image FROM staf WHERE staf_sl=" & Val(txtstatcd.Text) & ") WHERE usr_sl=" & usr_sl & "")
            close1()
            Image1.ImageUrl = "~/ShowStaffImage.ashx?id=" & CType(Session("usr_sl"), Integer)
        End If
    End Sub

    Protected Sub cmdclr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclr.Click
        txtfilterstate.Text = ""
        Me.dvdisp()
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        Dim ds1 As DataSet = get_dataset("SELECT row_number() over(ORDER BY staf_nm) as 'sl',staf.staf_nm, staf.short_nm, staf.emp_code, dept_mst.dept_nm, desg_mst.desg_nm FROM staf LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl WHERE staf_nm like '%" & UCase(Trim(txtfilterstate.Text)) & "%' AND staf.loc_cd=" & CType(Session("loc_cd"), Integer) & " ORDER BY staf_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub cmbgroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbgroup.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtgroupsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT group_sl FROM payroll_group WHERE group_nm='" & Trim(cmbgroup.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtgroupsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub

    Protected Sub cmbempcat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbempcat.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtcatsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT cat_sl FROM emp_category WHERE cat_nm='" & Trim(cmbempcat.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtcatsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub

    Protected Sub cmbshifttp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbshifttp.SelectedIndexChanged
        Me.shift()
    End Sub

    Private Sub shift()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbshiftcycle.Items.Clear()
        If cmbshifttp.SelectedIndex = 0 Then
            cmbshiftcycle.Items.Add("Select Shift")
            Dim ds As DataSet = get_dataset("SELECT shift_nm FROM shift_mst  WHERE loc_cd=" & loc_cd & " ORDER BY shift_nm")
            If ds.Tables(0).Rows.Count <> 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    cmbshiftcycle.Items.Add(ds.Tables(0).Rows(i).Item(0))
                Next
            End If
        ElseIf cmbshifttp.SelectedIndex = 2 Then
            cmbshiftcycle.Items.Add("Select Shift Cycle")
            Dim ds As DataSet = get_dataset("SELECT cycle_nm FROM cycle1  WHERE loc_cd=" & loc_cd & " ORDER BY cycle_nm")
            If ds.Tables(0).Rows.Count <> 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    cmbshiftcycle.Items.Add(ds.Tables(0).Rows(i).Item(0))
                Next
            End If
        ElseIf cmbshifttp.SelectedIndex = 3 Then
            cmbshiftcycle.Items.Add("Select Shift Cycle")
            Dim ds As DataSet = get_dataset("SELECT cycle_nm FROM cycle1  WHERE loc_cd=" & loc_cd & " ORDER BY cycle_nm")
            If ds.Tables(0).Rows.Count <> 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    cmbshiftcycle.Items.Add(ds.Tables(0).Rows(i).Item(0))
                Next
            End If
        End If
    End Sub
End Class
