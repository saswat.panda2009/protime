﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class payroll_reports_monthlyabs
    Inherits System.Web.UI.Page

    <System.Web.Script.Services.ScriptMethod(), _
System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            "staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblmsg.Visible = False
        txtfrmdt.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        Me.divisiondisp()
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
    End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)

        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = str & "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If
        Dim dsmonthy As DataSet = get_dataset("SELECT cast(ROW_NUMBER() OVER(ORDER BY emp_code) as varchar) as Sl,staf.emp_code AS [Emp. Code], staf.staf_nm AS [Staf Name], dept_mst.dept_nm AS Department, desg_mst.desg_nm AS Designation, staf.esi_no AS [ESIC No], staf.pf_no AS [PF No],staf.uan_no AS [UAN NO], Str(monthly_pay.basic_amt,12,2) as 'Basic', Str(monthly_pay.esic_amt,12,2) as 'Esic Amount', str(monthly_pay.comp_esic_amt,12,2) as 'Comp Esic Amount' FROM monthly_pay LEFT OUTER JOIN staf ON monthly_pay.staf_sl = staf.staf_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE pay_month=" & stringtodate(txtfrmdt.Text).Month & " AND pay_year=" & stringtodate(txtfrmdt.Text).Year & " AND monthly_pay.esic_amt<>0 AND monthly_pay.loc_cd=" & loc_cd & "  " & str & " ORDER BY emp_code")
        Dim dsmonthysum As DataSet = get_dataset("SELECT isnull(sum(monthly_pay.esic_amt),0), isnull(sum(monthly_pay.comp_esic_amt),0) FROM monthly_pay LEFT OUTER JOIN staf ON monthly_pay.staf_sl = staf.staf_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE pay_month=" & stringtodate(txtfrmdt.Text).Month & " AND pay_year=" & stringtodate(txtfrmdt.Text).Year & " AND monthly_pay.esic_amt<>0 AND monthly_pay.loc_cd=" & loc_cd & "  " & str & "")
        Dim dt As New DataTable
        dt = dsmonthy.Tables(0)
        dt.Rows.Add("", "", "", "", "", "", "", "", "Total :", Format(Val(dsmonthysum.Tables(0).Rows(0).Item(0)), "#####0.00"), Format(Val(dsmonthysum.Tables(0).Rows(0).Item(1)), "#####0.00"))

        If dt.Rows.Count <> 0 Then
            Dim GridView112 As New GridView()
            GridView112.AllowPaging = False
            GridView112.DataSource = dt
            GridView112.DataBind()
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=MonthlyESICRegister.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            For i As Integer = 0 To GridView112.Rows.Count - 1
                GridView112.Rows(i).Attributes.Add("class", "textmode")
            Next
            GridView112.RenderControl(hw)
            'style to format numbers to string
            Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()
        End If
    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub
End Class
