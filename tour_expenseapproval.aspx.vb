﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.Net.Mail

Partial Class tour_expenseapproval
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            txtmode.Text = "V"
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtdescription.Text = ""
        txttittle.Text = ""
        txttoursl.Text = ""
        txtfrom.Text = Format(Now, "dd/MM/yyyy")
        txtto.Text = Format(Now, "dd/MM/yyyy")
        If txtmode.Text = "M" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Tour Expense Approval ..."
            txtfrom.Focus()
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Tour Expense Approval ..."
            Me.dvdisp()
        End If
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT  Row_number() OVER(ORDER BY travel_startdt) as Sl,Travel_CreateNew.travel_tittle, CONVERT(varchar, Travel_CreateNew.travel_startdt, 103) AS Sdt, CONVERT(varchar, Travel_CreateNew.travel_enddt, 103) AS edt, Travel_CreateNew.travel_descr, Travel_CreateNew.travel_sl, staf.staf_nm, staf.emp_code, dept_mst.dept_nm, desg_mst.desg_nm FROM desg_mst RIGHT OUTER JOIN staf ON desg_mst.desg_sl = staf.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl RIGHT OUTER JOIN Travel_CreateNew ON staf.staf_sl = Travel_CreateNew.staf_sl WHERE travel_status=4 and Travel_CreateNew.loc_cd=" & loc_cd & " ORDER BY travel_startdt")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim lbl As Label = GridView1.Rows(rw).FindControl("lblslno")
            txtmode.Text = "M"
            Me.clr()
            Dim ds As DataSet = get_dataset("SELECT ROW_NUMBER() OVER(ORDER BY plan_time) as sl,CONVERT(varchar, Travel_planner.plan_dt, 103) AS [date], CONVERT(varchar, Travel_planner.plan_time, 108) AS [time], Travel_planner.plan_descr AS descr, staf.staf_nm, Travel_CreateNew.*, dept_mst.dept_nm, desg_mst.desg_nm FROM staf LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl RIGHT OUTER JOIN Travel_CreateNew ON staf.staf_sl = Travel_CreateNew.staf_sl RIGHT OUTER JOIN Travel_planner ON Travel_CreateNew.travel_sl = Travel_planner.travel_sl WHERE Travel_planner.travel_sl=" & lbl.Text & "  ORDER BY plan_time")
            If ds.Tables(0).Rows.Count <> 0 Then
                txtdescription.Text = ds.Tables(0).Rows(0).Item("travel_descr")
                txttittle.Text = ds.Tables(0).Rows(0).Item("travel_tittle")
                txttoursl.Text = ds.Tables(0).Rows(0).Item("travel_sl")
                txtstafnm.Text = ds.Tables(0).Rows(0).Item("staf_nm")
                txtdept.Text = ds.Tables(0).Rows(0).Item("dept_nm")
                txtdesg.Text = ds.Tables(0).Rows(0).Item("desg_nm")
                txtfrom.Text = Format(ds.Tables(0).Rows(0).Item("travel_startdt"), "dd/MM/yyyy")
                txtto.Text = Format(ds.Tables(0).Rows(0).Item("travel_enddt"), "dd/MM/yyyy")
                Me.dvdispexpense1()
                Me.dvdispexpense2()
                Me.dvdispexpense3()
                Me.dvdispexpense4()
            End If
        End If
    End Sub


    Private Sub dvdispexpense3()
        Dim ds As DataSet = get_dataset("SELECT Row_number() OVER(ORDER BY expense_dt) as slno,sl,Convert(varchar,expense_dt,103) as dt,expense_det,expense_mode,expense_from,expense_to,str(expense_amt,12,2) as amt,expense_amt,expense_bill,(Case WHEN bill_img ='' THEN 'No' WHEN bill_img <>'' THEN 'YES' END) as atch FROM Travel_Expenses_Conveyances2 WHERE travel_sl=" & Val(txttoursl.Text) & "  ORDER BY expense_dt")
        dvexpense3.DataSource = ds.Tables(0)
        dvexpense3.DataBind()
        If ds.Tables(0).Rows.Count <> 0 Then
            txttotalexpense3.Text = Format(ds.Tables(0).Compute("SUM(expense_amt)", ""), "######0.00")
        End If
        lbltotexpense.Text = Format(Val(txttotalexpense1.Text) + Val(txttotalexpense3.Text) + Val(txttotalexpense4.Text), "######0.00")
    End Sub

    Private Sub dvdispexpense1()
        Dim ds As DataSet = get_dataset("SELECT Row_number() OVER(ORDER BY expense_dt) as slno,sl,Convert(varchar,expense_dt,103) as dt,expense_det,expense_mode,expense_from,expense_to,str(expense_amt,12,2) as amt,expense_amt,expense_bill,(Case WHEN bill_img ='' THEN 'No' WHEN bill_img <>'' THEN 'YES' END) as atch FROM Travel_Expenses_Conveyances1 WHERE travel_sl=" & Val(txttoursl.Text) & "  ORDER BY expense_dt")
        dvexpense1.DataSource = ds.Tables(0)
        dvexpense1.DataBind()
        If ds.Tables(0).Rows.Count <> 0 Then
            txttotalexpense1.Text = Format(ds.Tables(0).Compute("SUM(expense_amt)", ""), "######0.00")
        End If
        lbltotexpense.Text = Format(Val(txttotalexpense1.Text) + Val(txttotalexpense3.Text) + Val(txttotalexpense4.Text), "######0.00")
    End Sub

    Private Sub dvdispexpense2()
        Dim ds As DataSet = get_dataset("SELECT Row_number() OVER(ORDER BY expense_dt) as slno,sl,Convert(varchar,expense_dt,103) as dt,expense_det,expense_days,expense_from,expense_to,str(expense_amt,12,2) as amt,expense_amt,expense_bill,(Case WHEN bill_img ='' THEN 'No' WHEN bill_img <>'' THEN 'YES' END) as atch FROM travel_expenses_lodging WHERE travel_sl=" & Val(txttoursl.Text) & "  ORDER BY expense_dt")
        dvexpense2.DataSource = ds.Tables(0)
        dvexpense2.DataBind()
        If ds.Tables(0).Rows.Count <> 0 Then
            txttotalexpense2.Text = Format(ds.Tables(0).Compute("SUM(expense_amt)", ""), "######0.00")
        End If
        lbltotexpense.Text = Format(Val(txttotalexpense1.Text) + Val(txttotalexpense3.Text) + Val(txttotalexpense4.Text), "######0.00")
    End Sub

    Private Sub dvdispexpense4()
        Dim ds As DataSet = get_dataset("SELECT Row_number() OVER(ORDER BY expense_dt) as slno,sl,Convert(varchar,expense_dt,103) as dt,expense_det,str(expense_amt,12,2) as amt,expense_amt,(Case WHEN bill_img ='' THEN 'No' WHEN bill_img <>'' THEN 'YES' END) as atch FROM travel_expenses_food WHERE travel_sl=" & Val(txttoursl.Text) & "  ORDER BY expense_dt")
        GridView2.DataSource = ds.Tables(0)
        GridView2.DataBind()
        If ds.Tables(0).Rows.Count <> 0 Then
            txttotalexpense4.Text = Format(ds.Tables(0).Compute("SUM(expense_amt)", ""), "######0.00")
        End If
        lbltotexpense.Text = Format(Val(txttotalexpense1.Text) + Val(txttotalexpense3.Text) + Val(txttotalexpense4.Text), "######0.00")
    End Sub

    Protected Sub dvexpense1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dvexpense1.RowCommand
        Dim rw As Integer = e.CommandArgument
        Dim lbl As Label = dvexpense1.Rows(rw).FindControl("lblsl6")
        Dim txtremarks As TextBox = dvexpense1.Rows(rw).FindControl("txtremarks")
        If e.CommandName = "delete_data" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM Travel_Expenses_Conveyances1 WHERE sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                start1()
                SQLInsert("UPDATE Travel_Expenses_Conveyances1 SET remarks='" & txtremarks.Text & "',expense_status='R' WHERE sl=" & lbl.Text & "")
                close1()
            End If
        ElseIf e.CommandName = "approved_data" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM Travel_Expenses_Conveyances1 WHERE sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                start1()
                SQLInsert("UPDATE Travel_Expenses_Conveyances1 SET remarks='" & txtremarks.Text & "',expense_status='A' WHERE sl=" & lbl.Text & "")
                close1()
            End If
        ElseIf e.CommandName = "view_data" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM Travel_Expenses_Conveyances1 WHERE sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                Image1.ImageUrl = "data:image/jpg;base64," & ds1.Tables(0).Rows(0).Item("bill_img")
            End If
        End If
    End Sub

    Protected Sub dvexpense3_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dvexpense3.RowCommand
        Dim rw As Integer = e.CommandArgument
        Dim lbl As Label = dvexpense3.Rows(rw).FindControl("lblsl6")
        Dim txtremarks As TextBox = dvexpense3.Rows(rw).FindControl("txtremarks")
        If e.CommandName = "delete_data" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM Travel_Expenses_Conveyances2 WHERE sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                start1()
                SQLInsert("UPDATE Travel_Expenses_Conveyances2 SET remarks='" & txtremarks.Text & "',expense_status='R' WHERE sl=" & lbl.Text & "")
                close1()
            End If
        ElseIf e.CommandName = "approved_data" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM Travel_Expenses_Conveyances2 WHERE sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                start1()
                SQLInsert("UPDATE Travel_Expenses_Conveyances2 SET remarks='" & txtremarks.Text & "',expense_status='A' WHERE sl=" & lbl.Text & "")
                close1()
            End If
        ElseIf e.CommandName = "view_data" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM Travel_Expenses_Conveyances2 WHERE sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                Image1.ImageUrl = "data:image/png;base64," & ds1.Tables(0).Rows(0).Item("bill_img")
            End If
        End If
    End Sub

    Protected Sub dvexpense2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dvexpense2.RowCommand
        Dim rw As Integer = e.CommandArgument
        Dim lbl As Label = dvexpense2.Rows(rw).FindControl("lblsl6")
        Dim txtremarks As TextBox = dvexpense2.Rows(rw).FindControl("txtremarks")
        If e.CommandName = "delete_data" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM travel_expenses_lodging WHERE sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                start1()
                SQLInsert("UPDATE travel_expenses_lodging SET remarks='" & txtremarks.Text & "',expense_status='R' WHERE sl=" & lbl.Text & "")
                close1()
            End If
        ElseIf e.CommandName = "approved_data" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM travel_expenses_lodging WHERE sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                start1()
                SQLInsert("UPDATE travel_expenses_lodging SET remarks='" & txtremarks.Text & "',expense_status='A' WHERE sl=" & lbl.Text & "")
                close1()
            End If
        ElseIf e.CommandName = "view_data" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM travel_expenses_lodging WHERE sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                Image1.ImageUrl = "data:image/jpg;base64," & ds1.Tables(0).Rows(0).Item("bill_img")
            End If
        End If
    End Sub

    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView2.RowCommand
        Dim rw As Integer = e.CommandArgument
        Dim lbl As Label = dvexpense1.Rows(rw).FindControl("lblsl6")
        Dim txtremarks As TextBox = dvexpense1.Rows(rw).FindControl("txtremarks")
        If e.CommandName = "delete_data" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM travel_expenses_food WHERE sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                start1()
                SQLInsert("UPDATE travel_expenses_food SET remarks='" & txtremarks.Text & "',expense_status='R' WHERE sl=" & lbl.Text & "")
                close1()
            End If
        ElseIf e.CommandName = "approved_data" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM travel_expenses_food WHERE sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                start1()
                SQLInsert("UPDATE travel_expenses_food SET remarks='" & txtremarks.Text & "',expense_status='A' WHERE sl=" & lbl.Text & "")
                close1()
            End If
        ElseIf e.CommandName = "view_data" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM travel_expenses_food WHERE sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                Image1.ImageUrl = "data:image/jpg;base64," & ds1.Tables(0).Rows(0).Item("bill_img")
            End If
        End If
    End Sub

    Protected Sub cmdsave0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave0.Click
        start1()
        SQLInsert("UPDATE travel_CreateNew SET mod_by=0,mod_dt='" & Now & "',travel_status=6 WHERE travel_sl=" & Val(txttoursl.Text) & "")
        close1()
        Dim SmtpServer As New SmtpClient()
        Dim mail As New MailMessage()
        SmtpServer.Credentials = New Net.NetworkCredential("protime.attendance@gmail.com", "14042016")
        SmtpServer.Port = 587
        SmtpServer.Host = "smtp.gmail.com"
        SmtpServer.EnableSsl = True
        mail = New MailMessage()
        mail.From = New MailAddress("protime.attendance@gmail.com")
        Dim ds As DataSet = get_dataset("SELECT staf.email FROM Travel_CreateNew LEFT OUTER JOIN staf ON Travel_CreateNew.r_user_sl = staf.staf_sl WHERE travel_Sl=" & Val(txttoursl.Text) & "")
        If ds.Tables(0).Rows.Count <> 0 Then
            mail.To.Add(ds.Tables(0).Rows(0).Item(0))
        End If
        mail.Subject = "Approval Pending For Tour Expenses"
        mail.IsBodyHtml = True

        Dim email_msg As String = "<html lang=""en""><head><meta http-equiv=Content-Type content=""text/html; charset=UTF-8""><style type=""text/css"" nonce=""9WKSAP4ptDMj7W7iuldXeA"">body,td,div,p,a,input {font-family: arial, sans-serif;}</style>" & _
                    "<style type=""text/css"" nonce=""9WKSAP4ptDMj7W7iuldXeA"">body, td {font-size:13px} a:link, a:active {color:#1155CC; text-decoration:none} a:hover {text-decoration:underline; cursor: pointer} a:visited{color:##6611CC} img{border:0px} pre { white-space: pre; white-space: -moz-pre-wrap; white-space: -o-pre-wrap; white-space: pre-wrap; word-wrap: break-word; max-width: 800px; overflow: auto;} .logo { left: -7px; position: relative; }" & _
                    "</style><body><div class=""bodycontainer""><div class=""maincontent""><div style=""margin:0px;padding:0px;font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:100%"">" & _
                    "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"" style=""max-width:600px;border:1px solid #dddada;font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:13px"">" & _
                    "<tbody><tr><td align=""left"" valign=""top"" style=""padding:20px 0 0 0""><table width=""90%"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"" style=""max-width:570px""><tbody><tr><td align=""left"" style=""font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:14px;color:#000000;margin:0px;padding:0px 0 20px 0;line-height:22px;font-weight:bold"">Hi Manager</td>" & _
                    "</tr><tr><td align=""left"" valign=""top"" style=""font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:13px;color:#000000;margin:0px;padding:0px 0 10px 0;line-height:22px""><strong>Approved By Admin</strong><br/><br/>Tour : " & txttittle.Text & " .From Date : " & txtfrom.Text & "  To Date : " & txtto.Text & " expenses are approved. So please approve all the expenses and proceed forward</td>" & _
                    "</tr><tr><td align=""left"" valign=""top"" style=""font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:13px;color:#000000;margin:0px;padding:0px 0 20px 0;line-height:22px;font-size:13px;white-space:pre-line""></td></tr><tr><td align=""left"" valign=""top"" style=""font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:13px;color:#000000;margin:0px;padding:10px 0 10px 0;line-height:22px""><strong>Please note:</strong> This e-mail was sent from a notification-only address that can&#39;t accept incoming e-mail.</td>" & _
                    "</tr><tr><td align=""left"" valign=""top"" style=""font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:13px;color:#000000;margin:0px;padding:10px 0 20px 0;line-height:22px"">Thank you for your patience and cooperation.</td>" & _
                    "</tr><tr><td align=""left"" valign=""top"" style=""font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:13px;color:#000000;margin:0px;padding:10px 0 20px 0;line-height:22px;font-weight:bold"">Regards," & _
                    "<br>Team Protime</td></tr><tr><td align=""left"" valign=""top"" style=""padding:0 0 20px 0""><img src=""https://ci3.googleusercontent.com/proxy/3QeAfcnOdG4ThfqqK54y7FJEgceRxGnWnfC-HY4iKmxgAvFdwPLtxHOPBkiyZaFex_U=s0-d-e1-ft#https://i.imgur.com/wnmTmlW.jpg"" border=""0"" style=""display:block"" alt=""Seprator image""></td>" & _
                    "</tr><tr><td align=""center"" valign=""top"" style=""font-family:Roboto-Regular,Arial,&#39; sans-serif&#39;,Gotham,Helvetica Neue,Helvetica;font-size:11px;color:#8e8e8e;padding:0 0 20px 0;line-height:24px;font-weight:300""><img src=""http://protime.co.in/images/logo.png"" border=""0"" style=""vertical-align:top;padding:2px 0 0 0"" alt=""Protime logo""><br /> © 2022. All rights reserved</td>" & _
                    "</tr></tbody></table></td></tr><tr><td height=""22""></td></tr></tbody></table></div></div></table></table></div></div></body>"

        mail.Body = email_msg
        SmtpServer.Send(mail)
        txtmode.Text = "V"
        Me.clr()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        start1()
        SQLInsert("UPDATE travel_CreateNew SET mod_by=0,mod_dt='" & Now & "',travel_status=5 WHERE travel_sl=" & Val(txttoursl.Text) & "")
        close1()
        Dim SmtpServer As New SmtpClient()
        Dim mail As New MailMessage()
        SmtpServer.Credentials = New Net.NetworkCredential("protime.attendance@gmail.com", "14042016")
        SmtpServer.Port = 587
        SmtpServer.Host = "smtp.gmail.com"
        SmtpServer.EnableSsl = True
        mail = New MailMessage()
        mail.From = New MailAddress("protime.attendance@gmail.com")
        Dim ds As DataSet = get_dataset("SELECT staf.email FROM Travel_CreateNew LEFT OUTER JOIN staf ON Travel_CreateNew.staf_sl = staf.staf_sl WHERE travel_Sl=" & Val(txttoursl.Text) & "")
        If ds.Tables(0).Rows.Count <> 0 Then
            mail.To.Add(ds.Tables(0).Rows(0).Item(0))
        End If
        mail.Subject = "Objection By Admin"
        mail.IsBodyHtml = True

        Dim email_msg As String = "<html lang=""en""><head><meta http-equiv=Content-Type content=""text/html; charset=UTF-8""><style type=""text/css"" nonce=""9WKSAP4ptDMj7W7iuldXeA"">body,td,div,p,a,input {font-family: arial, sans-serif;}</style>" & _
                    "<style type=""text/css"" nonce=""9WKSAP4ptDMj7W7iuldXeA"">body, td {font-size:13px} a:link, a:active {color:#1155CC; text-decoration:none} a:hover {text-decoration:underline; cursor: pointer} a:visited{color:##6611CC} img{border:0px} pre { white-space: pre; white-space: -moz-pre-wrap; white-space: -o-pre-wrap; white-space: pre-wrap; word-wrap: break-word; max-width: 800px; overflow: auto;} .logo { left: -7px; position: relative; }" & _
                    "</style><body><div class=""bodycontainer""><div class=""maincontent""><div style=""margin:0px;padding:0px;font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:100%"">" & _
                    "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"" style=""max-width:600px;border:1px solid #dddada;font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:13px"">" & _
                    "<tbody><tr><td align=""left"" valign=""top"" style=""padding:20px 0 0 0""><table width=""90%"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"" style=""max-width:570px""><tbody><tr><td align=""left"" style=""font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:14px;color:#000000;margin:0px;padding:0px 0 20px 0;line-height:22px;font-weight:bold"">Hi " & txtstafnm.Text & "</td>" & _
                    "</tr><tr><td align=""left"" valign=""top"" style=""font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:13px;color:#000000;margin:0px;padding:0px 0 10px 0;line-height:22px""><strong>Objection By Admin</strong><br/><br/>Admin has raised some objections on your Tour : " & txttittle.Text & " .From Date : " & txtfrom.Text & "  To Date : " & txtto.Text & ". So please provide the valid reason for the remarks provided by the Admin.</td>" & _
                    "</tr><tr><td align=""left"" valign=""top"" style=""font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:13px;color:#000000;margin:0px;padding:0px 0 20px 0;line-height:22px;font-size:13px;white-space:pre-line""></td></tr><tr><td align=""left"" valign=""top"" style=""font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:13px;color:#000000;margin:0px;padding:10px 0 10px 0;line-height:22px""><strong>Please note:</strong> This e-mail was sent from a notification-only address that can&#39;t accept incoming e-mail.</td>" & _
                    "</tr><tr><td align=""left"" valign=""top"" style=""font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:13px;color:#000000;margin:0px;padding:10px 0 20px 0;line-height:22px"">Thank you for your patience and cooperation.</td>" & _
                    "</tr><tr><td align=""left"" valign=""top"" style=""font-family:Arial,sans-serif,Gotham,&#39;Helvetica Neue&#39;,Helvetica;font-size:13px;color:#000000;margin:0px;padding:10px 0 20px 0;line-height:22px;font-weight:bold"">Regards," & _
                    "<br>Team Protime</td></tr><tr><td align=""left"" valign=""top"" style=""padding:0 0 20px 0""><img src=""https://ci3.googleusercontent.com/proxy/3QeAfcnOdG4ThfqqK54y7FJEgceRxGnWnfC-HY4iKmxgAvFdwPLtxHOPBkiyZaFex_U=s0-d-e1-ft#https://i.imgur.com/wnmTmlW.jpg"" border=""0"" style=""display:block"" alt=""Seprator image""></td>" & _
                    "</tr><tr><td align=""center"" valign=""top"" style=""font-family:Roboto-Regular,Arial,&#39; sans-serif&#39;,Gotham,Helvetica Neue,Helvetica;font-size:11px;color:#8e8e8e;padding:0 0 20px 0;line-height:24px;font-weight:300""><img src=""http://protime.co.in/images/logo.png"" border=""0"" style=""vertical-align:top;padding:2px 0 0 0"" alt=""Protime logo""><br /> © 2022. All rights reserved</td>" & _
                    "</tr></tbody></table></td></tr><tr><td height=""22""></td></tr></tbody></table></div></div></table></table></div></div></body>"

        mail.Body = email_msg
        SmtpServer.Send(mail)
        txtmode.Text = "V"
        Me.clr()
    End Sub
End Class
