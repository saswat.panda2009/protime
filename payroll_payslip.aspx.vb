﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class payroll_payslip
    Inherits System.Web.UI.Page

    <System.Web.Script.Services.ScriptMethod(), _
System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            "staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtfrom.Text = Format(Now, "dd/MM/yyyy")
        divmsg.Visible = False
        lblhdr.Text = "Payslip Generation"
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        Dim mnth As String = stringtodate(txtfrom.Text).Month.ToString() & "-" & stringtodate(txtfrom.Text).Year
        Dim ds As New DataSet
        If txtempcode.Text <> "" Then
            ds = get_dataset("SELECT  desg_mst.desg_nm, staf.* FROM staf LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl WHERE emp_code='" & Trim(txtempcode.Text) & "'")
        Else
            ds = get_dataset("SELECT desg_mst.desg_nm, staf.* FROM staf LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl")
        End If
        Dim dspay As DataSet = get_dataset("select * from monthly_pay WHERE pay_month=" & stringtodate(txtfrom.Text).Month & " AND pay_year=" & stringtodate(txtfrom.Text).Year & "")
        Dim dsern As DataSet = get_dataset("SELECT Sum(amt),head_mst.head_name,staf_sl FROM head_assignment2 LEFT OUTER JOIN head_mst ON head_assignment2.head_sl = head_mst.head_sl WHERE tp=1 AND ass_month=" & stringtodate(txtfrom.Text).Month & " AND ass_year=" & stringtodate(txtfrom.Text).Year & " GROUP BY staf_sl,head_name ORDER BY head_name")
        Dim dsdedu As DataSet = get_dataset("SELECT Sum(amt),head_mst.head_name,staf_sl FROM head_assignment2 LEFT OUTER JOIN head_mst ON head_assignment2.head_sl = head_mst.head_sl WHERE tp=2 AND ass_month=" & stringtodate(txtfrom.Text).Month & " AND ass_year=" & stringtodate(txtfrom.Text).Year & " GROUP BY staf_sl,head_name ORDER BY head_name")
        start1()
        SQLInsert("DELETE FROM print_payslip WHERE usr_sl=" & usr_sl & "")
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim staf_sl As Integer = ds.Tables(0).Rows(i).Item("staf_sl")
            Dim dr As DataRow()
            dr = dspay.Tables(0).Select("staf_sl=" & staf_sl & "")
            If dr.Length <> 0 Then
                Dim wages As Decimal = "0"
                Dim a1 As String = ""
                Dim a2 As String = ""
                Dim a3 As String = ""
                Dim a4 As String = ""
                Dim a5 As String = ""
                Dim a1_amt As String = ""
                Dim a2_amt As String = ""
                Dim a3_amt As String = ""
                Dim a4_amt As String = ""
                Dim a5_amt As String = ""
                Dim d1 As String = ""
                Dim d2 As String = ""
                Dim d3 As String = ""
                Dim d4 As String = ""
                Dim d5 As String = ""
                Dim d1_amt As String = ""
                Dim d2_amt As String = ""
                Dim d3_amt As String = ""
                Dim d4_amt As String = ""
                Dim d5_amt As String = ""
                Dim dr1 As DataRow()
                dr1 = dsern.Tables(0).Select("staf_sl=" & staf_sl & "")
                If dr1.Length <> 0 Then
                    For ern As Integer = 0 To dr1.Length - 1
                        If ern = 0 Then
                            a1 = dr1(ern).Item(1)
                            a1_amt = Format(Val(dr1(ern).Item(0)), "#####0.00")
                        ElseIf ern = 1 Then
                            a2 = dr1(ern).Item(1)
                            a2_amt = Format(Val(dr1(ern).Item(0)), "#####0.00")
                        ElseIf ern = 2 Then
                            a3 = dr1(ern).Item(1)
                            a3_amt = Format(Val(dr1(ern).Item(0)), "#####0.00")
                        ElseIf ern = 3 Then
                            a4 = dr1(ern).Item(1)
                            a4_amt = Format(Val(dr1(ern).Item(0)), "#####0.00")
                        ElseIf ern = 4 Then
                            a5 = dr1(ern).Item(1)
                            a5_amt = Format(Val(dr1(ern).Item(0)), "#####0.00")
                        End If
                    Next
                End If
                Dim dr11 As DataRow()
                dr11 = dsdedu.Tables(0).Select("staf_sl=" & staf_sl & "")
                If dr11.Length <> 0 Then
                    For ded As Integer = 0 To dr11.Length - 1
                        If ded = 0 Then
                            d1 = dr11(ded).Item(1)
                            d1_amt = Format(Val(dr11(ded).Item(0)), "#####0.00")
                        ElseIf ded = 1 Then
                            d2 = dr11(ded).Item(1)
                            d2_amt = Format(Val(dr11(ded).Item(0)), "#####0.00")
                        ElseIf ded = 2 Then
                            d3 = dr11(ded).Item(1)
                            d3_amt = Format(Val(dr11(ded).Item(0)), "#####0.00")
                        ElseIf ded = 3 Then
                            d4 = dr11(ded).Item(1)
                            d4_amt = Format(Val(dr11(ded).Item(0)), "#####0.00")
                        ElseIf ded = 4 Then
                            d5 = dr11(ded).Item(1)
                            d5_amt = Format(Val(dr11(ded).Item(0)), "#####0.00")
                        End If
                    Next
                End If
                Dim in_words As String = ""

                SQLInsert("INSERT INTO print_payslip(usr_sl,staf_sl,month_nm,emp_code,emp_nm,desg,category,uan_no,pay_month,pay_year,loc_cd,gross,basic_amt," & _
                "da_amt,hra_amt,transport_amt,child_amt,city_amt,spcl_amt,pf_amt,comp_pf_amt,esic_amt,comp_esic_amt,pt_amt,tds_amt,loan_amt,othr_earnings," & _
                "othr_deuction,tot_earnings,tot_deduction,wages,tot_reimbursment,total,pay_days,tot_days,tot_present,tot_half,tot_week,tot_absnt,tot_leave,tot_holi," & _
                "l1,l2,l3,l4,l5,l6,washing_amt,a1,a2,a3,a4,a5,d1,d2,d3,d4,d5,a1_amt,a2_amt,a3_amt,a4_amt,a5_amt,d1_amt,d2_amt,d3_amt,d4_amt,d5_amt,in_words,comp_name,mobile_amt,food_amt, medical_amt, insurance_amt) VALUES(" & _
                usr_sl & "," & staf_sl & ",'" & MonthName(stringtodate(txtfrom.Text).Month) & "','" & ds.Tables(0).Rows(i).Item("emp_code") & "','" & _
                ds.Tables(0).Rows(i).Item("staf_nm") & "','" & ds.Tables(0).Rows(i).Item("desg_nm") & "','','" & _
                ds.Tables(0).Rows(i).Item("uan_no") & "'," & stringtodate(txtfrom.Text).Month & "," & stringtodate(txtfrom.Text).Year & "," & ds.Tables(0).Rows(i).Item("loc_cd") & "," & _
                Val(dr(0).Item("gross")) & "," & Val(dr(0).Item("basic_amt")) & "," & Val(dr(0).Item("da_amt")) & "," & Val(dr(0).Item("hra_amt")) & "," & _
                Val(dr(0).Item("transport_amt")) & "," & Val(dr(0).Item("child_amt")) & "," & Val(dr(0).Item("city_amt")) & "," & Val(dr(0).Item("spcl_amt")) & _
                "," & Val(dr(0).Item("pf_amt")) & "," & Val(dr(0).Item("comp_pf_amt")) & "," & Val(dr(0).Item("esic_amt")) & "," & Val(dr(0).Item("comp_esic_amt")) & _
                "," & Val(dr(0).Item("pt_amt")) & "," & Val(dr(0).Item("tds_amt")) & "," & Val(dr(0).Item("loan_amt")) & "," & Val(dr(0).Item("othr_earnings")) & "," & _
                Val(dr(0).Item("othr_deuction")) & "," & Val(dr(0).Item("tot_earnings")) & "," & Val(dr(0).Item("tot_deduction")) & "," & Val(dr(0).Item("wages")) & "," & _
                Val(dr(0).Item("tot_reimbursment")) & "," & Val(dr(0).Item("total")) & "," & Val(dr(0).Item("pay_days")) & "," & Val(dr(0).Item("tot_days")) & "," & _
                Val(dr(0).Item("tot_present")) & "," & Val(dr(0).Item("tot_half")) & "," & Val(dr(0).Item("tot_week")) & "," & Val(dr(0).Item("tot_absnt")) & "," & _
                Val(dr(0).Item("tot_leave")) & "," & Val(dr(0).Item("tot_holi")) & "," & Val(dr(0).Item("l1")) & "," & Val(dr(0).Item("l2")) & "," & Val(dr(0).Item("l3")) & "," & _
                Val(dr(0).Item("l4")) & "," & Val(dr(0).Item("l5")) & "," & Val(dr(0).Item("l6")) & "," & Val(dr(0).Item("washing_amt")) & ",'" & a1 & "','" & a2 & "','" & a3 & "','" & _
                a4 & "','" & a5 & "','" & d1 & "','" & d2 & "','" & d3 & "','" & d4 & "','" & d5 & "','" & a1_amt & "','" & a2_amt & "','" & a3_amt & "','" & a4_amt & "','" & a5_amt & "','" & _
                d1_amt & "','" & d2_amt & "','" & d3_amt & "','" & d4_amt & "','" & d5_amt & "','" & in_words & "',(SELECT comp_nm FROM company)," & Val(dr(0).Item("mobile_amt")) & "," & Val(dr(0).Item("food_amt")) & "," & Val(dr(0).Item("medical")) & "," & Val(dr(0).Item("insurance_amt")) & ")")
            End If
        Next
        close1()
        Response.Redirect("payslip.aspx")
    End Sub

    Protected Sub txtempcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtempcode.TextChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If Trim(txtempcode.Text) <> "" Then
            Dim emp_code As String() = Trim(txtempcode.Text).Split("-")
            txtempcode.Text = emp_code(0)
            Dim ds1 As DataSet = get_dataset("SELECT staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm, location_mst.loc_nm, staf.loc_cd FROM location_mst RIGHT OUTER JOIN staf ON location_mst.loc_cd = staf.loc_cd LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            Else
                txtempcode.Text = ""
            End If
        End If
    End Sub
End Class
