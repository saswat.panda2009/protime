﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class reports_monthly_ot
    Inherits System.Web.UI.Page

    <System.Web.Script.Services.ScriptMethod(), _
System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            "staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblmsg.Visible = False
        txtfrmdt.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        txttodt.Text = Format(Now, "dd/MM/yyyy")
        Me.divisiondisp()
        Me.deptdisp()
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
        txtdept.Text = "All Departments"
    End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub deptdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds As DataSet = get_dataset("select distinct dept_nm from dept_mst WHERE loc_cd=" & loc_cd & " order by dept_nm")
        dvdept.DataSource = ds.Tables(0)
        dvdept.DataBind()
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"

        If Trim(txtempcode.Text) = "" Then
            If CheckBox1.Checked = False Then
                Me.report_displ()
            Else
                Me.report_displ1()
            End If
        Else
            Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
            Dim str As String = ""
            If Trim(txtdivsl.Text) <> "" Then
                str = "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
            End If
            txtdept.Text = ""
            For i As Integer = 0 To dvdept.Rows.Count - 1
                Dim chk As CheckBox = dvdept.Rows(i).FindControl("chk")
                If chk.Checked = True Then
                    Dim lbl As Label = dvdept.Rows(i).FindControl("lbldept")
                    txtdept.Text = txtdept.Text & "'" & lbl.Text & "',"
                End If
            Next
            If txtdept.Text <> "" Then
                txtdept.Text = vb.Left(txtdept.Text, txtdept.Text.Length - 1)
                str = str & "AND dept_mst.dept_nm IN (" & txtdept.Text & ")"
            Else
                txtdept.Text = "All Departments"
            End If
            If Trim(txtempcode.Text) <> "" Then
                str = ""
                str = "AND staf.emp_code='" & Trim(txtempcode.Text) & "'"
            End If
            Dim dsstaf As DataSet = get_dataset("SELECT staf.staf_sl, staf.staf_nm, staf.emp_code, staf.device_code, division_mst.div_nm, dept_mst.dept_nm, emp_category.min_ot, emp_category.max_ot FROM staf LEFT OUTER JOIN emp_category ON staf.cat_sl = emp_category.cat_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl WHERE staf.emp_status='I'  AND staf.loc_cd= " & loc_cd & "  " & str & " ORDER BY dept_nm")
            Dim ds1 As DataSet = get_dataset("SELECT atnd.* FROM dept_mst RIGHT OUTER JOIN staf ON dept_mst.dept_sl = staf.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl RIGHT OUTER JOIN atnd ON staf.staf_sl = atnd.staf_sl WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND atnd.loc_cd= " & loc_cd & "  " & str & "  ORDER BY log_dt")


            Dim dt As New DataTable
            dt.Columns.Add("Staf Name", GetType(String))
            dt.Columns.Add("Emp. Code", GetType(String))
            dt.Columns.Add("Device Code", GetType(String))
            dt.Columns.Add("Division", GetType(String))
            dt.Columns.Add("Department", GetType(String))
            dt.Columns.Add("In", GetType(String))
            dt.Columns.Add("Out", GetType(String))


            For i As Integer = 0 To dsstaf.Tables(0).Rows.Count - 1
                Dim tot_p As Integer = 0
                Dim tot_a As Integer = 0
                Dim tot_w As Integer = 0
                Dim tot_l As Integer = 0
                Dim tot_h As Integer = 0
                Dim tot_hr As Integer = 0


                Dim current1 As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
                Dim ending1 As New DateTime(stringtodate(txttodt.Text).Year, stringtodate(txttodt.Text).Month, stringtodate(txttodt.Text).Day)

                Dim dr As DataRow
                dr = dt.NewRow
                dr(0) = Trim(dsstaf.Tables(0).Rows(i).Item("staf_nm"))
                dr(1) = Trim(dsstaf.Tables(0).Rows(i).Item("emp_code"))
                dr(2) = Trim(dsstaf.Tables(0).Rows(i).Item("device_code"))
                dr(3) = Trim(dsstaf.Tables(0).Rows(i).Item("div_nm"))
                dr(4) = Trim(dsstaf.Tables(0).Rows(i).Item("dept_nm"))
                dr(5) = ""
                dr(6) = ""
                dt.Rows.Add(dr)
                While current1 <= ending1
                    Dim day_status As String = ""
                    Dim intime As String = ""
                    Dim outtime As String = ""

                    Dim dratnd As DataRow()
                    dratnd = ds1.Tables(0).Select("log_dt='" & Format(current1, "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                    If dratnd.Length <> 0 Then
                        If dratnd(0).Item("day_status") = "PRESENT" Then
                            day_status = "P"
                            intime = Format(dratnd(0).Item("first_punch"), "HH:mm")
                            outtime = Format(dratnd(0).Item("last_punch"), "HH:mm")
                            tot_hr = tot_hr + dratnd(0).Item("tot_hour")
                            tot_p = tot_p + 1
                        ElseIf dratnd(0).Item("day_status") = "ABSENT" Then
                            day_status = "A"
                            tot_a = tot_a + 1
                        ElseIf dratnd(0).Item("day_status") = "LEAVEDAY" Then
                            day_status = "L"
                            tot_l = tot_l + 1
                        ElseIf dratnd(0).Item("day_status") = "WEEKLYOFF" Then
                            day_status = "W"
                            tot_w = tot_w + 1
                        ElseIf dratnd(0).Item("day_status") = "HOLIDAY" Then
                            day_status = "H"
                            tot_h = tot_h + 1
                        End If
                    End If
                    Dim dr1 As DataRow
                    dr1 = dt.NewRow
                    dr1(0) = ""
                    dr1(1) = ""
                    dr1(2) = ""
                    dr1(3) = ""
                    dr1(4) = Format(current1, "dd/MM/yyyy")
                    dr1(5) = intime
                    dr1(6) = outtime
                    dt.Rows.Add(dr1)
                    current1 = current1.AddDays(1)
                End While

                Dim dr11 As DataRow
                dr11 = dt.NewRow
                dr11(0) = ""
                dr11(1) = ""
                dr11(2) = ""
                dr11(3) = ""
                dr11(4) = ""
                dr11(5) = "Present"
                dr11(6) = tot_p
                dt.Rows.Add(dr11)

                Dim dr12 As DataRow
                dr12 = dt.NewRow
                dr12(0) = ""
                dr12(1) = ""
                dr12(2) = ""
                dr12(3) = ""
                dr12(4) = ""
                dr12(5) = "Absent"
                dr12(6) = tot_a
                dt.Rows.Add(dr12)

                Dim dr13 As DataRow
                dr13 = dt.NewRow
                dr13(0) = ""
                dr13(1) = ""
                dr13(2) = ""
                dr13(3) = ""
                dr13(4) = ""
                dr13(5) = "Leave"
                dr13(6) = tot_l
                dt.Rows.Add(dr13)

                Dim dr14 As DataRow
                dr14 = dt.NewRow
                dr14(0) = ""
                dr14(1) = ""
                dr14(2) = ""
                dr14(3) = ""
                dr14(4) = ""
                dr14(5) = "WeeklyOff"
                dr14(6) = tot_w
                dt.Rows.Add(dr14)

                Dim dr15 As DataRow
                dr15 = dt.NewRow
                dr15(0) = ""
                dr15(1) = ""
                dr15(2) = ""
                dr15(3) = ""
                dr15(4) = ""
                dr15(5) = "Holiday"
                dr15(6) = tot_h
                dt.Rows.Add(dr15)

                Dim dr16 As DataRow
                dr16 = dt.NewRow
                dr16(0) = ""
                dr16(1) = ""
                dr16(2) = ""
                dr16(3) = ""
                dr16(4) = ""
                dr16(5) = "Total Hour"
                dr16(6) = tot_hr
                dt.Rows.Add(dr16)
            Next
            If dt.Rows.Count <> 0 Then
                Dim GridView112 As New GridView()
                GridView112.AllowPaging = False
                GridView112.DataSource = dt
                GridView112.DataBind()
                Response.Clear()
                Response.Buffer = True
                Response.AddHeader("content-disposition", "attachment;filename=MonthlyAttedanceRegister.xls")
                Response.Charset = ""
                Response.ContentType = "application/vnd.ms-excel"
                Dim sw As New StringWriter()
                Dim hw As New HtmlTextWriter(sw)
                For i As Integer = 0 To GridView112.Rows.Count - 1
                    GridView112.Rows(i).Attributes.Add("class", "textmode")
                Next
                GridView112.RenderControl(hw)
                'style to format numbers to string
                Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
                Response.Write(style)
                Response.Output.Write(sw.ToString())
                Response.Flush()
                Response.End()
            End If

        End If
    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub

    Protected Sub txtempcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtempcode.TextChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If Trim(txtempcode.Text) <> "" Then
            Dim emp_code As String() = Trim(txtempcode.Text).Split("-")
            txtempcode.Text = emp_code(0)
            Dim ds1 As DataSet = get_dataset("SELECT staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm, location_mst.loc_nm, staf.loc_cd FROM location_mst RIGHT OUTER JOIN staf ON location_mst.loc_cd = staf.loc_cd LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            Else
                txtempcode.Text = ""
            End If
        End If
    End Sub

    Private Sub report_displ1()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If
        txtdept.Text = ""
        For i As Integer = 0 To dvdept.Rows.Count - 1
            Dim chk As CheckBox = dvdept.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                Dim lbl As Label = dvdept.Rows(i).FindControl("lbldept")
                txtdept.Text = txtdept.Text & "'" & lbl.Text & "',"
            End If
        Next
        If txtdept.Text <> "" Then
            txtdept.Text = vb.Left(txtdept.Text, txtdept.Text.Length - 1)
            str = str & "AND dept_nm IN (" & txtdept.Text & ")"
        Else
            txtdept.Text = "All Departments"
        End If

        Dim dsstaf As DataSet = get_dataset("SELECT staf.staf_sl, staf.staf_nm, staf.emp_code, staf.device_code, division_mst.div_nm, dept_mst.dept_nm, emp_category.* FROM staf LEFT OUTER JOIN emp_category ON staf.cat_sl = emp_category.cat_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl WHERE staf.emp_status='I' AND staf.loc_cd= " & loc_cd & "  " & str & "")
        Dim ds1 As DataSet = get_dataset("SELECT atnd.*,Cast(tot_hour / 60 as Varchar) + ':'+ Cast(tot_hour % 60 as Varchar) as 'hr' FROM dept_mst RIGHT OUTER JOIN staf ON dept_mst.dept_sl = staf.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl RIGHT OUTER JOIN atnd ON staf.staf_sl = atnd.staf_sl WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND atnd.loc_cd= " & loc_cd & "  " & str & "  ORDER BY log_dt")
        Dim avg1 As DataSet = get_dataset("SELECT cast(cast(avg(cast(CAST(Convert(varchar,First_punch,108) as datetime) as float)) as datetime) as time)  as AvgTime,staf_sl FROM atnd WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND Cast(in_out as varchar)<>'' GROUP BY staf_sl")
        Dim avg2 As DataSet = get_dataset("SELECT cast(cast(avg(cast(CAST(Convert(varchar,last_punch,108) as datetime) as float)) as datetime) as time) as AvgTime,staf_sl FROM atnd WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND Cast(in_out as varchar)<>'' GROUP BY staf_sl")
        Dim avg3 As DataSet = get_dataset("SELECT CAST(AVG(cast(tot_hour as int)) AS decimal),staf_sl FROM atnd WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' GROUP BY staf_sl")

        Dim dt As New DataTable
        dt.Columns.Add("Staf Name", GetType(String))
        dt.Columns.Add("Emp. Code", GetType(String))


        Dim current As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
        Dim ending As New DateTime(stringtodate(txttodt.Text).Year, stringtodate(txttodt.Text).Month, stringtodate(txttodt.Text).Day)


        While current <= ending
            dt.Columns.Add(Format(current.Date, "dd"), GetType(String))
            current = current.AddDays(1)
        End While

        dt.Columns.Add("P", GetType(String))
        dt.Columns.Add("H/F", GetType(String))
        dt.Columns.Add("A", GetType(String))
        dt.Columns.Add("W", GetType(String))
        dt.Columns.Add("L", GetType(String))
        dt.Columns.Add("H", GetType(String))
        dt.Columns.Add("Total Hour", GetType(String))
        dt.Columns.Add("Actual Hour", GetType(String))
        dt.Columns.Add("Late", GetType(String))
        dt.Columns.Add("Ded. H/F", GetType(String))
        dt.Columns.Add("Ded. P", GetType(String))
        dt.Columns.Add("Total", GetType(String))
        dt.Columns.Add("Avg. IN", GetType(TimeSpan))
        dt.Columns.Add("Avg. Out", GetType(TimeSpan))
        dt.Columns.Add("Avg. Stay", GetType(String))
        dt.Columns.Add("Late Hour", GetType(String))

        For i As Integer = 0 To dsstaf.Tables(0).Rows.Count - 1
            Dim tot_p As Integer = 0
            Dim tot_a As Integer = 0
            Dim tot_w As Integer = 0
            Dim tot_h As Integer = 0
            Dim tot_l As Integer = 0
            Dim tot_hf As Integer = 0
            Dim tot_hr As Integer = 0
            Dim tot_acthr As Integer = 0
            Dim tot_dhf As Integer = 0
            Dim tot_dp As Integer = 0
            Dim tot_late As Integer = 0
            Dim tot_latehour As Integer = 0
            Dim tot_work As Integer = 0


            Dim continuos_late As Integer = 0
            Dim monthly_late As Integer = 0
            Dim absent As Integer = 0

            Dim continuos_late_status As String = ""
            Dim monthly_late_status As String = ""

            Dim continuos_late_for As Integer = 0
            Dim monthly_late_for As Integer = 0
            Dim absent_for As Integer = 0

            Dim current1 As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
            Dim ending1 As New DateTime(stringtodate(txttodt.Text).Year, stringtodate(txttodt.Text).Month, stringtodate(txttodt.Text).Day)

            Dim dr As DataRow
            dr = dt.NewRow
            dr(0) = Trim(dsstaf.Tables(0).Rows(i).Item("staf_nm"))
            dr(1) = Trim(dsstaf.Tables(0).Rows(i).Item("emp_code"))

            tot_work = Val(Format(dsstaf.Tables(0).Rows(i).Item("max_work"), "HH")) * 60


            continuos_late = dsstaf.Tables(0).Rows(i).Item("continuos_late")
            monthly_late = dsstaf.Tables(0).Rows(i).Item("monthly_late")
            absent = dsstaf.Tables(0).Rows(i).Item("absent_ifprevious_absent")

            continuos_late_status = dsstaf.Tables(0).Rows(i).Item("continuos_late_status")
            monthly_late_status = dsstaf.Tables(0).Rows(i).Item("monthly_late_status")

            continuos_late_for = dsstaf.Tables(0).Rows(i).Item("continuos_late_for")
            monthly_late_for = dsstaf.Tables(0).Rows(i).Item("monthly_late_for")
            absent_for = dsstaf.Tables(0).Rows(i).Item("absent_ifprevious_absent_days")

            Dim dr1 As DataRow
            dr1 = dt.NewRow
            dr1(0) = "Device Code : " & Trim(dsstaf.Tables(0).Rows(i).Item("device_code"))
            dr1(1) = "IN TIME"


            Dim dr2 As DataRow
            dr2 = dt.NewRow
            dr2(0) = "Division : " & Trim(dsstaf.Tables(0).Rows(i).Item("div_nm"))
            dr2(1) = "OUT TIME"


            Dim drq12 As DataRow
            drq12 = dt.NewRow
            drq12(0) = "Department : " & Trim(dsstaf.Tables(0).Rows(i).Item("dept_nm"))
            drq12(1) = "Late By (Min)"


            Dim drq2 As DataRow
            drq2 = dt.NewRow
            drq2(0) = ""
            drq2(1) = "TOTAL"

            Dim column_cnt = 2
            While current1 <= ending1
                Dim day_status As String = ""
                Dim intime As String = ""
                Dim outtime As String = ""
                Dim tot_working As String = ""
                Dim late_by As String = ""

                Dim dratnd As DataRow()
                dratnd = ds1.Tables(0).Select("log_dt='" & Format(current1, "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                If dratnd.Length <> 0 Then
                    If dratnd(0).Item("day_status") = "PRESENT" Then
                        day_status = "P"
                        intime = Format(dratnd(0).Item("first_punch"), "HH:mm")
                        outtime = Format(dratnd(0).Item("last_punch"), "HH:mm")
                        tot_hr = tot_hr + dratnd(0).Item("tot_hour")
                        tot_working = dratnd(0).Item("hr")
                        If dratnd(0).Item("late_in") <> 0 Then
                            tot_late = tot_late + 1
                            If continuos_late = 1 Then
                                Dim drlate As DataRow()
                                drlate = ds1.Tables(0).Select("log_dt <='" & Format(current1, "dd/MMM/yyyy") & "' AND log_dt >'" & Format(current1.AddDays(-continuos_late_for), "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                                If drlate.Length = continuos_late_for Then
                                    If continuos_late_status = "1" Then
                                        tot_dhf = tot_dhf + 1
                                    Else
                                        tot_dp = tot_dp + 1
                                    End If
                                End If
                            End If
                        End If
                        tot_p = tot_p + 1
                    ElseIf dratnd(0).Item("day_status") = "ABSENT" Then
                        day_status = "A"
                        tot_a = tot_a + 1
                        If absent = 1 Then
                            Dim drabsent As DataRow()
                            Dim new_day As Integer = absent_for - 1
                            drabsent = ds1.Tables(0).Select("log_dt <='" & Format(current1, "dd/MMM/yyyy") & "' AND log_dt >='" & Format(current1.AddDays(-new_day), "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & " AND day_status='ABSENT'")
                            If drabsent.Length = absent_for Then
                                tot_dp = tot_dp + 1
                            End If
                        End If
                    ElseIf dratnd(0).Item("day_status") = "LEAVEDAY" Then
                        day_status = "L"
                        tot_l = tot_l + 1
                    ElseIf dratnd(0).Item("day_status") = "WEEKLYOFF" Then
                        day_status = "W"
                        tot_w = tot_w + 1
                    ElseIf dratnd(0).Item("day_status") = "HOLIDAY" Then
                        day_status = "H"
                        tot_h = tot_h + 1
                    ElseIf dratnd(0).Item("day_status") = "HALFDAY" Then
                        day_status = "H/F"
                        intime = Format(dratnd(0).Item("first_punch"), "HH:mm")
                        outtime = Format(dratnd(0).Item("last_punch"), "HH:mm")
                        tot_hr = tot_hr + dratnd(0).Item("tot_hour")
                        tot_working = dratnd(0).Item("hr")
                        tot_hf = tot_hf + 1
                        If dratnd(0).Item("late_in") <> 0 Then
                            tot_late = tot_late + 1
                            If continuos_late = 1 Then
                                Dim drlate As DataRow()
                                drlate = ds1.Tables(0).Select("log_dt <='" & Format(current1, "dd/MMM/yyyy") & "' AND log_dt >'" & Format(current1.AddDays(-continuos_late_for), "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                                If drlate.Length = continuos_late_for Then
                                    If continuos_late_status = "1" Then
                                        tot_dhf = tot_dhf + 1
                                    Else
                                        tot_dp = tot_dp + 1
                                    End If
                                End If
                            End If
                        End If
                    End If
                    late_by = dratnd(0).Item("late_in")
                    tot_latehour = tot_latehour + Val(dratnd(0).Item("late_in"))
                End If
                dr(column_cnt) = day_status
                dr1(column_cnt) = intime
                dr2(column_cnt) = outtime
                drq12(column_cnt) = late_by
                drq2(column_cnt) = tot_working

                column_cnt = column_cnt + 1
                current1 = current1.AddDays(1)
            End While
            If monthly_late = 1 Then
                If tot_late > monthly_late_for Then
                    If monthly_late_status = "1" Then
                        tot_dhf = tot_dhf + (tot_late - monthly_late_for)
                    Else
                        tot_dp = tot_dp + (tot_late - monthly_late_for)
                    End If
                End If
            End If

            dr("P") = tot_p
            dr("H/F") = tot_hf
            dr("A") = tot_a
            dr("W") = tot_w
            dr("L") = tot_l
            dr("H") = tot_h
            Dim h As Integer = Int(tot_hr / 60)
            Dim min As Integer = tot_hr Mod 60
            dr("Total Hour") = h & ":" & min
            dr("Actual Hour") = (Int(tot_p + (tot_hf * 0.5)) * tot_work / 60) & ":" & ((tot_p + (tot_hf * 0.5)) * tot_work) Mod 60
            dr("Late") = tot_late
            dr("Ded. H/F") = tot_dhf
            dr("Ded. P") = tot_dp
            dr("Total") = tot_p + (tot_hf * 0.5) - tot_dp - (tot_dhf * 0.5)

            Dim avg_in As TimeSpan
            Dim avg_out As TimeSpan
            Dim avg_stay As String = "00:00"
            Dim dravg1 As DataRow()
            dravg1 = avg1.Tables(0).Select("staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
            If dravg1.Length <> 0 Then
                avg_in = dravg1(0).Item(0)
            End If
            Dim dravg2 As DataRow()
            dravg2 = avg2.Tables(0).Select("staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
            If dravg2.Length <> 0 Then
                avg_out = dravg2(0).Item(0)
            End If
            Dim dravg3 As DataRow()
            dravg3 = avg3.Tables(0).Select("staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
            If dravg3.Length <> 0 Then
                Dim h1 As Integer = Int(dravg3(0).Item(0) / 60)
                Dim min1 As Integer = dravg3(0).Item(0) Mod 60
                avg_stay = h1 & ":" & min1
            End If
            dr("Avg. IN") = avg_in
            dr("Avg. Out") = avg_out
            dr("Avg. Stay") = avg_stay
            Dim h11 As Integer = Int(tot_latehour / 60)
            Dim min11 As Integer = tot_latehour Mod 60
            dr("Late Hour") = h11 & ":" & min11

            dt.Rows.Add(dr)
            dt.Rows.Add(dr1)
            dt.Rows.Add(dr2)
            dt.Rows.Add(drq12)
            dt.Rows.Add(drq2)
        Next
        If dt.Rows.Count <> 0 Then
            'Create a dummy GridView

            Dim GridView112 As New GridView()
            GridView112.AllowPaging = False
            GridView112.DataSource = dt
            GridView112.DataBind()
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=MonthlyAttedanceRegister.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            For i As Integer = 0 To GridView112.Rows.Count - 1
                GridView112.Rows(i).Attributes.Add("class", "textmode")
            Next
            GridView112.RenderControl(hw)
            'style to format numbers to string
            Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()
        End If

    End Sub

    Private Sub report_displ()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If
        txtdept.Text = ""
        For i As Integer = 0 To dvdept.Rows.Count - 1
            Dim chk As CheckBox = dvdept.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                Dim lbl As Label = dvdept.Rows(i).FindControl("lbldept")
                txtdept.Text = txtdept.Text & "'" & lbl.Text & "',"
            End If
        Next
        If txtdept.Text <> "" Then
            txtdept.Text = vb.Left(txtdept.Text, txtdept.Text.Length - 1)
            str = str & "AND dept_mst.dept_nm IN (" & txtdept.Text & ")"
        Else
            txtdept.Text = "All Departments"
        End If

        Dim dsstaf As DataSet = get_dataset("SELECT staf.staf_sl, staf.staf_nm, staf.emp_code, staf.device_code, division_mst.div_nm, dept_mst.dept_nm, desg_mst.desg_nm, emp_category.* FROM staf LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN emp_category ON staf.cat_sl = emp_category.cat_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl WHERE staf.emp_status='I' AND staf.loc_cd= " & loc_cd & "  " & str & " ORDER BY dept_mst.dept_nm")
        Dim ds1 As DataSet = get_dataset("SELECT atnd.*,Cast(tot_hour / 60 as Varchar) + ':'+ Cast(tot_hour % 60 as Varchar) as 'hr' FROM dept_mst RIGHT OUTER JOIN staf ON dept_mst.dept_sl = staf.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl RIGHT OUTER JOIN atnd ON staf.staf_sl = atnd.staf_sl WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND atnd.loc_cd= " & loc_cd & "  " & str & "  ORDER BY log_dt")
        Dim avg1 As DataSet = get_dataset("SELECT cast(cast(avg(cast(CAST(Convert(varchar,First_punch,108) as datetime) as float)) as datetime) as time)  as AvgTime,staf_sl FROM atnd WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND Cast(in_out as varchar)<>'' GROUP BY staf_sl")
        Dim avg2 As DataSet = get_dataset("SELECT cast(cast(avg(cast(CAST(Convert(varchar,last_punch,108) as datetime) as float)) as datetime) as time) as AvgTime,staf_sl FROM atnd WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND Cast(in_out as varchar)<>'' GROUP BY staf_sl")
        Dim avg3 As DataSet = get_dataset("SELECT CAST(AVG(cast(tot_hour as int)) AS decimal),staf_sl FROM atnd WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' GROUP BY staf_sl")

        Dim dt As New DataTable
        dt.Columns.Add("Sl", GetType(String))
        dt.Columns.Add("Staf Name", GetType(String))
        dt.Columns.Add("Emp. Code", GetType(String))
        dt.Columns.Add("Device Code", GetType(String))
        dt.Columns.Add("Department", GetType(String))
        dt.Columns.Add("Designation", GetType(String))


        Dim current As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
        Dim ending As New DateTime(stringtodate(txttodt.Text).Year, stringtodate(txttodt.Text).Month, stringtodate(txttodt.Text).Day)


        While current <= ending
            dt.Columns.Add(Format(current.Date, "dd"), GetType(String))
            current = current.AddDays(1)
        End While

        dt.Columns.Add("P", GetType(String))
        dt.Columns.Add("OT", GetType(String))
        dt.Columns.Add("A", GetType(String))
        dt.Columns.Add("W", GetType(String))
        dt.Columns.Add("L", GetType(String))
        dt.Columns.Add("H", GetType(String))
        dt.Columns.Add("TOTAL", GetType(String))


        For i As Integer = 0 To dsstaf.Tables(0).Rows.Count - 1
            Dim tot_p As Decimal = 0
            Dim tot_a As Decimal = 0
            Dim tot_w As Decimal = 0
            Dim tot_h As Decimal = 0
            Dim tot_l As Decimal = 0
            Dim tot_hf As Decimal = 0
            Dim tot_hr As Decimal = 0
            Dim tot_acthr As Decimal = 0
            Dim tot_dhf As Decimal = 0
            Dim tot_dp As Decimal = 0
            Dim tot_late As Decimal = 0
            Dim tot_latehour As Decimal = 0
            Dim tot_work As Decimal = 0
            Dim tot_otcnt As Decimal = 0


            Dim continuos_late As Decimal = 0
            Dim monthly_late As Decimal = 0
            Dim absent As Decimal = 0

            Dim continuos_late_status As String = ""
            Dim monthly_late_status As String = ""

            Dim continuos_late_for As Decimal = 0
            Dim monthly_late_for As Decimal = 0
            Dim absent_for As Decimal = 0

            Dim current1 As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
            Dim ending1 As New DateTime(stringtodate(txttodt.Text).Year, stringtodate(txttodt.Text).Month, stringtodate(txttodt.Text).Day)

            Dim dr As DataRow
            dr = dt.NewRow
            dr(0) = i + 1
            dr(1) = Trim(dsstaf.Tables(0).Rows(i).Item("staf_nm"))
            dr(2) = Trim(dsstaf.Tables(0).Rows(i).Item("emp_code"))
            dr(3) = Trim(dsstaf.Tables(0).Rows(i).Item("device_code"))
            dr(4) = Trim(dsstaf.Tables(0).Rows(i).Item("dept_nm"))
            dr(5) = Trim(dsstaf.Tables(0).Rows(i).Item("desg_nm"))

            If dsstaf.Tables(0).Rows(i).Item("device_code") = "132" Then
                Dim x As Integer = 0
            End If

            tot_work = Val(Format(dsstaf.Tables(0).Rows(i).Item("max_work"), "HH")) * 60


            continuos_late = dsstaf.Tables(0).Rows(i).Item("continuos_late")
            monthly_late = dsstaf.Tables(0).Rows(i).Item("monthly_late")
            absent = dsstaf.Tables(0).Rows(i).Item("absent_ifprevious_absent")

            continuos_late_status = dsstaf.Tables(0).Rows(i).Item("continuos_late_status")
            monthly_late_status = dsstaf.Tables(0).Rows(i).Item("monthly_late_status")

            continuos_late_for = dsstaf.Tables(0).Rows(i).Item("continuos_late_for")
            monthly_late_for = dsstaf.Tables(0).Rows(i).Item("monthly_late_for")
            absent_for = dsstaf.Tables(0).Rows(i).Item("absent_ifprevious_absent_days")


            Dim dr1 As DataRow
            dr1 = dt.NewRow
            dr1(0) = ""
            dr1(1) = ""
            dr1(2) = ""
            dr1(3) = ""
            dr1(4) = ""
            dr1(5) = "IN TIME"

            Dim dr2 As DataRow
            dr2 = dt.NewRow
            dr2(0) = ""
            dr2(1) = ""
            dr2(2) = ""
            dr2(3) = ""
            dr2(4) = ""
            dr2(5) = "OUT TIME"

            Dim drq12 As DataRow
            drq12 = dt.NewRow
            drq12(0) = ""
            drq12(1) = ""
            drq12(2) = ""
            drq12(3) = ""
            drq12(4) = ""
            drq12(5) = "OT(Hour)"

            Dim drq2 As DataRow
            drq2 = dt.NewRow
            drq2(0) = ""
            drq2(1) = ""
            drq2(2) = ""
            drq2(3) = ""
            drq2(4) = ""
            drq2(5) = "TOTAL"

            Dim column_cnt = 6
            While current1 <= ending1
                Dim day_status As String = ""
                Dim intime As String = ""
                Dim outtime As String = ""
                Dim tot_working As String = ""
                Dim tot_ot As String = ""
                Dim late_by As String = ""

                Dim dratnd As DataRow()
                dratnd = ds1.Tables(0).Select("log_dt='" & Format(current1, "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                If dratnd.Length <> 0 Then
                    If dratnd(0).Item("day_status") = "PRESENT" Then
                        day_status = "P"
                        intime = Format(dratnd(0).Item("first_punch"), "HH:mm")
                        outtime = Format(dratnd(0).Item("last_punch"), "HH:mm")
                        tot_hr = tot_hr + dratnd(0).Item("tot_hour")
                        tot_working = dratnd(0).Item("hr")
                        If dratnd(0).Item("late_in") <> 0 Then
                            tot_late = tot_late + 1
                            If continuos_late = 1 Then
                                Dim drlate As DataRow()
                                drlate = ds1.Tables(0).Select("log_dt <='" & Format(current1, "dd/MMM/yyyy") & "' AND log_dt >'" & Format(current1.AddDays(-continuos_late_for), "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                                If drlate.Length = continuos_late_for Then
                                    If continuos_late_status = "1" Then
                                        tot_dhf = tot_dhf + 1
                                    Else
                                        tot_dp = tot_dp + 1
                                    End If
                                End If
                            End If
                        End If
                        tot_p = tot_p + 1
                    ElseIf dratnd(0).Item("day_status") = "ABSENT" Then
                        day_status = "A"
                        If dratnd(0).Item("in_out") <> "" Then
                            intime = Format(dratnd(0).Item("first_punch"), "HH:mm")
                            outtime = Format(dratnd(0).Item("last_punch"), "HH:mm")
                            tot_hr = tot_hr + dratnd(0).Item("tot_hour")
                            tot_working = dratnd(0).Item("hr")
                        End If
                        tot_a = tot_a + 1
                        If absent = 1 Then
                            Dim drabsent As DataRow()
                            Dim new_day As Integer = absent_for - 1
                            drabsent = ds1.Tables(0).Select("log_dt <='" & Format(current1, "dd/MMM/yyyy") & "' AND log_dt >='" & Format(current1.AddDays(-new_day), "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & " AND day_status='ABSENT'")
                            If drabsent.Length = absent_for Then
                                tot_dp = tot_dp + 1
                            End If
                        End If
                    ElseIf dratnd(0).Item("day_status") = "LEAVEDAY" Then
                        day_status = "L"
                        tot_l = tot_l + 1
                    ElseIf dratnd(0).Item("day_status") = "WEEKLYOFF" Then
                        day_status = "W"
                        tot_w = tot_w + 1
                    ElseIf dratnd(0).Item("day_status") = "HOLIDAY" Then
                        day_status = "H"
                        tot_h = tot_h + 1
                    ElseIf dratnd(0).Item("day_status") = "HALFDAY" Then
                        day_status = "H/F"
                        intime = Format(dratnd(0).Item("first_punch"), "HH:mm")
                        outtime = Format(dratnd(0).Item("last_punch"), "HH:mm")
                        tot_hr = tot_hr + dratnd(0).Item("tot_hour")
                        tot_working = dratnd(0).Item("hr")
                        tot_hf = tot_hf + 1
                        If dratnd(0).Item("late_in") <> 0 Then
                            tot_late = tot_late + 1
                            If continuos_late = 1 Then
                                Dim drlate As DataRow()
                                drlate = ds1.Tables(0).Select("log_dt <='" & Format(current1, "dd/MMM/yyyy") & "' AND log_dt >'" & Format(current1.AddDays(-continuos_late_for), "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                                If drlate.Length = continuos_late_for Then
                                    If continuos_late_status = "1" Then
                                        tot_dhf = tot_dhf + 1
                                    Else
                                        tot_dp = tot_dp + 1
                                    End If
                                End If
                            End If
                        End If
                    End If
                    late_by = dratnd(0).Item("late_in")
                    tot_latehour = tot_latehour + Val(dratnd(0).Item("late_in"))
                    tot_ot = dratnd(0).Item("tot_otmin")
                    If tot_ot > dsstaf.Tables(0).Rows(i).Item("max_ot") Then
                        tot_otcnt = tot_otcnt + 1
                    ElseIf tot_ot > dsstaf.Tables(0).Rows(i).Item("min_ot") Then
                        tot_otcnt = tot_otcnt + 0.5
                    End If
                End If
                dr(column_cnt) = day_status
                dr1(column_cnt) = intime
                dr2(column_cnt) = outtime
                Dim h1 As Integer = Int(tot_ot / 60)
                Dim min1 As Integer = tot_ot Mod 60
                drq12(column_cnt) = h1 & ":" & min1
                drq2(column_cnt) = tot_working

                column_cnt = column_cnt + 1
                current1 = current1.AddDays(1)
            End While
            Dim tot_actp As Decimal = 0
            Dim tot_acot As Decimal = 0
            If (tot_p + (tot_hf * 0.5) >= 25) Then
                tot_actp = 26
                tot_acot = (tot_p + (tot_hf * 0.5)) - 26
            Else
                tot_actp = (tot_p + (tot_hf * 0.5))
            End If
            dr("P") = tot_actp
            dr("A") = tot_a
            dr("OT") = tot_otcnt + tot_acot
            dr("W") = tot_w
            dr("L") = tot_l
            dr("H") = tot_h
            dr("Total") = tot_actp + tot_otcnt + tot_acot

            dt.Rows.Add(dr)
            dt.Rows.Add(dr1)
            dt.Rows.Add(dr2)
            dt.Rows.Add(drq12)
            dt.Rows.Add(drq2)
        Next
        If dt.Rows.Count <> 0 Then
            'Create a dummy GridView

            Dim GridView112 As New GridView()
            GridView112.AllowPaging = False
            GridView112.DataSource = dt
            GridView112.DataBind()
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=MonthlyAttedanceRegister.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            For i As Integer = 0 To GridView112.Rows.Count - 1
                GridView112.Rows(i).Attributes.Add("class", "textmode")
            Next
            GridView112.RenderControl(hw)
            'style to format numbers to string
            Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()
        End If

    End Sub
End Class
