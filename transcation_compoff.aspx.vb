﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.Net.Mail

Partial Class transcation_leave
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblbalance.Text = "0"
        txtcontact.Text = ""
        txtemail.Text = ""
        txtnodays.Text = ""
        txtreason.Text = ""
        txtstafnm.Text = ""
        txtdept.Text = ""
        txtdesg.Text = ""
        txtfrom.Text = Format(Now, "dd/MM/yyyy")
        txtto.Text = Format(Now, "dd/MM/yyyy")
        Me.leave_disp()
        Dim sdt As Date = stringtodate(txtfrom.Text)
        Dim endt As Date = stringtodate(txtto.Text)
        txtnodays.Text = DateDiff(DateInterval.Day, sdt, endt) + 1
    End Sub

    Private Sub leave_disp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbleavenm.Items.Clear()
        cmbleavenm.Items.Add("Please Select The Leave")
        Dim ds1 As DataSet = get_dataset("SELECT leave_nm From leave_mst WHERE loc_cd=" & loc_cd & " AND leave_tp = 8 ORDER BY leave_nm")
        For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
            cmbleavenm.Items.Add(ds1.Tables(0).Rows(i).Item("leave_nm"))
        Next
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim staf_sl As Integer = 0
        Dim dsstaf_sl As DataSet = get_dataset("SELECT staf_sl FROM staf WHERE emp_code='" & Trim(txtempcode.Text) & "' AND loc_cd=" & loc_cd & "")
        If dsstaf_sl.Tables(0).Rows.Count <> 0 Then
            staf_sl = dsstaf_sl.Tables(0).Rows(0).Item("staf_sl")
        End If
        Dim sdt As Date = stringtodate(txtfrom.Text)
        Dim endt As Date = stringtodate(txtto.Text)
        txtnodays.Text = DateDiff(DateInterval.Day, sdt, endt) + 1
        If Val(txtnodays.Text) <= 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Leave Days Should Be Greater Than 0');", True)
            txtfrom.Focus()
            Exit Sub
        End If
        Dim leave_sl As Integer = 0
        Dim dscheck As DataSet = get_dataset("SELECT leave_sl FROM leave_mst WHERE leave_nm='" & UCase(Trim(cmbleavenm.Text)) & "' AND loc_cd='" & loc_cd & "'")
        If dscheck.Tables(0).Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Sorry Leave Name Should Not Be Blank');", True)
            cmbleavenm.Focus()
            Exit Sub
        Else
            leave_sl = dscheck.Tables(0).Rows(0).Item(0)
        End If
        If Val(lblbalance.Text) - Val(txtnodays.Text) < 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Sorry No Leave Available');", True)
            Exit Sub
        End If
        Dim fnd As Integer = 0
        Dim totday As Integer = 0
        For i As Integer = 0 To dvleave.Rows.Count - 1
            Dim chk As CheckBox = dvleave.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                fnd = 1
                totday = totday + 1
                Exit For
            End If
        Next
        If fnd = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Select Atleast One Date');", True)
            dvleave.Focus()
            Exit Sub
        End If
        If Val(totday) <> Val(txtnodays.Text) Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Sorry Selected Date Doesnot Matched');", True)
            Exit Sub
        End If

        Dim max As Integer = 1
        Dim ds1 As DataSet = get_dataset("SELECT max(v_no) FROM lvoucher1")
        If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
            max = ds1.Tables(0).Rows(0).Item(0) + 1
        End If
        start1()
        SQLInsert("INSERT INTO lvoucher1(v_no,v_dt,loc_cd,staf_sl,from_dt,to_dt,leave_status,status_by,leave_tp,reason,cont_no,email_id,v_time,s_time,leave_sl) VALUES(" & max & _
        ",'" & Now & "'," & loc_cd & "," & staf_sl & ",'" & Format(sdt, "dd/MMM/yyyy") & "','" & Format(endt, "dd/MMM/yyyy") & "','A',0,'" & _
        vb.Left(cmbtp.Text, 1) & "','" & Trim(txtreason.Text) & "','" & Trim(txtcontact.Text) & "','" & Trim(txtemail.Text) & "','" & Now & "',''," & leave_sl & ")")
        SQLInsert("UPDATE lvoucher2 SET approved='Y' WHERE v_no=" & max & "")

        For i As Integer = 0 To dvleave.Rows.Count - 1
            Dim chk As CheckBox = dvleave.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                Dim lblslno As Label = dvleave.Rows(i).FindControl("lblslno")
                SQLInsert("UPDATE staf_comp_off SET v_no=" & max & ",comp_off_status='A' WHERE sl=" & lblslno.Text & "")
            End If
        Next
        close1()
        Dim mail_sub As String = "Leave Application Of :" & UCase(Trim(txtstafnm.Text)) & " From Dt :" & Trim(txtfrom.Text) & " To Dt :" & Trim(txtto.Text)
        Dim msg As String = "Dear Employer, <br/>" & _
                            "The following employee applied for leave as details given hereunder. Please approve the same.<br/>" & _
                            "Employee Name :" & UCase(Trim(txtstafnm.Text)) & "<br/>" & _
                            "Leave Type : " & Trim(cmbtp.Text) & "<br/>" & _
                            "Leave From : " & Trim(txtfrom.Text) & "<br/>" & _
                            "Leave Upto : " & Trim(txtto.Text) & "<br/>" & _
                            "No of Days : " & Trim(txtnodays.Text) & "<br/>" & _
                            "Reason : " & Trim(txtreason.Text) & "<br/>" & _
                            "Contact No : " & Trim(txtcontact.Text) & "<br/>" & _
                            "Email Id :  " & LCase(Trim(txtemail.Text)) & "<br/>" & _
                            "Apply Date & Time :  " & Now & "<br/>" & _
                            "Approved Date & Time :  " & Now
        Me.email_send("technohubitsolutions@gmail.com", mail_sub, msg)
        Dim dsdiv As DataSet = get_dataset("SELECT cont_email FROM division_mst WHERE div_sl=(SELECT div_sl FROM staf WHERE staf_sl=" & Val(staf_sl) & ") ")
        If dsdiv.Tables(0).Rows.Count <> 0 Then
            If dsdiv.Tables(0).Rows(0).Item("cont_email") <> "" Then
                Me.email_send(dsdiv.Tables(0).Rows(0).Item("cont_email"), mail_sub, msg)
            End If
        End If
        Me.clr()
        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
    End Sub

    Private Sub email_send(ByVal mail_to, ByVal mail_sub, ByVal mail_msg)
        Dim SmtpServer As New SmtpClient()
        Dim mail As New MailMessage()
        SmtpServer.Credentials = New Net.NetworkCredential("protime.attendance@gmail.com", "14042016")
        SmtpServer.Port = 587
        SmtpServer.Host = "smtp.gmail.com"
        SmtpServer.EnableSsl = True
        mail = New MailMessage()
        mail.From = New MailAddress("protime.attendance@gmail.com")
        mail.To.Add(mail_to)
        mail.Subject = mail_sub
        mail.IsBodyHtml = True
        mail.Body = mail_msg
        SmtpServer.Send(mail)
    End Sub

    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        txtfrom.Focus()
    End Sub

    Protected Sub txtfrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtfrom.TextChanged
        Dim sdt As Date = stringtodate(txtfrom.Text)
        Dim endt As Date = stringtodate(txtto.Text)
        txtnodays.Text = DateDiff(DateInterval.Day, sdt, endt) + 1
    End Sub

    Protected Sub txtto_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtto.TextChanged
        Dim sdt As Date = stringtodate(txtfrom.Text)
        Dim endt As Date = stringtodate(txtto.Text)
        txtnodays.Text = DateDiff(DateInterval.Day, sdt, endt) + 1
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        Dim ds1 As DataSet = get_dataset("SELECT row_number() OVER(ORDER BY staf.staf_nm) as sl,staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm FROM desg_mst RIGHT OUTER JOIN staf ON desg_mst.desg_sl = staf.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl  WHERE staf.staf_nm like '%" & UCase(Trim(txtfilterstud.Text)) & "%' AND staf.loc_cd= " & CType(Session("loc_cd"), Integer) & " ORDER BY staf.staf_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT staf.staf_sl,staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm FROM desg_mst RIGHT OUTER JOIN staf ON desg_mst.desg_sl = staf.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl  WHERE staf.emp_code = '" & Trim(GridView1.Rows(rw).Cells(1).Text) & "' AND staf.loc_cd= " & CType(Session("loc_cd"), Integer) & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                Me.clr()
                txtstafsl.Text = ds1.Tables(0).Rows(0).Item("staf_sl")
                txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
                txtstafnm.Text = ds1.Tables(0).Rows(0).Item("staf_nm")
                txtdept.Text = ds1.Tables(0).Rows(0).Item("dept_nm")
                txtdesg.Text = ds1.Tables(0).Rows(0).Item("desg_nm")
                Me.pnlsearch_CollapsiblePanelExtender.Collapsed = True
                Me.pnlsearch_CollapsiblePanelExtender.ClientState = True
            End If
        End If
    End Sub

    Protected Sub cmdsave0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave0.Click
        Dim ds1 As DataSet = get_dataset("SELECT staf.staf_sl,staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm FROM desg_mst RIGHT OUTER JOIN staf ON desg_mst.desg_sl = staf.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl  WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd= " & CType(Session("loc_cd"), Integer) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            Me.clr()
            txtstafsl.Text = ds1.Tables(0).Rows(0).Item("staf_sl")
            txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            txtstafnm.Text = ds1.Tables(0).Rows(0).Item("staf_nm")
            txtdept.Text = ds1.Tables(0).Rows(0).Item("dept_nm")
            txtdesg.Text = ds1.Tables(0).Rows(0).Item("desg_nm")
            lblbalance.Text = "0"
            Dim ds As DataSet = get_dataset("SELECT * FROM staf_comp_off WHERE staf_sl=" & txtstafsl.Text & " AND assign_year='" & CType(Session("cur_year"), String) & "' AND comp_off_status='P'")
            dvleave.DataSource = ds.Tables(0)
            dvleave.DataBind()
            If ds.Tables(0).Rows.Count <> 0 Then
                lblbalance.Text = ds.Tables(0).Rows.Count
            End If
        End If
    End Sub
End Class
