﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="transcation_punchmanage.aspx.vb" Inherits="transcation_punchmanage" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Punch Management . . ."></asp:Label></h3>
                                </div>

                     <div class="card-body">
                                        <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table style="width:100%;">
                                                                                         
                                              <tr>
                                                  <td>
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="45%">
                                                                  Employee Id</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td width="45%">
                                                                  <asp:TextBox ID="txtempcode" runat="server" class="form-control" 
                                                                      MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtempcode_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtempcode">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td width="10%">
                                                                  &nbsp; &nbsp; <asp:Button ID="cmdsave0" runat="server" class="btn btn-info" Text="Get" />
                                                              </td>
                                                              <td width="45%">
                                                                 <asp:Button ID="Button1" runat="server" class="btn btn-success" Text="View Last Punch" /></td>
                                                          </tr>
                                                                                                               
                                                          <tr>
                                                              <td width="45%">
                                                                  Name</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <asp:TextBox ID="txtstafnm" runat="server" class="form-control" MaxLength="100" 
                                                                      BackColor="White" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtstafnm_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtstafnm">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Department</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  Designation</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtdept" runat="server" BackColor="White" class="form-control" 
                                                                      MaxLength="100" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdept_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdept">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtdesg" runat="server" BackColor="White" class="form-control" 
                                                                      MaxLength="100" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdesg_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdesg">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <label for="simpleDataInput">
                                                                  From Date</label></td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <label for="simpleDataInput">
                                                                  To Date</label></td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                
                  
                                                                  <asp:TextBox ID="txtfordt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                  <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server" 
                                                                      Enabled="True" TargetControlID="txtfordt" PopupButtonID="txtfordt" Format="dd/MM/yyyy">
                                                                  </asp:CalendarExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                   
                                                                  <asp:TextBox ID="txtto1" runat="server" CssClass="form-control"></asp:TextBox>
                                                                  <asp:CalendarExtender ID="txtfordt0_CalendarExtender" runat="server" 
                                                                      Enabled="True" Format="dd/MM/yyyy" PopupButtonID="txtto1" 
                                                                      TargetControlID="txtto1">
                                                                  </asp:CalendarExtender>
                                                                   
                                                              </td>
                                                          </tr>

                                                          <tr>
                                                              <td>
                                                                  <asp:Button ID="cmdsave1" runat="server" class="btn btn-info" 
                                                                      Text="Get Punches" />
                                                                  &nbsp;<asp:Button ID="cmdsave6" runat="server" class="btn btn-info" 
                                                                      Text="Process" Visible="False" />
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <div style="width: 100%; height: 300px; overflow: auto;">
                                                                      <asp:GridView ID="dvstaf" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                                          AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                                          PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                                          <AlternatingRowStyle CssClass="alt" />
                                                                          <Columns>
                                                                              <asp:TemplateField>
                                                                                     <ItemTemplate>
                                                                                      <asp:CheckBox ID="chk" runat="Server" />
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField HeaderText="Date">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lbldt" runat="server" Text='<%#Eval("log_dt1") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField HeaderText="Time">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lbldevice_code" runat="server" Text='<%#Eval("log_time") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                               <asp:TemplateField HeaderText="Direction">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lbldirection" runat="server" Text='<%#Eval("DeviceDirection") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                               <asp:TemplateField HeaderText="Type">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lbltp" runat="server" Text='<%#Eval("tp") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                                 <asp:TemplateField HeaderText="View">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblview" runat="server" Text='<%#Eval("view_report") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                               <asp:TemplateField Visible="False">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblslno" runat="server" Text='<%#Eval("sl_no") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                          </Columns>
                                                                          <PagerStyle HorizontalAlign="Right" />
                                                                      </asp:GridView>
                                                                  </div>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3" style="font-size: 5px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td valign="bottom">
                                                                  <asp:Button ID="cmdsave2" runat="server" class="btn btn-primary" 
                                                                      Text="Remove Punch" />
                                                                  &nbsp;
                                                                  <asp:Button ID="cmdsave5" runat="server" class="btn btn-primary" 
                                                                      Text="Add Removed" />
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <table style="width:100%;">
                                                                      <tr>
                                                                          <td width="80%">
                                                                              <table style="width:100%;">
                                                                                  <tr>
                                                                                      <td width="50%">
                                                                                          Date</td>
                                                                                      <td width="30%">
                                                                                          Time</td>
                                                                                      <td width="20%">
                                                                                          Type</td>
                                                                                  </tr>
                                                                                  <tr>
                                                                                      <td width="50%">
                                                                                          <asp:TextBox ID="txtpunchdt" runat="server" class="form-control"></asp:TextBox>
                                                                                          <asp:CalendarExtender ID="txtpunchdt_CalendarExtender" runat="server" 
                                                                                              Enabled="True" Format="dd/MM/yyyy" PopupButtonID="txtpunchdt" 
                                                                                              TargetControlID="txtpunchdt">
                                                                                          </asp:CalendarExtender>
                                                                                      </td>
                                                                                      <td width="30%">
                                                                                          <asp:TextBox ID="txtsttm" runat="server" class="form-control" MaxLength="100"></asp:TextBox>
                                                                                          <asp:FilteredTextBoxExtender ID="txtsttm_FilteredTextBoxExtender" 
                                                                                              runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                                              TargetControlID="txtsttm">
                                                                                          </asp:FilteredTextBoxExtender>
                                                                                      </td>
                                                                                      <td width="40%">
                                                                                          <asp:DropDownList ID="cmbtype" runat="server" CssClass="form-control">
                                                                                              <asp:ListItem>IN</asp:ListItem>
                                                                                              <asp:ListItem>OUT</asp:ListItem>
                                                                                          </asp:DropDownList>
                                                                                      </td>
                                                                                  </tr>
                                                                              </table>
                                                                          </td>
                                                                          <td width="20%" valign="bottom">
                                                                              &nbsp;
                                                                              <asp:Button ID="cmdsave3" runat="server" class="btn btn-primary" 
                                                                                  Text="Add Punch" Width="100%" />
                                                                          </td>
                                                                      </tr>
                                                                  </table>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtstafsl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                                  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                ControlToValidate="txtsttm" Display="None" ErrorMessage="Invalid Format" ValidationExpression="^((0?[1-9]|1[012])(:[0-5]\d){0,2}(\ [AP]M))$|^([01]\d|2[0-3])(:[0-5]\d){0,2}$"></asp:RegularExpressionValidator>
                                            <asp:ValidatorCalloutExtender ID="RegularExpressionValidator1_ValidatorCalloutExtender" 
                                                runat="server" Enabled="True" PopupPosition="BottomLeft" 
                                                TargetControlID="RegularExpressionValidator1">
                                            </asp:ValidatorCalloutExtender>
                                                                 
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <div style="width: 100%; height: 500px; overflow: auto;">
                                                                      <asp:GridView ID="dvdata" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                                          AutGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                          PageSize="15" Width="100%">
                                                                          <AlternatingRowStyle CssClass="alt" />
                                                                          <PagerStyle HorizontalAlign="Right" />
                                                                      </asp:GridView>
                                                                  </div>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>       
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

