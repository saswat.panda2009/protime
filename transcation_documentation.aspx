﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="transcation_documentation.aspx.vb" Inherits="transcation_documentation" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Employee Documnet Management . . ."></asp:Label></h3>
                
                </div>

                     <div class="card-body">
                                     <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table style="width:100%;">
                                                                                         
                                              <tr>
                                                  <td>
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="45%">
                                                                  Employee Id</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td width="45%">
                                                                  <asp:TextBox ID="txtempcode" runat="server" class="form-control" 
                                                                      MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtempcode_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtempcode">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td width="10%">
                                                                  &nbsp; &nbsp; <asp:Button ID="cmdsave0" runat="server" class="btn btn-info" Text="Get" />
                                                              </td>
                                                              <td width="45%">
                                                                  <asp:TextBox ID="txtstafsl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                              </td>
                                                          </tr>
                                                                                                               
                                                          <tr>
                                                              <td width="45%">
                                                                  Name</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <asp:TextBox ID="txtstafnm" runat="server" class="form-control" MaxLength="100" 
                                                                      BackColor="White" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtstafnm_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtstafnm">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Department</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  Designation</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtdept" runat="server" BackColor="White" class="form-control" 
                                                                      MaxLength="100" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdept_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdept">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtdesg" runat="server" BackColor="White" class="form-control" 
                                                                      MaxLength="100" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdesg_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdesg">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3" style="font-size: 5px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Document Details</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <asp:TextBox ID="txtdocuments" runat="server" class="form-control" 
                                                                      MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdocuments_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdocuments">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  Upload File (.docx, .pdf, .jpg, .jpeg, .png)</td>
                                                          </tr>
                                                          <tr>
                                                              <td class="style1">
                                                                  <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                                                                 
                                                              </td>
                                                              <td class="style1">
                                                                  </td>
                                                              <td class="style1">
                                                                  <asp:Button ID="cmdget" runat="server" CausesValidation="False" 
                                                                      class="btn btn-success" Text="Import" />
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <div style="width: 100%; height: 300px; overflow: auto; font-size: 15px;">
                                                                      <asp:GridView ID="dvstaf" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                                          AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                                          PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                                          <AlternatingRowStyle CssClass="alt" />
                                                                          <Columns>
                                                                              <asp:TemplateField HeaderText="Sl">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lbldt" runat="server" Text='<%#Eval("sl") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                  <ItemStyle Width="25px" />
                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField HeaderText="Document Details">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lbldivision" runat="server" Text='<%#Eval("document_heading") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                            
                                                                              <asp:TemplateField Visible="False">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblstaf_sl" runat="server" Text='<%#Eval("staf_sl") %>'></asp:Label>
                                                                                      <asp:Label ID="lblid" runat="server" Text='<%#Eval("doc_sl") %>'></asp:Label>
                                                                                    
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                              <asp:ButtonField ButtonType="Image" CommandName="view" 
                                                                                  ImageUrl="images/view.png" Text="Button">
                                                                              <HeaderStyle Width="30px" />
                                                                              <ItemStyle Width="30px" />
                                                                              </asp:ButtonField>
                                                                              <asp:ButtonField ButtonType="Image" CommandName="delete_data" 
                                                                                  ImageUrl="images/delete.png" Text="Button">
                                                                              <HeaderStyle Width="30px" />
                                                                              <ItemStyle Width="30px" />
                                                                              </asp:ButtonField>
                                                                          </Columns>
                                                                          <PagerStyle HorizontalAlign="Right" />
                                                                      </asp:GridView>
                                                                  </div>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  &nbsp;</td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>       
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

