﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class transcation_leaveassign
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
        txtdays.Text = "0"
        chkall.Checked = False
        txtyear.Text = CType(Session("cur_year"), String)
        cmbleavetp.SelectedIndex = 0
        Me.divisiondisp()
        Me.deptdisp()
        Dim ds As DataSet = get_dataset("SELECT staf_nm, emp_code, device_code, staf_sl FROM staf")
        ds.Tables(0).Rows.Clear()
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
        If txtmode.Text = "E" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Leave Assignment (Entry Mode)"
            cmbdivsion.Focus()
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Leave Assignment (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub staf_disp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim str As String = ""
        Dim dspol As DataSet = get_dataset("Select * from Leave_policies WHERE leave_tp='" & vb.Left(cmbleavetp.Text, 1) & "'")
        If dspol.Tables(0).Rows.Count <> 0 Then
            If dspol.Tables(0).Rows(0).Item("doj_days") <> 0 Then
                str = "AND datediff(day,doj,getdate()) > " & dspol.Tables(0).Rows(0).Item("doj_days") & ""
            End If
        End If
        Dim ds As New DataSet
        If cmbdivsion.SelectedIndex = 0 Then
            If cmbdept.SelectedIndex = 0 Then
                ds = get_dataset("SELECT (SELECT no_day FROM leave_assignment WHERE staf_sl= staf.staf_sl AND assign_year='" & txtyear.Text & "' AND leave_tp='" & vb.Left(cmbleavetp.Text, 1) & "') as 'no_day',staf.staf_nm, staf.emp_code, staf.device_code, staf.staf_sl, desg_mst.desg_nm, dept_mst.dept_nm FROM staf LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.loc_cd=" & loc_cd & " AND emp_status='I' " & str & " ORDER BY staf_nm")
            Else
                ds = get_dataset("SELECT  (SELECT no_day FROM leave_assignment WHERE staf_sl= staf.staf_sl AND assign_year='" & txtyear.Text & "' AND leave_tp='" & vb.Left(cmbleavetp.Text, 1) & "') as 'no_day',staf.staf_nm, staf.emp_code, staf.device_code, staf.staf_sl, desg_mst.desg_nm, dept_mst.dept_nm FROM staf LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.loc_cd=" & loc_cd & " AND emp_status='I' AND staf.dept_sl=" & Val(txtdeptcd.Text) & " " & str & " ORDER BY staf_nm")
            End If
        Else
            If cmbdept.SelectedIndex = 0 Then
                ds = get_dataset("SELECT  (SELECT no_day FROM leave_assignment WHERE staf_sl= staf.staf_sl AND assign_year='" & txtyear.Text & "' AND leave_tp='" & vb.Left(cmbleavetp.Text, 1) & "') as 'no_day',staf.staf_nm, staf.emp_code, staf.device_code, staf.staf_sl, desg_mst.desg_nm, dept_mst.dept_nm FROM staf LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.loc_cd=" & loc_cd & " AND emp_status='I' AND div_sl=" & Val(txtdivsl.Text) & " " & str & " ORDER BY staf_nm")
            Else
                ds = get_dataset("SELECT (SELECT no_day FROM leave_assignment WHERE staf_sl= staf.staf_sl AND assign_year='" & txtyear.Text & "' AND leave_tp='" & vb.Left(cmbleavetp.Text, 1) & "') as 'no_day',staf.staf_nm, staf.emp_code, staf.device_code, staf.staf_sl, desg_mst.desg_nm, dept_mst.dept_nm FROM staf LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.loc_cd=" & loc_cd & " AND emp_status='I' AND staf.dept_sl=" & Val(txtdeptcd.Text) & " AND div_sl=" & Val(txtdivsl.Text) & " " & str & " ORDER BY staf_nm")
            End If
        End If
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
    End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub deptdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdept.Items.Clear()
        cmbdept.Items.Add("Please Select A Department")
        Dim ds As DataSet = get_dataset("SELECT dept_nm FROM dept_mst WHERE loc_cd=" & loc_cd & " ORDER BY dept_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdept.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Protected Sub cmbdept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdept.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdeptcd.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT dept_sl FROM dept_mst WHERE dept_nm='" & Trim(cmbdept.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdeptcd.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
        Me.staf_disp()
    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
        Me.staf_disp()
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT leave_assignment.sl,division_mst.div_nm, staf.emp_code, staf.staf_nm, leave_assignment.assign_year,(CASE WHEN leave_assignment.leave_tp=1 THEN 'Paid Leave' WHEN leave_assignment.leave_tp=2 THEN 'Casual Leave' END) as 'tp', leave_assignment.no_day FROM leave_assignment LEFT OUTER JOIN staf ON leave_assignment.staf_sl = staf.staf_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl WHERE leave_assignment.loc_cd=" & loc_cd & " ORDER BY assign_year DESC,staf_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        Dim shift_sl As Integer = 0
        If txtyear.Text = "" Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Select A Year Name');", True)
            txtyear.Focus()
            Exit Sub
        End If
        If chkall.Checked = True Then
            Dim ds As New DataSet
            If cmbdivsion.SelectedIndex = 0 Then
                If cmbdept.SelectedIndex = 0 Then
                    ds = get_dataset("SELECT (SELECT no_day FROM leave_assignment WHERE staf_sl= staf.staf_sl AND assign_year='" & txtyear.Text & "' AND leave_tp='" & vb.Left(cmbleavetp.Text, 1) & "') as 'no_day',staf.staf_nm, staf.emp_code, staf.device_code, staf.staf_sl, desg_mst.desg_nm, dept_mst.dept_nm FROM staf LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.loc_cd=" & loc_cd & " AND emp_status='I' ORDER BY staf_nm")
                Else
                    ds = get_dataset("SELECT  (SELECT no_day FROM leave_assignment WHERE staf_sl= staf.staf_sl AND assign_year='" & txtyear.Text & "' AND leave_tp='" & vb.Left(cmbleavetp.Text, 1) & "') as 'no_day',staf.staf_nm, staf.emp_code, staf.device_code, staf.staf_sl, desg_mst.desg_nm, dept_mst.dept_nm FROM staf LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.loc_cd=" & loc_cd & " AND emp_status='I' AND dept_sl=" & Val(txtdeptcd.Text) & " ORDER BY staf_nm")
                End If
            Else
                If cmbdept.SelectedIndex = 0 Then
                    ds = get_dataset("SELECT  (SELECT no_day FROM leave_assignment WHERE staf_sl= staf.staf_sl AND assign_year='" & txtyear.Text & "' AND leave_tp='" & vb.Left(cmbleavetp.Text, 1) & "') as 'no_day',staf.staf_nm, staf.emp_code, staf.device_code, staf.staf_sl, desg_mst.desg_nm, dept_mst.dept_nm FROM staf LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.loc_cd=" & loc_cd & " AND emp_status='I' AND div_sl=" & Val(txtdivsl.Text) & " ORDER BY staf_nm")
                Else
                    ds = get_dataset("SELECT (SELECT no_day FROM leave_assignment WHERE staf_sl= staf.staf_sl AND assign_year='" & txtyear.Text & "' AND leave_tp='" & vb.Left(cmbleavetp.Text, 1) & "') as 'no_day',staf.staf_nm, staf.emp_code, staf.device_code, staf.staf_sl, desg_mst.desg_nm, dept_mst.dept_nm FROM staf LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.loc_cd=" & loc_cd & " AND emp_status='I' AND dept_sl=" & Val(txtdeptcd.Text) & " AND div_sl=" & Val(txtdivsl.Text) & " ORDER BY staf_nm")
                End If
            End If
            Dim dsout As DataSet = get_dataset("SELECT * FROM leave_assignment WHERE loc_cd=" & loc_cd & " AND assign_year='" & txtyear.Text & "' AND leave_tp='" & vb.Left(cmbleavetp.Text, 1) & "'")
            start1()
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim staf_sl As Integer = ds.Tables(0).Rows(i).Item("staf_sl")
                Dim days As Integer = Val(txtdays.Text)
                If Val(days) > 0 Then
                    Dim dr As DataRow()
                    dr = dsout.Tables(0).Select("staf_sl=" & staf_sl & "")
                    If dr.Length = 0 Then
                        Dim max As Integer = 1
                        Dim ds1 As DataSet = get_dataset("SELECT max(sl) FROM leave_assignment")
                        If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                            max = ds1.Tables(0).Rows(0).Item(0) + 1
                        End If
                        SQLInsert("INSERT INTO leave_assignment(sl,loc_cd,assign_year,leave_tp,staf_sl,no_day) VALUES(" & max & "," & loc_cd & _
                        ",'" & txtyear.Text & "','" & vb.Left(cmbleavetp.Text, 1) & "'," & staf_sl & "," & days & ")")
                    Else
                        SQLInsert("UPDATE leave_assignment SET no_day=" & days & " WHERE loc_cd=" & loc_cd & " AND assign_year='" & txtyear.Text & "' AND leave_tp='" & vb.Left(cmbleavetp.Text, 1) & "' AND staf_sl=" & staf_sl & "")
                    End If
                End If
            Next
            close1()
        Else
            Dim fnd As Integer = 0
            For i As Integer = 0 To dvstaf.Rows.Count - 1
                Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
                If chk.Checked = True Then
                    fnd = 1
                    Exit For
                End If
            Next
            If fnd = 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Select Atleast One Employee');", True)
                dvstaf.Focus()
                Exit Sub
            End If
            Dim dsout As DataSet = get_dataset("SELECT * FROM leave_assignment WHERE loc_cd=" & loc_cd & " AND assign_year='" & txtyear.Text & "' AND leave_tp='" & vb.Left(cmbleavetp.Text, 1) & "'")
            start1()
            For j As Integer = 0 To dvstaf.Rows.Count - 1
                Dim chk As CheckBox = dvstaf.Rows(j).FindControl("chk")
                If chk.Checked = True Then
                    Dim lblsl As Label = dvstaf.Rows(j).FindControl("lblstaf_sl")
                    Dim days As TextBox = dvstaf.Rows(j).FindControl("txtday")
                    If Val(days.Text) > 0 Then
                        Dim dr As DataRow()
                        dr = dsout.Tables(0).Select("staf_sl=" & lblsl.Text & "")
                        If dr.Length = 0 Then
                            Dim max As Integer = 1
                            Dim ds1 As DataSet = get_dataset("SELECT max(sl) FROM leave_assignment")
                            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                                max = ds1.Tables(0).Rows(0).Item(0) + 1
                            End If
                            SQLInsert("INSERT INTO leave_assignment(sl,loc_cd,assign_year,leave_tp,staf_sl,no_day) VALUES(" & max & "," & loc_cd & _
                            ",'" & txtyear.Text & "','" & vb.Left(cmbleavetp.Text, 1) & "'," & lblsl.Text & "," & days.Text & ")")
                        Else
                            SQLInsert("UPDATE leave_assignment SET no_day=" & days.Text & " WHERE loc_cd=" & loc_cd & " AND assign_year='" & txtyear.Text & "' AND leave_tp='" & vb.Left(cmbleavetp.Text, 1) & "' AND staf_sl=" & lblsl.Text & "")
                        End If
                    End If
                End If
            Next
            close1()
        End If
        Me.clr()
        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim lbl As Label = GridView1.Rows(rw).FindControl("lblslno")
            start1()
            SQLInsert("DELETE FROM leave_assignment WHERE sl=" & lbl.Text & " AND loc_cd=" & loc_cd & "")
            close1()
        End If
        Me.dvdisp()
    End Sub


    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        cmbdivsion.Focus()
    End Sub

    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkhdr As CheckBox = dvstaf.HeaderRow.FindControl("chk1")
        If chkhdr.Checked = True Then
            For i As Integer = 0 To dvstaf.Rows.Count - 1
                Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
                chk.Checked = True
            Next
        ElseIf chkhdr.Checked = False Then
            For i As Integer = 0 To dvstaf.Rows.Count - 1
                Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
                chk.Checked = False
            Next
        End If
    End Sub

    Protected Sub cmbleavetp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbleavetp.SelectedIndexChanged
        Me.staf_disp()
    End Sub
End Class
