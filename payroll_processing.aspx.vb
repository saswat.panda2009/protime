﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class payroll_processing
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtfrom.Text = Format(Now, "dd/MM/yyyy")
        divmsg.Visible = False
        lblhdr.Text = "Processing Payroll"
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim sdt As Date = CDate(stringtodate(txtfrom.Text).Month & "/01/" & stringtodate(txtfrom.Text).Year)
        Dim edt As Date = CDate(stringtodate(txtfrom.Text).Month & "/" & DateTime.DaysInMonth(stringtodate(txtfrom.Text).Year, stringtodate(txtfrom.Text).Month) & "/" & stringtodate(txtfrom.Text).Year)
        Dim dsatnd As DataSet = get_dataset("select count(staf_sl),day_status,staf_sl FROM atnd WHERE log_dt>='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_dt<='" & Format(edt, "dd/MMM/yyyy") & "' GROUP BY staf_sl,day_status")
        Dim dsatnd1 As DataSet = get_dataset("select staf_sl,sum(tot_otmin) as ot FROM atnd WHERE log_dt>='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_dt<='" & Format(edt, "dd/MMM/yyyy") & "' GROUP BY staf_sl")
        Dim dsleave As DataSet = get_dataset("SELECT Count(staf_sl),leave_mst.leave_tp,staf_sl FROM atnd LEFT OUTER JOIN leave_mst ON atnd.leave_sl = leave_mst.leave_sl WHERE day_status='LEAVEDAY' GROUP BY staf_sl,leave_mst.leave_tp")
        Dim dsearn As DataSet = get_dataset("select sum(amt),tp,staf_sl from head_assignment2 WHERE ass_month=" & stringtodate(txtfrom.Text).Month & " AND ass_Year=" & stringtodate(txtfrom.Text).Year & " GROUP BY staf_sl,tp")
        Dim dsloan As DataSet = get_dataset("select sum(loan_amount),staf_sl from loan_planner2 WHERE loan_month=" & stringtodate(txtfrom.Text).Month & "  and loan_year=" & stringtodate(txtfrom.Text).Year & " Group BY staf_sl")
        Dim dsstaf As DataSet = get_dataset("SELECT payroll_group.*, staf.* FROM staf LEFT OUTER JOIN payroll_group ON staf.group_sl = payroll_group.group_sl WHERE emp_status='I' AND staf.group_sl <> 0 and staf.loc_cd=" & loc_cd & "")
        Dim dsexpense As DataSet = get_dataset("SELECT sum(Expense_Entry.amount),staf_sl FROM Expense_Entry WHERE status='A' AND paid='N' AND loc_cd=" & loc_cd & " AND month(expense_dt)=" & stringtodate(txtfrom.Text).Month & " AND year(expense_dt)=" & stringtodate(txtfrom.Text).Year & " GROUP BY staf_sl")
        Dim dspay As DataSet = get_dataset("select top 1 * FROM monthly_pay")
        dspay.Tables(0).Rows.Clear()
        start1()
        SQLInsert("DELETE FROM monthly_pay WHERE pay_month=" & stringtodate(txtfrom.Text).Month & " AND pay_year=" & stringtodate(txtfrom.Text).Year & " AND loc_cd=" & loc_cd & "")
        close1()
        Dim dt_atnd As DataTable = dspay.Tables(0)
        Dim dr_atnd As DataRow
        For i As Integer = 0 To dsstaf.Tables(0).Rows.Count - 1
            Dim staf_sl As Decimal = 0
            Dim pay_month As Decimal = 0
            Dim pay_year As Decimal = 0
            Dim gross As Decimal = 0
            Dim ctc As Decimal = 0
            Dim basic_amt As Decimal = 0
            Dim da_amt As Decimal = 0
            Dim hra_amt As Decimal = 0
            Dim washing_amt As Decimal = 0
            Dim transport_amt As Decimal = 0
            Dim child_amt As Decimal = 0
            Dim city_amt As Decimal = 0
            Dim spcl_amt As Decimal = 0
            Dim mobile_amt As Decimal = 0
            Dim food_amt As Decimal = 0
            Dim meical_amt As Decimal = 0
            Dim insurance_amt As Decimal = 0
            Dim pf_amt As Decimal = 0
            Dim comp_pf_amt As Decimal = 0
            Dim esic_amt As Decimal = 0
            Dim comp_esic_amt As Decimal = 0
            Dim pt_amt As Decimal = 0
            Dim tds_amt As Decimal = 0
            Dim loan_amt As Decimal = 0
            Dim othr_earnings As Decimal = 0
            Dim othr_deuction As Decimal = 0
            Dim tot_earnings As Decimal = 0
            Dim tot_deduction As Decimal = 0
            Dim fuel_amt As Decimal = 0
            Dim rate_per_km As Decimal = 0
            Dim wages As Decimal = 0
            Dim tot_reimbursment As Decimal = 0
            Dim total As Decimal = 0
            Dim pay_days As Decimal = 0
            Dim tot_days As Decimal = 0
            Dim tot_present As Decimal = 0
            Dim tot_half As Decimal = 0
            Dim tot_week As Decimal = 0
            Dim tot_absnt As Decimal = 0
            Dim tot_leave As Decimal = 0
            Dim tot_holi As Decimal = 0
            Dim l1 As Decimal = 0
            Dim l2 As Decimal = 0
            Dim l3 As Decimal = 0
            Dim l4 As Decimal = 0
            Dim l5 As Decimal = 0
            Dim l6 As Decimal = 0
            Dim ot_hr As Integer = 0
            Dim ot_amt As Decimal = 0

            staf_sl = dsstaf.Tables(0).Rows(i).Item("staf_sl")
            rate_per_km = dsstaf.Tables(0).Rows(i).Item("rate_per_km")
            tot_days = DateTime.DaysInMonth(stringtodate(txtfrom.Text).Year, stringtodate(txtfrom.Text).Month)
            ctc = dsstaf.Tables(0).Rows(i).Item("wages")
            wages = 0
            If dsstaf.Tables(0).Rows(i).Item("wages_type") = "2" Then
                wages = dsstaf.Tables(0).Rows(i).Item("wages")
            Else
                wages = dsstaf.Tables(0).Rows(i).Item("wages")
                wages = wages / tot_days
            End If

            Dim drp As DataRow()
            drp = dsatnd.Tables(0).Select("staf_sl=" & staf_sl & " AND day_status='PRESENT'")
            If drp.Length <> 0 Then
                tot_present = drp(0).Item(0)
            End If
            Dim drhf As DataRow()
            drhf = dsatnd.Tables(0).Select("staf_sl=" & staf_sl & " AND day_status='HALFDAY'")
            If drhf.Length <> 0 Then
                tot_half = drhf(0).Item(0)
            End If
            Dim drhweek As DataRow()
            drhweek = dsatnd.Tables(0).Select("staf_sl=" & staf_sl & " AND day_status='WEEKLYOFF'")
            If drhweek.Length <> 0 Then
                tot_week = drhweek(0).Item(0)
            End If
            Dim drhholiday As DataRow()
            drhholiday = dsatnd.Tables(0).Select("staf_sl=" & staf_sl & " AND day_status='HOLIDAY'")
            If drhholiday.Length <> 0 Then
                tot_holi = drhholiday(0).Item(0)
            End If
            Dim drabsent As DataRow()
            drabsent = dsatnd.Tables(0).Select("staf_sl=" & staf_sl & " AND day_status='ABSENT'")
            If drabsent.Length <> 0 Then
                tot_absnt = drabsent(0).Item(0)
            End If
            Dim drleave As DataRow()
            drleave = dsatnd.Tables(0).Select("staf_sl=" & staf_sl & " AND day_status='LEAVEDAY'")
            If drleave.Length <> 0 Then
                tot_leave = drleave(0).Item(0)
            End If
            Dim drl1 As DataRow()
            drl1 = dsleave.Tables(0).Select("staf_sl=" & staf_sl & " AND leave_tp=1")
            If drl1.Length <> 0 Then
                l1 = drl1(0).Item(0)
            End If
            Dim drl2 As DataRow()
            drl2 = dsleave.Tables(0).Select("staf_sl=" & staf_sl & " AND leave_tp=2")
            If drl2.Length <> 0 Then
                l2 = drl2(0).Item(0)
            End If
            Dim drl3 As DataRow()
            drl3 = dsleave.Tables(0).Select("staf_sl=" & staf_sl & " AND leave_tp=3")
            If drl3.Length <> 0 Then
                l3 = drl3(0).Item(0)
            End If
            Dim drl4 As DataRow()
            drl4 = dsleave.Tables(0).Select("staf_sl=" & staf_sl & " AND leave_tp=4")
            If drl4.Length <> 0 Then
                l4 = drl4(0).Item(0)
            End If
            Dim drl5 As DataRow()
            drl5 = dsleave.Tables(0).Select("staf_sl=" & staf_sl & " AND leave_tp=5")
            If drl5.Length <> 0 Then
                l5 = drl5(0).Item(0)
            End If
            Dim drl6 As DataRow()
            drl6 = dsleave.Tables(0).Select("staf_sl=" & staf_sl & " AND leave_tp=6")
            If drl6.Length <> 0 Then
                l6 = drl6(0).Item(0)
            End If

            If dsstaf.Tables(0).Rows(i).Item("wages_type") = "2" Then
                pay_days = tot_present + (tot_half * 0.5) + tot_holi + l2 + l4 + l5 + l6 + l1
            Else
                pay_days = tot_present + (tot_half * 0.5) + tot_holi + tot_week + l2 + l4 + l5 + l6 + l1
            End If
            'pay_days = 31

            gross = (wages * Val(dsstaf.Tables(0).Rows(i).Item("gross"))) / 100
            gross = Val(gross) * Val(pay_days)
            ctc = Val(wages) * Val(pay_days)

            If dsstaf.Tables(0).Rows(i).Item("fix_basic") = "Y" Then
                basic_amt = (Val(dsstaf.Tables(0).Rows(i).Item("basic_amt")) / tot_days) * pay_days
            Else
                If dsstaf.Tables(0).Rows(i).Item("basic_tp") = "GROSS" Then
                    basic_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("basic_amt"))) / 100
                End If
            End If
            If dsstaf.Tables(0).Rows(i).Item("fix_mobile") = "Y" Then
                mobile_amt = Val(dsstaf.Tables(0).Rows(i).Item("mobile_amt"))
            Else
                If dsstaf.Tables(0).Rows(i).Item("mobile_tp") = "CTC" Then
                    mobile_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("mobile_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("mobile_tp") = "GROSS" Then
                    mobile_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("mobile_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("mobile_tp") = "BASIC" Then
                    mobile_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("mobile_amt"))) / 100
                End If
            End If

            If Val(dsstaf.Tables(0).Rows(i).Item("da_amt")) <> 0 Then
                If dsstaf.Tables(0).Rows(i).Item("da_tp") = "CTC" Then
                    da_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("da_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("da_tp") = "GROSS" Then
                    da_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("da_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("da_tp") = "BASIC" Then
                    da_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("da_amt"))) / 100
                End If
            End If
            If Val(dsstaf.Tables(0).Rows(i).Item("food_amt")) <> 0 Then
                If dsstaf.Tables(0).Rows(i).Item("food_tp") = "CTC" Then
                    food_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("food_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("food_tp") = "GROSS" Then
                    food_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("food_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("food_tp") = "BASIC" Then
                    food_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("food_amt"))) / 100
                End If
            End If

            If Val(dsstaf.Tables(0).Rows(i).Item("medical_amt")) <> 0 Then
                If dsstaf.Tables(0).Rows(i).Item("medical_tp") = "CTC" Then
                    meical_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("medical_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("medical_tp") = "GROSS" Then
                    meical_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("medical_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("medical_tp") = "BASIC" Then
                    meical_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("medical_amt"))) / 100
                End If
            End If

            If Val(dsstaf.Tables(0).Rows(i).Item("hra_amt")) <> 0 Then
                If Val(dsstaf.Tables(0).Rows(i).Item("hra_app")) <> 0 Then
                    hra_amt = dsstaf.Tables(0).Rows(i).Item("hra_amt")
                Else
                    If dsstaf.Tables(0).Rows(i).Item("hra_tp") = "CTC" Then
                        hra_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("hra_amt"))) / 100
                    ElseIf dsstaf.Tables(0).Rows(i).Item("hra_tp") = "GROSS" Then
                        hra_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("hra_amt"))) / 100
                    ElseIf dsstaf.Tables(0).Rows(i).Item("hra_tp") = "BASIC" Then
                        hra_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("hra_amt"))) / 100
                    End If
                End If
            End If
            If Val(dsstaf.Tables(0).Rows(i).Item("transport_amt")) <> 0 Then
                If Val(dsstaf.Tables(0).Rows(i).Item("ta_app")) <> 0 Then
                    transport_amt = dsstaf.Tables(0).Rows(i).Item("transport_amt")
                Else
                    If dsstaf.Tables(0).Rows(i).Item("transport_tp") = "CTC" Then
                        transport_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("transport_amt"))) / 100
                    ElseIf dsstaf.Tables(0).Rows(i).Item("transport_tp") = "GROSS" Then
                        transport_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("transport_amt"))) / 100
                    ElseIf dsstaf.Tables(0).Rows(i).Item("transport_tp") = "BASIC" Then
                        transport_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("transport_amt"))) / 100
                    End If
                End If
            End If
            If Val(dsstaf.Tables(0).Rows(i).Item("child_amt")) <> 0 Then
                If dsstaf.Tables(0).Rows(i).Item("child_tp") = "CTC" Then
                    child_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("child_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("child_tp") = "GROSS" Then
                    child_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("child_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("child_tp") = "BASIC" Then
                    child_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("child_amt"))) / 100
                End If
            End If
            If Val(dsstaf.Tables(0).Rows(i).Item("city_amt")) <> 0 Then
                If dsstaf.Tables(0).Rows(i).Item("city_tp") = "CTC" Then
                    city_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("city_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("city_tp") = "GROSS" Then
                    city_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("city_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("city_tp") = "BASIC" Then
                    city_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("city_amt"))) / 100
                End If
            End If
            If Val(dsstaf.Tables(0).Rows(i).Item("washing_amt")) <> 0 Then
                If dsstaf.Tables(0).Rows(i).Item("washing_tp") = "CTC" Then
                    washing_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("washing_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("washing_tp") = "GROSS" Then
                    washing_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("washing_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("washing_tp") = "BASIC" Then
                    washing_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("washing_amt"))) / 100
                End If
            End If
            If Val(dsstaf.Tables(0).Rows(i).Item("spcl_amt")) <> 0 Then
                If dsstaf.Tables(0).Rows(i).Item("spcl_tp") = "CTC" Then
                    spcl_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("spcl_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("spcl_tp") = "GROSS" Then
                    spcl_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("spcl_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("spcl_tp") = "BASIC" Then
                    spcl_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("spcl_amt"))) / 100
                End If
            End If


            start1()
            SQLInsert("delete from fuel_allowance WHERE all_month=" & stringtodate(txtfrom.Text).Month & " AND all_year=" & stringtodate(txtfrom.Text).Year & " AND staf_sl=" & staf_sl & "")
            SQLInsert("INSERT INTO fuel_allowance(allow_dt,staf_sl,loc_cd,start_km,stop_km,tot_km,rate,allow_amount,all_month,all_year) SELECT distinct log_dt," & staf_sl & "," & loc_cd & ",(SELECT top 1 odometre_km FROM odometer_entry as oe WHERE oe.log_dt=odometer_entry.log_dt AND staf_sl=" & staf_sl & " and log_status='A' ORDER BY odometre_km),(SELECT top 1 odometre_km FROM odometer_entry as oe WHERE oe.log_dt=odometer_entry.log_dt AND staf_sl=" & staf_sl & " and log_status='A' ORDER BY odometre_km desc),0," & rate_per_km & ",0," & stringtodate(txtfrom.Text).Month & " ," & stringtodate(txtfrom.Text).Year & " FROM odometer_entry WHERE month(log_dt)=" & stringtodate(txtfrom.Text).Month & " AND Year(log_dt)=" & stringtodate(txtfrom.Text).Year & " AND staf_sl=" & staf_sl & "")
            SQLInsert("UPDATE fuel_allowance SET tot_km=stop_km-start_km,allow_amount=(stop_km-start_km) * rate")
            close1()

            Dim dsfuel As DataSet = get_dataset("SELECT isnull(sum(allow_amount),0) FROM fuel_allowance WHERE staf_Sl=" & staf_sl & " AND all_month=" & stringtodate(txtfrom.Text).Month & " AND all_year=" & stringtodate(txtfrom.Text).Year & " ")
            If dsfuel.Tables(0).Rows.Count <> 0 Then
                fuel_amt = dsfuel.Tables(0).Rows(0).Item(0)
            End If

            Dim drot As DataRow()
            drot = dsatnd1.Tables(0).Select("staf_sl=" & staf_sl & "")
            If drot.Length <> 0 Then
                ot_hr = drot(0).Item("ot")
                ot_hr = ot_hr / 60
                ot_amt = Math.Round((wages / 8) * ot_hr, 0)
            End If

            tot_earnings = basic_amt + da_amt + hra_amt + transport_amt + child_amt + city_amt + spcl_amt + ot_amt + washing_amt + mobile_amt + food_amt + meical_amt

            If Val(dsstaf.Tables(0).Rows(i).Item("comp_pf_amt")) <> 0 Then
                If dsstaf.Tables(0).Rows(i).Item("comp_pf_tp") = "CTC" Then
                    comp_pf_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("comp_pf_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("comp_pf_tp") = "GROSS" Then
                    comp_pf_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("comp_pf_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("comp_pf_tp") = "BASIC" Then
                    comp_pf_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("comp_pf_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("comp_pf_tp") = "EARNINGS" Then
                    comp_pf_amt = (tot_earnings * Val(dsstaf.Tables(0).Rows(i).Item("comp_pf_amt"))) / 100
                End If
            End If

            If Val(dsstaf.Tables(0).Rows(i).Item("comp_esi_amt")) <> 0 Then
                If dsstaf.Tables(0).Rows(i).Item("comp_esi_tp") = "CTC" Then
                    comp_esic_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("comp_esi_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("comp_esi_tp") = "GROSS" Then
                    comp_esic_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("comp_esi_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("comp_esi_tp") = "BASIC" Then
                    comp_esic_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("comp_esi_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("comp_esi_tp") = "EARNINGS" Then
                    comp_esic_amt = (tot_earnings * Val(dsstaf.Tables(0).Rows(i).Item("comp_esi_amt"))) / 100
                End If
            ElseIf dsstaf.Tables(0).Rows(i).Item("comp_esic_formula") = "Y" Then
                If (ctc - washing_amt) <= 21000 Then
                    comp_esic_amt = ((ctc - washing_amt - comp_pf_amt) / 104.75) * 3.25
                End If
            End If

            If dsstaf.Tables(0).Rows(i).Item("spl_formula") = "Y" Then
                spcl_amt = (ctc - (comp_esic_amt + comp_pf_amt)) - (basic_amt + hra_amt + washing_amt + transport_amt)
            End If
            If spcl_amt < 0 Then
                spcl_amt = 0
            End If
            tot_earnings = tot_earnings + spcl_amt

            'If dsstaf.Tables(0).Rows(i).Item("pf_app") = "Y" Then
            If Val(dsstaf.Tables(0).Rows(i).Item("emp_pf_amt")) <> 0 Then
                If dsstaf.Tables(0).Rows(i).Item("emp_pf_tp") = "CTC" Then
                    pf_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("emp_pf_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("emp_pf_tp") = "GROSS" Then
                    pf_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("emp_pf_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("emp_pf_tp") = "BASIC" Then
                    pf_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("emp_pf_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("emp_pf_tp") = "EARNINGS" Then
                    pf_amt = (tot_earnings * Val(dsstaf.Tables(0).Rows(i).Item("emp_pf_amt"))) / 100
                End If
            End If

            'End If

            ' If dsstaf.Tables(0).Rows(i).Item("esi_app") = "Y" Then
            If Val(dsstaf.Tables(0).Rows(i).Item("emp_esi_amt")) <> 0 Then
                If dsstaf.Tables(0).Rows(i).Item("emp_esi_tp") = "CTC" Then
                    esic_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("emp_esi_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("emp_esi_tp") = "GROSS" Then
                    esic_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("emp_esi_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("emp_esi_tp") = "BASIC" Then
                    esic_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("emp_esi_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("emp_esi_tp") = "EARNINGS" Then
                    esic_amt = ((tot_earnings - washing_amt) * Val(dsstaf.Tables(0).Rows(i).Item("emp_esi_amt"))) / 100
                End If
            End If

            If Val(dsstaf.Tables(0).Rows(i).Item("insurance_amt")) <> 0 Then
                If dsstaf.Tables(0).Rows(i).Item("insurance_tp") = "CTC" Then
                    insurance_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("insurance_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("insurance_tp") = "GROSS" Then
                    insurance_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("insurance_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("insurance_tp") = "BASIC" Then
                    insurance_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("insurance_amt"))) / 100
                ElseIf dsstaf.Tables(0).Rows(i).Item("insurance_tp") = "EARNINGS" Then
                    insurance_amt = ((tot_earnings - washing_amt) * Val(dsstaf.Tables(0).Rows(i).Item("emp_esi_amt"))) / 100
                End If
            End If

            If dsstaf.Tables(0).Rows(i).Item("pt_formula") = "Y" Then
                If ctc >= 24999 Then
                    pt_amt = 200
                ElseIf ctc >= 13333 Then
                    pt_amt = 125
                End If
            Else
                If Val(dsstaf.Tables(0).Rows(i).Item("pt_amt")) <> 0 Then
                    If dsstaf.Tables(0).Rows(i).Item("pt_tp") = "CTC" Then
                        pt_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("pt_amt"))) / 100
                    ElseIf dsstaf.Tables(0).Rows(i).Item("pt_tp") = "GROSS" Then
                        pt_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("pt_amt"))) / 100
                    ElseIf dsstaf.Tables(0).Rows(i).Item("pt_tp") = "BASIC" Then
                        pt_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("pt_amt"))) / 100
                    ElseIf dsstaf.Tables(0).Rows(i).Item("pt_tp") = "EARNINGS" Then
                        pt_amt = (tot_earnings * Val(dsstaf.Tables(0).Rows(i).Item("pt_amt"))) / 100
                    End If
                End If
            End If

            If dsstaf.Tables(0).Rows(i).Item("tds_formula") = "Y" Then

            Else
                If Val(dsstaf.Tables(0).Rows(i).Item("tds_amt")) <> 0 Then
                    If dsstaf.Tables(0).Rows(i).Item("tds_tp") = "CTC" Then
                        tds_amt = (ctc * Val(dsstaf.Tables(0).Rows(i).Item("tds_amt"))) / 100
                    ElseIf dsstaf.Tables(0).Rows(i).Item("tds_tp") = "GROSS" Then
                        tds_amt = (gross * Val(dsstaf.Tables(0).Rows(i).Item("tds_amt"))) / 100
                    ElseIf dsstaf.Tables(0).Rows(i).Item("tds_tp") = "BASIC" Then
                        tds_amt = (basic_amt * Val(dsstaf.Tables(0).Rows(i).Item("tds_amt"))) / 100
                    ElseIf dsstaf.Tables(0).Rows(i).Item("tds_tp") = "EARNINGS" Then
                        tds_amt = (tot_earnings * Val(dsstaf.Tables(0).Rows(i).Item("tds_amt"))) / 100
                    End If
                End If
            End If

            Dim drloan As DataRow()
            drloan = dsloan.Tables(0).Select("staf_sl=" & staf_sl & "")
            If drloan.Length <> 0 Then
                loan_amt = drloan(0).Item(0)
            End If

            Dim drearnings As DataRow()
            drearnings = dsearn.Tables(0).Select("staf_sl=" & staf_sl & " AND tp=1")
            If drearnings.Length <> 0 Then
                othr_earnings = drearnings(0).Item(0)
            End If
            Dim drdeduct As DataRow()
            drdeduct = dsearn.Tables(0).Select("staf_sl=" & staf_sl & " AND tp=2")
            If drdeduct.Length <> 0 Then
                othr_earnings = drdeduct(0).Item(0)
            End If

            Dim drexpense As DataRow()
            drexpense = dsexpense.Tables(0).Select("staf_sl=" & staf_sl & "")
            If drexpense.Length <> 0 Then
                tot_reimbursment = drexpense(0).Item(0)
            End If

            tot_earnings = tot_earnings + othr_earnings + fuel_amt
            tot_deduction = pf_amt + esic_amt + pt_amt + tds_amt + loan_amt + insurance_amt

            dr_atnd = dt_atnd.NewRow
            dr_atnd("staf_sl") = staf_sl
            dr_atnd("pay_month") = stringtodate(txtfrom.Text).Month
            dr_atnd("pay_year") = stringtodate(txtfrom.Text).Year
            dr_atnd("loc_cd") = loc_cd
            dr_atnd("gross") = Format(gross, "#0.00")
            dr_atnd("basic_amt") = Format(basic_amt, "#0.00")
            dr_atnd("da_amt") = Format(da_amt, "#0.00")
            dr_atnd("hra_amt") = Format(hra_amt, "#0.00")
            dr_atnd("transport_amt") = Format(transport_amt, "#0.00")
            dr_atnd("child_amt") = Format(child_amt, "#0.00")
            dr_atnd("city_amt") = Format(city_amt, "#0.00")
            dr_atnd("spcl_amt") = Format(spcl_amt, "#0.00")
            dr_atnd("pf_amt") = Format(pf_amt, "#0.00")
            dr_atnd("comp_pf_amt") = Format(comp_pf_amt, "#0.00")
            dr_atnd("esic_amt") = Format(esic_amt, "#0.00")
            dr_atnd("comp_esic_amt") = Format(comp_esic_amt, "#0.00")
            dr_atnd("pt_amt") = Format(pt_amt, "#0.00")
            dr_atnd("tds_amt") = Format(tds_amt, "#0.00")
            dr_atnd("loan_amt") = Format(loan_amt, "#0.00")
            dr_atnd("othr_earnings") = Format(othr_earnings, "#0.00")
            dr_atnd("othr_deuction") = Format(othr_deuction, "#0.00")
            dr_atnd("tot_earnings") = Format(tot_earnings, "#0.00")
            dr_atnd("tot_deduction") = Format(tot_deduction, "#0.00")
            dr_atnd("wages") = wages
            dr_atnd("tot_reimbursment") = "0"
            dr_atnd("total") = Format(tot_earnings - tot_deduction, "#0")
            dr_atnd("pay_days") = pay_days
            dr_atnd("tot_days") = tot_days
            dr_atnd("tot_present") = tot_present
            dr_atnd("tot_half") = tot_half
            dr_atnd("tot_week") = tot_week
            dr_atnd("tot_absnt") = tot_absnt
            dr_atnd("tot_leave") = tot_leave
            dr_atnd("tot_holi") = tot_holi
            dr_atnd("l1") = l1
            dr_atnd("l2") = l2
            dr_atnd("l3") = l3
            dr_atnd("l4") = l4
            dr_atnd("l5") = Val(l5)
            dr_atnd("l6") = Val(l6)
            dr_atnd("washing_amt") = Format(washing_amt, "#0.00")
            dr_atnd("ot") = Format(ot_hr, "#0.00")
            dr_atnd("ot_amt") = Format(ot_amt, "#0.00")
            dr_atnd("mobile_amt") = Format(mobile_amt, "#0.00")
            dr_atnd("food_amt") = Format(food_amt, "#0.00")
            dr_atnd("medical") = Format(meical_amt, "#0.00")
            dr_atnd("insurance_amt") = Format(insurance_amt, "#0.00")
            dr_atnd("fuel_amt") = Format(fuel_amt, "#0.00")
            dt_atnd.Rows.Add(dr_atnd)
        Next
        start1()
        For cnt As Integer = 0 To dt_atnd.Rows.Count - 1
            SQLInsert("INSERT INTO monthly_pay(staf_sl,pay_month,pay_year,loc_cd,gross,basic_amt,da_amt,hra_amt,transport_amt,child_amt,city_amt," & _
            "spcl_amt,pf_amt,comp_pf_amt,esic_amt,comp_esic_amt,pt_amt,tds_amt,loan_amt,othr_earnings,othr_deuction,tot_earnings,tot_deduction,wages," & _
            "tot_reimbursment,total,pay_days,tot_days,tot_present,tot_half,tot_week,tot_absnt,tot_leave,tot_holi,l1,l2,l3,l4,l5,l6,washing_amt,ot,ot_amt," & _
            "mobile_amt,food_amt,medical,insurance_amt,fuel_amt) VALUES(" & _
            dt_atnd.Rows(cnt).Item("staf_sl") & "," & dt_atnd.Rows(cnt).Item("pay_month") & "," & dt_atnd.Rows(cnt).Item("pay_year") & "," & _
            dt_atnd.Rows(cnt).Item("loc_cd") & "," & dt_atnd.Rows(cnt).Item("gross") & "," & dt_atnd.Rows(cnt).Item("basic_amt") & "," & _
            dt_atnd.Rows(cnt).Item("da_amt") & "," & dt_atnd.Rows(cnt).Item("hra_amt") & "," & dt_atnd.Rows(cnt).Item("transport_amt") & "," & _
            dt_atnd.Rows(cnt).Item("child_amt") & "," & dt_atnd.Rows(cnt).Item("city_amt") & "," & dt_atnd.Rows(cnt).Item("spcl_amt") & "," & _
            dt_atnd.Rows(cnt).Item("pf_amt") & "," & dt_atnd.Rows(cnt).Item("comp_pf_amt") & "," & dt_atnd.Rows(cnt).Item("esic_amt") & "," & _
            dt_atnd.Rows(cnt).Item("comp_esic_amt") & "," & dt_atnd.Rows(cnt).Item("pt_amt") & "," & dt_atnd.Rows(cnt).Item("tds_amt") & "," & _
            dt_atnd.Rows(cnt).Item("loan_amt") & "," & dt_atnd.Rows(cnt).Item("othr_earnings") & "," & dt_atnd.Rows(cnt).Item("othr_deuction") & _
            "," & dt_atnd.Rows(cnt).Item("tot_earnings") & "," & dt_atnd.Rows(cnt).Item("tot_deduction") & "," & dt_atnd.Rows(cnt).Item("wages") & _
            "," & dt_atnd.Rows(cnt).Item("tot_reimbursment") & "," & dt_atnd.Rows(cnt).Item("total") & "," & dt_atnd.Rows(cnt).Item("pay_days") & _
            "," & dt_atnd.Rows(cnt).Item("tot_days") & "," & dt_atnd.Rows(cnt).Item("tot_present") & "," & dt_atnd.Rows(cnt).Item("tot_half") & "," & _
            dt_atnd.Rows(cnt).Item("tot_week") & "," & dt_atnd.Rows(cnt).Item("tot_absnt") & "," & dt_atnd.Rows(cnt).Item("tot_leave") & "," & _
            dt_atnd.Rows(cnt).Item("tot_holi") & "," & dt_atnd.Rows(cnt).Item("l1") & "," & dt_atnd.Rows(cnt).Item("l2") & "," & dt_atnd.Rows(cnt).Item("l3") & _
            "," & dt_atnd.Rows(cnt).Item("l4") & "," & Val(dt_atnd.Rows(cnt).Item("l5")) & "," & Val(dt_atnd.Rows(cnt).Item("l6")) & "," & Val(dt_atnd.Rows(cnt).Item("washing_amt")) & _
            "," & Val(dt_atnd.Rows(cnt).Item("ot")) & "," & Val(dt_atnd.Rows(cnt).Item("ot_amt")) & "," & Val(dt_atnd.Rows(cnt).Item("mobile_amt")) & "," & _
            Val(dt_atnd.Rows(cnt).Item("food_amt")) & "," & Val(dt_atnd.Rows(cnt).Item("medical")) & "," & Val(dt_atnd.Rows(cnt).Item("insurance_amt")) & "," & Val(dt_atnd.Rows(cnt).Item("fuel_amt")) & ")")
        Next
        close1()
        divmsg.Visible = True
        divmsg.Attributes("class") = "alert bg-success"
        lblmsg.Text = "Payment Processed Successfully"
    End Sub
End Class
