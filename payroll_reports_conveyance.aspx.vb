﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient
Imports System.Drawing

Partial Class reports_absentee
    Inherits System.Web.UI.Page

    <System.Web.Script.Services.ScriptMethod(), _
System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            "staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblmsg.Visible = False
        txtfrmdt.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        txttodt.Text = Format(Now, "dd/MM/yyyy")
        
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As New DataSet
        If Trim(txtempcode.Text) = "" Then
            ds1 = get_dataset("SELECT ROW_NUMBER() OVER(ORDER BY MAX(staf.emp_code)) as Sl,     MAX(staf.emp_code) AS [Employee Code], MAX(staf.staf_nm) AS [Employee Name], Str(SUM(fuel_allowance.tot_km),12,3) AS Total, str(SUM(fuel_allowance.allow_amount),12,2) AS Amount FROM fuel_allowance LEFT OUTER JOIN staf ON fuel_allowance.staf_sl = staf.staf_sl WHERE (fuel_allowance.type = 'Calculation') AND allow_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND allow_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' and fuel_allowance.loc_cd= " & loc_cd & " GROUP BY fuel_allowance.staf_sl ORDER BY MAX(staf.emp_code)")
        Else
            ds1 = get_dataset("SELECT Convert(varchar,allow_dt,103) AS [Date], start_location AS 'From', end_location AS 'To', tot_km AS Total, str(rate,12,2) AS Rate, str(allow_amount,12,2) AS Amount FROM fuel_allowance LEFT OUTER JOIN staf ON fuel_allowance.staf_sl = staf.staf_sl WHERE (fuel_allowance.type = 'Calculation') AND allow_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND allow_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' and fuel_allowance.loc_cd= " & loc_cd & " AND emp_code='" & txtempcode.Text & "' ORDER BY sl")
        End If
        If ds1.Tables(0).Rows.Count <> 0 Then
            dvdata.DataSource = ds1.Tables(0)
            dvdata.DataBind()
        Else
            dvdata.DataSource = ds1.Tables(0)
            dvdata.DataBind()
            lblmsg.Text = "No Record Found."
        End If

    End Sub

    Protected Sub txtempcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtempcode.TextChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If Trim(txtempcode.Text) <> "" Then
            Dim emp_code As String() = Trim(txtempcode.Text).Split("-")
            txtempcode.Text = emp_code(0)
            Dim ds1 As DataSet = get_dataset("SELECT staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm, location_mst.loc_nm, staf.loc_cd FROM location_mst RIGHT OUTER JOIN staf ON location_mst.loc_cd = staf.loc_cd LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            Else
                txtempcode.Text = ""
            End If
        End If
    End Sub

    Protected Sub cmdsearch0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch0.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As New DataSet
        If Trim(txtempcode.Text) = "" Then
            ds1 = get_dataset("SELECT ROW_NUMBER() OVER(ORDER BY MAX(staf.emp_code)) as Sl,     MAX(staf.emp_code) AS [Employee Code], MAX(staf.staf_nm) AS [Employee Name], Str(SUM(fuel_allowance.tot_km),12,3) AS Total, str(SUM(fuel_allowance.allow_amount),12,2) AS Amount FROM fuel_allowance LEFT OUTER JOIN staf ON fuel_allowance.staf_sl = staf.staf_sl WHERE (fuel_allowance.type = 'Calculation') AND allow_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND allow_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' and fuel_allowance.loc_cd= " & loc_cd & " GROUP BY fuel_allowance.staf_sl ORDER BY MAX(staf.emp_code)")
        Else
            ds1 = get_dataset("SELECT Convert(varchar,allow_dt,103) AS [Date], start_location AS 'From', end_location AS 'To', tot_km AS Total, str(rate,12,2) AS Rate, str(allow_amount,12,2) AS Amount FROM fuel_allowance LEFT OUTER JOIN staf ON fuel_allowance.staf_sl = staf.staf_sl WHERE (fuel_allowance.type = 'Calculation') AND allow_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND allow_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' and fuel_allowance.loc_cd= " & loc_cd & " AND emp_code='" & txtempcode.Text & "' ORDER BY sl")
        End If
        Dim dtcust As New DataTable
        dtcust = ds1.Tables(0)



        'Create a dummy GridView

        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.DataSource = dtcust
        GridView1.DataBind()

        Dim comp_nm As String = ""
        Dim ds As DataSet = get_dataset("SELECT comp_nm FROM company")
        If ds.Tables(0).Rows.Count <> 0 Then
            comp_nm = ds.Tables(0).Rows(0).Item("comp_nm")
        End If

        Dim row As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal)
        Dim cell As New TableHeaderCell()
        cell.Text = comp_nm & "<br/><br/>"
        Dim cnt As Integer = ds1.Tables(0).Columns.Count
        cell.ColumnSpan = cnt
        row.Controls.Add(cell)
        row.Font.Bold = True
        row.Font.Size = 16
        'row.BackColor = ColorTranslator.FromHtml("#3AC0F2")
        row.HorizontalAlign = HorizontalAlign.Center
        GridView1.HeaderRow.Parent.Controls.AddAt(0, row)

        Dim row1 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal)
        Dim cell1 As New TableHeaderCell()
        cell1.Text = "Conveyance Register For Date : " & txtfrmdt.Text & " To Date : " & txttodt.Text
        cell1.ColumnSpan = cnt
        row1.Controls.Add(cell1)
        row1.Font.Bold = True
        row1.Font.Size = 16
        'row.BackColor = ColorTranslator.FromHtml("#3AC0F2")
        row1.HorizontalAlign = HorizontalAlign.Center
        GridView1.HeaderRow.Parent.Controls.AddAt(1, row1)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=ConveyanceRegister.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Using sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)

            GridView1.HeaderRow.BackColor = Color.White
            For Each cell2 As TableCell In GridView1.HeaderRow.Cells
                cell2.BackColor = ColorTranslator.FromHtml("#3AC0F2")
            Next
            GridView1.RenderControl(hw)
            'style to format numbers to string
            Dim style As String = "<style> .textmode { } </style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.[End]()
        End Using
    End Sub
End Class
