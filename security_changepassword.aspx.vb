﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class security_changepassword
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtconfirmpswd.Text = ""
        txtnewpswd.Text = ""
        txtoldpassword.Text = ""
        txtusrsl.Text = CType(Session("usr_sl"), Integer)
        Dim ds As DataSet = get_dataset("SELECT usr_pwd FROM usr_login WHERE usr_sl=" & Val(txtusrsl.Text) & "")
        If ds.Tables(0).Rows.Count <> 0 Then
            txtpassword.Text = ds.Tables(0).Rows(0).Item("usr_pwd")
        End If
        txtoldpassword.Focus()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim comp_sl As Integer = CType(Session("comp_sl"), Integer)
        If Trim(txtpassword.Text) <> Trim(txtoldpassword.Text) Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide A Valid Password');", True)
            txtoldpassword.Focus()
            Exit Sub
        End If
        If Trim(txtconfirmpswd.Text) <> Trim(txtnewpswd.Text) Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('New Password And Confirm Password Does Not Match');", True)
            txtnewpswd.Focus()
            Exit Sub
        End If
        start1()
        SQLInsert("UPDATE usr_login SET usr_pwd='" & Trim(txtnewpswd.Text) & "' WHERE usr_sl=" & Val(txtusrsl.Text) & " AND comp_sl=" & comp_sl & "")
        close1()
        Me.clr()
        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Changing Password Completed Successfully');", True)
    End Sub
End Class
