﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Copy of transcation_verifymanualapproval.aspx.vb" Inherits="transcation_verifymanualapproval" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Manual Attendance Approval . . ."></asp:Label></h3>
                                  </div>

                     <div class="card-body">
                                            <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table style="width:100%;">
                                                                                         
                                              <tr>
                                                  <td>
                                                      <table style="width:100%;">
                                                             <tr>
                                                              <td width="20%" valign="top">
                                                                  <table style="width:100%;">
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              <asp:Image ID="Image1" runat="server" Height="150px" Width="150px" 
                                                                                  />
                                                                          </td>
                                                                      </tr>
                                                                     
                                                                  </table>
                                                                 </td>
                                                              
                                                              <td width="80%" valign="top">
                                                                  <table style="width:100%;">
                                                                      <tr>
                                                                          <td>
                                                                              <asp:Label ID="lbladdress" runat="server" Font-Size="15px" Text="Label"></asp:Label>
                                                                          </td>
                                                                          <td>
                                                                              &nbsp;</td>
                                                                          <td>
                                                                              &nbsp;</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              <asp:TextBox ID="txtid" runat="server" Visible="False"></asp:TextBox>
                                                                              <asp:TextBox ID="txtstafsl" runat="server" Visible="False"></asp:TextBox>
                                                                              <asp:TextBox ID="txttime" runat="server" Visible="False"></asp:TextBox>
                                                                              <asp:TextBox ID="txtdevicecode" runat="server" Visible="False"></asp:TextBox>
                                                                              <asp:TextBox ID="txtdt" runat="server" Visible="False"></asp:TextBox>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              &nbsp;<asp:HyperLink ID="HyperLink1" runat="server" 
                                                                                  NavigateUrl="https://www.google.com/maps?q=20.3107398,85.8168556" 
                                                                                  Target="_blank">Click To View Location In Google Map</asp:HyperLink>
                                                                              &nbsp;</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              &nbsp;</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              <asp:Button ID="cmdsave" runat="server" class="btn btn-success" Text="Approve" 
                                                                                  Visible="False" />
                                                                              <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                                                  class="btn btn-warning" Text="Cancel" Visible="False" />
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              &nbsp;</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              &nbsp;</td>
                                                                      </tr>
                                                                  </table>

                                                                                                                                      </td>
                                                          </tr>
                                                                                                               </table>
                                               <table style="width:100%;">
                                                             <tr>
                                                              <td>
                                                                   <table style="width:100%;">
                                                                       <tr>
                                                                           <td width="20%">
                                                                               &nbsp;</td>
                                                                           <td width="20%">
                                                                               &nbsp;</td>
                                                                           <td width="30%">
                                                                               &nbsp;</td>
                                                                           <td width="20%">
                                                                               &nbsp;</td>
                                                                           <td width="10%">
                                                                               &nbsp;</td>
                                                                       </tr>
                                                                       <tr>
                                                                           <td>
                                                                               From Date</td>
                                                                           <td>
                                                                               To Date</td>
                                                                           <td>
                                                                               Employee Code</td>
                                                                           <td>
                                                                               Status</td>
                                                                           <td>
                                                                               &nbsp;</td>
                                                                       </tr>
                                                                       <tr>
                                                                           <td>
                                                                               <asp:TextBox ID="txtfrmdt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                               <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" 
                                                                                   PopupButtonID="txtfrmdt" TargetControlID="txtfrmdt">
                                                                               </asp:CalendarExtender>
                                                                               <asp:FilteredTextBoxExtender ID="TextBox1_FilteredTextBoxExtender" 
                                                                                   runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                                   TargetControlID="txtfrmdt">
                                                                               </asp:FilteredTextBoxExtender>
                                                                           </td>
                                                                           <td>
                                                                               <asp:TextBox ID="txttodt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                               <asp:CalendarExtender ID="txttodt_CalendarExtender" runat="server" 
                                                                                   Format="dd/MM/yyyy" PopupButtonID="txttodt" TargetControlID="txttodt">
                                                                               </asp:CalendarExtender>
                                                                               <asp:FilteredTextBoxExtender ID="txttodt_FilteredTextBoxExtender" 
                                                                                   runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                                   TargetControlID="txttodt">
                                                                               </asp:FilteredTextBoxExtender>
                                                                           </td>
                                                                           <td>
                                                                               <asp:TextBox ID="txtempcode" runat="server" AutoPostBack="True" 
                                                                                   class="form-control" MaxLength="100" placeholder="Emp. Code"></asp:TextBox>
                                                                               <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" 
                                                                                   CompletionInterval="100" CompletionListCssClass="wordWheel listMain .box" 
                                                                                   CompletionListHighlightedItemCssClass="wordWheel itemsSelected" 
                                                                                   CompletionListItemCssClass="wordWheel itemsMain" CompletionSetCount="10" 
                                                                                   EnableCaching="false" FirstRowSelected="false" MinimumPrefixLength="1" 
                                                                                   ServiceMethod="SearchEmei" TargetControlID="txtempcode">
                                                                               </asp:AutoCompleteExtender>
                                                                               <asp:FilteredTextBoxExtender ID="txtempcode_FilteredTextBoxExtender" 
                                                                                   runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                                   TargetControlID="txtempcode">
                                                                               </asp:FilteredTextBoxExtender>
                                                                           </td>
                                                                           <td>
                                                                               <asp:DropDownList ID="cmbtp" runat="server" AutoPostBack="True" 
                                                                                   class="form-control" Height="42px">
                                                                                   <asp:ListItem>Select</asp:ListItem>
                                                                                   <asp:ListItem>Approved</asp:ListItem>
                                                                                   <asp:ListItem>Canceled</asp:ListItem>
                                                                               </asp:DropDownList>
                                                                           </td>
                                                                           <td>
                                                                               <asp:Button ID="cmdsave1" runat="server" class="btn btn-success" 
                                                                                   Text="Generate" />
                                                                           </td>
                                                                       </tr>
                                                                       <tr>
                                                                           <td>
                                                                               &nbsp;</td>
                                                                           <td>
                                                                               &nbsp;</td>
                                                                           <td>
                                                                               &nbsp;</td>
                                                                           <td>
                                                                               &nbsp;</td>
                                                                           <td>
                                                                               &nbsp;</td>
                                                                       </tr>
                                                                   </table>
                                                                   <td>
                                                                   </td>
                                                                 </td>
                                                                 <tr>
                                                                     <td>
                                                                         <div style="width: 100%; height: 300px; overflow: auto; font-size: 15px;">
                                                                             <asp:GridView ID="dvstaf" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                                                 AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                                                 PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                                                 <AlternatingRowStyle CssClass="alt" />
                                                                                 <Columns>
                                                                                     <asp:TemplateField HeaderText="Date">
                                                                                         <ItemTemplate>
                                                                                             <asp:Label ID="lbldt" runat="server" Text='<%#Eval("dt") %>'></asp:Label>
                                                                                         </ItemTemplate>
                                                                                     </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Divission">
                                                                                         <ItemTemplate>
                                                                                             <asp:Label ID="lbldivision" runat="server" Text='<%#Eval("div_nm") %>'></asp:Label>
                                                                                         </ItemTemplate>
                                                                                     </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Employee Name">
                                                                                         <ItemTemplate>
                                                                                             <asp:Label ID="lblstaf_nm" runat="server" Text='<%#Eval("staf_nm") %>'></asp:Label>
                                                                                         </ItemTemplate>
                                                                                     </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Department">
                                                                                         <ItemTemplate>
                                                                                             <asp:Label ID="lbldept" runat="server" Text='<%#Eval("dept_nm") %>'></asp:Label>
                                                                                         </ItemTemplate>
                                                                                     </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Designation">
                                                                                         <ItemTemplate>
                                                                                             <asp:Label ID="lbldesg" runat="server" Text='<%#Eval("desg_nm") %>'></asp:Label>
                                                                                         </ItemTemplate>
                                                                                     </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Time">
                                                                                         <ItemTemplate>
                                                                                             <asp:Label ID="lbltm" runat="server" Text='<%#Eval("tm") %>'></asp:Label>
                                                                                         </ItemTemplate>
                                                                                     </asp:TemplateField>
                                                                                     <asp:TemplateField Visible="False">
                                                                                         <ItemTemplate>
                                                                                             <asp:Label ID="lblstaf_sl" runat="server" Text='<%#Eval("staf_sl") %>'></asp:Label>
                                                                                             <asp:Label ID="lblid" runat="server" Text='<%#Eval("id") %>'></asp:Label>
                                                                                             <asp:Label ID="lbldevicecode" runat="server" Text='<%#Eval("device_code") %>'></asp:Label>
                                                                                              <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("log_status") %>'></asp:Label>
                                                                                         </ItemTemplate>
                                                                                     </asp:TemplateField>
                                                                                     <asp:ButtonField ButtonType="Image" CommandName="view" 
                                                                                         ImageUrl="images/view.png" Text="Button">
                                                                                     <HeaderStyle Width="30px" />
                                                                                     <ItemStyle Width="30px" />
                                                                                     </asp:ButtonField>
                                                                                 </Columns>
                                                                                 <PagerStyle HorizontalAlign="Right" />
                                                                             </asp:GridView>
                                                                         </div>
                                                                     </td>
                                                                 </tr>
                                                   </tr>
                                                                                                               </table>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>           
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

