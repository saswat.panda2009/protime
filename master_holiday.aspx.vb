﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class master_holiday
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub


    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtstate.Text = ""
        txtstatecd.Text = ""
        txtfrom.Value = Format(Now, "dd/MM/yyyy")
        txtto.Value = Format(Now, "dd/MM/yyyy")
        cmbactive.SelectedIndex = 0
        Me.divisiondisp()
        If txtmode.Text = "E" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Holiday Master (Entry Mode)"
            txtstate.Focus()
        ElseIf txtmode.Text = "M" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Holiday Master (Edit Mode)"
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Holiday Master (View Mode)"
            Me.dvdisp()
        End If
    End Sub


    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm,div_sl FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        cmbdivsion.DataSource = ds.Tables(0)
        cmbdivsion.DataTextField = "div_nm"
        cmbdivsion.DataValueField = "div_sl"
        cmbdivsion.DataBind()
        cmbdivsion.Items.Insert(0, New ListItem("All Division", "0"))
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT row_number() over(ORDER BY holi_nm) as 'sl',(CASE WHEN div_sl=0 THEN 'All' WHEN div_sl<>0 THEN (SELECT div_nm FROM division_mst WHERE loc_cd=" & loc_cd & " AND div_sl=holiday1.div_sl) END) as div,holi_nm,convert(varchar,holi_from,103) as 'from',convert(varchar,holi_to,103) as 'to',(case when active='Y' Then 'Yes' WHEN active='N' Then 'No' END)as 'active' FROM holiday1 WHERE loc_cd=" & loc_cd & " ORDER BY holi_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If txtmode.Text = "E" Then
            Dim dscheck As DataSet = get_dataset("SELECT holi_nm FROM holiday1 WHERE holi_nm='" & UCase(Trim(txtstate.Text)) & "' AND loc_cd=" & loc_cd & "")
            If dscheck.Tables(0).Rows.Count <> 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Holiday Name Already Exits');", True)
                txtstate.Focus()
                Exit Sub
            End If
            txtstatecd.Text = "1"
            Dim ds1 As DataSet = get_dataset("SELECT max(holi_sl) FROM holiday1 ")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                txtstatecd.Text = ds1.Tables(0).Rows(0).Item(0) + 1
            End If
            start1()
            SQLInsert("INSERT INTO holiday1(holi_sl,loc_cd,holi_nm,holi_from,holi_to,active,div_sl) VALUES(" & Val(txtstatecd.Text) & _
            "," & loc_cd & ",'" & UCase(Trim(txtstate.Text)) & "','" & Format(stringtodate(txtfrom.Value), "dd/MMM/yyyy") & "','" & _
            Format(stringtodate(txtto.Value), "dd/MMM/yyyy") & "','" & vb.Left(cmbactive.Text, 1) & "'," & Val(cmbdivsion.SelectedValue) & ")")
            Dim sdt As Date = stringtodate(txtfrom.Value)
            Dim endt As Date = stringtodate(txtto.Value)
            While sdt <= endt
                SQLInsert("INSERT INTO holiday2(holi_sl,holi_dt,div_sl) VALUES(" & Val(txtstatecd.Text) & _
                ",'" & Format(sdt, "dd/MMM/yyyy") & "'," & Val(cmbdivsion.SelectedValue) & ")")
                sdt = sdt.AddDays(1)
            End While
            close1()
            Me.clr()
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
        ElseIf txtmode.Text = "M" Then
            Dim ds As DataSet = get_dataset("SELECT holi_nm FROM holiday1 WHERE holi_sl=" & Val(txtstatecd.Text) & "")
            If ds.Tables(0).Rows.Count <> 0 Then
                If Trim(txtstate.Text) <> ds.Tables(0).Rows(0).Item("holi_nm") Then
                    Dim dscheck As DataSet = get_dataset("SELECT holi_nm FROM holiday1 WHERE holi_nm='" & UCase(Trim(txtstate.Text)) & "' AND loc_cd=" & loc_cd & "")
                    If dscheck.Tables(0).Rows.Count <> 0 Then
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Holiday Name Already Exits');", True)
                        txtstate.Focus()
                        Exit Sub
                    End If
                End If
                start1()
                SQLInsert("UPDATE holiday1 SET holi_nm='" & UCase(Trim(txtstate.Text)) & "',holi_from='" & Format(stringtodate(txtfrom.Value), "dd/MMM/yyyy") & _
                "',holi_to='" & Format(stringtodate(txtto.Value), "dd/MMM/yyyy") & "',active='" & _
                vb.Left(cmbactive.Text, 1) & "',div_sl=" & Val(cmbdivsion.SelectedValue) & " WHERE holi_sl=" & Val(txtstatecd.Text) & "")
                SQLInsert("DELETE FROM holiday2 WHERE holi_sl=" & Val(txtstatecd.Text) & "")
                Dim sdt As Date = stringtodate(txtfrom.Value)
                Dim endt As Date = stringtodate(txtto.Value)
                While sdt <= endt
                    SQLInsert("INSERT INTO holiday2(holi_sl,holi_dt,div_sl) VALUES(" & Val(txtstatecd.Text) & _
                    ",'" & Format(sdt, "dd/MMM/yyyy") & "'," & Val(cmbdivsion.SelectedValue) & ")")
                    sdt = sdt.AddDays(1)
                End While
                close1()
                Me.clr()
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Modified Succesffuly');", True)
            End If
        End If
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT holi_sl FROM holiday1 WHERE holi_nm='" & Trim(GridView1.Rows(rw).Cells(1).Text) & "' AND loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtmode.Text = "M"
                Me.clr()
                txtstatecd.Text = Val(ds1.Tables(0).Rows(0).Item("holi_sl"))
                Me.dvsel()
            End If
        End If
    End Sub

    Private Sub dvsel()
        Dim ds1 As DataSet = get_dataset("SELECT * FROM holiday1 WHERE holi_sl=" & Val(txtstatecd.Text) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtstate.Text = ds1.Tables(0).Rows(0).Item("holi_nm")
            txtfrom.Value = Format(ds1.Tables(0).Rows(0).Item("holi_from"), "dd/MM/yyyy")
            txtto.Value = Format(ds1.Tables(0).Rows(0).Item("holi_to"), "dd/MM/yyyy")
            cmbdivsion.SelectedValue = ds1.Tables(0).Rows(0).Item("div_sl")
            If ds1.Tables(0).Rows(0).Item("active") = "Y" Then
                cmbactive.SelectedIndex = 0
            Else
                cmbactive.SelectedIndex = 1
            End If
        End If
    End Sub

    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        txtstate.Focus()
    End Sub
End Class
