﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.IO
Partial Class transcation_usbdownload
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        divmsg.Visible = False
        lblmsg.Text = ""
        txtfrom.Text = Format(Now, "dd/MM/yyyy")
        txtto.Text = Format(Now, "dd/MM/yyyy")
        dv1.DataSource = Nothing
        dv2.DataSource = Nothing
    End Sub

    Protected Sub cmdget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdget.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim fl_nm As String = Format(loc_cd, "0000") & Format(Now, "ddMMyyyy")
        Dim str1 As String
        str1 = FileUpload1.FileName.ToString
        If File.Exists(Server.MapPath(".\Upload\") & fl_nm & FileUpload1.FileName) Then
            File.Delete(Server.MapPath(".\Upload\") & fl_nm & FileUpload1.FileName)
        End If
        If str1 <> "" Then
            FileUpload1.SaveAs(Server.MapPath(".\Upload\") & fl_nm & FileUpload1.FileName)
        End If
        Dim dt As New DataTable
        dt.Columns.Add("FileName", GetType(String))
        Dim dr As DataRow
        Dim i As Integer = 0
        Dim filepaths() As String = Directory.GetFiles(Server.MapPath(".\Upload\"), "*.dat*")
        Dim files As List(Of ListItem) = New List(Of ListItem)
        For Each filepath As String In filepaths
            If InStr(Path.GetFileName(filepath), fl_nm) Then
                files.Add(New ListItem(Path.GetFileName(filepath), filepath))
                dr = dt.NewRow
                dr(0) = files.Item(0).Text
                dt.Rows.Add(dr)
            End If
            i += 1
        Next
        dv1.Datasource = dt
        dv1.DataBind()
        cmdsave.visible = True
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        lblmsg.text = ""
        divmsg.visible = True
        Me.dvsave()
    End Sub

    Private Sub dvsave()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim sdt_chk, edt_chk As Date
        sdt_chk = stringtodate(txtfrom.Text)
        edt_chk = stringtodate(txtfrom.Text)

        Dim udisk As New Structs.UDisk
        Dim byDtataBuf() As Byte = Nothing
        Dim ilength As Integer

        Dim sPin2 As String = ""
        Dim sVerified As String = ""
        Dim sTime_second As String = ""
        Dim sDeviceID As String = ""
        Dim sStatus As String = ""
        Dim sWorkcode As String = ""
        Dim logfile As String = ""

        Dim dr As DataRow

        start1()
        SQLInsert("IF EXISTS (SELECT * FROM sysobjects WHERE name = 'elog2' AND type='U') BEGIN DROP TABLE elog2 END")
        SQLInsert("CREATE TABLE elog2(sl_no BIGINT,Device_CODE INT,log_dt DATETIME,log_time DATETIME,log_tp VARCHAR(1),slno BIGINT,read_mark VARCHAR(1),loc_cd int,device_no int,staf_sl INT)")
        close1()

        Dim slno_cnt As Integer = 1
        Dim dsmaxslno As DataSet = get_dataset("SELECT isnull(max(slno),1) FROM elog")
        If dsmaxslno.Tables(0).Rows(0).Item(0) > 1 Then
            slno_cnt = dsmaxslno.Tables(0).Rows(0).Item(0) + 1
        End If
        Dim dselog As DataSet
        If chk1.Checked = True Then
            dselog = get_dataset("SELECT * FROM elog WHERE log_dt>='" & Format(sdt_chk, "dd/MMM/yyyy") & "' AND log_dt<='" & Format(edt_chk, "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & " ORDER BY log_dt")
        Else
            dselog = get_dataset("SELECT * FROM elog WHERE loc_cd=" & loc_cd & " ORDER BY log_dt")
        End If
        Dim dt As New DataTable
        dt.Columns.Add("sl_no", GetType(Integer))
        dt.Columns.Add("Device_code", GetType(Integer))
        dt.Columns.Add("log_dt", GetType(Date))
        dt.Columns.Add("log_time", GetType(Date))
        dt.Columns.Add("log_tp", GetType(String))
        dt.Columns.Add("slno", GetType(Integer))
        dt.Columns.Add("read_mark", GetType(String))
        dt.Columns.Add("loc_cd", GetType(Integer))
        dt.Columns.Add("device_no", GetType(Integer))
        dt.Columns.Add("staf_sl", GetType(Integer))
        Dim rw As Integer = 1
        Dim chkselected1 As CheckBox
        For k As Integer = 0 To dv1.Rows.Count - 1
            chkselected1 = dv1.Rows(k).FindControl("chk")
            If chkselected1.Checked = True Then
                logfile = Server.MapPath(".\Upload\") & dv1.Rows(k).Cells(1).Text
                Dim stream As FileStream
                stream = New FileStream(logfile, FileMode.OpenOrCreate, FileAccess.Read)
                byDtataBuf = File.ReadAllBytes(logfile)
                ilength = Convert.ToInt32(stream.Length)
                Dim iStartIndex As Integer = 0
                Dim iOneLogLength As Integer
                For i As Integer = iStartIndex To ilength - 2
                    Dim y As Integer = 0
                    Dim m As Integer = 0
                    Dim d As Integer = 0
                    Dim h As Integer = 0
                    Dim mnt As Integer = 0
                    Dim s As Integer = 0
                    Dim tm As String = ""
                    If byDtataBuf(i) = 13 And byDtataBuf(i + 1) = 10 Then
                        iOneLogLength = (i + 1) + 1 - iStartIndex
                        Dim bySSRATTLOG(iOneLogLength - 1) As Byte
                        Array.Copy(byDtataBuf, iStartIndex, bySSRATTLOG, 0, iOneLogLength)

                        udisk.GetAttLogFromDat(bySSRATTLOG, iOneLogLength, sPin2, sTime_second, sDeviceID, sStatus, sVerified, sWorkcode)
                        y = Val(vb.Left(Trim(sTime_second), 4))
                        m = Val(vb.Mid(Trim(sTime_second), 6, 2))
                        d = Val(vb.Mid(Trim(sTime_second), 9, 2))
                        tm = vb.Right(Trim(sTime_second), 8)
                        h = Val(vb.Left(Trim(tm), 2))
                        mnt = Val(vb.Mid(Trim(tm), 4, 2))
                        s = Val(vb.Mid(Trim(tm), 7, 2))

                        If chk1.Checked = True Then
                            Dim lgdt As String = Format(m, "00") & "/" & Format(d, "00") & "/" & Format(y, "0000")
                            Dim lgtm As String = Format(h, "00") & ":" & Format(mnt, "00") & ":" & Format(s, "00")
                            Dim edivid As Long = 0
                            Dim devno As Integer = 0
                            Dim logdt As Date
                            Dim logtm As Date
                            edivid = Val(sPin2)
                            logdt = Format(CDate(lgdt), "dd/MMM/yyyy")
                            logtm = Format(CDate(#1/1/1900# & " " & lgtm), "dd/MMM/yyyy HH:mm")
                            devno = Val(sDeviceID)
                            Dim staf_sl As Integer = 0
                            Dim dsstafsl As DataSet = get_dataset("Select staf_sl FROM staf WHERE device_code=" & edivid & " AND loc_cd=" & loc_cd & "")
                            If dsstafsl.Tables(0).Rows.Count <> 0 Then
                                staf_sl = dsstafsl.Tables(0).Rows(0).Item(0)
                            End If
                            Dim dr1() As DataRow
                            dr1 = dselog.Tables(0).Select("device_code=" & edivid & " AND loc_cd=" & loc_cd & " and log_dt='" & Format(logdt, "dd/MMM/yyyy") & "' and log_time='" & logtm & "'")
                            If dr1.Length = 0 Then
                                dr = dt.NewRow
                                dr(0) = slno_cnt
                                dr(1) = edivid
                                dr(2) = logdt
                                dr(3) = logtm
                                dr(4) = "D"
                                dr(5) = 0
                                dr(6) = "N"
                                dr(7) = loc_cd
                                dr(8) = devno
                                dr(9) = staf_sl
                                dt.Rows.Add(dr)
                                slno_cnt = slno_cnt + 1
                            End If
                        Else
                            Dim sdt As String = Format(m, "00") & "/" & Format(d, "00") & "/" & Format(y, "0000")
                            Dim ldt As Date = CDate(sdt)
                            If ldt >= sdt_chk And ldt <= edt_chk Then
                                Dim lgdt As String = Format(m, "00") & "/" & Format(d, "00") & "/" & Format(y, "0000")
                                Dim lgtm As String = Format(h, "00") & ":" & Format(mnt, "00") & ":" & Format(s, "00")
                                Dim edivid As Long = 0
                                Dim devno As Integer = 0
                                Dim logdt As Date
                                Dim logtm As Date
                                edivid = Val(sPin2)
                                logdt = Format(CDate(lgdt), "dd/MMM/yyyy")
                                logtm = Format(CDate(#1/1/1900# & " " & lgtm), "dd/MMM/yyyy HH:mm")
                                devno = Val(sDeviceID)
                                Dim staf_sl As Integer = 0
                                Dim dsstafsl As DataSet = get_dataset("Select staf_sl FROM staf WHERE device_code=" & edivid & " AND loc_cd=" & loc_cd & " ")
                                If dsstafsl.Tables(0).Rows.Count <> 0 Then
                                    staf_sl = dsstafsl.Tables(0).Rows(0).Item(0)
                                End If
                                Dim dr12() As DataRow
                                dr12 = dselog.Tables(0).Select("device_code=" & edivid & " AND loc_cd=" & loc_cd & "")
                                If dr12.Length = 0 Then
                                    dr = dt.NewRow
                                    dr(0) = slno_cnt
                                    dr(1) = edivid
                                    dr(2) = logdt
                                    dr(3) = logtm
                                    dr(4) = "D"
                                    dr(5) = 0
                                    dr(6) = "N"
                                    dr(7) = loc_cd
                                    dr(8) = devno
                                    dr(9) = staf_sl
                                    dt.Rows.Add(dr)
                                    slno_cnt = slno_cnt + 1
                                End If
                            End If
                        End If
                        bySSRATTLOG = Nothing
                        iStartIndex += iOneLogLength
                        iOneLogLength = 0
                    End If
                Next
            End If
        Next
        dv2.DataSource = dt
        dv2.DataBind()
        Dim sqlcstring As String = ConfigurationManager.ConnectionStrings("dbnm").ConnectionString.ToString
        Using cn1 As New SqlConnection(sqlcstring)
            cn1.Open()
            Using COPY As New SqlBulkCopy(cn1)
                COPY.ColumnMappings.Add(0, 0)
                COPY.ColumnMappings.Add(1, 1)
                COPY.ColumnMappings.Add(2, 2)
                COPY.ColumnMappings.Add(3, 3)
                COPY.ColumnMappings.Add(4, 4)
                COPY.ColumnMappings.Add(5, 5)
                COPY.ColumnMappings.Add(6, 6)
                COPY.ColumnMappings.Add(7, 7)
                COPY.ColumnMappings.Add(8, 8)
                COPY.ColumnMappings.Add(9, 9)
                COPY.DestinationTableName = "elog2"
                COPY.WriteToServer(dt)
            End Using
        End Using
        start1()
        SQLInsert("INSERT INTO elog(sl_no,device_code,log_dt,log_time,log_tp,slno,read_mark,loc_cd,device_no,staf_sl) SELECT max(sl_no) as sl_no,device_code,log_dt,log_time,max(log_tp) as log_tp,max(slno) as slno,max(read_mark) as read_mark,max(loc_cd) as loc_cd ,max(device_no) as device_no,max(staf_sl) as staf_sl FROM elog2 WHERE loc_cd=" & loc_cd & " GROUP BY device_code,log_dt,log_time")
        close1()
    End Sub
End Class
