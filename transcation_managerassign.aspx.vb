﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient

Partial Class transcation_managerassign
    Inherits System.Web.UI.Page
    <System.Web.Script.Services.ScriptMethod(), _
System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            "staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
        txtempcode.Text = ""
        Me.divisiondisp()
        Me.deptdisp()
        Dim ds As DataSet = get_dataset("SELECT staf_nm, emp_code, device_code, staf_sl FROM staf")
        ds.Tables(0).Rows.Clear()
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
        If txtmode.Text = "E" Then
            pnladd.Visible = True
            lblhdr.Text = "Reporting Manager Assignment (Entry Mode)"
            cmbdivsion.Focus()
        End If
    End Sub

    Private Sub staf_disp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds As New DataSet
        If cmbdivsion.SelectedIndex = 0 Then
            If cmbdept.SelectedIndex = 0 Then
                ds = get_dataset("SELECT staf_nm, emp_code, device_code, staf_sl FROM staf WHERE loc_cd=" & loc_cd & " AND emp_status='I' ORDER BY staf_nm")
            Else
                ds = get_dataset("SELECT staf_nm, emp_code, device_code, staf_sl FROM staf WHERE loc_cd=" & loc_cd & " AND emp_status='I' AND dept_sl=" & Val(txtdeptcd.Text) & " ORDER BY staf_nm")
            End If
        Else
            If cmbdept.SelectedIndex = 0 Then
                ds = get_dataset("SELECT staf_nm, emp_code, device_code, staf_sl FROM staf WHERE loc_cd=" & loc_cd & " AND emp_status='I' AND div_sl=" & Val(txtdivsl.Text) & " ORDER BY staf_nm")
            Else
                ds = get_dataset("SELECT staf_nm, emp_code, device_code, staf_sl FROM staf WHERE loc_cd=" & loc_cd & " AND emp_status='I' AND dept_sl=" & Val(txtdeptcd.Text) & " AND div_sl=" & Val(txtdivsl.Text) & " ORDER BY staf_nm")
            End If
        End If
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
    End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub deptdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdept.Items.Clear()
        cmbdept.Items.Add("Please Select A Department")
        Dim ds As DataSet = get_dataset("SELECT dept_nm FROM dept_mst WHERE loc_cd=" & loc_cd & " ORDER BY dept_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdept.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Protected Sub cmbdept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdept.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdeptcd.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT dept_sl FROM dept_mst WHERE dept_nm='" & Trim(cmbdept.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdeptcd.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
        Me.staf_disp()
    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
        Me.staf_disp()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        Dim fnd As Integer = 0
        For i As Integer = 0 To dvstaf.Rows.Count - 1
            Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                fnd = 1
                Exit For
            End If
        Next
        If fnd = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Select Atleast One Employee');", True)
            dvstaf.Focus()
            Exit Sub
        End If
        Dim ds As DataSet = get_dataset("SELECT * FROM staf WHERE emp_code='" & Trim(txtempcode.Text) & "' AND loc_cd=" & loc_cd & "")
        Dim emp_sl As Integer = 0
        If ds.Tables(0).Rows.Count <> 0 Then
            emp_sl = ds.Tables(0).Rows(0).Item("staf_sl")
        End If
        If Val(emp_sl) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide The Reporting Manager Employee Code');", True)
            txtempcode.Focus()
            Exit Sub
        End If
        start1()
        For j As Integer = 0 To dvstaf.Rows.Count - 1
            Dim chk As CheckBox = dvstaf.Rows(j).FindControl("chk")
            If chk.Checked = True Then
                Dim lblsl As Label = dvstaf.Rows(j).FindControl("lblstaf_sl")
                SQLInsert("UPDATE staf SET r_usr_sl =" & Val(emp_sl) & " WHERE staf_sl=" & lblsl.Text & " AND loc_cd=" & loc_cd & "")
            End If
        Next
        close1()
        Me.clr()
        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
    End Sub

    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        cmbdivsion.Focus()
    End Sub

    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkhdr As CheckBox = dvstaf.HeaderRow.FindControl("chk1")
        If chkhdr.Checked = True Then
            For i As Integer = 0 To dvstaf.Rows.Count - 1
                Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
                chk.Checked = True
            Next
        ElseIf chkhdr.Checked = False Then
            For i As Integer = 0 To dvstaf.Rows.Count - 1
                Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
                chk.Checked = False
            Next
        End If
    End Sub

    Protected Sub txtempcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtempcode.TextChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If Trim(txtempcode.Text) <> "" Then
            Dim emp_code As String() = Trim(txtempcode.Text).Split("-")
            txtempcode.Text = emp_code(0)
            Dim ds1 As DataSet = get_dataset("SELECT staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm, location_mst.loc_nm, staf.loc_cd FROM location_mst RIGHT OUTER JOIN staf ON location_mst.loc_cd = staf.loc_cd LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            Else
                txtempcode.Text = ""
            End If
        End If
    End Sub
End Class
