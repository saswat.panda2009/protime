﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Data.SqlClient

Partial Class transcation_verifymanualapproval
    Inherits System.Web.UI.Page
    <System.Web.Script.Services.ScriptMethod(), _
System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            " staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        Image1.ImageUrl = "~/img/avatar-1.jpg"
        lbladdress.Text = ""
        txtid.Text = ""
        txtstafsl.Text = ""
        txttime.Text = ""
        txtdt.Text = ""
        txtfrmdt.Text = Format(Now, "dd/MM/yyyy")
        txttodt.Text = Format(Now, "dd/MM/yyyy")
    End Sub

    Protected Sub dvstaf_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dvstaf.RowCommand
        Dim rw As Integer = e.CommandArgument
        txtid.Text = ""
        txtstafsl.Text = ""
        txttime.Text = ""
        txtdevicecode.Text = ""
        txtdt.Text = ""
        cmdsave.Visible = False
        cmdclear.Visible = False
        Dim lbl As Label = dvstaf.Rows(rw).FindControl("lblid")
        Dim lblstafsl As Label = dvstaf.Rows(rw).FindControl("lblstaf_sl")
        Dim lbltime As Label = dvstaf.Rows(rw).FindControl("lbltm")
        Dim lbldevicecode As Label = dvstaf.Rows(rw).FindControl("lbldevicecode")
        Dim lbldt As Label = dvstaf.Rows(rw).FindControl("lbldt")
        Dim lblstatus As Label = dvstaf.Rows(rw).FindControl("lblstatus")
        If e.CommandName = "view" Then
            Dim ds1 As DataSet = get_dataset("SELECT log_img,log_location,log_longitude,log_lattitude FROM manual_posting WHERE id=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                Image1.ImageUrl = "data:image/jpg;base64," & ds1.Tables(0).Rows(0).Item("log_img")
                lbladdress.Text = ds1.Tables(0).Rows(0).Item("log_location")
                HyperLink1.NavigateUrl = "https://www.google.com/maps?q=" & ds1.Tables(0).Rows(0).Item("log_longitude") & "," & ds1.Tables(0).Rows(0).Item("log_lattitude")
                txtid.Text = lbl.Text
                txtstafsl.Text = lblstafsl.Text
                txttime.Text = lbltime.Text
                txtdevicecode.Text = lbldevicecode.Text
                txtdt.Text = lbldt.Text
                If lblstatus.Text = "C" Then
                    cmdsave.Visible = True
                ElseIf lblstatus.Text = "A" Then
                    cmdclear.Visible = True
                End If
            End If
        ElseIf e.CommandName = "reset_state" Then

        End If
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        start1()
        SQLInsert("Update manual_posting Set log_status='A'   WHERE id=" & txtid.Text & "")
        SQLInsert("INSERT INTO elog(device_code,log_dt,log_time,log_tp,slno,read_mark,device_no,loc_cd,staf_sl,view_report) VALUES('" & Trim(txtdevicecode.Text) & _
        "','" & Format(stringtodate(txtdt.Text), "dd/MMM/yyyy") & "','" & txttime.Text & "','A',0,'N',0," & loc_cd & "," & txtstafsl.Text & ",'Y')")
        close1()
        Me.clr()
        Me.dvdisp()
    End Sub

    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        start1()
        SQLInsert("Update  manual_posting SET log_status='C'  WHERE id=" & txtid.Text & "")
        SQLInsert("Update  elog SET view_report='N'  WHERE staf_sl=" & txtstafsl.Text & " AND log_dt='" & Format(stringtodate(txtdt.Text), "dd/MMM/yyyy") & "' AND log_time='" & txttime.Text & "' AND log_tp='A'")
        close1()
        Me.clr()
        Me.dvdisp()
    End Sub

    Protected Sub cmdsave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave1.Click
        Me.dvdisp()
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim str As String = ""
        If cmbtp.SelectedIndex = 0 Then
            str = "AND log_status<>'P'"
        ElseIf cmbtp.SelectedIndex = 1 Then
            str = "AND log_status='A'"
        ElseIf cmbtp.SelectedIndex = 2 Then
            str = "AND log_status='C'"
        End If
        If txtempcode.Text <> "" Then
            Dim ds1 As DataSet = get_dataset("select staf_sl from staf where loc_cd=" & loc_cd & " AND emp_code='" & txtempcode.Text & "'")
            If ds1.Tables(0).Rows.Count <> 0 Then
                str = str & "AND manual_posting.staf_sl=" & Val(ds1.Tables(0).Rows(0).Item(0))
            End If
        End If
        Dim ds As DataSet = get_dataset("SELECT CONVERT(varchar, manual_posting.log_dt, 103) AS dt, manual_posting.staf_sl, manual_posting.id, CONVERT(varchar, manual_posting.log_time, 108) AS tm, staf.staf_nm, division_mst.div_nm, dept_mst.dept_nm, desg_mst.desg_nm, staf.device_code,log_status,post_type FROM dept_mst INNER JOIN staf ON dept_mst.dept_sl = staf.dept_sl INNER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl RIGHT OUTER JOIN manual_posting ON staf.staf_sl = manual_posting.staf_sl LEFT OUTER JOIN division_mst ON manual_posting.div_sl = division_mst.div_sl  WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND manual_posting.loc_cd=" & loc_cd & " " & str & " ORDER BY log_dt ")
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
    End Sub

    Protected Sub txtempcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtempcode.TextChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If Trim(txtempcode.Text) <> "" Then
            Dim emp_code As String() = Trim(txtempcode.Text).Split("-")
            txtempcode.Text = emp_code(0)
            Dim ds1 As DataSet = get_dataset("SELECT staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm, location_mst.loc_nm, staf.loc_cd FROM location_mst RIGHT OUTER JOIN staf ON location_mst.loc_cd = staf.loc_cd LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            Else
                txtempcode.Text = ""
            End If
        End If
    End Sub
End Class
