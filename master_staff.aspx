﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="master_staff.aspx.vb" Inherits="master_staff" EnableEventValidation="true" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Master</h2>
            </div>
          </header>
          <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">

                   <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="A1" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                      aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="master_staff.aspx">Add New Employee</a>
                      <a class="dropdown-item" href="master_staff.aspx?mode=V">View Employee List</a>
                    
                    </div>
                  </div>
                </div>

                                    <div class="card-body" align="center">
                                  <table style="width:99%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnlview" runat="server">
                                          <table style="width: 100%;">
                                              <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel1" runat="server">
                                                      <div class="row">
                                                          <div class="col-lg-3">
                                                             <asp:TextBox ID="txtfilterstate" runat="server"  class="form-control" placeholder="Employee Name"></asp:TextBox>
                                                          </div>
                                                          <div class="col-lg-3">
                                                          </div>
                                                          
                                                          <div class="col-lg-6">
                                                              <asp:Button ID="cmdsearch" runat="server" class="btn btn-primary" Text="Go" />
                                                              <asp:Button ID="cmdclr" runat="server" class="btn btn-default" Text="Clear" />
                                                              <asp:TextBox ID="txtstatcd" runat="server" Visible="False" Width="16px"></asp:TextBox>
                                                              <asp:TextBox ID="txtcitycd" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                              <asp:TextBox ID="txtmode0" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                          </div>
                                                      </div>
                                                      
                                                      </asp:Panel>
                                                      </td>
                                              </tr>
                                           <tr>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel2" runat="server">
                                                          <div style="width: 100%;">
                                                              <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="100" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <Columns>
                                                                      <asp:BoundField DataField="sl" HeaderText="Sl">
                                                                      <HeaderStyle Width="30px" />
                                                                      <ItemStyle Width="30px" />
                                                                      </asp:BoundField>
                                                                      <asp:BoundField DataField="staf_nm" HeaderText="Employee Name" />
                                                                      <asp:BoundField DataField="short_nm" HeaderText="Short Name">
                                                                      </asp:BoundField>
                                                                      <asp:BoundField DataField="emp_code" HeaderText="Employee ID" />
                                                                      <asp:BoundField DataField="dept_nm" HeaderText="Department" />
                                                                      <asp:BoundField DataField="desg_nm" HeaderText="Designation" />
                                                                      <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                                                          ImageUrl="images/edit.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                                                      <ItemStyle Height="30px" Width="30px" />
                                                                      </asp:ButtonField>
                                                                      <asp:ButtonField ButtonType="Image" CommandName="reset_state" 
                                                                          ImageUrl="images/reset.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                                                      <ItemStyle Height="30px" Width="30px" />
                                                                      </asp:ButtonField>
                                                                  </Columns>
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>
                                                          </div>
                                                      </asp:Panel>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table width="100%">
                                          <tr><div class="alert bg-success" id="divmsg" runat="server"> 
                            <asp:Label ID="lblmsg" runat="server" Text="Label"></asp:Label>
                                                    </div></tr>
                                              <tr>
                                                  <td>
                                                      <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" 
                                                          Height="1400px" CssClass="Tab">
                                                          <asp:TabPanel ID="TabPanel1" runat="server" HeaderText="Employee Details" Height="50">
                                                          <ContentTemplate>
                                                           <div class="row">
                              
                                <div class="col-lg-8">
                                <table style="width:100%;"><tr><td width="2%">&nbsp;</td><td width="98%"><asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
                                     <table style="width: 100%;">
                                        <tr>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                      <tr>
                                                <td>
                                                   Division</td>
                                            </tr>
                                             <tr>
                                                <td>
                                                   <asp:DropDownList ID="cmbdivsion" runat="server" class="form-control" 
                                                        AutoPostBack="True">
                                                                                                      </asp:DropDownList></td>
                                            </tr>
                                     
                                            <tr>
                                                                                            <td>
                                                   Employee Name</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   <asp:TextBox ID="txtstaff" runat="server" class="form-control" MaxLength="100"></asp:TextBox>
                                                      <asp:FilteredTextBoxExtender ID="txtstaff_FilteredTextBoxExtender" 
                                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                          TargetControlID="txtstaff">
                                                      </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                               <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Short Name
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   <asp:TextBox ID="txtshortname" runat="server" class="form-control" 
                                                        MaxLength="100"></asp:TextBox>
                                                      <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                          TargetControlID="txtshortname">
                                                      </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   Employee Code
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   <asp:TextBox ID="txtempid" runat="server" class="form-control" MaxLength="25"></asp:TextBox>
                                                      <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" 
                                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                          TargetControlID="txtempid">
                                                      </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   Device Code
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   <asp:TextBox ID="txtdevicecode" runat="server" class="form-control"></asp:TextBox>
                                                         <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" 
                                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                          TargetControlID="txtdevicecode">
                                                      </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   RFID Card No
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   <asp:TextBox ID="txtrfid" runat="server" class="form-control"></asp:TextBox>
                                                      <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" 
                                                          runat="server" Enabled="True" 
                                                          TargetControlID="txtrfid" FilterType="Numbers">
                                                      </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   Employee Status
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                    <asp:DropDownList ID="cmbempstatus" runat="server" class="form-control">
                                                    <asp:ListItem Value="Is Working"></asp:ListItem>
                                                          <asp:ListItem Value="Not Working"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   Date Of Resign
                                                                                                  </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   <asp:TextBox ID="txtresign" runat="server" class="form-control"></asp:TextBox>
                                                      <asp:CalendarExtender ID="CalendarExtender2" runat="server" 
                                                        Enabled="True" Format="dd/MM/yyyy" PopupButtonID="txtresign" 
                                                        TargetControlID="txtresign">
                                                    </asp:CalendarExtender>
                                                      
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   Reason Of Resign
                                                                                                  </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   <asp:TextBox ID="txtreason" runat="server" class="form-control"></asp:TextBox>
                                                     
                                                       <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" 
                                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                          TargetControlID="txtreason">
                                                      </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   Employee Type
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                    <asp:DropDownList ID="cmbemptp" runat="server" class="form-control">
                                                    <asp:ListItem Value="Temporary"></asp:ListItem>
                                                          <asp:ListItem Value="Permanent"></asp:ListItem>
                                                           <asp:ListItem Value="Contractual"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                              <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                    &nbsp;</td>
                                            </tr>
                                              <tr>
                                                  <td align="left" height="8">
                                                      Employee Category</td>
                                         </tr>
                                         <tr>
                                             <td align="center" height="8">
                                                 <asp:DropDownList ID="cmbempcat" runat="server" class="form-control" 
                                                     AutoPostBack="True">
                                                 </asp:DropDownList>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td align="center" height="8" style="font-size: 8px">
                                             </td>
                                         </tr>
                                              <tr>
                                                <td>
                                                   Department
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                    <asp:DropDownList ID="cmbdept" runat="server" class="form-control" 
                                                        AutoPostBack="True">
                                                                                                      </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   Designation
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                    <asp:DropDownList ID="cmbdesg" runat="server" class="form-control" 
                                                        AutoPostBack="True">
                                                                                                      </asp:DropDownList>
                                                </td>
                                            </tr>
                                              <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   Date Of Joining
                                                                                                  </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                   <asp:TextBox ID="txtjoindate" runat="server" class="form-control"></asp:TextBox>
                                                      <asp:CalendarExtender ID="txtjoindate_CalendarExtender" runat="server" 
                                                        Enabled="True" Format="dd/MM/yyyy" PopupButtonID="txtjoindate" 
                                                        TargetControlID="txtjoindate">
                                                    </asp:CalendarExtender>
                                                      <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" 
                                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                          TargetControlID="txtempid">
                                                      </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                  Shift Type
                                                                                                  </td>
                                            </tr>
                                                 <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td width="50%">
                                                                <asp:DropDownList ID="cmbshifttp" runat="server" class="form-control" 
                                                                    AutoPostBack="True">
                                                    <asp:ListItem Value="Fix Shift"></asp:ListItem>
                                                          <asp:ListItem Value="Non-Fix Shift"></asp:ListItem>
                                                           <asp:ListItem Value="Auto Shift"></asp:ListItem>
                                                                    <asp:ListItem>In / Out</asp:ListItem>
                                                    </asp:DropDownList>
                                                            </td>
                                                            <td width="50%">
                                                               <asp:DropDownList ID="cmbshiftcycle" runat="server" class="form-control">
                                                    
                                                    </asp:DropDownList>
                                                            </td>
                                                           
                                                        </tr>
                                                                                                           </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                  Weekly Off Type
                                                                                                  </td>
                                            </tr>
                                                 <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButton ID="rdglobal" runat="server" GroupName="weekoff" Text="Global"/>
                                                            </td>
                                                            <td>
                                                                 <asp:RadioButton ID="rdmanual" runat="server" GroupName="weekoff" Text="Manual" />
                                                            </td>
                                                           
                                                        </tr>
                                                                                                           </table>
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                                  Comp. Off
                                                                                                  </td>
                                            </tr>
                                               <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                               <asp:CheckBox ID="chkweekcompoff" runat="server" Text="Allow Weekly Off"></asp:CheckBox>
                                                            </td>
                                                            <td>
                                                                 <asp:CheckBox ID="chkholidayoff" runat="server" Text="Allow Holiday"></asp:CheckBox>
                                                            </td>
                                                           
                                                        </tr>
                                                                                                           </table>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td>
                                                            <asp:CheckBox ID="chkapp" runat="server" Text="Allow Mobile App"></asp:CheckBox>
                                                            </td>
                                                                 <td>
                                                                 <asp:CheckBox ID="chkconveyances" runat="server" Text="Calculate Conveyances"></asp:CheckBox>
                                                            </td>                                                     
                                                        </tr>
                                                                                                           </table>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td>
                                                   
                                                    <asp:CustomValidator ID="valid_dept" runat="server" 
                                                        ErrorMessage="<b>Required Field<b/><br/>Please Select A Valid Department." SetFocusOnError="True" ControlToValidate="cmbdept" Display="None"></asp:CustomValidator>
                                                   
                                                    <asp:ValidatorCalloutExtender ID="valid_dept_ValidatorCalloutExtender" 
                                                        runat="server" Enabled="True" PopupPosition="BottomLeft" 
                                                        TargetControlID="valid_dept">
                                                    </asp:ValidatorCalloutExtender>
                                                    <asp:CustomValidator ID="valid_desg" runat="server" 
                                                        ErrorMessage="<b>Required Field<b/><br/>Please Select A Valid Designation." ControlToValidate="cmbdesg" Display="None" 
                                                        SetFocusOnError="True"></asp:CustomValidator>
                                                    <asp:ValidatorCalloutExtender ID="valid_desg_ValidatorCalloutExtender" 
                                                        runat="server" Enabled="True" TargetControlID="valid_desg" 
                                                        PopupPosition="BottomLeft">
                                                    </asp:ValidatorCalloutExtender>
                                                     <asp:RequiredFieldValidator ID="valid_staff" runat="server" 
                                                          ErrorMessage="<b>RequiredField</b><br/>Please Provide The Employee Name." 
                                                          ControlToValidate="txtstaff" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                                        
                                                                                                        <asp:ValidatorCalloutExtender ID="valid_staff_ValidatorCalloutExtender" 
                                                          runat="server" Enabled="True" PopupPosition="BottomLeft" 
                                                          TargetControlID="valid_staff">
                                                      </asp:ValidatorCalloutExtender>
                                                      <asp:RequiredFieldValidator ID="valid_short" runat="server" 
                                                          ErrorMessage="&lt;b&gt;RequiredField&lt;/b&gt;&lt;br/&gt;Please Provide The Short Name." 
                                                          ControlToValidate="txtshortname" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                                        
                                                                                                        <asp:ValidatorCalloutExtender ID="valid_short_ValidatorCalloutExtender" 
                                                          runat="server" Enabled="True" PopupPosition="BottomLeft" 
                                                          TargetControlID="valid_short">
                                                      </asp:ValidatorCalloutExtender>
                                                    <asp:CustomValidator ID="valid_empid" runat="server" 
                                                        ErrorMessage="&lt;b&gt;Sorry Employee ID Already Exits.&lt;/b&gt;" 
                                                        ControlToValidate="txtempid" Display="None" SetFocusOnError="True"></asp:CustomValidator>
                                                    <asp:ValidatorCalloutExtender ID="valid_empid_ValidatorCalloutExtender" 
                                                        runat="server" Enabled="True" PopupPosition="BottomLeft" 
                                                        TargetControlID="valid_empid">
                                                    </asp:ValidatorCalloutExtender>
                                                </td>
                                            </tr>
                                        </table>
                                         </ContentTemplate>
    
    </asp:UpdatePanel></td></table>
                                                          </div>
                                                          <div class="col-lg-4">
                                     <table style="width: 100%;">
                                      <tr>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Image ID="Image1" runat="server" Height="200px" Width="200px" />
                                                </td>
                                            </tr>
                                             <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                                                                    <asp:FileUpload ID="FileUpload1" runat="server" Width="200px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Button ID="cmdview" runat="server" class="btn btn-success" 
                                                        Text="View Image" Width="200px" CausesValidation=False/>
                                                </td>
                                            </tr>
                                        </table>
                                                          </div>
                               </div>
                               
        </ContentTemplate></asp:TabPanel>
                                                          <asp:TabPanel ID="TabPanel2" runat="server" HeaderText="Contact details" Height="50">
                                                          <ContentTemplate>
                                                           <div class="col-lg-12">
                                     <table style="width: 100%;">
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                             <tr>
                                                <td style="color: #800000">
                                                   Present Address
                                                  </td>
                                            </tr>
                                             <tr>
                                                <td>
                                                   Address
                                                  </td>
                                            </tr>
                                            <tr><td><asp:TextBox ID="txtpreadd" runat="server" class="form-control"></asp:TextBox>
                                                      <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" 
                                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                          TargetControlID="txtpreadd">
                                                      </asp:FilteredTextBoxExtender></td></tr>
                                                      <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                            <tr><td>
                                             <table style="width:100%;">
                                                          <tr>
                                                              <td width="40%">
                                                                  District</td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  State </td>
                                                          </tr>
                                                          <tr>
                                                              <td width="40%">
                                                                  <asp:DropDownList ID="cmbpredist" runat="server" class="form-control" AutoPostBack="True">
                                                                  </asp:DropDownList>
                                                              </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  <asp:TextBox ID="txtprestate" runat="server" class="form-control" ReadOnly="True"></asp:TextBox>
                                                                                                                                </td>
                                                          </tr>
                                                          
                                                </table></td></tr>
                                                <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                <tr><td> <table style="width:100%;">
                                                          <tr>
                                                              <td width="40%">
                                                                  Phone Number -1 </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  Phone Number -2 </td>
                                                          </tr>
                                                          <tr>
                                                              <td width="40%">
                                                                  <asp:TextBox ID="txtpreph1" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtph1_FilteredTextBoxExtender" runat="server" 
                                                                      Enabled="True" FilterType="Numbers" TargetControlID="txtpreph1">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  <asp:TextBox ID="txtpreph2" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtph2_FilteredTextBoxExtender" runat="server" 
                                                                      Enabled="True" FilterType="Numbers" TargetControlID="txtpreph2">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                      </table></td></tr>
                                                      <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                 <tr>
                                                <td style="color: #800000">
                                                   Permanent Address
                                                  </td>
                                            </tr>
                                             <tr>
                                                <td>
                                                   Address
                                                  </td>
                                            </tr>
                                            <tr><td><asp:TextBox ID="txtperadd" runat="server" class="form-control"></asp:TextBox>
                                                      <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" 
                                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                          TargetControlID="txtperadd">
                                                      </asp:FilteredTextBoxExtender></td></tr>
                                                      <tr>
                                                <td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td>
                                            </tr>
                                            <tr><td>
                                             <table style="width:100%;">
                                                          <tr>
                                                              <td width="40%">
                                                                  District</td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  State </td>
                                                          </tr>
                                                          <tr>
                                                              <td width="40%">
                                                                  <asp:DropDownList ID="cmbperdist" runat="server" class="form-control" AutoPostBack="True">
                                                                  </asp:DropDownList>
                                                              </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  <asp:TextBox ID="txtperstate" runat="server" class="form-control" ReadOnly="True"></asp:TextBox>
                                                                                                                                </td>
                                                          </tr>
                                                          
                                                </table></td></tr>
                                                <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                <tr><td> 
                                                <table style="width:100%;">
                                                          <tr>
                                                              <td width="40%">
                                                                  Phone Number -1 </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  Phone Number -2 </td>
                                                          </tr>
                                                          <tr>
                                                              <td width="40%">
                                                                  <asp:TextBox ID="txtperph1" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" 
                                                                      Enabled="True" FilterType="Numbers" TargetControlID="txtperph1">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  <asp:TextBox ID="txtperph2" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" 
                                                                      Enabled="True" FilterType="Numbers" TargetControlID="txtperph2">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                      </table></td></tr>
                                                      <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                <tr>
                                                    <td align="left" style="color: #800000">
                                                        Emergency Contact</td>
                                            </tr>
                                            <tr>
                                                <td align="left" height="8">
                                                    Name</td>
                                            </tr>
                                            <tr>
                                                <td align="left" height="8">
                                                    <asp:TextBox ID="txtemergencynm" runat="server" class="form-control"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="txtemergencynm_FilteredTextBoxExtender" 
                                                        runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                        TargetControlID="txtemergencynm">
                                                    </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                 <tr><td> 
                                                <table style="width:100%;">
                                                          <tr>
                                                              <td width="40%">
                                                                  Relation</td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  Contact No</td>
                                                          </tr>
                                                          <tr>
                                                              <td width="40%">
                                                                  <asp:TextBox ID="txtrelation" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" 
                                                                      Enabled="True" TargetControlID="txtrelation" FilterMode="InvalidChars" 
                                                                      InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  <asp:TextBox ID="txtemergencycontact" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" 
                                                                      Enabled="True" FilterType="Numbers" TargetControlID="txtemergencycontact">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                      </table></td></tr>
                                                      <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                 <tr>
                                                <td align="left" height="8">
                                                    Address</td>
                                            </tr>
                                            <tr>
                                                <td align="left" height="8">
                                                    <asp:TextBox ID="txtemergencyaddr" runat="server" class="form-control"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" 
                                                        runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                        TargetControlID="txtemergencyaddr">
                                                    </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                                <tr>
                                                <td>
                                                   Email ID
                                                   <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                                          ControlToValidate="txtemail" Display="None" 
                                                          ErrorMessage="&lt;b&gt;Invalid Email Id Format&lt;/b&gt;" 
                                                          SetFocusOnError="True" 
                                                          ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                      <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" 
                                                          runat="server" Enabled="True" TargetControlID="RegularExpressionValidator3" PopupPosition="BottomLeft">
                                                      </asp:ValidatorCalloutExtender>
                                                  </td>
                                            </tr>
                                            <tr><td><asp:TextBox ID="txtemail" runat="server" class="form-control"></asp:TextBox>
                                                      <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" 
                                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                          TargetControlID="txtemail">
                                                      </asp:FilteredTextBoxExtender></td></tr>
                                           </table>
                                           </div>
                                                          </ContentTemplate>
                                                          </asp:TabPanel>
                                                          <asp:TabPanel ID="TabPanel3" runat="server" HeaderText="Personal Details" Height="50">
                                                          <ContentTemplate>
                                                          <div class="col-lg-12">
                                     <table style="width: 100%;">
                                     <tr><td></td></tr>
                                     <tr><td> <table style="width:100%;">
                                     <tr><td>&nbsp;</td></tr>
                                                          <tr>
                                                              <td width="40%">
                                                                  Date Of Birth 
                                                                 </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  Gender </td>
                                                          </tr>
                                                          <tr>
                                                              <td width="40%">
                                                                  <asp:TextBox ID="txtdob" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" 
                                                                      Enabled="True" FilterType="Custom" TargetControlID="txtdob" FilterMode="InvalidChars" InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender>
                                                                   <asp:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                        Enabled="True" Format="dd/MM/yyyy" PopupButtonID="txtdob" 
                                                        TargetControlID="txtdob">
                                                    </asp:CalendarExtender>
                                                              </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  <asp:DropDownList ID="cmbgender" class="form-control" runat="server">
                                                                  <asp:ListItem Value="Male"></asp:ListItem>
                                                           <asp:ListItem Value="Female"></asp:ListItem>
                                                                  </asp:DropDownList>
                                                              </td>
                                                          </tr>
                                                      </table></td></tr>
                                                      <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                     <tr><td><table style="width:100%;">
                                                          <tr>
                                                              <td width="40%">
                                                                  Maritial Status
                                                                  </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  Nationality </td>
                                                          </tr>
                                                          <tr>
                                                              <td width="40%">
                                                              <asp:DropDownList ID="cmbmaried" class="form-control" runat="server">
                                                                  <asp:ListItem Value="Married"></asp:ListItem>
                                                           <asp:ListItem Value="Un-Married"></asp:ListItem>
                                                                  </asp:DropDownList>
                                                                 
                                                              </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                   <asp:TextBox ID="txtnal" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" 
                                                                      Enabled="True" FilterType="Custom" TargetControlID="txtnal" FilterMode="InvalidChars" InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                      </table></td></tr>
                                     <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                <tr><td><table style="width:100%;">
                                                          <tr>
                                                              <td width="40%">
                                                                  Religion
                                                                  </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  Caste </td>
                                                          </tr>
                                                          <tr>
                                                              <td width="40%">
                                                                 <asp:TextBox ID="txtreligion" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" 
                                                                      Enabled="True" FilterType="Custom" TargetControlID="txtreligion" FilterMode="InvalidChars" InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender>
                                                                 
                                                              </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                   <asp:TextBox ID="txtcaste" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" 
                                                                      Enabled="True" FilterType="Custom" TargetControlID="txtcaste" FilterMode="InvalidChars" InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                      </table></td></tr>
                                     <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                 <tr><td><table style="width:100%;">
                                                          <tr>
                                                              <td width="40%">
                                                                  Blood Group
                                                                  </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                   Aadhar Card No</td>
                                                          </tr>
                                                          <tr>
                                                              <td width="40%">
                                                                 <asp:TextBox ID="txtbloodgroup" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" 
                                                                      Enabled="True" FilterType="Custom" TargetControlID="txtbloodgroup" FilterMode="InvalidChars" InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender>
                                                                 
                                                              </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                    <asp:TextBox ID="txtaadhar" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" 
                                                                      Enabled="True" FilterType="Custom" TargetControlID="txtaadhar" FilterMode="InvalidChars" InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                      </table></td></tr>
                                     <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                       <tr><td><table style="width:100%;">
                                                          <tr>
                                                              <td width="40%">
                                                                  Voter ID
                                                                  </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                  Driving License </td>
                                                          </tr>
                                                          <tr>
                                                              <td width="40%">
                                                                 <asp:TextBox ID="txtvoterid" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" 
                                                                      Enabled="True" FilterType="Custom" TargetControlID="txtvoterid" FilterMode="InvalidChars" InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender>
                                                                 
                                                              </td>
                                                              <td width="20%">
                                                                  &nbsp;</td>
                                                              <td width="40%">
                                                                   <asp:TextBox ID="txtdriving" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" 
                                                                      Enabled="True" FilterType="Custom" TargetControlID="txtdriving" FilterMode="InvalidChars" InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                      </table></td></tr>
                                     <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                <tr><td>Name Of The Father</td></tr>
                                                <tr><td> <asp:TextBox ID="txtguardian" runat="server" class="form-control" MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" 
                                                                      Enabled="True" FilterType="Custom" TargetControlID="txtguardian" FilterMode="InvalidChars" InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender></td></tr>
                                                                  <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                <tr><td>Name Of The Mother</td></tr>
                                                <tr><td> <asp:TextBox ID="txtmother" runat="server" class="form-control" MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" 
                                                                      Enabled="True" FilterType="Custom" TargetControlID="txtmother" FilterMode="InvalidChars" InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender></td></tr>
                                                                  <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                  <tr><td>Name Of The Spouse</td></tr>
                                                <tr><td> <asp:TextBox ID="txtspouse" runat="server" class="form-control" MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" 
                                                                      Enabled="True" FilterType="Custom" TargetControlID="txtspouse" FilterMode="InvalidChars" InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender></td></tr>
                                                                  <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                 <tr><td>Name Of The Children</td></tr>
                                                <tr><td> <asp:TextBox ID="txtchildren" runat="server" class="form-control" MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" 
                                                                      Enabled="True" FilterType="Custom" TargetControlID="txtchildren" FilterMode="InvalidChars" InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender></td></tr>
                                                                  <tr><td align="center" height="8" style="font-size: 8px">
                                                   
                                                </td></tr>
                                                <tr><td>Qualification Details</td></tr>
                                                <tr><td> <asp:TextBox ID="txtquali" runat="server" class="form-control" MaxLength="200"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" 
                                                                      Enabled="True" FilterType="Custom" TargetControlID="txtquali" FilterMode="InvalidChars" InvalidChars="'">
                                                                  </asp:FilteredTextBoxExtender></td></tr>

                                                           
                                     </table>
                                     </div>
                                                          </ContentTemplate>
                                                          </asp:TabPanel>
                                                           <asp:TabPanel ID="TabPanel4" runat="server" HeaderText="Payroll" Height="50">
                                                          <ContentTemplate>
                                                          <div class="col-lg-12">
                                                            <table style="width:100%;">
                                                        <tr>
                                                            <td width="45%">
                                                                &nbsp;</td>
                                                            <td width="10%">
                                                                &nbsp;</td>
                                                            <td width="45%">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="45%%">
                                                                Payment Mode</td>
                                                            <td width="10%">
                                                                &nbsp;</td>
                                                            <td width="45%%">
                                                                PAN NO</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="45%%">
                                                                <asp:DropDownList ID="cmbpaypayment" runat="server" AutoPostBack="True" 
                                                                    class="form-control">
                                                                    <asp:ListItem>1.Bank Transfer</asp:ListItem>
                                                                    <asp:ListItem>2.Cheque/DD</asp:ListItem>
                                                                    <asp:ListItem>3.Cash</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td width="10%">
                                                                &nbsp;</td>
                                                            <td width="45%%">
                                                                <asp:TextBox ID="txtppanno" runat="server" class="form-control"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="font-size: 5px">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="45%%">
                                                                Bank Name</td>
                                                            <td width="10%">
                                                                &nbsp;</td>
                                                            <td width="45%%">
                                                                A/C No</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="45%%">
                                                                <asp:TextBox ID="txtpbanknm" runat="server" class="form-control"></asp:TextBox>
                                                            </td>
                                                            <td width="10%">
                                                                &nbsp;</td>
                                                            <td width="45%%">
                                                                <asp:TextBox ID="txtpacno" runat="server" class="form-control"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 5px" width="45%%">
                                                                &nbsp;</td>
                                                            <td style="font-size: 5px" width="10%">
                                                                &nbsp;</td>
                                                            <td style="font-size: 5px" width="45%%">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="45%%">
                                                                IFSC Code</td>
                                                            <td width="10%">
                                                                &nbsp;</td>
                                                            <td width="45%%">
                                                                ESIC No</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="45%%">
                                                                <asp:TextBox ID="txtifsc" runat="server" class="form-control"></asp:TextBox>
                                                            </td>
                                                            <td width="10%">
                                                                &nbsp;</td>
                                                            <td width="45%%">
                                                                <asp:TextBox ID="txtesic" runat="server" class="form-control"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 5px" width="45%%">
                                                                &nbsp;</td>
                                                            <td style="font-size: 5px" width="10%">
                                                                &nbsp;</td>
                                                            <td style="font-size: 5px" width="45%%">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="45%%">
                                                                PF No</td>
                                                            <td width="10%">
                                                                &nbsp;</td>
                                                            <td width="45%%">
                                                                UAN No</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="45%%">
                                                                <asp:TextBox ID="txtpfno" runat="server" class="form-control"></asp:TextBox>
                                                            </td>
                                                            <td width="10%">
                                                                &nbsp;</td>
                                                            <td width="45%%">
                                                                <asp:TextBox ID="txtuno" runat="server" class="form-control"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                         <tr>
                                                            <td width="45%%">
                                                                Payroll Group</td>
                                                            <td width="10%">
                                                                &nbsp;</td>
                                                            <td width="45%">
<table style="width:100%;"><tr><td width="40%">Type</td><td width="60%">CTC/Wages</td></tr></table>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="45%">
<asp:DropDownList ID="cmbgroup" runat="server" class="form-control" AutoPostBack="True"></asp:DropDownList>
                                                                                                                          </td>
                                                            <td width="10%">
                                                                &nbsp;</td>
                                                            <td width="45%">
                                                              <table style="width:100%;"><tr><td width="40%">
<asp:DropDownList ID="cmbwagestype" runat="server" class="form-control">
<asp:ListItem>1.Monthly</asp:ListItem>
<asp:ListItem>2.Daily</asp:ListItem>
</asp:DropDownList></td><td width="60%"> <asp:TextBox ID="txtwages" runat="server" class="form-control" placeholder="0.00"></asp:TextBox></td></tr></table>
                                                            </td>
                                                        </tr>






                                                          <tr>
                                                            <td colspan="3"><asp:CheckBox ID="chkpf" runat="server" Text="Avail PF"></asp:CheckBox>&nbsp;<asp:CheckBox ID="chkesic" runat="server" Text="Avail ESIC"></asp:CheckBox>&nbsp;<asp:CheckBox ID="chktds" runat="server" Text="Avail TDS"></asp:CheckBox>&nbsp;&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                   
                                    
                                     </div>
                                                          </ContentTemplate>
                                                          </asp:TabPanel>
                                                      </asp:TabContainer>
                                                      </td>
                                              </tr>
                                              <tr><td>&nbsp;</td></tr>
                                              <tr><td>&nbsp;</td></tr>
                                              <tr>
                                                  <td>
                                                      <asp:Button ID="cmdsave" runat="server" class="btn btn-primary" Text="Submit" />
                                                      <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                          class="btn btn-default" Text="Reset" />
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtcyclesl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtshiftsl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtmode" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtdivsl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtdeptcd" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtdesgcd" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtcatsl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtperdistcd" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtpredistcd" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                       <asp:TextBox ID="txtgroupsl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                     </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>     
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
    </div>
</asp:Content>

