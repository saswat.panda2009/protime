﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class reports_absentee
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblmsg.Visible = False
        txtfrmdt.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        txttodt.Text = Format(Now, "dd/MM/yyyy")
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds As DataSet = get_dataset("select cat_nm from emp_category WHERE loc_cd=" & loc_cd & " order by cat_nm")
        Dim cat_nm As String = ""
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            If i <> 0 Then
                cat_nm = cat_nm & ","
            End If
            cat_nm = cat_nm & ds.Tables(0).Rows(i).Item(0)
        Next
        Dim ds1 As DataSet = get_dataset("select *,(select count(atnd.staf_sl) from emp_category RIGHT OUTER JOIN staf ON emp_category.cat_sl = staf.cat_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl RIGHT OUTER JOIN atnd ON staf.staf_sl = atnd.staf_sl WHERE day_status in ('PRESENT','HALFDAY') and div_nm=PivotTable.Division and dept_nm=PivotTable.Department and log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND atnd.loc_cd= " & loc_cd & ") as Total from ( select a.staf_sl,div_nm as Division,dept_nm as 'Department',emp_category.cat_nm  from emp_category RIGHT OUTER JOIN staf ON emp_category.cat_sl = staf.cat_sl LEFT OUTER JOIN division_mst as div ON staf.div_sl = div.div_sl RIGHT OUTER JOIN atnd  as a ON staf.staf_sl = a.staf_sl WHERE day_status in ('PRESENT','HALFDAY') and  log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND a.loc_cd= " & loc_cd & ") as totalcount PIVOT (Count(staf_sl) FOR cat_nm IN (" & cat_nm & ")) AS PivotTable ORDER BY Division,Department")
        If ds1.Tables(0).Rows.Count <> 0 Then
            dvdata.DataSource = ds1.Tables(0)
            dvdata.DataBind()
        Else
            dvdata.DataSource = ds1.Tables(0)
            dvdata.DataBind()
            lblmsg.Text = "No Record Found."
        End If

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds As DataSet = get_dataset("select cat_nm from emp_category WHERE loc_cd=" & loc_cd & " order by cat_nm")
        Dim cat_nm As String = ""
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            If i <> 0 Then
                cat_nm = cat_nm & ","
            End If
            cat_nm = cat_nm & ds.Tables(0).Rows(i).Item(0)
        Next
        Dim ds1 As DataSet = get_dataset("select *,(select count(atnd.staf_sl) from emp_category RIGHT OUTER JOIN staf ON emp_category.cat_sl = staf.cat_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl RIGHT OUTER JOIN atnd ON staf.staf_sl = atnd.staf_sl WHERE day_status in ('PRESENT','HALFDAY') and div_nm=PivotTable.Division and dept_nm=PivotTable.Department and log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND atnd.loc_cd= " & loc_cd & ") as Total from ( select a.staf_sl,div_nm as Division,dept_nm as 'Department',emp_category.cat_nm  from emp_category RIGHT OUTER JOIN staf ON emp_category.cat_sl = staf.cat_sl LEFT OUTER JOIN division_mst as div ON staf.div_sl = div.div_sl RIGHT OUTER JOIN atnd  as a ON staf.staf_sl = a.staf_sl WHERE day_status in ('PRESENT','HALFDAY') and  log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND a.loc_cd= " & loc_cd & ") as totalcount PIVOT (Count(staf_sl) FOR cat_nm IN (" & cat_nm & ")) AS PivotTable ORDER BY Division,Department")

        Dim dtcust As New DataTable
        dtcust = ds1.Tables(0)



        'Create a dummy GridView

        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.DataSource = dtcust
        GridView1.DataBind()
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=DailyHeadCount.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        For i As Integer = 0 To GridView1.Rows.Count - 1
            GridView1.Rows(i).Attributes.Add("class", "textmode")
        Next
        GridView1.RenderControl(hw)
        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()
    End Sub
End Class
