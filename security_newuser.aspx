﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="security_newuser.aspx.vb" Inherits="company_division" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="A1" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                      aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="security_newuser.aspx">Add New User</a>
                      <a class="dropdown-item" href="security_newuser.aspx?mode=V">View User List</a>
                    
                    </div>
                  </div>
                </div>

                     <div class="card-body">
                                     <div runat="server" id="divview" style="width: 100%">

                                <asp:GridView ID="dv" runat="server" AllowPaging="True" 
                                    AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                    AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                    PageSize="15" Width="100%">
                                    <AlternatingRowStyle CssClass="alt" />
                                    <Columns>
                                        <asp:BoundField DataField="sl" HeaderText="Sl">
                                        <HeaderStyle Width="30px" />
                                        <ItemStyle Width="30px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="usr_name" HeaderText="User Name" />
                                        <asp:BoundField DataField="Last Login" HeaderText="Last Login">
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Level" DataField="lvl" />
                                        <asp:BoundField DataField="blck" HeaderText="Blocked" />
                                        <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                            ImageUrl="images/edit.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                        <ItemStyle Height="30px" Width="30px" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                </asp:GridView>

                            </div>
                            <div runat="server" id="divadd">
                                    <div class="col-md-12">
                                     <table style="width:100%;">
                                         <tr>
                                             <td>
                                                 User ID</td>
                                         </tr>
                                         <tr>
                                             <td>
                                            <asp:TextBox ID="txtusrnm" runat="server" class="form-control" MaxLength="50"></asp:TextBox>
                                            
                                                    <asp:FilteredTextBoxExtender ID="txtusrnm_FilteredTextBoxExtender" 
                                                     runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                     TargetControlID="txtusrnm">
                                                 </asp:FilteredTextBoxExtender>
                                            
                                                    </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 Level</td>
                                         </tr>
                                         <tr>
                                             <td>
                                            <asp:DropDownList ID="cmblvl" runat="server" class="form-control" AutoPostBack="True" 
                                                >
                                                <asp:ListItem Value="Administator"></asp:ListItem>
                                                <asp:ListItem Value="Manager"></asp:ListItem>
                                                <asp:ListItem Value="Operator"></asp:ListItem>
                                            </asp:DropDownList>
                                                    </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 User Name</td>
                                         </tr>
                                         <tr>
                                             <td>
                                            <asp:TextBox ID="txtusername" runat="server" class="form-control" MaxLength="15"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="txtusername_FilteredTextBoxExtender" 
                                                runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                TargetControlID="txtusername">
                                            </asp:FilteredTextBoxExtender>
                                                    </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 Password</td>
                                         </tr>
                                         <tr>
                                             <td>
                                            <asp:TextBox ID="txtpswd" runat="server" class="form-control" MaxLength="10"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="txtpswd_FilteredTextBoxExtender" 
                                                runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                TargetControlID="txtpswd">
                                            </asp:FilteredTextBoxExtender>
                                                    </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 Blocked</td>
                                         </tr>
                                         <tr>
                                             <td>
                                            <asp:DropDownList ID="cmbactive" runat="server" class="form-control" AutoPostBack="True" 
                                                >
                                                <asp:ListItem Value="Yes"></asp:ListItem>
                                                <asp:ListItem Value="No"></asp:ListItem>
                                            </asp:DropDownList>
                                                    </td>
                                         </tr>
                                         <tr>
                                             <td>
                                            <asp:TextBox ID="txtmode" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                            <asp:TextBox ID="txtusrsl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>&nbsp;&nbsp;&nbsp;&nbsp;<asp:RequiredFieldValidator 
                                                     ID="RequiredFieldValidator1" runat="server" 
                                                     ErrorMessage="Please Provide The User ID." ControlToValidate="txtusrnm" 
                                                     Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                 <asp:ValidatorCalloutExtender ID="RequiredFieldValidator1_ValidatorCalloutExtender" 
                                                     runat="server" Enabled="True" TargetControlID="RequiredFieldValidator1" 
                                                     PopupPosition="BottomLeft">
                                                 </asp:ValidatorCalloutExtender>
                                                 <asp:RequiredFieldValidator 
                                                     ID="RequiredFieldValidator2" runat="server" 
                                                     ErrorMessage="Please Provide The User Name." 
                                                     ControlToValidate="txtusername" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                 <asp:ValidatorCalloutExtender ID="RequiredFieldValidator2_ValidatorCalloutExtender" 
                                                     runat="server" Enabled="True" TargetControlID="RequiredFieldValidator2" 
                                                     PopupPosition="BottomLeft">
                                                 </asp:ValidatorCalloutExtender>
                                                 <asp:RequiredFieldValidator 
                                                     ID="RequiredFieldValidator3" runat="server" 
                                                     ErrorMessage="Please Provide The User Password." 
                                                     ControlToValidate="txtpswd" Display="None" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                 <asp:ValidatorCalloutExtender ID="RequiredFieldValidator3_ValidatorCalloutExtender" 
                                                     runat="server" Enabled="True" TargetControlID="RequiredFieldValidator3" 
                                                     PopupPosition="BottomLeft">
                                                 </asp:ValidatorCalloutExtender>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                         </tr>
                                         <tr>
                                             <td>
                                            <asp:Button ID="cmdsave" runat="server" class="btn btn-info" 
                                                Text="Submit" /> &nbsp; &nbsp;
                                            <asp:Button ID="cmdclr" runat="server" class="btn btn-default" 
                                                Text="Clear" />
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 &nbsp;</td>
                                         </tr>
                                     </table>
                                 </div>
                            </div>       
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

