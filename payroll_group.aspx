﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="payroll_group.aspx.vb" Inherits="payroll_group" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="A1" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                      aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="payroll_group.aspx">Add New Payroll Group</a>
                      <a class="dropdown-item" href="payroll_group.aspx?mode=V">View Payroll Group List</a>
                    
                    </div>
                  </div>
                </div>

                     <div class="card-body">
                                         <table style="width:100%;">
                               <tr>
                                  <td>
                                      <asp:Panel ID="pnlview" runat="server">
                                          <table style="width: 100%;">
                                                                               <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel2" runat="server">
                                                          <div style="width: 100%; height: 600px">
                                                              <asp:GridView ID="GridView1" runat="server" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="20" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <Columns>
                                                                      <asp:BoundField DataField="sl" HeaderText="Sl">
                                                                      <HeaderStyle Width="30px" />
                                                                      <ItemStyle Width="30px" />
                                                                      </asp:BoundField>
                                                                      <asp:BoundField DataField="group_nm" HeaderText="group Name" />
                                                                    
                                                                      <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                                                          ImageUrl="images/edit.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                                                      <ItemStyle Height="30px" Width="30px" />
                                                                      </asp:ButtonField>
                                                                  </Columns>
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>
                                                          </div>
                                                      </asp:Panel>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                         <table style="width:100%;"><tr><td>Group Name</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr>
                                             <td colspan="3">
                                                 <asp:TextBox ID="txtgroupnm" runat="server" CssClass="form-control"></asp:TextBox>
                                             </td></tr>
                                             <tr>
                                                 <td colspan="3">
                                                     <table style="width:100%;">
                                                         <tr>
                                                             <td width="45%">
                                                                 Emp. PF</td>
                                                             <td width="10%">
                                                                 &nbsp;</td>
                                                             <td width="45%">
                                                                 Emp. ESIC</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 <asp:DropDownList ID="cmbpfappl" runat="server" CssClass="form-control">
                                                                     <asp:ListItem>Yes</asp:ListItem>
                                                                     <asp:ListItem>No</asp:ListItem>
                                                                 </asp:DropDownList>
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 <asp:DropDownList ID="cmbesiappl" runat="server" CssClass="form-control">
                                                                     <asp:ListItem>Yes</asp:ListItem>
                                                                     <asp:ListItem>No</asp:ListItem>
                                                                 </asp:DropDownList>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 Gross(%)</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 Rate/ KM</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 <asp:TextBox ID="txtgross" runat="server" CssClass="form-control"></asp:TextBox>
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 <asp:TextBox ID="txtkmrate" runat="server" CssClass="form-control"></asp:TextBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td colspan="3" style="font-size: 5px">
                                                                 &nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td align="center" bgcolor="#FFFF99" colspan="3" 
                                                                 style="border: 1px solid #000000; color: #800000; font-size: large;">
                                                                 Earnings</td>
                                                         </tr>
                                                         <tr>
                                                             <td colspan="3" style="font-size: 5px">
                                                                 &nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 Basic</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 DA</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 <table style="width:100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbbasictp" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td width="70%" valign="top">
                                                                             <table style="width:100%;">
                                                                                 <tr>
                                                                                     <td width="70%">
                                                                                         <asp:TextBox ID="txtbasic" runat="server" CssClass="form-control" Height="38px"></asp:TextBox>
                                                                                     </td>
                                                                                     <td width="30%">
                                                                                         <asp:CheckBox ID="chkbasic" runat="server" Text="Fix Amount" />
                                                                                     </td>
                                                                                 </tr>
                                                                             </table>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbdatp" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td width="70%" valign="top">
                                                                             <asp:TextBox ID="txtda" runat="server" CssClass="form-control" Height="38px"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 HRA</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 Transport</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbhratp" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td width="50%" valign="top">
                                                                             <asp:TextBox ID="txthra" runat="server" CssClass="form-control" Height="38px"></asp:TextBox>
                                                                         </td>
                                                                         <td width="20%">
                                                                             <asp:CheckBox ID="chkhra" runat="server" Text="Full" />
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbtransporttp" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td width="70%" valign="top">
                                                                             <table style="width:100%;">
                                                                                 <tr>
                                                                                     <td width="70%">
                                                                                         <asp:TextBox ID="txttransport" runat="server" CssClass="form-control" 
                                                                                             Height="38px"></asp:TextBox>
                                                                                     </td>
                                                                                     <td width="30%">
                                                                                         <asp:CheckBox ID="chktafull" runat="server" Text="Full" />
                                                                                     </td>
                                                                                 </tr>
                                                                             </table>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 Child EDN</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 City COMM</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbchildtp" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td width="70%" valign="top">
                                                                             <asp:TextBox ID="txtchild" runat="server" CssClass="form-control" Height="38px"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbcity" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td width="70%" valign="top">
                                                                             <asp:TextBox ID="txtcity" runat="server" CssClass="form-control" Height="38px"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 Mobile</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 Washing</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 <table style="width:100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbmobile" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td valign="top" width="70%">
                                                                             <table style="width:100%;">
                                                                                 <tr>
                                                                                     <td width="70%">
                                                                                         <asp:TextBox ID="txtmobile" runat="server" CssClass="form-control" 
                                                                                             Height="38px"></asp:TextBox>
                                                                                     </td>
                                                                                     <td width="30%">
                                                                                         <asp:CheckBox ID="chkmobile" runat="server" Text="Fix Amount" />
                                                                                     </td>
                                                                                 </tr>
                                                                             </table>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbwashing" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td valign="top" width="70%">
                                                                             <asp:TextBox ID="txtwashing" runat="server" CssClass="form-control" 
                                                                                 Height="38px"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 Medical</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 Food</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbmedical" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td valign="top" width="70%">
                                                                             <asp:TextBox ID="txtmedical" runat="server" CssClass="form-control" 
                                                                                 Height="38px"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbfood" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td valign="top" width="70%">
                                                                             <asp:TextBox ID="txtfood" runat="server" CssClass="form-control" Height="38px"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 Special</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbspecial" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td valign="top" width="70%">
                                                                             <table style="width:100%;">
                                                                                 <tr>
                                                                                     <td width="70%">
                                                                                         <asp:TextBox ID="txtspecial" runat="server" CssClass="form-control" 
                                                                                             Height="38px"></asp:TextBox>
                                                                                     </td>
                                                                                     <td width="30%">
                                                                                         <asp:CheckBox ID="chkspclformula" runat="server" Text="Formula Based" />
                                                                                     </td>
                                                                                 </tr>
                                                                             </table>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td align="center" bgcolor="#FFFF99" colspan="3" 
                                                                 style="border: 1px solid #000000; color: #800000; font-size: large;">
                                                                 Deduction</td>
                                                         </tr>
                                                         <tr>
                                                             <td colspan="3" style="font-size: 5px">
                                                                 &nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 Emp. PF<asp:CheckBox ID="chkemppfoptional" runat="server" Text="Optional" />
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 Emp. ESI</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbemppf" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                                 <asp:ListItem>EARNINGS</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td width="70%" valign="top">
                                                                             <asp:TextBox ID="txtemppf" runat="server" CssClass="form-control" Height="38px"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbempesi" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                                 <asp:ListItem>EARNINGS</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td width="70%" valign="top">
                                                                             <asp:TextBox ID="txtempesi" runat="server" CssClass="form-control" 
                                                                                 Height="38px"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 Comp. PF</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 Comp. ESI</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbcomppf" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                                 <asp:ListItem>EARNINGS</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td width="70%" valign="top">
                                                                             <asp:TextBox ID="txtcomppf" runat="server" CssClass="form-control" 
                                                                                 Height="38px"></asp:TextBox>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbcompesi" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                                 <asp:ListItem>EARNINGS</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td width="70%" valign="top">
                                                                             <table style="width:100%;">
                                                                                 <tr>
                                                                                     <td width="70%">
                                                                                         <asp:TextBox ID="txtcompesi" runat="server" CssClass="form-control" 
                                                                                             Height="38px"></asp:TextBox>
                                                                                     </td>
                                                                                     <td width="30%">
                                                                                         <asp:CheckBox ID="chkcompesicformula" runat="server" Text="Formula Based" />
                                                                                     </td>
                                                                                 </tr>
                                                                             </table>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 PT
                                                                 <asp:CheckBox ID="chkptoptional" runat="server" Text="Optional" />
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 TDS&nbsp;
                                                                 <asp:CheckBox ID="chktdsoptional" runat="server" Text="Optional" />
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbpt" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                                 <asp:ListItem>EARNINGS</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td width="70%" valign="top">
                                                                             <table style="width:100%;">
                                                                                 <tr>
                                                                                     <td width="70%">
                                                                                         <asp:TextBox ID="txtpt" runat="server" CssClass="form-control" Height="38px"></asp:TextBox>
                                                                                     </td>
                                                                                     <td width="30%">
                                                                                         <asp:CheckBox ID="chkptformula" runat="server" Text="Formula Based" />
                                                                                     </td>
                                                                                 </tr>
                                                                             </table>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 <table style="width: 100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbtds" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                                 <asp:ListItem>BASIC</asp:ListItem>
                                                                                 <asp:ListItem>EARNINGS</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td width="70%" valign="top">
                                                                             <table style="width:100%;">
                                                                                 <tr>
                                                                                     <td width="70%">
                                                                                         <asp:TextBox ID="txttds" runat="server" CssClass="form-control" Height="38px"></asp:TextBox>
                                                                                     </td>
                                                                                     <td width="30%">
                                                                                         <asp:CheckBox ID="chktdsformula" runat="server" Text="Formula Based" />
                                                                                     </td>
                                                                                 </tr>
                                                                             </table>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 Insurance</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 <table style="width:100%;">
                                                                     <tr>
                                                                         <td width="30%">
                                                                             <asp:DropDownList ID="cmbinsurance" runat="server" CssClass="form-control">
                                                                                 <asp:ListItem>CTC</asp:ListItem>
                                                                                 <asp:ListItem>GROSS</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                         <td valign="top" width="70%">
                                                                             <table style="width:100%;">
                                                                                 <tr>
                                                                                     <td width="70%">
                                                                                         <asp:TextBox ID="txtinsurance" runat="server" CssClass="form-control" 
                                                                                             Height="38px"></asp:TextBox>
                                                                                     </td>
                                                                                     <td width="30%">
                                                                                         <asp:CheckBox ID="chkinsurance" runat="server" Text="Fix Amount" />
                                                                                     </td>
                                                                                 </tr>
                                                                             </table>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td>
                                                                 <asp:Button ID="cmdsave" runat="server" class="btn btn-primary" Text="Submit" />
                                                                 <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                                     class="btn btn-default" Text="Reset" />
                                                             </td>
                                                             <td>
                                                                 &nbsp;</td>
                                                             <td>
                                                                 &nbsp;</td>
                                                         </tr>
                                                     </table>
                                                 </td>
                                             </tr>
                                             <tr><td>
                                                 <asp:TextBox ID="txtmode" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                 <asp:TextBox ID="txtgroupsl" runat="server" Height="22px" Visible="False" 
                                                     Width="20px"></asp:TextBox>
                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                     ControlToValidate="txtgroupnm" Display="None" 
                                                     ErrorMessage="&lt;b&gt;Required Field&lt;/b&gt;&lt;br/&gt;Please Provide The Group Name" 
                                                     SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                 </td><td>&nbsp;</td><td>&nbsp;</td></tr></table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>         
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

