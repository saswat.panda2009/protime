﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="transcation_compoffapproval.aspx.vb" Inherits="transcation_compoffapproval" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .style1
    {
        height: 26px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Comp-Off Entry . . ."></asp:Label></h3>
                   <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="A1" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                      aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="transcation_compoffapproval.aspx">View Pending List</a>
                                         
                    </div>
                  </div>
                </div>

                     <div class="card-body">
                                         <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table style="width:100%;">
                                                     <tr>
                                                     <td>
                                                      <asp:Panel ID="pnlview" runat="server">
                                          <table style="width: 100%;">
                                           
                                              <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel3" runat="server">
                                                          <div style="width: 100%; height: 600px">
                                                              <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="15" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <Columns>
                                                                  <asp:TemplateField HeaderText="Division">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblDivision" Text='<%#Eval("div_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Apply Date">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblvdt" Text='<%#Eval("v_dt") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Emp. Id">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblEmpId" Text='<%#Eval("emp_code") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Employee Name">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblStaffName" Text='<%#Eval("staf_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="From Date">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblDate" Text='<%#Eval("frm_dt") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="To Date">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblInTime" Text='<%#Eval("to_dt") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="slno" Visible="False">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblslno" Text='<%#Eval("v_no") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                      <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                                                          ImageUrl="images/edit.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                                                      <ItemStyle Height="30px" Width="30px" />
                                                                      </asp:ButtonField>
                                                                  </Columns>
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>
                                                          </div>
                                                      </asp:Panel>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                                     </td>
                                                     </tr>                                    
                                              <tr>
                                                  <td>
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="45%">
                                                                  Employee Id</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td width="45%">
                                                                  <asp:TextBox ID="txtempcode" runat="server" class="form-control" 
                                                                      MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtempcode_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtempcode">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td width="10%">
                                                                  &nbsp; &nbsp; 
                                                              </td>
                                                              <td width="45%">
                                                                  &nbsp;&nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td height="5" style="font-size: 5px" width="45%">
                                                                  &nbsp;</td>
                                                              <td height="5" style="font-size: 5px" width="10%">
                                                                  &nbsp;</td>
                                                              <td height="5" style="font-size: 5px" width="45%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td width="45%">
                                                                  Name</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <asp:TextBox ID="txtstafnm" runat="server" class="form-control" MaxLength="100" 
                                                                      BackColor="White" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtstafnm_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtstafnm">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td class="style1">
                                                                  Department</td>
                                                              <td class="style1">
                                                                  </td>
                                                              <td class="style1">
                                                                  Designation</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtdept" runat="server" BackColor="White" class="form-control" 
                                                                      MaxLength="100" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdept_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdept">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtdesg" runat="server" BackColor="White" class="form-control" 
                                                                      MaxLength="100" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdesg_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdesg">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  From Date</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  To Date</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtfrom" runat="server" class="form-control" 
                                                                      AutoPostBack="True"></asp:TextBox>
                                                                  <asp:CalendarExtender ID="txtfrom_CalendarExtender" runat="server" 
                                                                      Enabled="True" Format="dd/MM/yyyy" PopupButtonID="txtfrom" 
                                                                      TargetControlID="txtfrom">
                                                                  </asp:CalendarExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtto" runat="server" class="form-control" AutoPostBack="True"></asp:TextBox>
                                                                  <asp:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" 
                                                                      Format="dd/MM/yyyy" PopupButtonID="txtto" TargetControlID="txtto">
                                                                  </asp:CalendarExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  No Of Days</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  Leave Type</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtnodays" runat="server" class="form-control" MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtnodays_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtnodays">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:DropDownList ID="cmbtp" runat="server" CssClass="form-control">
                                                                      <asp:ListItem>Normal</asp:ListItem>
                                                                      <asp:ListItem>Emergency</asp:ListItem>
                                                                  </asp:DropDownList>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Leave Name</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtleavename" runat="server" class="form-control" 
                                                                      MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtleavename_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtleavename">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:Label ID="lblleave" runat="server" Font-Bold="True" Font-Names="Baloo" Font-Size="18pt" 
                                                                      Text="Leave Balance :"></asp:Label>
                                                                  &nbsp;
                                                                  <asp:Label ID="lblbalance" runat="server" Font-Bold="True" Font-Names="Baloo" 
                                                                      Font-Size="18pt" Text="0"></asp:Label></td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <div style="width: 100%; height: 300px; overflow: auto;">
                                                                      <asp:GridView ID="dvleave" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                                          AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                                          PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                                          <AlternatingRowStyle CssClass="alt" />
                                                                          <Columns>
                                                                              <asp:TemplateField>
                                                                                  <ItemTemplate>
                                                                                      <asp:CheckBox ID="chk" runat="Server" />
                                                                                  </ItemTemplate>
                                                                                  <ItemStyle Width="50px" />
                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField HeaderText="Date">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lbldt" runat="server" Text='<%#Eval("comp_off_date") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField Visible="False">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblslno" runat="server" Text='<%#Eval("sl") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                          </Columns>
                                                                          <PagerStyle HorizontalAlign="Right" />
                                                                      </asp:GridView>
                                                                  </div>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Reason</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <asp:TextBox ID="txtreason" runat="server" class="form-control" 
                                                                      MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtreason_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtreason">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Contact No</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  Email ID</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtcontact" runat="server" class="form-control" 
                                                                      MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtcontact_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtcontact">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtemail" runat="server" class="form-control" MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtemail_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtemail">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:DropDownList ID="cmbapproved" runat="server" CssClass="form-control">
                                                                      <asp:ListItem>Select Status</asp:ListItem>
                                                                      <asp:ListItem>Approved</asp:ListItem>
                                                                      <asp:ListItem>Cancel</asp:ListItem>
                                                                  </asp:DropDownList>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtcancelnote" runat="server" class="form-control" 
                                                                      MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtcancelnote_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtcancelnote">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtstafsl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                                  <asp:TextBox ID="txtleavetp" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                                  <asp:TextBox ID="txtvno" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:Button ID="cmdsave" runat="server" class="btn btn-primary" Text="Submit" />
                                                                  <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                                      class="btn btn-default" Text="Reset" />
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>       
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

