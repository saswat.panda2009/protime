﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="payroll_payslip.aspx.vb" Inherits="payroll_payslip" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                  
                </div>
                
                     <div class="card-body">
                                     <table style="width: 100%;">
                                <tr>
                                    <td colspan="3">
                                       <div class="alert bg-success" id="divmsg" runat="server"> 
                            <asp:Label ID="lblmsg" runat="server" Text="Label" ForeColor="White"></asp:Label>
                                                    </div></td>
                                </tr>
                                <tr>
                                    <td width="45%">
                                        Employee Id</td>
                                    <td width="10%">
                                        &nbsp;</td>
                                    <td width="45%">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="45%">
                                      <asp:TextBox ID="txtempcode" runat="server" AutoPostBack="True" 
                                          class="form-control" MaxLength="100" placeholder="Emp. Code"></asp:TextBox>
                                      <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" 
                                          CompletionInterval="100" CompletionListCssClass="wordWheel listMain .box" 
                                          CompletionListHighlightedItemCssClass="wordWheel itemsSelected" 
                                          CompletionListItemCssClass="wordWheel itemsMain" CompletionSetCount="10" 
                                          EnableCaching="false" FirstRowSelected="false" MinimumPrefixLength="1" 
                                          ServiceMethod="SearchEmei" TargetControlID="txtempcode">
                                      </asp:AutoCompleteExtender>
                                      <asp:FilteredTextBoxExtender ID="txtempcode_FilteredTextBoxExtender" 
                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                          TargetControlID="txtempcode">
                                      </asp:FilteredTextBoxExtender>
                                    </td>
                                    <td width="10%">
                                        &nbsp;</td>
                                    <td width="45%">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="45%">
                                        For Month</td>
                                    <td width="10%">
                                        &nbsp;</td>
                                    <td width="45%">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                                                  <asp:TextBox ID="txtfrom" runat="server" 
                                            class="form-control"></asp:TextBox>
                                                                  <asp:CalendarExtender ID="txtfrom_CalendarExtender" runat="server" 
                                                                      Enabled="True" Format="dd/MM/yyyy" PopupButtonID="txtfrom" 
                                                                      TargetControlID="txtfrom">
                                                                  </asp:CalendarExtender>
                                                              </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                                                  &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                                      <asp:Button ID="Button1" runat="server" class="btn btn-primary" 
                                            Text="Submit" />
                                                       <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                          class="btn btn-default" Text="Reset" />
                                                  </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>      
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

