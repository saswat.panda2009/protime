﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="payroll_expenseadd.aspx.vb" Inherits="payroll_assign" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Add Expenses . . ."></asp:Label></h3>
                 
                </div>

                     <div class="card-body">
                                    <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table style="width:100%;">
                                                                                         
                                              <tr>
                                                  <td>
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="45%">
                                                                  Expense Date</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  Employee Id</td>
                                                          </tr>
                                                          <tr>
                                                              <td width="45%">
                                                                  <div ID="simple-date2" class="form-group">
                                                                      <asp:TextBox ID="txtfordt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                      <asp:CalendarExtender ID="txtfordt_CalendarExtender" runat="server" 
                                                                          Enabled="True" Format="dd/MM/yyyy" PopupButtonID="txtfordt" 
                                                                          TargetControlID="txtfordt">
                                                                      </asp:CalendarExtender>
                                                                  </div>
                                                              </td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  <table style="width:100%;">
                                                                      <tr>
                                                                          <td width="90%">
                                                                              <asp:TextBox ID="txtempcode" runat="server" class="form-control" 
                                                                                  MaxLength="100"></asp:TextBox>
                                                                              <asp:FilteredTextBoxExtender ID="txtempcode_FilteredTextBoxExtender" 
                                                                                  runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                                  TargetControlID="txtempcode">
                                                                              </asp:FilteredTextBoxExtender>
                                                                          </td>
                                                                          <td width="1%">
                                                                              &nbsp;</td>
                                                                          <td width="9%">
                                                                              <asp:Button ID="cmdsave0" runat="server" class="btn btn-info" Text="Get" 
                                                                                  Width="100%" />
                                                                          </td>
                                                                      </tr>
                                                                  </table>
                                                              </td>
                                                          </tr>
                                                                                                               
                                                          <tr>
                                                              <td width="45%">
                                                                  Name</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <asp:TextBox ID="txtstafnm" runat="server" class="form-control" MaxLength="100" 
                                                                      BackColor="White" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtstafnm_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtstafnm">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Department</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  Designation</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtdept" runat="server" BackColor="White" class="form-control" 
                                                                      MaxLength="100" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdept_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdept">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtdesg" runat="server" BackColor="White" class="form-control" 
                                                                      MaxLength="100" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdesg_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdesg">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Expense Head</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                   
                                                                  Amount</td>
                                                          </tr>

                                                          <tr>
                                                              <td>
                                                                  <asp:DropDownList ID="cmbearnings" runat="server" CssClass="form-control">
                                                                  </asp:DropDownList>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtamount" runat="server" BackColor="White" 
                                                                     placeholder="0.00" class="form-control" MaxLength="100" style="text-align:right;"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtamount_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtamount">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Description</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <asp:TextBox ID="txtdescr" runat="server" BackColor="White" class="form-control" 
                                                                      MaxLength="100" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdescr_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdescr">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Image</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:FileUpload ID="FileUpload1" runat="server" Width="100%" />
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:Button ID="cmdsave1" runat="server" class="btn btn-info" Text="Add" />
                                                                  &nbsp;
                                                                  <asp:Button ID="cmdsave3" runat="server" class="btn btn-info" 
                                                                      Text="Add &amp; Pay" />
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>

                                                          <tr>
                                                              <td colspan="3">
                                                                  <div style="width: 100%; height: 300px; overflow: auto;">
                                                                      <asp:GridView ID="dvstaf" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                                          AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                                          PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                                          <AlternatingRowStyle CssClass="alt" />
                                                                          <Columns>
                                                                              <asp:TemplateField HeaderText="Sl">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblsl" runat="server" Text='<%#Eval("sl") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                  <ItemStyle Width="5%" />
                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField HeaderText="Date">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lbldt" runat="server" Text='<%#Eval("dt") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                  <HeaderStyle Width="20%" />
                                                                                  <ItemStyle Width="20%" />
                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField HeaderText="Expense">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblnm" runat="server" Text='<%#Eval("expense_name") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                  <HeaderStyle Width="30%" />
                                                                                  <ItemStyle Width="30%" />
                                                                              </asp:TemplateField>
                                                                               <asp:TemplateField HeaderText="Remark">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lbldescr" runat="server" Text='<%#Eval("expense_desc") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                  <HeaderStyle Width="45%" />
                                                                                  <ItemStyle Width="45%" />
                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField HeaderText="Amount">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblamt" runat="server" Text='<%#Eval("amt") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                  <ItemStyle HorizontalAlign="Right" Width="15%" />
                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField Visible="False">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblslno" runat="server" Text='<%#Eval("ent_sl") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                               <asp:TemplateField Visible="False">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblpaid" runat="server" Text='<%#Eval("paid") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                              <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                                                                  ImageUrl="~/images/delete.png" Text="Button">
                                                                              <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                                              </asp:ButtonField>
                                                                          </Columns>
                                                                          <PagerStyle HorizontalAlign="Right" />
                                                                      </asp:GridView>
                                                                  </div>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3" style="font-size: 5px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtstafsl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                                 
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  &nbsp;</td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>    
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

