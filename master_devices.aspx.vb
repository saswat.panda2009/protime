﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class master_devices
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtstate.Text = ""
        txtstatecd.Text = ""
        txtip.Text = ""
        txtsrl.Text = ""
        cmbactive.SelectedIndex = 0
        If txtmode.Text = "E" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Device Master (Entry Mode)"
            txtstate.Focus()
        ElseIf txtmode.Text = "M" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Device Master (Edit Mode)"
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Device Master (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT row_number() over(ORDER BY DeviceFName) as 'sl',DeviceFName,SerialNumber,DeviceDirection  FROM Devices WHERE loc_cd=" & loc_cd & " ORDER BY DeviceFName")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If txtmode.Text = "E" Then
            Dim dscheck As DataSet = get_dataset("SELECT DeviceFName FROM Devices WHERE DeviceFName='" & UCase(Trim(txtstate.Text)) & "' AND loc_cd=" & loc_cd & "")
            If dscheck.Tables(0).Rows.Count <> 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Device Name Already Exits');", True)
                txtstate.Focus()
                Exit Sub
            End If
            Dim dscheck1 As DataSet = get_dataset("SELECT SerialNumber FROM Devices WHERE SerialNumber='" & Trim(txtsrl.Text) & "'")
            If dscheck1.Tables(0).Rows.Count <> 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Device Serial No Already Exits');", True)
                txtsrl.Focus()
                Exit Sub
            End If
            start1()
            SQLInsert("INSERT INTO Devices(loc_cd,DeviceFName,DeviceSName,DeviceDirection,SerialNumber,ConnectionType,IpAddress,BaudRate," & _
            "CommKey,ComPort,LastLogDownloadDate,TransactionStamp,LastPing,DeviceType,OpStamp,DownLoadType,Timezone,DeviceLocation," & _
            "TimeOut) VALUES(" & loc_cd & ",'" & UCase(Trim(txtstate.Text)) & "','" & UCase(Trim(txtstate.Text)) & "','" & cmbdevices.Text & "','" & Trim(txtsrl.Text) & _
            "','Tcp/IP','" & Trim(txtip.Text) & "','',0,'','',0,'','Attendance',0,1,330,'',330)")
            close1()
            Me.clr()
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
        ElseIf txtmode.Text = "M" Then
            Dim ds As DataSet = get_dataset("SELECT DeviceFName,SerialNumber FROM Devices WHERE Deviceid=" & Val(txtstatecd.Text) & " AND loc_cd=" & loc_cd & "")
            If ds.Tables(0).Rows.Count <> 0 Then
                If Trim(txtstate.Text) <> ds.Tables(0).Rows(0).Item("DeviceFName") Then
                    Dim dsd As DataSet = get_dataset("SELECT DeviceFName FROM Devices WHERE DeviceFName='" & UCase(Trim(txtstate.Text)) & "'")
                    If dsd.Tables(0).Rows.Count <> 0 Then
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Device Name Already Exits');", True)
                        txtstate.Focus()
                        Exit Sub
                    End If
                End If
                If Trim(txtsrl.Text) <> ds.Tables(0).Rows(0).Item("SerialNumber") Then
                    Dim dscheck1 As DataSet = get_dataset("SELECT SerialNumber FROM devices WHERE SerialNumber='" & Trim(txtsrl.Text) & "'")
                    If dscheck1.Tables(0).Rows.Count <> 0 Then
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Device Serial No Already Exits');", True)
                        txtsrl.Focus()
                        Exit Sub
                    End If
                End If
                start1()
                SQLInsert("UPDATE Devices SET DeviceFName='" & UCase(Trim(txtstate.Text)) & "',SerialNumber='" & Trim(txtsrl.Text) & _
                "',IpAddress='" & Trim(txtip.Text) & "',DeviceDirection='" & cmbdevices.Text & "' WHERE Deviceid=" & Val(txtstatecd.Text) & " and loc_cd=" & loc_cd & "")
                close1()
                Me.clr()
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Modified Succesffuly');", True)
            End If
        End If
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT Deviceid FROM devices WHERE SerialNumber='" & Trim(GridView1.Rows(rw).Cells(2).Text) & "' AND loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtmode.Text = "M"
                Me.clr()
                txtstatecd.Text = Val(ds1.Tables(0).Rows(0).Item("Deviceid"))
                Me.dvsel()
            End If
        End If
    End Sub

    Private Sub dvsel()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT * FROM devices WHERE Deviceid=" & Val(txtstatecd.Text) & " AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtstate.Text = ds1.Tables(0).Rows(0).Item("DeviceFName")
            txtsrl.Text = ds1.Tables(0).Rows(0).Item("SerialNumber")
            txtip.Text = ds1.Tables(0).Rows(0).Item("IpAddress")
            'cmbdevices.Text = ds1.Tables(0).Rows(0).Item("DeviceDirection")
            'If ds1.Tables(0).Rows(0).Item("active") = "Y" Then
            '    cmbactive.SelectedIndex = 0
            'Else
            '    cmbactive.SelectedIndex = 1
            'End If
        End If
    End Sub

    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        txtstate.Focus()
    End Sub
End Class
