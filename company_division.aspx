﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="company_division.aspx.vb" Inherits="company_division" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="A1" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                      aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="company_division.aspx">Add New Division</a>
                      <a class="dropdown-item" href="company_division.aspx?mode=V">View Division List</a>
                    
                    </div>
                  </div>
                </div>

                     <div class="card-body">
                                       <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnlview" runat="server">
                                          <table style="width: 100%;">
                                           
                                              <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel2" runat="server">
                                                          <div style="width: 100%; height: 600px">
                                                              <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="15" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <Columns>
                                                                      <asp:BoundField DataField="sl" HeaderText="Sl">
                                                                      <HeaderStyle Width="30px" />
                                                                      <ItemStyle Width="30px" />
                                                                      </asp:BoundField>
                                                                      <asp:BoundField DataField="div_nm" HeaderText="Division Name" />
                                                                      <asp:BoundField DataField="cont_per" HeaderText="Contact Person" />
                                                                      <asp:BoundField DataField="desg" HeaderText="Designation" />
                                                                      <asp:BoundField DataField="cont_ph1" HeaderText="Contact No" />
                                                                      <asp:BoundField DataField="cont_email" HeaderText="Email Id" />
                                                                      <asp:BoundField DataField="active" HeaderText="Active">
                                                                      <HeaderStyle Width="50px" />
                                                                      <ItemStyle Width="50px" />
                                                                      </asp:BoundField>
                                                                      <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                                                          ImageUrl="images/edit.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                                                      <ItemStyle Height="25px" Width="25px" />
                                                                      </asp:ButtonField>
                                                                  </Columns>
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>
                                                          </div>
                                                      </asp:Panel>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table width="100%">
                                                                                   <tr>
                                                  <td>
                                                      Division Name</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtdivnm" runat="server" class="form-control"></asp:TextBox>
                                                      <asp:FilteredTextBoxExtender ID="txtdivnm_FilteredTextBoxExtender" 
                                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                          TargetControlID="txtdivnm">
                                                      </asp:FilteredTextBoxExtender>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td height="8" style="font-size: 8px">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td height="8">
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="45%">
                                                                  Contact Person</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  Designation</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtcontper" runat="server" class="form-control"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtcontper_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtcontper">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtdesg" runat="server" class="form-control"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdesg_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdesg">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td height="8" style="font-size: 8px">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td height="8">
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="45%">
                                                                  Contact No-1</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  Contact No-2</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtcontact1" runat="server" class="form-control"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtcontact1_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterType="Numbers" 
                                                                      TargetControlID="txtcontact1">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtcont2" runat="server" class="form-control"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtcont2_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtcont2">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td height="8" style="font-size: 8px">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td height="8">
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="45%">
                                                                  Email ID</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  Active</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtemailid" runat="server" class="form-control"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtemailid_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtemailid">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:DropDownList ID="cmbactive" runat="server" class="form-control" 
                                                                      Width="100px">
                                                                      <asp:ListItem Value="Yes"></asp:ListItem>
                                                                      <asp:ListItem Value="No"></asp:ListItem>
                                                                  </asp:DropDownList>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:Button ID="cmdsave" runat="server" class="btn btn-primary" Text="Submit" />
                                                      <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                          class="btn btn-default" Text="Reset" />
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtmode" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtdivsl" runat="server" Height="22px" Visible="False" 
                                                          Width="20px"></asp:TextBox>
                                                      <asp:RequiredFieldValidator ID="valid_state" runat="server" 
                                                          ControlToValidate="txtdivnm" Display="None" 
                                                          ErrorMessage="&lt;b&gt;Required Field&lt;/b&gt;&lt;br/&gt;Please Provide The Division Name." 
                                                          SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                      <asp:ValidatorCalloutExtender ID="valid_state_ValidatorCalloutExtender" 
                                                          runat="server" Enabled="True" PopupPosition="BottomLeft" 
                                                          TargetControlID="valid_state">
                                                      </asp:ValidatorCalloutExtender>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>       
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

