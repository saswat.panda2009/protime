﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class payroll_reports_monthlyabs
    Inherits System.Web.UI.Page

    <System.Web.Script.Services.ScriptMethod(), _
System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            "staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblmsg.Visible = False
        txtfrmdt.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        Me.divisiondisp()
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
    End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)

        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = str & "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If

        txtfrmdt.Text = "01/" & Format(stringtodate(txtfrmdt.Text).Month, "00") & "/" & Format(stringtodate(txtfrmdt.Text).Year, "0000")
        Dim tot_days As Integer = DateTime.DaysInMonth(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month)
        txtto.Text = Format(tot_days, "00") & "/" & Format(stringtodate(txtfrmdt.Text).Month, "00") & "/" & Format(stringtodate(txtfrmdt.Text).Year, "0000")

        Dim dsstaf As DataSet = get_dataset("SELECT staf_sl,staf.staf_nm, staf.emp_code, staf.device_code, division_mst.div_nm, dept_mst.dept_nm FROM staf LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl WHERE staf.emp_status='I'  AND staf.loc_cd= " & loc_cd & "  " & str & "")
        Dim ds1 As DataSet = get_dataset("SELECT atnd.* FROM dept_mst RIGHT OUTER JOIN staf ON dept_mst.dept_sl = staf.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl RIGHT OUTER JOIN atnd ON staf.staf_sl = atnd.staf_sl WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txtto.Text), "dd/MMM/yyyy") & "' AND atnd.loc_cd= " & loc_cd & "  " & str & "  ORDER BY log_dt")
        Dim dsmonthly As DataSet = get_dataset("SELECT * FROM monthly_pay WHERE pay_month=" & stringtodate(txtfrmdt.Text).Month & " AND pay_year=" & stringtodate(txtfrmdt.Text).Year & "")
        Dim dsleave As DataSet = get_dataset("select leave_nm,leave_tp from leave_mst  ORDER BY leave_tp")
        Dim dt As New DataTable
        dt.Columns.Add("Sl.", GetType(String))
        dt.Columns.Add("Staf Name", GetType(String))
        dt.Columns.Add("Emp. Code", GetType(String))
        dt.Columns.Add("Division", GetType(String))
        dt.Columns.Add("Department", GetType(String))


        Dim current As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
        Dim ending As New DateTime(stringtodate(txtto.Text).Year, stringtodate(txtto.Text).Month, stringtodate(txtto.Text).Day)
        While current <= ending
            dt.Columns.Add(current.Date.Day, GetType(String))
            current = current.AddDays(1)
        End While
        dt.Columns.Add("P", GetType(String))
        dt.Columns.Add("A", GetType(String))
        dt.Columns.Add("W", GetType(String))
        For j As Integer = 0 To dsleave.Tables(0).Rows.Count - 1
            dt.Columns.Add(dsleave.Tables(0).Rows(j).Item("leave_nm"), GetType(String))
        Next
        dt.Columns.Add("H", GetType(String))
        dt.Columns.Add("Pay Days", GetType(String))
        dt.Columns.Add("Wages", GetType(String))
        dt.Columns.Add("Basic Amount", GetType(String))
        dt.Columns.Add("DA", GetType(String))
        dt.Columns.Add("HRA", GetType(String))
        dt.Columns.Add("Transport", GetType(String))
        dt.Columns.Add("Washing", GetType(String))
        dt.Columns.Add("Children", GetType(String))
        dt.Columns.Add("City", GetType(String))
        dt.Columns.Add("Mobile", GetType(String))
        dt.Columns.Add("Medical", GetType(String))
        dt.Columns.Add("Food", GetType(String))
        dt.Columns.Add("Special", GetType(String))
        dt.Columns.Add("Fuel", GetType(String))
        dt.Columns.Add("OT Hours", GetType(String))
        dt.Columns.Add("OT", GetType(String))
        dt.Columns.Add("Oth. Earn.", GetType(String))
        dt.Columns.Add("Total Earnings", GetType(String))
        dt.Columns.Add("PF", GetType(String))
        dt.Columns.Add("ESI", GetType(String))
        dt.Columns.Add("PT", GetType(String))
        dt.Columns.Add("TDS", GetType(String))
        dt.Columns.Add("Insurance", GetType(String))
        dt.Columns.Add("Loan / Advance", GetType(String))
        dt.Columns.Add("Oth. Ded.", GetType(String))
        dt.Columns.Add("Total Deduction", GetType(String))
        dt.Columns.Add("Net Total", GetType(String))
        dt.Columns.Add("Comp. PF", GetType(String))
        dt.Columns.Add("Comp. ESIC", GetType(String))
        For i As Integer = 0 To dsstaf.Tables(0).Rows.Count - 1
            Dim tot_p As Integer = 0
            Dim tot_a As Integer = 0
            Dim tot_w As Integer = 0
            Dim tot_l As Integer = 0
            Dim tot_h As Integer = 0
            Dim tot_hr As Integer = 0

            Dim current1 As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
            Dim ending1 As New DateTime(stringtodate(txtto.Text).Year, stringtodate(txtto.Text).Month, stringtodate(txtto.Text).Day)

            Dim dr As DataRow
            dr = dt.NewRow
            dr(0) = i + 1
            dr(1) = Trim(dsstaf.Tables(0).Rows(i).Item("staf_nm"))
            dr(2) = Trim(dsstaf.Tables(0).Rows(i).Item("emp_code"))
            dr(3) = Trim(dsstaf.Tables(0).Rows(i).Item("div_nm"))
            dr(4) = Trim(dsstaf.Tables(0).Rows(i).Item("dept_nm"))
            Dim column_cnt = 5
            While current1 <= ending1
                Dim day_status As String = ""
                Dim intime As String = ""
                Dim outtime As String = ""

                Dim dratnd As DataRow()
                dratnd = ds1.Tables(0).Select("log_dt='" & Format(current1, "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                If dratnd.Length <> 0 Then
                    If dratnd(0).Item("day_status") = "PRESENT" Then
                        day_status = "P"
                        tot_p = tot_p + 1
                    ElseIf dratnd(0).Item("day_status") = "ABSENT" Then
                        day_status = "A"
                        tot_a = tot_a + 1
                    ElseIf dratnd(0).Item("day_status") = "LEAVEDAY" Then
                        day_status = dratnd(0).Item("leave_nm")
                        tot_l = tot_l + 1
                    ElseIf dratnd(0).Item("day_status") = "WEEKLYOFF" Then
                        day_status = "W"
                        tot_w = tot_w + 1
                    ElseIf dratnd(0).Item("day_status") = "HOLIDAY" Then
                        day_status = "H"
                        tot_h = tot_h + 1
                    End If
                End If
                dr(column_cnt) = day_status
                column_cnt = column_cnt + 1
                current1 = current1.AddDays(1)
            End While
            Dim drpay As DataRow()
            drpay = dsmonthly.Tables(0).Select("staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
            If drpay.Length <> 0 Then
                column_cnt = column_cnt + 3
                dr("P") = drpay(0).Item("tot_present") + (drpay(0).Item("tot_half") * 0.5)
                dr("A") = drpay(0).Item("tot_absnt")
                dr("W") = drpay(0).Item("tot_week")
                For k As Integer = 0 To dsleave.Tables(0).Rows.Count - 1
                    If dsleave.Tables(0).Rows(k).Item("leave_tp") = 1 Then
                        dr(column_cnt) = drpay(0).Item("l1")
                    ElseIf dsleave.Tables(0).Rows(k).Item("leave_tp") = 2 Then
                        dr(column_cnt) = drpay(0).Item("l2")
                    ElseIf dsleave.Tables(0).Rows(k).Item("leave_tp") = 3 Then
                        dr(column_cnt) = drpay(0).Item("l3")
                    ElseIf dsleave.Tables(0).Rows(k).Item("leave_tp") = 4 Then
                        dr(column_cnt) = drpay(0).Item("l4")
                    ElseIf dsleave.Tables(0).Rows(k).Item("leave_tp") = 5 Then
                        dr(column_cnt) = drpay(0).Item("l5")
                    ElseIf dsleave.Tables(0).Rows(k).Item("leave_tp") = 6 Then
                        dr(column_cnt) = drpay(0).Item("l6")
                    ElseIf dsleave.Tables(0).Rows(k).Item("leave_tp") = 7 Then
                        dr(column_cnt) = drpay(0).Item("l7")
                    End If
                    column_cnt = column_cnt + 1
                Next
                dr("H") = drpay(0).Item("tot_holi")
                dr("Pay Days") = drpay(0).Item("pay_days")
                dr("Wages") = Format(Val(drpay(0).Item("wages")), "#####0.00")
                dr("Basic Amount") = Format(Val(drpay(0).Item("basic_amt")), "#####0.00")
                dr("DA") = Format(Val(drpay(0).Item("da_amt")), "#####0.00")
                dr("HRA") = Format(Val(drpay(0).Item("hra_amt")), "#####0.00")
                dr("Transport") = Format(Val(drpay(0).Item("transport_amt")), "#####0.00")
                dr("Washing") = Format(Val(drpay(0).Item("washing_amt")), "#####0.00")
                dr("Children") = Format(Val(drpay(0).Item("child_amt")), "#####0.00")
                dr("City") = Format(Val(drpay(0).Item("city_amt")), "#####0.00")
                dr("Mobile") = Format(Val(drpay(0).Item("mobile_amt")), "#####0.00")
                dr("Medical") = Format(Val(drpay(0).Item("medical")), "#####0.00")
                dr("Food") = Format(Val(drpay(0).Item("food_amt")), "#####0.00")
                dr("Special") = Format(Val(drpay(0).Item("spcl_amt")), "#####0.00")
                dr("Fuel") = Format(Val(drpay(0).Item("fuel_amt")), "#####0.00")
                dr("OT Hours") = Format(Val(drpay(0).Item("ot")), "#####0.00")
                dr("OT") = Format(Val(drpay(0).Item("ot_amt")), "#####0.00")
                dr("Oth. Earn.") = Format(Val(drpay(0).Item("othr_earnings")), "#####0.00")
                dr("Total Earnings") = Format(Val(drpay(0).Item("tot_earnings")), "#####0.00")
                dr("PF") = Format(Val(drpay(0).Item("pf_amt")), "#####0.00")
                dr("ESI") = Format(Val(drpay(0).Item("esic_amt")), "#####0.00")
                dr("PT") = Format(Val(drpay(0).Item("pt_amt")), "#####0.00")
                dr("TDS") = Format(Val(drpay(0).Item("tds_amt")), "#####0.00")
                dr("Insurance") = Format(Val(drpay(0).Item("insurance_amt")), "#####0.00")
                dr("Loan / Advance") = Format(Val(drpay(0).Item("loan_amt")), "#####0.00")
                dr("Oth. Ded.") = Format(Val(drpay(0).Item("othr_deuction")), "#####0.00")
                dr("Total Deduction") = Format(Val(drpay(0).Item("tot_deduction")), "#####0.00")
                dr("Net Total") = Format(Val(drpay(0).Item("total")), "#####0.00")
                dr("Comp. PF") = Format(Val(drpay(0).Item("comp_pf_amt")), "#####0.00")
                dr("Comp. ESIC") = Format(Val(drpay(0).Item("comp_esic_amt")), "#####0.00")
            End If
            dt.Rows.Add(dr)
        Next
        If dt.Rows.Count <> 0 Then
            Dim GridView112 As New GridView()
            GridView112.AllowPaging = False
            GridView112.DataSource = dt
            GridView112.DataBind()
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=MonthlyPaymentRegister.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            For i As Integer = 0 To GridView112.Rows.Count - 1
                GridView112.Rows(i).Attributes.Add("class", "textmode")
            Next
            GridView112.RenderControl(hw)
            'style to format numbers to string
            Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()
        End If
    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub
End Class
