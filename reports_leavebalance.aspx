﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="reports_leavebalance.aspx.vb" Inherits="reports_leavebalance" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Leave Balance Register . . ."></asp:Label></h3>
               
                </div>

                     <div class="card-body">
                  <table style="width:100%;"><tr><td width="20%" valign="top"><asp:TextBox ID="txtfrmdt" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtfrmdt" PopupButtonID="txtfrmdt" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:FilteredTextBoxExtender ID="TextBox1_FilteredTextBoxExtender" 
                            runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                            TargetControlID="txtfrmdt">
                        </asp:FilteredTextBoxExtender></td><td width="20%" valign="top"><asp:TextBox ID="txttodt" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:CalendarExtender ID="txttodt_CalendarExtender" runat="server" TargetControlID="txttodt" PopupButtonID="txttodt" 
                                                                      Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:FilteredTextBoxExtender ID="txttodt_FilteredTextBoxExtender" 
                            runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                            TargetControlID="txttodt">
                        </asp:FilteredTextBoxExtender></td>
                   <td width="25%" valign="top">
                      <asp:DropDownList ID="cmbdivsion" 
                            runat="server" AutoPostBack="True" 
                                                                      class="form-control" Height="42px">
                                                                  </asp:DropDownList></td>
                   <td width="35%" valign="top"> 
                   <table style="width:100%;"><tr><td width="90%"> <asp:TextBox ID="txtdept" 
                           runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox></td><td width="10%">
                       <asp:ImageButton ID="btnselect" runat="server" ImageUrl="~/images/select.png" 
                           Width="100%"></asp:ImageButton></td></tr></table>
</td></tr>
                                                                      <tr><td colspan="4"> <asp:Panel ID="pnlsearch" runat="server">
                                                      <table style="width: 100%;">
                                                  
                                              <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel2" runat="server">
                                                          <div style="width: 100%; height: 500px; overflow: auto;">
                                                              <asp:GridView ID="dvdept" runat="server" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  Width="100%" PageSize="6">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <Columns>
                                                                  <asp:TemplateField>
                                                                  <ItemTemplate>
                                                                  <asp:CheckBox  runat="Server" ID="chk"/>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Department Name">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lbldept" Text='<%#Eval("dept_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>                                                                   
                                                              </Columns>
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>
                                                          </div>
                                                      </asp:Panel>
                                                  </td>
                                              </tr>
                                          </table>
                                                      </asp:Panel>
                                                      <asp:CollapsiblePanelExtender ID="pnlsearch_CollapsiblePanelExtender" 
                                                          runat="server" Enabled="True" TargetControlID="pnlsearch" CollapsedSize="0"
    ExpandedSize="500"
    Collapsed="True"
    ExpandControlID="btnselect"
    CollapseControlID="btnselect"
    AutoCollapse="False"
    AutoExpand="False"
    TextLabelID="Label1"
    CollapsedText="Show Details..."
    ExpandedText="Hide Details" 
    ImageControlID="Image1"
    ExpandDirection="Vertical">
                                                      </asp:CollapsiblePanelExtender></td></tr>
                              <tr>
                                  <td>
                                      <asp:TextBox ID="txtempcode" runat="server" AutoPostBack="True" 
                                          class="form-control" MaxLength="100" placeholder="Emp. Code"></asp:TextBox>
                                      <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" 
                                          CompletionInterval="100" CompletionListCssClass="wordWheel listMain .box" 
                                          CompletionListHighlightedItemCssClass="wordWheel itemsSelected" 
                                          CompletionListItemCssClass="wordWheel itemsMain" CompletionSetCount="10" 
                                          EnableCaching="false" FirstRowSelected="false" MinimumPrefixLength="1" 
                                          ServiceMethod="SearchEmei" TargetControlID="txtempcode">
                                      </asp:AutoCompleteExtender>
                                      <asp:FilteredTextBoxExtender ID="txtempcode_FilteredTextBoxExtender" 
                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                          TargetControlID="txtempcode">
                                      </asp:FilteredTextBoxExtender>
                                  </td>
                                  <td align="right">
                                      <asp:Button ID="cmdsearch" runat="server" class="btn btn-primary" 
                                          Text="Generate" />
                                           
                                  </td>
                                  <td>
                                     <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/xcel.png"></asp:ImageButton>
                                  </td>
                                  <td>
                                     <asp:Label ID="lblmsg" runat="server" Text="Label"></asp:Label>
                                      <asp:TextBox ID="txtdeptcd" runat="server" Height="22px" Visible="False" 
                                          Width="20px"></asp:TextBox>
                                      <asp:TextBox ID="txtdivsl" runat="server" Height="22px" Visible="False" 
                                          Width="20px"></asp:TextBox></td>
                              </tr>
                              <tr><td>&nbsp;</td><td>&nbsp;</td>
                   <td>
                       &nbsp;</td>
                   <td>&nbsp;</td></tr>
                   <tr>
                       <td colspan="4">
                          <div style="width: 100%; height: 500px; overflow: auto;">
                                                              <asp:GridView ID="dvdata" runat="server" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="15" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>


                                        </div></td>
                   </tr>
                 </table>            
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

