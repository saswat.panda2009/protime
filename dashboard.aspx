﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="dashboard.aspx.vb" Inherits="dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        function UpdateTime(data,label) {
        
// Bar Chart Example
var ctx = document.getElementById("myBarChart1");
var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: label,
    datasets: [{
      label: "No Of Employee : ",
      backgroundColor: "#4e73df",
      hoverBackgroundColor: "#2e59d9",
      borderColor: "#4e73df",
      data: data,
    }],
  },
  options: {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'month'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 6
        },
        maxBarThickness: 25,
      }],
      yAxes: [{
        ticks: {
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return number_format(value);
          }
        },
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        }
      }],
    },
    legend: {
      display: false
    },
    tooltips: {
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel + number_format(tooltipItem.yLabel);
        }
      }
    },
  }
});


        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid" id="container-wrapper">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="./">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
            </ol>
          </div>

          <div class="row mb-3">
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card h-100">
                <div class="card-body">
                  <div class="row align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-uppercase mb-1">Total Division</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><asp:Label ID="lbl1" runat="server" Text="Label"></asp:Label></div>                     
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-building fa-2x text-primary"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

             <!-- New User Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-uppercase mb-1">Total Employee</div>
                      <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><asp:Label ID="lbl2" runat="server" Text="Label"></asp:Label></div>
                     
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-users fa-2x text-info"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Earnings (Annual) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-uppercase mb-1">Today Present</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><asp:Label ID="lbl3" runat="server" Text="Label"></asp:Label></div>
                   
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-users fa-2x text-success"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
           
            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-uppercase mb-1">Today Absents</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><asp:Label ID="lbl4" runat="server" Text="Label"></asp:Label></div>
                   
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-users fa-2x text-danger"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Area Chart -->
            <div class="col-xl-6 col-lg-7">
              <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Todays Attendance Report</h6>
                 
                </div>
                <div class="card-body">
                    <div class="chart-bar">
                    <canvas id="myBarChart1"></canvas>
                  </div>
                </div>
              </div>
            </div>
            <!-- Pie Chart -->
            <div class="col-xl-6 col-lg-5">
              <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Division Wise Attendance</h6>
                 
                </div>
                <div class="card-body" style="height: 360px">
                <div style="overflow: scroll; height: 300px;">  
                  <asp:GridView ID="GridView1" runat="server" 
                                                                  
                        AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  AutoGenerateColumns="False" 
                        CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="15" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <Columns>
                                                                      <asp:BoundField HeaderText="Sl" DataField="sl">
                                                                      </asp:BoundField>
                                                                      <asp:BoundField HeaderText="Division" DataField="div_nm" />
                                                                      <asp:BoundField HeaderText="Total" DataField="Total">
                                                                      </asp:BoundField>
                                                                      <asp:BoundField HeaderText="Present" DataField="Present" />
                                                                      <asp:BoundField HeaderText="Absent" DataField="Absent" />
                                                                  </Columns>
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>
                </div> 
                
                </div>
              
              </div>
            </div>
            <!-- Invoice Example -->
            <div class="col-xl-12 col-lg-7 mb-4">
              <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Device List</h6>
                  <a class="m-0 float-right btn btn-info btn-sm" href="#">Refresh <i
                      class="fas fa-refresh"></i></a>
                </div>
                <div class="table-responsive">
                  <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                      <tr>
                        <th>Device Name</th>
                        <th>Serial No</th>
                        <th>Last Ping</th>
                        <th>Status</th>
                      
                      </tr>
                    </thead>
                    <tbody>
                          <asp:Repeater ID="Repeater1" runat="server">
                          <ItemTemplate>
            <tr>
                <td><%# Eval("DeviceFName")%></td>
                <td><%# Eval("SerialNumber")%></td>
               <td><%# Eval("LastPing")%></td>
               <td><%# Eval("Status")%></td>               
            </tr>
            </ItemTemplate>
                          </asp:Repeater>
                     
                    </tbody>
                  </table>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>
            
          </div>
          <!--Row-->

        
        </div>
</asp:Content>

