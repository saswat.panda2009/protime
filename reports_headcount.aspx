﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="reports_headcount.aspx.vb" Inherits="reports_absentee" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" 
                           Text="Head Count Register . . ."></asp:Label></h3>
               
                </div>

                     <div class="card-body">
                                                 <table style="width:100%;"><tr><td width="20%" valign="top"><asp:TextBox ID="txtfrmdt" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtfrmdt" PopupButtonID="txtfrmdt" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:FilteredTextBoxExtender ID="TextBox1_FilteredTextBoxExtender" 
                            runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                            TargetControlID="txtfrmdt">
                        </asp:FilteredTextBoxExtender></td><td width="20%" valign="top"><asp:TextBox ID="txttodt" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:CalendarExtender ID="txttodt_CalendarExtender" runat="server" TargetControlID="txttodt" PopupButtonID="txttodt" 
                                                                      Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:FilteredTextBoxExtender ID="txttodt_FilteredTextBoxExtender" 
                            runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                            TargetControlID="txttodt">
                        </asp:FilteredTextBoxExtender></td>
                   <td width="25%" valign="top">
                      &nbsp;
                                      <asp:Button ID="cmdsearch" runat="server" class="btn btn-primary" 
                                          Text="Generate" />
                                           
                                  &nbsp;
                                     <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/xcel.png"></asp:ImageButton>
                                                     </td>
                   <td width="35%" valign="top"> 
                   <table style="width:100%;"><tr><td width="90%"> 
                                     <asp:Label ID="lblmsg" runat="server" Text="Label"></asp:Label>
                                      <asp:TextBox ID="txtdeptcd" runat="server" Height="22px" Visible="False" 
                                          Width="20px"></asp:TextBox>
                                      <asp:TextBox ID="txtdivsl" runat="server" Height="22px" Visible="False" 
                                          Width="20px"></asp:TextBox></td><td width="10%">
                           &nbsp;</td></tr></table>
</td></tr>
                              <tr><td>&nbsp;</td><td>&nbsp;</td>
                   <td>
                       &nbsp;</td>
                   <td>&nbsp;</td></tr>
                   <tr>
                       <td colspan="4">
                          <div style="width: 100%; height: 500px; overflow: auto;">
                                                              <asp:GridView ID="dvdata" runat="server" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="15" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>


                                        </div></td>
                   </tr>
                 </table>       
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

