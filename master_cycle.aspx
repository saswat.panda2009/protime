﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="master_cycle.aspx.vb" Inherits="master_state" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="A1" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                      aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="master_cycle.aspx">Add New Shift Cycle</a>
                      <a class="dropdown-item" href="master_cycle.aspx?mode=V">View Shift Cycle List</a>
                    
                    </div>
                  </div>
                </div>

                     <div class="card-body">
                                    <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnlview" runat="server">
                                          <table style="width: 100%;">
                                                                                       <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel2" runat="server">
                                                          <div style="width: 100%; height: 600px">
                                                              <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="15" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <Columns>
                                                                      <asp:BoundField DataField="sl" HeaderText="Sl">
                                                                      <HeaderStyle Width="30px" />
                                                                      <ItemStyle Width="30px" />
                                                                      </asp:BoundField>
                                                                      <asp:BoundField DataField="cycle_nm" HeaderText="Shift Cycle Name" >
                                                                      <HeaderStyle HorizontalAlign="Left" />
                                                                      </asp:BoundField>
                                                                      <asp:BoundField DataField="active" HeaderText="Active">
                                                                      <HeaderStyle Width="50px" />
                                                                      <ItemStyle Width="50px" />
                                                                      </asp:BoundField>
                                                                      <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                                                          ImageUrl="images/edit.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                                                      <ItemStyle Height="30px" Width="30px" />
                                                                      </asp:ButtonField>
                                                                  </Columns>
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>
                                                          </div>
                                                      </asp:Panel>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table width="100%">
                                                                                     <tr>
                                                  <td>
                                                      Cycle Name</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtcycle" runat="server" class="form-control" required></asp:TextBox>
                                                                                                      </td>
                                              </tr>
                                              <tr>
                                                  <td height="8" style="font-size: 8px">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td align="center" style="font-size: 8px;" width="20%">
                                                                  &nbsp;</td>
                                                              <td align="center" style="font-size: 8px;" width="60%">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px" width="20%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtsl" runat="server" class="form-control" BackColor="White" 
                                                                      ReadOnly="True"></asp:TextBox>
                                                                
                                                              </td>
                                                              <td>
                                                                  <asp:DropDownList ID="cmbshift" runat="server" 
                                                                      class="form-control">
                                                                  </asp:DropDownList>
                                                              </td>
                                                              <td align="right">
                                                                  <asp:Button ID="cmdnext" runat="server" class="btn btn-success" 
                                                                      Text="Submit" Width="95%" />
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3" style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <div style="width: 100%; height: 300px; overflow: auto;">
                                                                      <asp:GridView ID="dvstaf" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                                          AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                                          PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                                          <AlternatingRowStyle CssClass="alt" />
                                                                          <Columns>
                                                                              <asp:TemplateField HeaderText="Sl">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblsl" runat="server" Text='<%#Eval("sl") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                  <ItemStyle Width="20%" />
                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField HeaderText="Shift Name">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblshift" runat="server" Text='<%#Eval("Shift") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                  <ItemStyle Width="60%" />
                                                                              </asp:TemplateField>
                                                                               <asp:TemplateField Visible="False">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblshift_sl" runat="server" Text='<%#Eval("Shift_sl") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                               <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                                                          ImageUrl="images/delete.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                                                      <ItemStyle Height="30px" Width="30px" />
                                                                      </asp:ButtonField>
                                                                             
                                                                          </Columns>
                                                                          <PagerStyle HorizontalAlign="Right" />
                                                                      </asp:GridView>
                                                                  </div>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td style="font-size: 8px">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      Active</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:DropDownList ID="cmbactive" runat="server" class="form-control" 
                                                          Width="100px">
                                                          <asp:ListItem Value="Yes"></asp:ListItem>
                                                          <asp:ListItem Value="No"></asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:Button ID="cmdsave" runat="server" class="btn btn-primary" Text="Submit" />
                                                      <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                          class="btn btn-default" Text="Reset" />
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtmode" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtstatecd" runat="server" Height="22px" Visible="False" 
                                                          Width="20px"></asp:TextBox>
                                                    
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>      
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

