﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="payroll_reports_earnings.aspx.vb" Inherits="payroll_reports_monthlyabs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Monthly Earnings Ledger"></asp:Label></h3>              
                </div>

                     <div class="card-body">
                                        <div class="card-body">
                                                     <table style="width:100%;"><tr><td width="20%" valign="top"><asp:TextBox ID="txtfrmdt" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtfrmdt" PopupButtonID="txtfrmdt" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:FilteredTextBoxExtender ID="TextBox1_FilteredTextBoxExtender" 
                            runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                            TargetControlID="txtfrmdt">
                        </asp:FilteredTextBoxExtender>
                                           
                                                         &nbsp;
                             
                                                         </td><td width="25%" valign="top">
                      <asp:DropDownList ID="cmbdivsion" 
                            runat="server" AutoPostBack="True" 
                                                                      class="form-control" Height="42px">
                                                                  </asp:DropDownList></td>
                                                                  <td width="25%" valign="top">
                      <asp:DropDownList ID="cmbhead" 
                            runat="server" AutoPostBack="True" 
                                                                      class="form-control" Height="42px">
                                                                  </asp:DropDownList></td>
                                                                  <td width="25%" valign="top">
                                      <asp:TextBox ID="txtempcode" runat="server" AutoPostBack="True" 
                                          class="form-control" MaxLength="100" placeholder="Emp. Code"></asp:TextBox>
                                      <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" 
                                          CompletionInterval="100" CompletionListCssClass="wordWheel listMain .box" 
                                          CompletionListHighlightedItemCssClass="wordWheel itemsSelected" 
                                          CompletionListItemCssClass="wordWheel itemsMain" CompletionSetCount="10" 
                                          EnableCaching="false" FirstRowSelected="false" MinimumPrefixLength="1" 
                                          ServiceMethod="SearchEmei" TargetControlID="txtempcode">
                                      </asp:AutoCompleteExtender>
                                      <asp:FilteredTextBoxExtender ID="txtempcode_FilteredTextBoxExtender" 
                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                          TargetControlID="txtempcode">
                                      </asp:FilteredTextBoxExtender>
                                     <asp:Label ID="lblmsg" runat="server" Text="Label"></asp:Label>
                                      <asp:TextBox ID="txtdeptcd" runat="server" Height="22px" Visible="False" 
                                          Width="20px"></asp:TextBox>
                                      <asp:TextBox ID="txtdivsl" runat="server" Height="22px" Visible="False" 
                                          Width="20px"></asp:TextBox><asp:TextBox ID="txtto" runat="server" 
                                         Visible="False"></asp:TextBox>
                        <asp:CalendarExtender ID="txtto_CalendarExtender" runat="server" TargetControlID="txtto" 
                                         PopupButtonID="txtfrmdt" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:FilteredTextBoxExtender ID="txtto_FilteredTextBoxExtender" 
                            runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                            TargetControlID="txtto">
                        </asp:FilteredTextBoxExtender></td>
                                                                  <td width="25%" valign="top">
                                      <asp:Button ID="cmdsearch" runat="server" class="btn btn-primary" 
                                          Text="Generate" />
                                           
                                                         </td>
                 </tr>
                              <tr>
                                  <td colspan="5">
                                     <div style="width: 100%; height: 500px; overflow: auto;">
                                                              <asp:GridView ID="dvdata" runat="server" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="15" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>


                                        </div></td>
                              </tr>
                   <tr>
                       <td colspan="5">
                           &nbsp;</td>
                   </tr>
                 </table>  
                    </div>  
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

