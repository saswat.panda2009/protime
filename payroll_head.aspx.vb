﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class payroll_head
    Inherits System.Web.UI.Page
  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtcity.Text = ""
        txtstatecd.Text = ""
        txtcitycd.Text = ""
        cmbactive.SelectedIndex = 0
        If txtmode.Text = "E" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Earnings / Deduction Head Master (Entry Mode)"
            txtcity.Focus()
        ElseIf txtmode.Text = "M" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Earnings / Deduction Head Master (Edit Mode)"
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Earnings / Deduction Head Master (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub dvdisp()
        Dim ds1 As DataSet = get_dataset("SELECT row_number() over(ORDER BY head_name) as 'sl',head_name,(case when head_mst.type='1' Then 'Earnings' WHEN head_mst.type='2' Then 'Deduction' END)as 'tp',(case when head_mst.active='Y' Then 'Yes' WHEN head_mst.active='N' Then 'No' END)as 'active' FROM head_mst ORDER BY head_name")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        If cmbstate.SelectedIndex = 0 Then
            cmbstate.Focus()
            Exit Sub
        End If
        If txtmode.Text = "E" Then
            Dim dscheck As DataSet = get_dataset("SELECT head_name FROM head_mst WHERE head_name='" & UCase(Trim(txtcity.Text)) & "'")
            If dscheck.Tables(0).Rows.Count <> 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Earnings / Deduction Head Name Already Exits');", True)
                txtcity.Focus()
                Exit Sub
            End If
            txtcitycd.Text = "1"
            Dim ds1 As DataSet = get_dataset("SELECT max(head_sl) FROM head_mst ")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                txtcitycd.Text = ds1.Tables(0).Rows(0).Item(0) + 1
            End If
            start1()
            SQLInsert("INSERT INTO head_mst(head_sl,type,head_name,active) VALUES(" & Val(txtcitycd.Text) & _
            ",'" & vb.Left(Trim(cmbstate.SelectedIndex), 1) & "','" & UCase(Trim(txtcity.Text)) & "','" & vb.Left(cmbactive.Text, 1) & "')")
            close1()
            Me.clr()
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
        ElseIf txtmode.Text = "M" Then
            Dim ds As DataSet = get_dataset("SELECT head_name FROM head_mst WHERE head_sl=" & Val(txtcitycd.Text) & "")
            If ds.Tables(0).Rows.Count <> 0 Then
                If Trim(txtcity.Text) <> ds.Tables(0).Rows(0).Item("head_name") Then
                    Dim dsd As DataSet = get_dataset("SELECT head_name FROM head_mst WHERE head_name='" & UCase(Trim(txtcity.Text)) & "'")
                    If dsd.Tables(0).Rows.Count <> 0 Then
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Earnings / Deduction Head Name Already Exits');", True)
                        txtcity.Focus()
                        Exit Sub
                    End If
                End If
                start1()
                SQLInsert("UPDATE head_mst SET head_name='" & UCase(Trim(txtcity.Text)) & "',active='" & _
                vb.Left(cmbactive.Text, 1) & "',type='" & vb.Left(Trim(cmbstate.SelectedIndex), 1) & "' WHERE head_sl=" & Val(txtcitycd.Text) & "")
                close1()
                Me.clr()
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Modified Succesffuly');", True)
            End If
        End If
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT head_sl FROM head_mst WHERE head_name='" & Trim(GridView1.Rows(rw).Cells(1).Text) & "'")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtmode.Text = "M"
                Me.clr()
                txtcitycd.Text = Val(ds1.Tables(0).Rows(0).Item("head_sl"))
                Me.dvsel()
            End If
        End If
    End Sub

    Private Sub dvsel()
        Dim ds1 As DataSet = get_dataset("SELECT head_mst.* FROM head_mst WHERE head_sl=" & Val(txtcitycd.Text) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtcity.Text = ds1.Tables(0).Rows(0).Item("head_name")
            cmbstate.SelectedIndex = Val(ds1.Tables(0).Rows(0).Item("type"))
            If ds1.Tables(0).Rows(0).Item("active") = "Y" Then
                cmbactive.SelectedIndex = 0
            Else
                cmbactive.SelectedIndex = 1
            End If
        End If
    End Sub

    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        txtcity.Focus()
    End Sub
End Class
