﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class transcation_documentation
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtempcode.Text = ""
        txtstafsl.Text = ""
        txtstafnm.Text = ""
        txtdept.Text = ""
        txtdesg.Text = ""
        txtdocuments.Text = ""
        dvstaf.DataSource = Nothing
        dvstaf.DataBind()
    End Sub

    Protected Sub cmdsave0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave0.Click
        Dim ds1 As DataSet = get_dataset("SELECT staf.staf_sl,staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm FROM desg_mst RIGHT OUTER JOIN staf ON desg_mst.desg_sl = staf.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl  WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd= " & CType(Session("loc_cd"), Integer) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            Me.clr()
            txtstafsl.Text = ds1.Tables(0).Rows(0).Item("staf_sl")
            txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            txtstafnm.Text = ds1.Tables(0).Rows(0).Item("staf_nm")
            txtdept.Text = ds1.Tables(0).Rows(0).Item("dept_nm")
            txtdesg.Text = ds1.Tables(0).Rows(0).Item("desg_nm")
            Me.dvdisp()
        End If
    End Sub

    Private Sub dvdisp()
        Dim ds As DataSet = get_dataset("SELECT Row_number() OVER(Order by doc_sl) as sl,* FROM staff_documentation WHERE staf_sl=" & Val(txtstafsl.Text) & "")
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
    End Sub

    Protected Sub cmdget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdget.Click
        Dim filePath As String = FileUpload1.PostedFile.FileName
        Dim filename As String = Path.GetFileName(filePath)
        Dim ext As String = Path.GetExtension(filename)
        Dim contenttype As String = String.Empty

        'Set the contenttype based on File Extension
        Select Case LCase(ext)
            Case ".jpg"
                contenttype = "image/jpg"
                Exit Select
            Case ".jpeg"
                contenttype = "image/jpg"
                Exit Select
            Case ".png"
                contenttype = "image/png"
                Exit Select
            Case ".gif"
                contenttype = "image/gif"
                Exit Select
            Case ".docx"
                contenttype = "application/vnd.ms-word"
                Exit Select
            Case ".pdf"
                contenttype = "application/pdf"
                Exit Select
        End Select
        If contenttype <> String.Empty Then
            Dim fs As Stream = FileUpload1.PostedFile.InputStream
            Dim br As New BinaryReader(fs)
            Dim bytes As Byte() = br.ReadBytes(fs.Length)

            'insert the file into database
            Dim strQuery As String = "insert into staff_documentation(staf_sl, document_heading, ContentType,document_image,document_nm) values (@staf_sl, @document_heading, @ContentType,@document_image,@document_nm)"
            Dim cmd As SqlCommand = New SqlCommand(strQuery)
            cmd.Parameters.Add("@staf_sl", SqlDbType.Int).Value = Val(txtstafsl.Text)
            cmd.Parameters.Add("@document_heading", SqlDbType.VarChar).Value = Trim(txtdocuments.Text)
            cmd.Parameters.Add("@ContentType", SqlDbType.VarChar).Value = contenttype
            cmd.Parameters.Add("@document_nm", SqlDbType.VarChar).Value = filename
            cmd.Parameters.Add("@document_image", SqlDbType.Binary).Value = bytes
            InsertUpdateData(cmd)
            Me.dvdisp()
            txtdocuments.Text = ""
            txtdocuments.Focus()
        Else
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('File format not recognised. Upload Image formats');", True)
            Exit Sub
        End If
    End Sub

    Public Function InsertUpdateData(ByVal cmd As SqlCommand) As Boolean
        Dim strConnString As String = System.Configuration.ConfigurationManager.ConnectionStrings("dbnm").ConnectionString
        Dim con As New SqlConnection(strConnString)
        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Response.Write(ex.Message)
            Return False
        Finally
            con.Close()
            con.Dispose()
        End Try
    End Function


    Protected Sub dvstaf_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dvstaf.RowCommand
        Dim rw As Integer = e.CommandArgument
        Dim lbl As Label = dvstaf.Rows(rw).FindControl("lblid")
        If e.CommandName = "view" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM staff_documentation WHERE doc_sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                Dim strQuery As String = "select * from staff_documentation where doc_sl=@id"
                Dim cmd As SqlCommand = New SqlCommand(strQuery)
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = Val(lbl.Text)
                Dim dt As DataTable = GetData(cmd)
                If dt IsNot Nothing Then
                    download(dt)
                End If
            End If
        ElseIf e.CommandName = "delete_data" Then
            Dim ds1 As DataSet = get_dataset("SELECT * FROM staff_documentation WHERE doc_sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                start1()
                SQLInsert("delete from staff_documentation WHERE doc_sl=" & lbl.Text & "")
                close1()
                Me.dvdisp()
            End If
        End If
    End Sub

    Public Function GetData(ByVal cmd As SqlCommand) As DataTable
        Dim dt As New DataTable
        Dim strConnString As String = System.Configuration.ConfigurationManager.ConnectionStrings("dbnm").ConnectionString
        Dim con As New SqlConnection(strConnString)
        Dim sda As New SqlDataAdapter
        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            sda.SelectCommand = cmd
            sda.Fill(dt)
            Return dt
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        Finally
            con.Close()
            sda.Dispose()
            con.Dispose()
        End Try
    End Function


    Protected Sub download(ByVal dt As DataTable)
        Dim bytes() As Byte = CType(dt.Rows(0)("document_image"), Byte())
        Response.Buffer = True
        Response.Charset = ""
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = dt.Rows(0)("ContentType").ToString()
        Response.AddHeader("content-disposition", "attachment;filename=" & dt.Rows(0)("document_nm").ToString())
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub
End Class
