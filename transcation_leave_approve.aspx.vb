﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.Net.Mail

Partial Class transcation_leave_approve
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            pnlview.Visible = True
            pnladd.Visible = False
            Me.clr()
            Me.dvdisp()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblbalance.Text = "0"
        txtleavetp.Text = ""
        txtcontact.Text = ""
        txtemail.Text = ""
        txtnodays.Text = ""
        txtreason.Text = ""
        txtstafnm.Text = ""
        txtcancelnote.Text = ""
        txtvno.Text = ""
        txtstafsl.Text = ""
        txtfrom.Text = Format(Now, "dd/MM/yyyy")
        txtto.Text = Format(Now, "dd/MM/yyyy")
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT  lvoucher1.v_no, division_mst.div_nm, CONVERT(varchar, lvoucher1.v_dt, 103) AS v_dt, staf.emp_code, staf.staf_nm, CONVERT(varchar, lvoucher1.from_dt, 103) AS frm_dt, CONVERT(varchar, lvoucher1.to_dt, 103) AS to_dt FROM  leave_mst RIGHT OUTER JOIN lvoucher1 ON leave_mst.leave_sl = lvoucher1.leave_sl LEFT OUTER JOIN division_mst LEFT OUTER JOIN staf ON division_mst.div_sl = staf.div_sl ON lvoucher1.staf_sl = staf.staf_sl WHERE leave_mst.leave_tp<>8 AND lvoucher1.loc_cd=" & loc_cd & " AND leave_status='P' ORDER BY v_dt,staf_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If cmbapproved.SelectedIndex = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Select A Valid Status');", True)
            cmbapproved.Focus()
            Exit Sub
        End If
        start1()
        Dim mail_sub As String = ""
        Dim msg As String = ""
        If cmbapproved.SelectedIndex = 1 Then
            If txtleavetp.Text <> "3" Then
                If Val(lblbalance.Text) - Val(txtnodays.Text) <= 0 Then
                    ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Sorry No Leave Available');", True)
                    Exit Sub
                End If
            End If
            SQLInsert("UPDATE lvoucher1 SET leave_status='A' ,status_by=" & usr_sl & ",s_time='" & Now & "' WHERE v_no=" & Val(txtvno.Text) & "")
            SQLInsert("UPDATE lvoucher2 SET approved='Y'  WHERE v_no=" & Val(txtvno.Text) & "")
            SQLInsert("INSERT INTO staf_notification(staf_sl,notice_hdr,notice_details,notification_view,not_dt,not_time) VALUES(" & Val(txtstafsl.Text) & _
            ",'Leave Approval','Your Leave Application From Date : " & Trim(txtfrom.Text) & " To Date : " & Trim(txtto.Text) & " Has Been Approved.','N','" & Format(Now, "dd/MMM/yyyy") & "','" & Format(Now, "HH:mm") & "')")
            mail_sub = "Leave Approval Of :" & UCase(Trim(txtstafnm.Text)) & " From Dt :" & Trim(txtfrom.Text) & " To Dt :" & Trim(txtto.Text)
            msg = "Dear Employee, <br/>" & _
                  "Your leave With details as given hereunder.<br/>" & _
                  "Employee Name :" & UCase(Trim(txtstafnm.Text)) & "<br/>" & _
                  "Leave Type : " & Trim(cmbtp.Text) & "<br/>" & _
                  "Leave From : " & Trim(txtfrom.Text) & "<br/>" & _
                  "Leave Upto : " & Trim(txtto.Text) & "<br/>" & _
                  "No of Days : " & Trim(txtnodays.Text) & "<br/>" & _
                  "Reason : " & Trim(txtreason.Text) & "<br/>" & _
                  "Contact No : " & Trim(txtcontact.Text) & "<br/>" & _
                  "Email Id :  " & LCase(Trim(txtemail.Text)) & "<br/>" & _
                  "Approved Date & Time :  " & Now & "<br/>" & _
                  "Has Been Approved."

        ElseIf cmbapproved.SelectedIndex = 2 Then
            SQLInsert("UPDATE lvoucher1 SET leave_status='C' ,status_by=" & usr_sl & ",s_time='" & Now & "' WHERE v_no=" & Val(txtvno.Text) & "")
            SQLInsert("UPDATE lvoucher2 SET approved='N'  WHERE v_no=" & Val(txtvno.Text) & "")
            SQLInsert("INSERT INTO staf_notification(staf_sl,notice_hdr,notice_details,notification_view,not_dt,not_time) VALUES(" & Val(txtstafsl.Text) & _
           ",'Leave Cancellation','Your Leave Application From Date : " & Trim(txtfrom.Text) & " To Date : " & Trim(txtto.Text) & " Has Been Canceled. Due To " & Trim(txtcancelnote.Text) & "','N','" & Format(Now, "dd/MMM/yyyy") & "','" & Format(Now, "HH:mm") & "')")
            mail_sub = "Leave Cancellation Of :" & UCase(Trim(txtstafnm.Text)) & " From Dt :" & Trim(txtfrom.Text) & " To Dt :" & Trim(txtto.Text)
            msg = "Dear Employee, <br/>" & _
                  "Your leave With details as given hereunder.<br/>" & _
                  "Employee Name :" & UCase(Trim(txtstafnm.Text)) & "<br/>" & _
                  "Leave Type : " & Trim(cmbtp.Text) & "<br/>" & _
                  "Leave From : " & Trim(txtfrom.Text) & "<br/>" & _
                  "Leave Upto : " & Trim(txtto.Text) & "<br/>" & _
                  "No of Days : " & Trim(txtnodays.Text) & "<br/>" & _
                  "Reason : " & Trim(txtreason.Text) & "<br/>" & _
                  "Contact No : " & Trim(txtcontact.Text) & "<br/>" & _
                  "Email Id :  " & LCase(Trim(txtemail.Text)) & "<br/>" & _
                  "Cancelled Date & Time :  " & Now & "<br/>" & _
                  "Has Been Cancelled. Due to " & Trim(txtcancelnote.Text)
        End If
        'Me.email_send("technohubitsolutions@gmail.com", mail_sub, msg)
        'Dim dsdiv As DataSet = get_dataset("SELECT email FROM staf WHERE staf_sl=" & Val(txtstafsl.Text) & "")
        'If dsdiv.Tables(0).Rows.Count <> 0 Then
        '    If dsdiv.Tables(0).Rows(0).Item("email") <> "" Then
        '        Me.email_send(dsdiv.Tables(0).Rows(0).Item("email"), mail_sub, msg)
        '    End If
        'End If
        close1()
        Me.clr()
        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
    End Sub

    Private Sub email_send(ByVal mail_to, ByVal mail_sub, ByVal mail_msg)
        Dim SmtpServer As New SmtpClient()
        Dim mail As New MailMessage()
        SmtpServer.Credentials = New Net.NetworkCredential("protime.attendance@gmail.com", "14042016")
        SmtpServer.Port = 587
        SmtpServer.Host = "smtp.gmail.com"
        SmtpServer.EnableSsl = True
        mail = New MailMessage()
        mail.From = New MailAddress("protime.attendance@gmail.com")
        mail.To.Add(mail_to)
        mail.Subject = mail_sub
        mail.IsBodyHtml = True
        mail.Body = mail_msg
        SmtpServer.Send(mail)
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim lbl As Label = GridView1.Rows(rw).FindControl("lblslno")
            Dim ds As DataSet = get_dataset("SELECT staf.staf_nm, leave_mst.leave_nm,leave_mst.leave_tp, lvoucher1.* FROM lvoucher1 INNER JOIN staf ON lvoucher1.staf_sl = staf.staf_sl LEFT OUTER JOIN leave_mst ON lvoucher1.leave_sl = leave_mst.leave_sl WHERE lvoucher1.loc_cd=" & loc_cd & " AND v_no=" & lbl.Text & "")
            If ds.Tables(0).Rows.Count <> 0 Then
                txtvno.Text = ds.Tables(0).Rows(0).Item("v_no")
                txtstafsl.Text = ds.Tables(0).Rows(0).Item("staf_sl")
                txtcontact.Text = ds.Tables(0).Rows(0).Item("cont_no")
                txtemail.Text = ds.Tables(0).Rows(0).Item("email_id")
                txtfrom.Text = Format(ds.Tables(0).Rows(0).Item("from_dt"), "dd/MM/yyyy")
                txtreason.Text = ds.Tables(0).Rows(0).Item("reason")
                txtstafnm.Text = ds.Tables(0).Rows(0).Item("staf_nm")
                txtleavenm.Text = ds.Tables(0).Rows(0).Item("leave_nm")
                txtleavetp.Text = ds.Tables(0).Rows(0).Item("leave_tp")
                txtto.Text = Format(ds.Tables(0).Rows(0).Item("to_dt"), "dd/MM/yyyy")
                Dim sdt As Date = stringtodate(txtfrom.Text)
                Dim endt As Date = stringtodate(txtto.Text)
                txtnodays.Text = DateDiff(DateInterval.Day, sdt, endt) + 1
                pnladd.Visible = True
                pnlview.Visible = False
                If txtleavetp.Text <> "3" Then
                    Dim dsleaveassign As DataSet = get_dataset("select (CASE WHEN leave_tp='1' THEN 'PL' WHEN leave_tp='2' THEN 'CL' WHEN leave_tp='3' THEN 'NL' WHEN leave_tp='4' THEN 'OL' WHEN leave_tp='5' THEN 'SL' WHEN leave_tp='6' THEN 'ML' END) as tp,isnull(sum(no_day),0) as no_day from leave_assignment WHERE assign_year='" & CType(Session("cur_year"), String) & "' AND staf_sl=" & Val(txtstafsl.Text) & " AND leave_tp=" & ds.Tables(0).Rows(0).Item("leave_tp") & " GROUP BY staf_sl,leave_tp")
                    If dsleaveassign.Tables(0).Rows.Count <> 0 Then
                        Dim dsleaveapp As DataSet = get_dataset("SELECT isnull(COUNT(lvoucher2.v_sl),0) AS Expr1, lvoucher2.staf_sl, (CASE WHEN leave_mst.leave_tp = '1' THEN 'PL' WHEN leave_mst.leave_tp = '2' THEN 'CL' WHEN leave_mst.leave_tp = '3' THEN 'NL' WHEN leave_tp='4' THEN 'OL' WHEN leave_tp='5' THEN 'SL' WHEN leave_tp='6' THEN 'ML' END) AS leave_tp FROM lvoucher2 LEFT OUTER JOIN leave_mst ON lvoucher2.leave_sl = leave_mst.leave_sl WHERE approved='Y' AND l_dt >='" & Format(stringtodate(CType(Session("st_dt"), String)), "dd/MMM/yyyy") & "' and l_dt <= '" & Format(stringtodate(CType(Session("en_dt"), String)), "dd/MMM/yyyy") & "' AND staf_sl=" & Val(txtstafsl.Text) & " AND leave_tp=" & ds.Tables(0).Rows(0).Item("leave_tp") & " GROUP BY staf_sl,leave_tp")
                        If dsleaveapp.Tables(0).Rows.Count <> 0 Then
                            lblbalance.Text = dsleaveassign.Tables(0).Rows(0).Item("no_day") - dsleaveapp.Tables(0).Rows(0).Item(0)
                        Else
                            lblbalance.Text = dsleaveassign.Tables(0).Rows(0).Item("no_day")
                        End If
                    Else
                        lblbalance.Text = 0
                    End If
                Else
                    lblbalance.Text = 0
                End If
            End If
        End If
        Me.dvdisp()
    End Sub


    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        txtfrom.Focus()
    End Sub

    Protected Sub txtfrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtfrom.TextChanged
        Dim sdt As Date = stringtodate(txtfrom.Text)
        Dim endt As Date = stringtodate(txtto.Text)
        txtnodays.Text = DateDiff(DateInterval.Day, sdt, endt)
    End Sub

    Protected Sub txtto_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtto.TextChanged
        Dim sdt As Date = stringtodate(txtfrom.Text)
        Dim endt As Date = stringtodate(txtto.Text)
        txtnodays.Text = DateDiff(DateInterval.Day, sdt, endt)
    End Sub
End Class
