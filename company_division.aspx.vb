﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class company_division
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtcont2.Text = ""
        txtcontact1.Text = ""
        txtcontper.Text = ""
        txtdivnm.Text = ""
        txtdivsl.Text = ""
        txtdesg.Text = ""
        txtemailid.Text = ""
        cmbactive.SelectedIndex = 0
        If txtmode.Text = "E" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Division Creation (Entry Mode)"
            txtdivnm.Focus()
        ElseIf txtmode.Text = "M" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Division Creation (Edit Mode)"
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Division Creation (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT row_number() over(ORDER BY div_nm) as 'sl',div_nm, cont_per, desg, cont_ph1, cont_email,(case when active='Y' Then 'Yes' WHEN active='N' Then 'No' END)as 'active' FROM division_mst WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If txtmode.Text = "E" Then
            Dim dscheck As DataSet = get_dataset("SELECT div_nm FROM division_mst WHERE div_nm='" & UCase(Trim(txtdivnm.Text)) & "' AND loc_cd=" & loc_cd & "")
            If dscheck.Tables(0).Rows.Count <> 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Division Name Already Exits');", True)
                txtdivnm.Focus()
                Exit Sub
            End If
            txtdivsl.Text = "1"
            Dim ds1 As DataSet = get_dataset("SELECT max(div_sl) FROM division_mst ")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0) + 1
            End If
            start1()
            SQLInsert("INSERT INTO division_mst(div_sl,loc_cd,div_nm,cont_per,cont_ph1,cont_ph2,cont_email,active,desg) VALUES(" & Val(txtdivsl.Text) & _
            "," & loc_cd & ",'" & UCase(Trim(txtdivnm.Text)) & "','" & UCase(Trim(txtcontper.Text)) & "','" & Trim(txtcontact1.Text) & "','" & Trim(txtcont2.Text) & _
            "','" & LCase(Trim(txtemailid.Text)) & "','" & vb.Left(cmbactive.Text, 1) & "','" & UCase(Trim(txtdesg.Text)) & "')")
            close1()
            Me.clr()
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
        ElseIf txtmode.Text = "M" Then
            Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst WHERE div_sl=" & Val(txtdivsl.Text) & "")
            If ds.Tables(0).Rows.Count <> 0 Then
                If Trim(txtdivnm.Text) <> ds.Tables(0).Rows(0).Item("div_nm") Then
                    Dim dscheck As DataSet = get_dataset("SELECT div_nm FROM division_mst WHERE div_nm='" & UCase(Trim(txtdivnm.Text)) & "' AND loc_cd=" & loc_cd & "")
                    If dscheck.Tables(0).Rows.Count <> 0 Then
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Division Name Already Exits');", True)
                        txtdivnm.Focus()
                        Exit Sub
                    End If
                End If
                start1()
                SQLInsert("UPDATE division_mst SET div_nm='" & UCase(Trim(txtdivnm.Text)) & "',cont_per='" & UCase(Trim(txtcontper.Text)) & "',cont_ph1='" & _
                Trim(txtcontact1.Text) & "',cont_ph2='" & Trim(txtcont2.Text) & "',cont_email='" & LCase(Trim(txtemailid.Text)) & "',active='" & _
                vb.Left(cmbactive.Text, 1) & "',desg='" & UCase(Trim(txtdesg.Text)) & "' WHERE div_sl=" & Val(txtdivsl.Text) & "")
                close1()
                Me.clr()
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Modified Successfully');", True)
                Me.dvdisp()
            End If
        End If
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = 1 'CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(GridView1.Rows(rw).Cells(1).Text) & "' AND loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtmode.Text = "M"
                Me.clr()
                txtdivsl.Text = Val(ds1.Tables(0).Rows(0).Item("div_sl"))
                Me.dvsel()
            End If
        End If
    End Sub

    Private Sub dvsel()
        Dim ds1 As DataSet = get_dataset("SELECT * FROM division_mst WHERE div_sl=" & Val(txtdivsl.Text) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtcont2.Text = ds1.Tables(0).Rows(0).Item("cont_ph2")
            txtcontact1.Text = ds1.Tables(0).Rows(0).Item("cont_ph1")
            txtcontper.Text = ds1.Tables(0).Rows(0).Item("cont_per")
            txtdivnm.Text = ds1.Tables(0).Rows(0).Item("div_nm")
            txtdesg.Text = ds1.Tables(0).Rows(0).Item("desg")
            txtemailid.Text = ds1.Tables(0).Rows(0).Item("cont_email")
            If ds1.Tables(0).Rows(0).Item("active") = "Y" Then
                cmbactive.SelectedIndex = 0
            Else
                cmbactive.SelectedIndex = 1
            End If
        End If
    End Sub

    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        txtdivnm.Focus()
    End Sub
End Class
