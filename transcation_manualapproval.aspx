﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="transcation_manualapproval.aspx.vb" Inherits="transcation_manualapproval" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Manual Attendance Approval"></asp:Label></h3>
                                </div>

                     <div class="card-body">
                                        <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table style="width:100%;">
                                                                                         
                                              <tr>
                                                  <td>
                                                      <table style="width:100%;">
                                                             <tr>
                                                              <td width="20%" valign="top">
                                                                  <table style="width:100%;">
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              <asp:Image ID="Image1" runat="server" Height="150px" Width="150px" 
                                                                                  />
                                                                          </td>
                                                                      </tr>
                                                                     
                                                                  </table>
                                                                 </td>
                                                              
                                                              <td width="80%" valign="top">
                                                                  <table style="width:100%;">
                                                                      <tr>
                                                                          <td>
                                                                              <asp:Label ID="lbladdress" runat="server" Font-Size="15px" Text="Label"></asp:Label>
                                                                          </td>
                                                                          <td>
                                                                              &nbsp;</td>
                                                                          <td>
                                                                              &nbsp;</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              <asp:TextBox ID="txtid" runat="server" Visible="False"></asp:TextBox>
                                                                              <asp:TextBox ID="txtstafsl" runat="server" Visible="False"></asp:TextBox>
                                                                              <asp:TextBox ID="txttime" runat="server" Visible="False"></asp:TextBox>
                                                                              <asp:TextBox ID="txtdevicecode" runat="server" Visible="False"></asp:TextBox>
                                                                              <asp:TextBox ID="txtdt" runat="server" Visible="False"></asp:TextBox>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              &nbsp;<asp:HyperLink ID="HyperLink1" runat="server" 
                                                                                  NavigateUrl="https://www.google.com/maps?q=20.3107398,85.8168556" 
                                                                                  Target="_blank">Click To View Location In Google Map</asp:HyperLink>
                                                                              &nbsp;</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              <asp:Label ID="lblpoints" runat="server" Font-Size="15px" Text="Label"></asp:Label>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              <asp:Button ID="cmdsave" runat="server" class="btn btn-success" Text="Approve" />
                                                                              <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                                                  class="btn btn-warning" Text="Cancel" />
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              &nbsp;</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td colspan="3">
                                                                              <asp:Button ID="cmdsave0" runat="server" class="btn btn-success" 
                                                                                  Text="Approve All" />
                                                                              &nbsp;<asp:Button ID="cmdclear0" runat="server" CausesValidation="false" 
                                                                                  class="btn btn-warning" Text="Cancel All" />
                                                                          </td>
                                                                      </tr>
                                                                  </table>

                                                                                                                                      </td>
                                                          </tr>
                                                                                                               </table>
                                               <table style="width:100%;">
                                                             <tr>
                                                              <td>
                                                                   <div style="width: 100%; height: 300px; overflow: auto; font-size: 15px;">
                                                          <asp:GridView ID="dvstaf" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                              AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                              PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                              <AlternatingRowStyle CssClass="alt" />
                                                              <Columns>
                                                                 
                                                                   <asp:TemplateField HeaderText="Date">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lbldt" Text='<%#Eval("dt") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Divission">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lbldivision" Text='<%#Eval("div_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Employee Name">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblstaf_nm" Text='<%#Eval("staf_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Department">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lbldept" Text='<%#Eval("dept_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Designation">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lbldesg" Text='<%#Eval("desg_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Punch Type">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblposttype" Text='<%#Eval("post_type") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Time">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lbltm" Text='<%#Eval("tm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                 <asp:TemplateField Visible="False">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblstaf_sl" Text='<%#Eval("staf_sl") %>'></asp:Label>
                                                                   <asp:Label runat="server" ID="lblid" Text='<%#Eval("id") %>'></asp:Label>
                                                                     <asp:Label runat="server" ID="lbldevicecode" Text='<%#Eval("device_code") %>'></asp:Label>                                                                      
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:ButtonField ButtonType="Image" Text="Button" ImageUrl="images/view.png" CommandName="view">
                                                                   <HeaderStyle Width="30px" />
                                                                   <ItemStyle Width="30px" />
                                                                   </asp:ButtonField>                                                                 
                                                                
                                                              </Columns>
                                                              <PagerStyle HorizontalAlign="Right" />
                                                          </asp:GridView>
                                                      </div></td></tr>
                                                                                                               </table>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>           
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

