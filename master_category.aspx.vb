﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class master_category
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtcategory.Text = ""
        txtcategorysl.Text = ""
        txtfullday.Text = ""
        txthalfday.Text = ""
        txtinpunchendaft.Text = ""
        txtinpunstbf.Text = ""
        txtmaxot.Text = ""
        txtminot.Text = ""
        txtoutpunchendaft.Text = ""
        txtshortname.Text = ""
        time06.Text = ""
        time07.Text = ""
        time08.Text = ""
        chk1.Checked = False
        chk4.Checked = False
        chk6.Checked = False
        chkot.Checked = False
        chkweekoff.Checked = False
        chkwo1.Checked = False
        chkwo2.Checked = False
        chkwo3.Checked = False
        chkwo4.Checked = False
        chkwo5.Checked = False
        chkcontinuoslate.Checked = False
        chklatemonthly.Checked = False
        chkabsent.Checked = False
        cmbcontinuoslate.SelectedIndex = 0
        cmblatemonthly.SelectedIndex = 0
        txtcontinuoslate.Text = 0
        txtlatemonthly.Text = 0
        txtabsent.Text = 0
        cmbautoshift.SelectedIndex = 0
        cmbot.SelectedIndex = 0
        cmbhalf.SelectedIndex = 0
        cmbholiday.SelectedIndex = 0
        cmbholiday1.SelectedIndex = 0
        cmblate.SelectedIndex = 0
        cmbmin4.SelectedIndex = 0
        cmbotweekoff.SelectedIndex = 0
        cmbotweekoff.SelectedIndex = 0
        cmbovertime.SelectedIndex = 0
        cmbpresent.SelectedIndex = 0
        cmbtime4.SelectedIndex = 7
        cmbweekoff1.SelectedIndex = 0
        cmbweekoff2.SelectedIndex = 0
        If txtmode.Text = "E" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Employee Category Master (Entry Mode)"
            txtcategory.Focus()
        ElseIf txtmode.Text = "M" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Employee Category Master (Edit Mode)"
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Employee Category Master (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT row_number() over(ORDER BY cat_nm) as 'sl',cat_nm, cat_snm FROM emp_category WHERE loc_cd=" & loc_cd & " ORDER BY cat_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT cat_sl FROM emp_category WHERE cat_nm='" & Trim(GridView1.Rows(rw).Cells(1).Text) & "' AND loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtmode.Text = "M"
                Me.clr()
                txtcategorysl.Text = Val(ds1.Tables(0).Rows(0).Item("cat_sl"))
                Me.dvsel()
            End If
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)


        Dim weekoff, weekoff1, weekoff2, weekoff3, weekoff4, weekoff5, consider_firstpunch, continuos_late, monthly_late, absent_ifprevious_absent, transfer_ot_weeklyoff, transfer_ot_holiday, consider_latecoming, consider_earlygoing, deduct_break, markweekoff As Integer

        weekoff = IIf(chkweekoff.Checked, Val(cmbweekoff2.SelectedIndex), 0)
        weekoff1 = IIf(chkwo1.Checked, 1, 0)
        weekoff2 = IIf(chkwo2.Checked, 1, 0)
        weekoff3 = IIf(chkwo3.Checked, 1, 0)
        weekoff4 = IIf(chkwo4.Checked, 1, 0)
        weekoff5 = IIf(chkwo5.Checked, 1, 0)
        consider_firstpunch = IIf(chk1.Checked, 1, 0)
        deduct_break = IIf(chk4.Checked, 1, 0)
        markweekoff = IIf(chk6.Checked, 1, 0)
        continuos_late = IIf(chkcontinuoslate.Checked, 1, 0)
        monthly_late = IIf(chklatemonthly.Checked, 1, 0)
        absent_ifprevious_absent = IIf(chkabsent.Checked, 1, 0)
        transfer_ot_weeklyoff = IIf(chk7.Checked, 1, 0)
        transfer_ot_holiday = IIf(chk8.Checked, 1, 0)


        Dim work_hour As String = ""
        Dim wohr As Date
        work_hour = Val(cmbtime4.Text) & ":" & Val(cmbmin4.Text)
        wohr = CDate(work_hour)


        If txtmode.Text = "E" Then
            Dim dscheck As DataSet = get_dataset("SELECT cat_nm FROM emp_category WHERE cat_nm='" & UCase(Trim(txtcategory.Text)) & "' AND loc_cd=" & loc_cd & "")
            If dscheck.Tables(0).Rows.Count <> 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Category Name Already Exits');", True)
                txtcategory.Focus()
                Exit Sub
            End If
            Dim dscheck1 As DataSet = get_dataset("SELECT cat_snm FROM emp_category WHERE cat_snm='" & UCase(Trim(txtshortname.Text)) & "' AND loc_cd=" & loc_cd & "")
            If dscheck1.Tables(0).Rows.Count <> 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Category Short Name Already Exits');", True)
                txtshortname.Focus()
                Exit Sub
            End If
            txtcategorysl.Text = "1"
            Dim ds1 As DataSet = get_dataset("SELECT max(cat_sl) FROM emp_category")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                txtcategorysl.Text = ds1.Tables(0).Rows(0).Item(0) + 1
            End If
            start1()
            SQLInsert("INSERT INTO emp_category(cat_sl,loc_cd,cat_nm,cat_snm,ot_formula,min_ot,max_ot,single_punch," & _
            "week_off1,week_off2,week_off2_1,week_off2_2,week_off2_3,week_off2_4,week_off2_5,consider_firstpunch," & _
            "deduct_break,consider_half,consider_absent,created_by,created_date,modified_by,modified_date," & _
            "oton_weekoff,oton_holiday,no_shift_assign,mark_weekoff,in_punch_stbfr,inpunch_endafter,outpunch_endafter,early_time,late_time," & _
            "leave_time,half_day,late_arrv,erly_dprt,overtime,present_wo,present_hl,max_work,continuos_late,continuos_late_status,continuos_late_for" & _
            ",monthly_late,monthly_late_status,monthly_late_for, absent_ifprevious_absent,absent_ifprevious_absent_days,transfer_ot_weeklyoff,transfer_ot_holiday,odd_punch_status) VALUES(" & _
            Val(txtcategorysl.Text) & "," & loc_cd & ",'" & UCase(Trim(txtcategory.Text)) & "','" & Trim(txtshortname.Text) & "','" & _
            vb.Left(cmbot.Text, 1) & "'," & Val(txtminot.Text) & "," & Val(txtmaxot.Text) & ",''," & Val(cmbweekoff1.SelectedIndex) & _
            "," & weekoff & "," & weekoff1 & "," & weekoff2 & "," & weekoff3 & "," & weekoff4 & "," & weekoff5 & "," & consider_firstpunch & _
            "," & deduct_break & "," & Val(txthalfday.Text) & "," & Val(txtfullday.Text) & "," & usr_sl & ",'" & Now & "'," & usr_sl & ",'" & Now & _
            "','" & vb.Left(cmbotweekoff.Text, 1) & "','" & vb.Left(cmbholiday.Text, 1) & "','" & vb.Left(cmbautoshift.Text, 1) & _
            "'," & markweekoff & "," & Val(txtinpunstbf.Text) & "," & Val(txtinpunchendaft.Text) & "," & Val(txtoutpunchendaft.Text) & "," & Val(time06.Text) & _
            "," & Val(time08.Text) & "," & Val(time07.Text) & ",'" & vb.Left(cmbhalf.Text, 1) & "','" & vb.Left(cmblate.Text, 1) & "','" & vb.Left(cmbearly.Text, 1) & _
            "','" & vb.Left(cmbovertime.Text, 1) & "','" & vb.Left(cmbpresent.Text, 1) & "','" & vb.Left(cmbholiday1.Text, 1) & "','" & Format(wohr, "HH:mm") & _
            "'," & continuos_late & ",'" & cmbcontinuoslate.SelectedIndex + 1 & "'," & Val(txtcontinuoslate.Text) & "," & monthly_late & ",'" & cmblatemonthly.SelectedIndex + 1 & _
            "'," & Val(txtlatemonthly.Text) & "," & absent_ifprevious_absent & "," & Val(txtabsent.Text) & "," & transfer_ot_weeklyoff & "," & transfer_ot_holiday & ",'" & vb.Left(cmboddpunch.Text, 1) & "')")

            close1()
            Me.clr()
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
        Else
            Dim ds As DataSet = get_dataset("SELECT * FROM emp_category WHERE cat_sl=" & Val(txtcategorysl.Text) & "")
            If ds.Tables(0).Rows.Count <> 0 Then
                If Trim(txtcategory.Text) <> ds.Tables(0).Rows(0).Item("cat_nm") Then
                    Dim dscheck As DataSet = get_dataset("SELECT cat_nm FROM emp_category WHERE cat_nm='" & UCase(Trim(txtcategory.Text)) & "' AND loc_cd=" & loc_cd & "")
                    If dscheck.Tables(0).Rows.Count <> 0 Then
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Category Name Already Exits');", True)
                        txtcategory.Focus()
                        Exit Sub
                    End If
                End If
                If Trim(txtshortname.Text) <> ds.Tables(0).Rows(0).Item("cat_snm") Then
                    Dim dscheck1 As DataSet = get_dataset("SELECT cat_snm FROM emp_category WHERE cat_snm='" & UCase(Trim(txtshortname.Text)) & "' AND loc_cd=" & loc_cd & "")
                    If dscheck1.Tables(0).Rows.Count <> 0 Then
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Category Short Name Already Exits');", True)
                        txtshortname.Focus()
                        Exit Sub
                    End If
                End If
                start1()

                SQLInsert("UPDATE emp_category SET cat_nm='" & UCase(Trim(txtcategory.Text)) & "',cat_snm='" & Trim(txtshortname.Text) & "',ot_formula='" & _
                vb.Left(cmbot.Text, 1) & "',min_ot=" & Val(txtminot.Text) & ",max_ot=" & Val(txtmaxot.Text) & ",single_punch='',week_off1=" & Val(cmbweekoff1.SelectedIndex) & _
                ",week_off2=" & weekoff & ",week_off2_1=" & weekoff1 & ",week_off2_2=" & weekoff2 & ",week_off2_3=" & weekoff3 & ",week_off2_4=" & weekoff4 & ",week_off2_5=" & _
                weekoff5 & ",consider_firstpunch=" & consider_firstpunch & ",deduct_break=" & deduct_break & ",consider_half=" & Val(txthalfday.Text) & ",consider_absent=" & _
                Val(txtfullday.Text) & ",modified_by=" & usr_sl & ",modified_date='" & Now & "',oton_weekoff='" & vb.Left(cmbotweekoff.Text, 1) & "',oton_holiday='" & _
                vb.Left(cmbholiday.Text, 1) & "',no_shift_assign='" & vb.Left(cmbautoshift.Text, 1) & "',mark_weekoff=" & markweekoff & ",in_punch_stbfr=" & Val(txtinpunstbf.Text) & _
                ",inpunch_endafter=" & Val(txtinpunchendaft.Text) & ",outpunch_endafter=" & Val(txtoutpunchendaft.Text) & ",early_time=" & Val(time06.Text) & _
                ",late_time=" & Val(time08.Text) & ",leave_time=" & Val(time07.Text) & ",half_day='" & vb.Left(cmbhalf.Text, 1) & "',late_arrv='" & vb.Left(cmblate.Text, 1) & _
                "',erly_dprt='" & vb.Left(cmbearly.Text, 1) & "',overtime='" & vb.Left(cmbovertime.Text, 1) & "',present_wo='" & vb.Left(cmbpresent.Text, 1) & "',present_hl='" & _
                vb.Left(cmbholiday1.Text, 1) & "',max_work='" & Format(wohr, "HH:mm") & "',continuos_late =" & continuos_late & ",continuos_late_status ='" & cmbcontinuoslate.SelectedIndex + 1 & _
                "',continuos_late_for =" & Val(txtcontinuoslate.Text) & ",monthly_late =" & monthly_late & ",monthly_late_status = '" & cmblatemonthly.SelectedIndex + 1 & _
                "',monthly_late_for =" & Val(txtlatemonthly.Text) & ", absent_ifprevious_absent =" & absent_ifprevious_absent & ",absent_ifprevious_absent_days=" & Val(txtabsent.Text) & _
                ",transfer_ot_weeklyoff=" & transfer_ot_weeklyoff & ",transfer_ot_holiday=" & transfer_ot_holiday & ",odd_punch_status='" & vb.Left(cmboddpunch.Text, 1) & "' WHERE cat_sl=" & Val(txtcategorysl.Text) & " AND loc_cd=" & loc_cd & "")
                close1()
                Me.clr()
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Modified Succesffuly');", True)
            End If

        End If

    End Sub

    Private Sub dvsel()
        Dim ds1 As DataSet = get_dataset("SELECT * FROM emp_category WHERE cat_sl=" & Val(txtcategorysl.Text) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtcategory.Text = ds1.Tables(0).Rows(0).Item("cat_nm")
            txtcategorysl.Text = ds1.Tables(0).Rows(0).Item("cat_sl")
            txtfullday.Text = ds1.Tables(0).Rows(0).Item("consider_absent")
            txthalfday.Text = ds1.Tables(0).Rows(0).Item("consider_half")
            txtinpunchendaft.Text = ds1.Tables(0).Rows(0).Item("inpunch_endafter")
            txtinpunstbf.Text = ds1.Tables(0).Rows(0).Item("in_punch_stbfr")
            txtmaxot.Text = ds1.Tables(0).Rows(0).Item("max_ot")
            txtminot.Text = ds1.Tables(0).Rows(0).Item("min_ot")
            txtoutpunchendaft.Text = ds1.Tables(0).Rows(0).Item("outpunch_endafter")
            txtshortname.Text = ds1.Tables(0).Rows(0).Item("cat_snm")
            time08.Text = ds1.Tables(0).Rows(0).Item("late_time")
            time07.Text = ds1.Tables(0).Rows(0).Item("leave_time")
            time06.Text = ds1.Tables(0).Rows(0).Item("early_time")
            If ds1.Tables(0).Rows(0).Item("consider_firstpunch") = 1 Then
                chk1.Checked = True
            Else
                chk1.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("deduct_break") = 1 Then
                chk4.Checked = True
            Else
                chk4.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("mark_weekoff") = 1 Then
                chk6.Checked = True
            Else
                chk6.Checked = False
            End If
            If Val(ds1.Tables(0).Rows(0).Item("max_ot")) = 0 Then
                chkot.Checked = False
            Else
                chkot.Checked = True
            End If
            chkweekoff.Checked = ds1.Tables(0).Rows(0).Item("week_off2")
            If Trim(ds1.Tables(0).Rows(0).Item("week_off2_1")) = "0" Then
                chkwo1.Checked = False
            Else
                chkwo1.Checked = True
            End If
            If Trim(ds1.Tables(0).Rows(0).Item("week_off2_2")) = "0" Then
                chkwo2.Checked = False
            Else
                chkwo2.Checked = True
            End If
            If Trim(ds1.Tables(0).Rows(0).Item("week_off2_3")) = "0" Then
                chkwo3.Checked = False
            Else
                chkwo3.Checked = True
            End If
            If Trim(ds1.Tables(0).Rows(0).Item("week_off2_4")) = "0" Then
                chkwo4.Checked = False
            Else
                chkwo4.Checked = True
            End If
            If Trim(ds1.Tables(0).Rows(0).Item("week_off2_5")) = "0" Then
                chkwo5.Checked = False
            Else
                chkwo5.Checked = True
            End If
            If Trim(ds1.Tables(0).Rows(0).Item("no_shift_assign")) = "A" Then
                cmbautoshift.SelectedIndex = 0
            Else
                cmbautoshift.SelectedIndex = 1
            End If
            cmbot.SelectedIndex = Val(ds1.Tables(0).Rows(0).Item("ot_formula")) - 1
            If Trim(ds1.Tables(0).Rows(0).Item("half_day")) = "N" Then
                cmbhalf.SelectedIndex = 0
            Else
                cmbhalf.SelectedIndex = 1
            End If
            If Trim(ds1.Tables(0).Rows(0).Item("oton_holiday")) = "N" Then
                cmbholiday.SelectedIndex = 0
            Else
                cmbholiday.SelectedIndex = 1
            End If
            If Trim(ds1.Tables(0).Rows(0).Item("present_hl")) = "N" Then
                cmbholiday1.SelectedIndex = 0
            Else
                cmbholiday1.SelectedIndex = 1
            End If
            If Trim(ds1.Tables(0).Rows(0).Item("late_arrv")) = "N" Then
                cmblate.SelectedIndex = 0
            Else
                cmblate.SelectedIndex = 1
            End If
            cmbmin4.Text = Format(ds1.Tables(0).Rows(0).Item("max_work"), "mm")
            If Trim(ds1.Tables(0).Rows(0).Item("oton_weekoff")) = "N" Then
                cmbotweekoff.SelectedIndex = 0
            Else
                cmbotweekoff.SelectedIndex = 1
            End If
            If Trim(ds1.Tables(0).Rows(0).Item("overtime")) = "N" Then
                cmbovertime.SelectedIndex = 0
            Else
                cmbovertime.SelectedIndex = 1
            End If
            If Trim(ds1.Tables(0).Rows(0).Item("present_wo")) = "N" Then
                cmbpresent.SelectedIndex = 0
            Else
                cmbpresent.SelectedIndex = 1
            End If
            If ds1.Tables(0).Rows(0).Item("continuos_late") = 1 Then
                chkcontinuoslate.Checked = True
            Else
                chkcontinuoslate.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("monthly_late") = 1 Then
                chklatemonthly.Checked = True
            Else
                chklatemonthly.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("absent_ifprevious_absent") = 1 Then
                chkabsent.Checked = True
            Else
                chkabsent.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("transfer_ot_weeklyoff") = 1 Then
                chk7.Checked = True
            Else
                chk7.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("transfer_ot_holiday") = 1 Then
                chk8.Checked = True
            Else
                chk8.Checked = False
            End If
            cmbcontinuoslate.SelectedIndex = Val(ds1.Tables(0).Rows(0).Item("continuos_late_status")) - 1
            cmboddpunch.SelectedIndex = Val(ds1.Tables(0).Rows(0).Item("odd_punch_status")) - 1
            cmblatemonthly.SelectedIndex = Val(ds1.Tables(0).Rows(0).Item("monthly_late_status")) - 1
            txtcontinuoslate.Text = Val(ds1.Tables(0).Rows(0).Item("continuos_late_for"))
            txtlatemonthly.Text = Val(ds1.Tables(0).Rows(0).Item("monthly_late_for"))
            txtabsent.Text = Val(ds1.Tables(0).Rows(0).Item("absent_ifprevious_absent_days"))

            cmbtime4.Text = Format(ds1.Tables(0).Rows(0).Item("max_work"), "HH")
            cmbweekoff1.SelectedIndex = Val(ds1.Tables(0).Rows(0).Item("week_off1"))
            cmbweekoff2.SelectedIndex = Val(ds1.Tables(0).Rows(0).Item("week_off2"))
        End If
    End Sub

    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        txtcategory.Focus()
    End Sub
End Class
