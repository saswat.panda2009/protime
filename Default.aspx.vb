﻿Imports System.Data

Partial Class _Default
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            txtpassword.Value = ""
            txtuser.Value = ""
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim dsuser As DataSet = get_dataset("SELECT * FROM usr_login WHERE usr_name='" & Trim(txtuser.Value) & "'")
        If dsuser.Tables(0).Rows.Count <> 0 Then
            If Trim(txtpassword.Value) = dsuser.Tables(0).Rows(0).Item("usr_pwd") Then
                If dsuser.Tables(0).Rows(0).Item("block") = "Y" Then
                    ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Sorry This User Is Blocked');", True)
                    txtpassword.Value = ""
                    txtuser.Value = ""
                    txtuser.Focus()
                    Exit Sub
                End If
                Session("bt") = Request.Browser.Browser
                Session("usr_sl") = dsuser.Tables(0).Rows(0).Item("usr_sl")
                Session("usr_name") = dsuser.Tables(0).Rows(0).Item("name")
                Session("lvl") = dsuser.Tables(0).Rows(0).Item("lvl")
                Session("loc_cd") = dsuser.Tables(0).Rows(0).Item("loc_cd")
                Session("cur_year") = ""
                Session("st_dt") = ""
                Session("en_dt") = ""
                start1()
                SQLInsert("UPDATE usr_login SET last_login='" & Format(Now, "dd/MMM/yyyy") & "' WHERE usr_sl=" & Val(dsuser.Tables(0).Rows(0).Item("usr_sl")) & "")
                close1()
                Dim dsyear As DataSet = get_dataset("SELECT * FROM attedance_year WHERE active='Y'")
                If dsyear.Tables(0).Rows.Count <> 0 Then
                    Session("cur_year") = dsyear.Tables(0).Rows(0).Item("year_nm")
                    Session("st_dt") = Format(dsyear.Tables(0).Rows(0).Item("st_dt"), "dd/MM/yyyy")
                    Session("en_dt") = Format(dsyear.Tables(0).Rows(0).Item("end_dt"), "dd/MM/yyyy")
                End If
                Dim dscompany As DataSet = get_dataset("SELECT * FROM company")
                If dscompany.Tables(0).Rows.Count <> 0 Then
                    Dim str As String = crypto.decrpt(dscompany.Tables(0).Rows(0).Item("valid_upto"), "proth140416")
                    If Today <= CDate(str) Then
                        Response.Redirect("dashboard.aspx")
                    Else
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Sorry Your License Has Been Expired');", True)
                        txtpassword.Value = ""
                        txtuser.Value = ""
                        txtuser.Focus()
                    End If
                End If
            Else
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide The Valid User Id or Password');", True)
                txtpassword.Value = ""
                txtuser.Value = ""
                txtuser.Focus()
            End If
        Else
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide The Valid User Id or Password');", True)
            txtpassword.Value = ""
            txtuser.Value = ""
            txtuser.Focus()
        End If
    End Sub
End Class
