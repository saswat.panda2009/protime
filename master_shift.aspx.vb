﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class master_shift
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtsl.Text = ""
        txtname.Text = ""
        txtsttm.Text = ""
        txtendtm.Text = ""
        cmbactive.SelectedIndex = 0
        cmbnight.SelectedIndex = 1
        If txtmode.Text = "E" Then
            divadd.Visible = True
            divview.Visible = False
            lblhdr.Text = "Shift Master (Entry Mode)"
            txtname.Focus()
        ElseIf txtmode.Text = "M" Then
            divadd.Visible = True
            divview.Visible = False
            lblhdr.Text = "Shift Master (Edit Mode)"
        ElseIf txtmode.Text = "V" Then
            divadd.Visible = False
            divview.Visible = True
            lblhdr.Text = "Shift Master (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT ROW_NUMBER() OVER(ORDER BY shift_sdt) as sl,shift_nm, convert(varchar,shift_sdt,108) as sdt, convert(varchar,shift_edt,108) as edt FROM shift_mst WHERE loc_cd=" & loc_cd & " ORDER BY shift_sdt")
        dv.DataSource = ds1.Tables(0)
        dv.DataBind()
    End Sub

    Protected Sub dv_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dv.PageIndexChanging
        dv.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub dv_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dv.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT shift_sl FROM shift_mst WHERE shift_nm='" & Trim(dv.Rows(rw).Cells(1).Text) & "' AND loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtmode.Text = "M"
                Me.clr()
                txtsl.Text = Val(ds1.Tables(0).Rows(0).Item("shift_sl"))
                Me.dvsel()
            End If
        End If
    End Sub

    Private Sub dvsel()
        Dim ds As DataSet = get_dataset("SELECT * FROM shift_mst WHERE shift_sl=" & Val(txtsl.Text) & "")
        If ds.Tables(0).Rows.Count <> 0 Then
            txtsl.Text = ds.Tables(0).Rows(0).Item("shift_sl")
            txtname.Text = ds.Tables(0).Rows(0).Item("shift_nm")
            txtsttm.Text = Format(ds.Tables(0).Rows(0).Item("shift_sdt"), "HH:mm")
            txtendtm.Text = Format(ds.Tables(0).Rows(0).Item("shift_edt"), "HH:mm")
            txtbrksttm.Text = Format(ds.Tables(0).Rows(0).Item("break_start"), "HH:mm")
            txtbrkendtm.Text = Format(ds.Tables(0).Rows(0).Item("break_end"), "HH:mm")
            If ds.Tables(0).Rows(0).Item("active") = "Y" Then
                cmbactive.SelectedIndex = 0
            Else
                cmbactive.SelectedIndex = 1
            End If
            If ds.Tables(0).Rows(0).Item("is_nightshift") = "Y" Then
                cmbnight.SelectedIndex = 0
            Else
                cmbnight.SelectedIndex = 1
            End If
            If ds.Tables(0).Rows(0).Item("incl_break") = "Y" Then
                cmbinclude.SelectedIndex = 0
            Else
                cmbinclude.SelectedIndex = 1
            End If
        End If
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim sdt As Date
        sdt = CDate(Trim(txtsttm.Text))
        Dim edt As Date
        edt = CDate(Trim(txtendtm.Text))
        Dim bsdt As Date
        bsdt = CDate(Trim(txtbrksttm.Text))
        Dim bedt As Date
        bedt = CDate(Trim(txtbrkendtm.Text))
        If cmbnight.SelectedIndex = 1 Then
            If sdt >= edt Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('End Time Should Be Greater Than Start Time');", True)
                txtendtm.Focus()
                Exit Sub
            End If
        End If
        If txtmode.Text = "E" Then
            Dim dscheck As DataSet = get_dataset("SELECT shift_sl FROM shift_mst WHERE shift_nm='" & UCase(Trim(txtname.Text)) & "' AND loc_cd=" & loc_cd & "")
            If dscheck.Tables(0).Rows.Count <> 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Shift Name Already Exits');", True)
                txtname.Focus()
                Exit Sub
            End If
            txtsl.Text = "1"
            Dim ds1 As DataSet = get_dataset("SELECT max(shift_sl) FROM shift_mst")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                txtsl.Text = ds1.Tables(0).Rows(0).Item(0) + 1
            End If

            start1()
            SQLInsert("INSERT INTO shift_mst(shift_sl,shift_nm,shift_sdt,shift_edt,active,loc_cd,is_nightshift," & _
            "min_work,incl_break,break_start,break_end,consider_halfday,halfday_time,consider_absent,absent_time) VALUES(" & Val(txtsl.Text) & _
            ",'" & UCase(Trim(txtname.Text)) & "','" & Format(sdt, "HH:mm") & "','" & Format(edt, "HH:mm") & _
            "','" & vb.Left(cmbactive.Text, 1) & "'," & loc_cd & ",'" & vb.Left(cmbnight.Text, 1) & "','08:00','" & vb.Left(cmbinclude.Text, 1) & _
            "','" & Format(bsdt, "HH:mm") & "','" & Format(bedt, "HH:mm") & "','N',0,'N',0)")
            close1()
            Me.clr()
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
        ElseIf txtmode.Text = "M" Then
            Dim ds As DataSet = get_dataset("SELECT shift_nm FROM shift_mst WHERE shift_sl=" & Val(txtsl.Text) & "")
            If ds.Tables(0).Rows.Count <> 0 Then
                If Trim(txtname.Text) <> ds.Tables(0).Rows(0).Item("shift_nm") Then
                    Dim dscheck As DataSet = get_dataset("SELECT shift_sl FROM shift_mst WHERE shift_nm='" & UCase(Trim(txtname.Text)) & "' AND loc_cd=" & loc_cd & "")
                    If dscheck.Tables(0).Rows.Count <> 0 Then
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Shift Name Already Exits');", True)
                        txtname.Focus()
                        Exit Sub
                    End If
                End If
                start1()
                SQLInsert("UPDATE shift_mst SET shift_nm='" & UCase(Trim(txtname.Text)) & "',shift_sdt='" & Format(sdt, "HH:mm") & _
                "',shift_edt='" & Format(edt, "HH:mm") & "',active='" & vb.Left(cmbactive.Text, 1) & "',is_nightshift='" & _
                vb.Left(cmbnight.Text, 1) & "',incl_break='" & vb.Left(cmbinclude.Text, 1) & "',break_start='" & Format(bsdt, "HH:mm") & _
                "',break_end='" & Format(bedt, "HH:mm") & "' WHERE shift_sl=" & Val(txtsl.Text) & "")
                close1()
                txtmode.Text = "M"
                Me.clr()
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Modified Succesffuly');", True)
            End If
        End If
    End Sub
End Class
