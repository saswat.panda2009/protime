﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class master_district
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtcity.Text = ""
        txtstatecd.Text = ""
        txtcitycd.Text = ""
        cmbactive.SelectedIndex = 0
        Me.state_disp()
        If txtmode.Text = "E" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "City Master (Entry Mode)"
            txtcity.Focus()
        ElseIf txtmode.Text = "M" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "City Master (Edit Mode)"
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "City Master (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub dvdisp()
        Dim ds1 As DataSet = get_dataset("SELECT row_number() over(ORDER BY city_name) as 'sl',city_name,stat_name,(case when city.active='Y' Then 'Yes' WHEN city.active='N' Then 'No' END)as 'active' FROM city LEFT OUTER JOIN stat ON city.stat_cd = stat.stat_cd ORDER BY city_name")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        If Val(txtstatecd.Text) = 0 Then
            Exit Sub
        End If
        If txtmode.Text = "E" Then
            Dim dscheck As DataSet = get_dataset("SELECT city_name FROM city WHERE city_name='" & UCase(Trim(txtcity.Text)) & "'")
            If dscheck.Tables(0).Rows.Count <> 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('City Name Already Exits');", True)
                txtcity.Focus()
                Exit Sub
            End If
            txtcitycd.Text = "1"
            Dim ds1 As DataSet = get_dataset("SELECT max(city_cd) FROM city ")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                txtcitycd.Text = ds1.Tables(0).Rows(0).Item(0) + 1
            End If
            start1()
            SQLInsert("INSERT INTO city(city_cd,stat_cd,city_name,active) VALUES(" & Val(txtcitycd.Text) & _
            "," & Val(txtstatecd.Text) & ",'" & UCase(Trim(txtcity.Text)) & "','" & vb.Left(cmbactive.Text, 1) & "')")
            close1()
            Me.clr()
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
        ElseIf txtmode.Text = "M" Then
            Dim ds As DataSet = get_dataset("SELECT city_name FROM city WHERE city_cd=" & Val(txtcitycd.Text) & "")
            If ds.Tables(0).Rows.Count <> 0 Then
                If Trim(txtcity.Text) <> ds.Tables(0).Rows(0).Item("city_name") Then
                    Dim dsd As DataSet = get_dataset("SELECT city_name FROM city WHERE city_name='" & UCase(Trim(txtcity.Text)) & "'")
                    If dsd.Tables(0).Rows.Count <> 0 Then
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('City Name Already Exits');", True)
                        txtcity.Focus()
                        Exit Sub
                    End If
                End If
                start1()
                SQLInsert("UPDATE city SET city_name='" & UCase(Trim(txtcity.Text)) & "',active='" & _
                vb.Left(cmbactive.Text, 1) & "',stat_cd=" & Val(txtstatecd.Text) & " WHERE city_cd=" & Val(txtcitycd.Text) & "")
                close1()
                Me.clr()
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Modified Succesffuly');", True)
            End If
        End If
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT city_cd FROM city WHERE city_name='" & Trim(GridView1.Rows(rw).Cells(1).Text) & "'")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtmode.Text = "M"
                Me.clr()
                txtcitycd.Text = Val(ds1.Tables(0).Rows(0).Item("city_cd"))
                Me.dvsel()
            End If
        End If
    End Sub

    Private Sub dvsel()
        Dim ds1 As DataSet = get_dataset("SELECT city.*, stat.stat_name FROM city LEFT OUTER JOIN stat ON city.stat_cd = stat.stat_cd WHERE city_cd=" & Val(txtcitycd.Text) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            Me.state_disp()
            txtcity.Text = ds1.Tables(0).Rows(0).Item("city_name")
            txtstatecd.Text = ds1.Tables(0).Rows(0).Item("stat_cd")
            cmbstate.Text = ds1.Tables(0).Rows(0).Item("stat_name")
            If ds1.Tables(0).Rows(0).Item("active") = "Y" Then
                cmbactive.SelectedIndex = 0
            Else
                cmbactive.SelectedIndex = 1
            End If
        End If
    End Sub

    Private Sub state_disp()
        cmbstate.Items.Clear()
        cmbstate.Items.Add("Please Select The State")
        Dim ds1 As DataSet = get_dataset("SELECT stat_name From stat ORDER BY stat_name")
        For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
            cmbstate.Items.Add(ds1.Tables(0).Rows(i).Item("stat_name"))
        Next
    End Sub

    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        txtcity.Focus()
    End Sub

    Protected Sub cmbstate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbstate.SelectedIndexChanged
        Dim ds1 As DataSet = get_dataset("SELECT * FROM stat WHERE stat_name='" & Trim(cmbstate.Text) & "'")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtstatecd.Text = ds1.Tables(0).Rows(0).Item("stat_cd")
        Else
            txtstatecd.Text = ""
        End If
    End Sub
End Class
