﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class reports_monthly_stattement
    Inherits System.Web.UI.Page

    <System.Web.Script.Services.ScriptMethod(), _
System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            "staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblmsg.Visible = False
        txtfrmdt.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        txtfrmdt.Text = Format(Now, "dd/MM/yyyy")
        Me.divisiondisp()
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
    End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        If chk1.Checked = True Then
            Me.abstract()
        Else
            Me.details()
        End If
    End Sub

    Private Sub details()
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)

        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = str & "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If

        Dim dsstaf As DataSet = get_dataset("SELECT staf_sl,staf.staf_nm, staf.emp_code, staf.device_code, division_mst.div_nm, dept_mst.dept_nm, desg_mst.desg_nm FROM staf LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl WHERE staf.emp_status='I'  AND staf.loc_cd= " & loc_cd & "  " & str & "")
        Dim ds1 As DataSet = get_dataset("SELECT atnd.* FROM dept_mst RIGHT OUTER JOIN staf ON dept_mst.dept_sl = staf.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl RIGHT OUTER JOIN atnd ON staf.staf_sl = atnd.staf_sl WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txtto.Text), "dd/MMM/yyyy") & "' AND atnd.loc_cd= " & loc_cd & "  " & str & "  ORDER BY log_dt")
        Dim dsleave As DataSet = get_dataset("select leave_nm,leave_tp from leave_mst  ORDER BY leave_tp")
        Dim dsstaf_leave As DataSet = get_dataset("SELECT  count(staf_sl) as no,leave_mst.leave_tp,staf_sl FROM atnd LEFT OUTER JOIN leave_mst ON atnd.leave_sl = leave_mst.leave_sl WHERE atnd.leave_sl <> 0 AND log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txtto.Text), "dd/MMM/yyyy") & "' AND atnd.loc_cd= " & loc_cd & "  GROUP BY leave_tp,staf_sl ")
        Dim dt As New DataTable
        dt.Columns.Add("Sl.", GetType(String))
        dt.Columns.Add("Staf Name", GetType(String))
        dt.Columns.Add("Emp. Code", GetType(String))
        dt.Columns.Add("Division", GetType(String))
        dt.Columns.Add("Department", GetType(String))
        dt.Columns.Add("Designation", GetType(String))
        Dim current As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
        Dim ending As New DateTime(stringtodate(txtto.Text).Year, stringtodate(txtto.Text).Month, stringtodate(txtto.Text).Day)
        While current <= ending
            dt.Columns.Add(current.Date.Day, GetType(String))
            current = current.AddDays(1)
        End While
        dt.Columns.Add("Days Worked", GetType(String))
        dt.Columns.Add("Weekoff / Holiday", GetType(String))
        For j As Integer = 0 To dsleave.Tables(0).Rows.Count - 1
            dt.Columns.Add(dsleave.Tables(0).Rows(j).Item("leave_nm"), GetType(String))
        Next
        dt.Columns.Add("Total Days", GetType(String))

        For i As Integer = 0 To dsstaf.Tables(0).Rows.Count - 1
            Dim tot_p As Integer = 0
            Dim tot_a As Integer = 0
            Dim tot_w As Integer = 0
            Dim tot_l As Integer = 0
            Dim tot_h As Integer = 0
            Dim tot_hr As Integer = 0

            Dim current1 As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
            Dim ending1 As New DateTime(stringtodate(txtto.Text).Year, stringtodate(txtto.Text).Month, stringtodate(txtto.Text).Day)

            Dim dr As DataRow
            dr = dt.NewRow
            dr(0) = i + 1
            dr(1) = Trim(dsstaf.Tables(0).Rows(i).Item("staf_nm"))
            dr(2) = Trim(dsstaf.Tables(0).Rows(i).Item("emp_code"))
            dr(3) = Trim(dsstaf.Tables(0).Rows(i).Item("div_nm"))
            dr(4) = Trim(dsstaf.Tables(0).Rows(i).Item("dept_nm"))
            dr(5) = Trim(dsstaf.Tables(0).Rows(i).Item("desg_nm"))
            Dim column_cnt = 6
            While current1 <= ending1
                Dim day_status As String = ""
                Dim intime As String = ""
                Dim outtime As String = ""

                Dim dratnd As DataRow()
                dratnd = ds1.Tables(0).Select("log_dt='" & Format(current1, "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                If dratnd.Length <> 0 Then
                    If dratnd(0).Item("day_status") = "PRESENT" Then
                        day_status = "P"
                        tot_p = tot_p + 1
                    ElseIf dratnd(0).Item("day_status") = "HALFDAY" Then
                        day_status = "P"
                        tot_p = tot_p + 0.5
                    ElseIf dratnd(0).Item("day_status") = "ABSENT" Then
                        day_status = "A"
                        tot_a = tot_a + 1
                    ElseIf dratnd(0).Item("day_status") = "LEAVEDAY" Then
                        day_status = dratnd(0).Item("leave_nm")
                        tot_l = tot_l + 1
                    ElseIf dratnd(0).Item("day_status") = "WEEKLYOFF" Then
                        day_status = "W"
                        tot_w = tot_w + 1
                    ElseIf dratnd(0).Item("day_status") = "HOLIDAY" Then
                        day_status = "H"
                        tot_h = tot_h + 1
                    End If
                End If
                dr(column_cnt) = day_status
                column_cnt = column_cnt + 1
                current1 = current1.AddDays(1)
            End While
            column_cnt = column_cnt + 2
            dr("Days Worked") = tot_p
            dr("Weekoff / Holiday") = tot_w + tot_h
            Dim l1 As Integer = 0
            Dim drpay As DataRow()
            For k As Integer = 0 To dsleave.Tables(0).Rows.Count - 1
                drpay = dsstaf_leave.Tables(0).Select("leave_tp=" & dsleave.Tables(0).Rows(k).Item("leave_tp") & " AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                If drpay.Length <> 0 Then
                    dr(column_cnt) = drpay(0).Item(0)
                    If dsleave.Tables(0).Rows(k).Item("leave_tp") <> 3 Then
                        l1 = l1 + 1
                    End If
                End If
                column_cnt = column_cnt + 1
            Next
            dr("Total Days") = tot_p + tot_w + tot_h + l1
            dt.Rows.Add(dr)
        Next
        If dt.Rows.Count <> 0 Then
            Dim GridView112 As New GridView()
            GridView112.AllowPaging = False
            GridView112.DataSource = dt
            GridView112.DataBind()
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=MonthlyStatement.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            For i As Integer = 0 To GridView112.Rows.Count - 1
                GridView112.Rows(i).Attributes.Add("class", "textmode")
            Next
            GridView112.RenderControl(hw)
            'style to format numbers to string
            Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()
        End If
    End Sub

    Private Sub abstract()
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)

        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = str & "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If

        txtfrmdt.Text = "01/" & Format(stringtodate(txtfrmdt.Text).Month, "00") & "/" & Format(stringtodate(txtfrmdt.Text).Year, "0000")
        Dim tot_days As Integer = DateTime.DaysInMonth(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month)
        txtto.Text = Format(tot_days, "00") & "/" & Format(stringtodate(txtfrmdt.Text).Month, "00") & "/" & Format(stringtodate(txtfrmdt.Text).Year, "0000")

        Dim dsstaf As DataSet = get_dataset("SELECT staf_sl,staf.staf_nm, staf.emp_code, staf.device_code, division_mst.div_nm, dept_mst.dept_nm, desg_mst.desg_nm FROM staf LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl WHERE staf.emp_status='I'  AND staf.loc_cd= " & loc_cd & "  " & str & "")
        Dim ds1 As DataSet = get_dataset("SELECT atnd.* FROM dept_mst RIGHT OUTER JOIN staf ON dept_mst.dept_sl = staf.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl RIGHT OUTER JOIN atnd ON staf.staf_sl = atnd.staf_sl WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txtto.Text), "dd/MMM/yyyy") & "' AND atnd.loc_cd= " & loc_cd & "  " & str & "  ORDER BY log_dt")
        Dim dsleave As DataSet = get_dataset("select leave_nm,leave_tp from leave_mst  ORDER BY leave_tp")
        Dim dsstaf_leave As DataSet = get_dataset("SELECT  count(staf_sl) as no,leave_mst.leave_tp,staf_sl FROM atnd LEFT OUTER JOIN leave_mst ON atnd.leave_sl = leave_mst.leave_sl WHERE atnd.leave_sl <> 0 AND log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txtto.Text), "dd/MMM/yyyy") & "' AND atnd.loc_cd= " & loc_cd & "  GROUP BY leave_tp,staf_sl ")
        Dim dt As New DataTable
        dt.Columns.Add("Sl.", GetType(String))
        dt.Columns.Add("Staf Name", GetType(String))
        dt.Columns.Add("Emp. Code", GetType(String))
        dt.Columns.Add("Division", GetType(String))
        dt.Columns.Add("Department", GetType(String))
        dt.Columns.Add("Designation", GetType(String))

        Dim current As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
        Dim ending As New DateTime(stringtodate(txtto.Text).Year, stringtodate(txtto.Text).Month, stringtodate(txtto.Text).Day)
        dt.Columns.Add("Days Worked", GetType(String))
        dt.Columns.Add("Weekoff / Holiday", GetType(String))
        For j As Integer = 0 To dsleave.Tables(0).Rows.Count - 1
            dt.Columns.Add(dsleave.Tables(0).Rows(j).Item("leave_nm"), GetType(String))
        Next
        dt.Columns.Add("Total Days", GetType(String))

        For i As Integer = 0 To dsstaf.Tables(0).Rows.Count - 1
            Dim tot_p As Integer = 0
            Dim tot_a As Integer = 0
            Dim tot_w As Integer = 0
            Dim tot_l As Integer = 0
            Dim tot_h As Integer = 0
            Dim tot_hr As Integer = 0

            Dim current1 As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
            Dim ending1 As New DateTime(stringtodate(txtto.Text).Year, stringtodate(txtto.Text).Month, stringtodate(txtto.Text).Day)

            Dim dr As DataRow
            dr = dt.NewRow
            dr(0) = i + 1
            dr(1) = Trim(dsstaf.Tables(0).Rows(i).Item("staf_nm"))
            dr(2) = Trim(dsstaf.Tables(0).Rows(i).Item("emp_code"))
            dr(3) = Trim(dsstaf.Tables(0).Rows(i).Item("div_nm"))
            dr(4) = Trim(dsstaf.Tables(0).Rows(i).Item("dept_nm"))
            dr(5) = Trim(dsstaf.Tables(0).Rows(i).Item("desg_nm"))
            Dim column_cnt = 6
            While current1 <= ending1
                Dim day_status As String = ""
                Dim intime As String = ""
                Dim outtime As String = ""

                Dim dratnd As DataRow()
                dratnd = ds1.Tables(0).Select("log_dt='" & Format(current1, "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                If dratnd.Length <> 0 Then
                    If dratnd(0).Item("day_status") = "PRESENT" Then
                        day_status = "P"
                        tot_p = tot_p + 1
                    ElseIf dratnd(0).Item("day_status") = "HALFDAY" Then
                        day_status = "P"
                        tot_p = tot_p + 0.5
                    ElseIf dratnd(0).Item("day_status") = "ABSENT" Then
                        day_status = "A"
                        tot_a = tot_a + 1
                    ElseIf dratnd(0).Item("day_status") = "LEAVEDAY" Then
                        day_status = dratnd(0).Item("leave_nm")
                        tot_l = tot_l + 1
                    ElseIf dratnd(0).Item("day_status") = "WEEKLYOFF" Then
                        day_status = "W"
                        tot_w = tot_w + 1
                    ElseIf dratnd(0).Item("day_status") = "HOLIDAY" Then
                        day_status = "H"
                        tot_h = tot_h + 1
                    End If
                End If
                current1 = current1.AddDays(1)
            End While
            column_cnt = column_cnt + 2
            dr("Days Worked") = tot_p
            dr("Weekoff / Holiday") = tot_w + tot_h
            Dim l1 As Integer = 0
            Dim drpay As DataRow()
            For k As Integer = 0 To dsleave.Tables(0).Rows.Count - 1
                drpay = dsstaf_leave.Tables(0).Select("leave_tp=" & dsleave.Tables(0).Rows(k).Item("leave_tp") & " AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                If drpay.Length <> 0 Then
                    dr(column_cnt) = drpay(0).Item(0)
                    If dsleave.Tables(0).Rows(k).Item("leave_tp") <> 3 Then
                        l1 = l1 + 1
                    End If
                End If
                column_cnt = column_cnt + 1
            Next
            dr("Total Days") = tot_p + tot_w + tot_h + l1
            dt.Rows.Add(dr)
        Next
        If dt.Rows.Count <> 0 Then
            Dim GridView112 As New GridView()
            GridView112.AllowPaging = False
            GridView112.DataSource = dt
            GridView112.DataBind()
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=MonthlyStatement.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            For i As Integer = 0 To GridView112.Rows.Count - 1
                GridView112.Rows(i).Attributes.Add("class", "textmode")
            Next
            GridView112.RenderControl(hw)
            'style to format numbers to string
            Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()
        End If
    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub
End Class
