﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class payroll_odometerapproval
    Inherits System.Web.UI.Page
     Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        Image1.ImageUrl = "~/img/avatar-1.jpg"
        Me.dvdispl()
    End Sub

    Private Sub dvdispl()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        start1()
        SQLInsert("UPDATE Expense_Entry SET loc_cd=" & loc_cd & "")
        close1()
        Dim ds As DataSet = get_dataset("SELECT Row_number() OVER(ORDER BY expense_dt,expense_name) as Sl,CONVERT(varchar, Expense_Entry.expense_dt, 103) AS dt, Expense_Master.expense_name, Expense_Entry.expense_desc, STR(Expense_Entry.amount, 12, 2) AS amt, Expense_Entry.paid,Expense_Entry.ent_sl, staf.staf_nm, staf.emp_code, dept_mst.dept_nm, desg_mst.desg_nm FROM staf RIGHT OUTER JOIN Expense_Entry ON staf.staf_sl = Expense_Entry.staf_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl RIGHT OUTER JOIN Expense_Master ON Expense_Entry.expense_cd = Expense_Master.expense_cd WHERE status='P' and Expense_Entry.loc_cd=" & loc_cd & " ORDER BY expense_dt,expense_name")
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
    End Sub

    Protected Sub dvstaf_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dvstaf.RowCommand
        Dim rw As Integer = e.CommandArgument
        Dim lbl As Label = dvstaf.Rows(rw).FindControl("lblslno")
        If e.CommandName = "view" Then
            Dim ds1 As DataSet = get_dataset("SELECT exp_img FROM Expense_Entry WHERE ent_sl=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                Image1.ImageUrl = "data:image/jpg;base64," & ds1.Tables(0).Rows(0).Item("exp_img")
            End If
        ElseIf e.CommandName = "Approve" Then
            start1()
            SQLInsert("UPDATE Expense_Entry SET status='A',approved_by=" & CType(Session("usr_sl"), Integer) & ",approved_status_by='U' WHERE ent_sl=" & lbl.Text & "")
            close1()
        ElseIf e.CommandName = "pay" Then
            start1()
            SQLInsert("UPDATE Expense_Entry SET status='A',Paid='Y',approved_by=" & CType(Session("usr_sl"), Integer) & ",approved_status_by='U' WHERE ent_sl=" & lbl.Text & "")
            close1()
        ElseIf e.CommandName = "Cancel_expense" Then
            start1()
            SQLInsert("UPDATE Expense_Entry SET status='C' WHERE ent_sl=" & lbl.Text & "")
            close1()
        End If
        Me.dvdispl()
    End Sub
End Class
