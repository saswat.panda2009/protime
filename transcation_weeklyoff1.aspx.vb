﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class transcation_weeklyoff1
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
        txtmonth.Text = Format(Now, "dd/MM/yyyy")
        Me.divisiondisp()
        Me.deptdisp()
        Dim ds As DataSet = get_dataset("SELECT staf_nm, emp_code, device_code, staf_sl FROM staf")
        ds.Tables(0).Rows.Clear()
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
        If txtmode.Text = "E" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Weekly Off Assignment (Entry Mode)"
            cmbdivsion.Focus()
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Weekly Off Assignment (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub staf_disp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds As New DataSet
        If cmbdivsion.SelectedIndex = 0 Then
            If cmbdept.SelectedIndex = 0 Then
                ds = get_dataset("SELECT staf_nm, emp_code, device_code, staf_sl FROM staf WHERE loc_cd=" & loc_cd & " AND emp_status='I' ORDER BY staf_nm")
            Else
                ds = get_dataset("SELECT staf_nm, emp_code, device_code, staf_sl FROM staf WHERE loc_cd=" & loc_cd & " AND emp_status='I' AND dept_sl=" & Val(txtdeptcd.Text) & " ORDER BY staf_nm")
            End If
        Else
            If cmbdept.SelectedIndex = 0 Then
                ds = get_dataset("SELECT staf_nm, emp_code, device_code, staf_sl FROM staf WHERE loc_cd=" & loc_cd & " AND emp_status='I' AND div_sl=" & Val(txtdivsl.Text) & " ORDER BY staf_nm")
            Else
                ds = get_dataset("SELECT staf_nm, emp_code, device_code, staf_sl FROM staf WHERE loc_cd=" & loc_cd & " AND emp_status='I' AND staf.dept_sl=" & Val(txtdeptcd.Text) & " AND div_sl=" & Val(txtdivsl.Text) & " ORDER BY staf_nm")
            End If
        End If
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
    End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub deptdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdept.Items.Clear()
        cmbdept.Items.Add("Please Select A Department")
        Dim ds As DataSet = get_dataset("SELECT dept_nm FROM dept_mst WHERE loc_cd=" & loc_cd & " ORDER BY dept_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdept.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Protected Sub cmbdept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdept.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdeptcd.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT dept_sl FROM dept_mst WHERE dept_nm='" & Trim(cmbdept.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdeptcd.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
        Me.staf_disp()
    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
        Me.staf_disp()
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT division_mst.div_nm, staf.emp_code, staf.staf_nm, CONVERT(varchar, staf_shift.shift_date, 103) AS dt, shift_mst.shift_nm, staf_shift.sl FROM staf_shift INNER JOIN staf ON staf_shift.staf_sl = staf.staf_sl LEFT OUTER JOIN shift_mst ON staf_shift.shift_sl = shift_mst.shift_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl WHERE staf_shift.loc_cd=" & loc_cd & " ORDER BY shift_date,staf_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        Dim fnd As Integer = 0
        For i As Integer = 0 To dvstaf.Rows.Count - 1
            Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                fnd = 1
                Exit For
            End If
        Next
        If fnd = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Select Atleast One Employee');", True)
            dvstaf.Focus()
            Exit Sub
        End If
        Dim dsout As DataSet = get_dataset("SELECT * FROM staf_shift WHERE loc_cd=" & loc_cd & "")
        start1()
        For j As Integer = 0 To dvstaf.Rows.Count - 1
            Dim chk As CheckBox = dvstaf.Rows(j).FindControl("chk")
            If chk.Checked = True Then
                Dim lblsl As Label = dvstaf.Rows(j).FindControl("lblstaf_sl")
                Dim lbldevice As Label = dvstaf.Rows(j).FindControl("lbldevice_code")
                Dim cmbweekoff1 As DropDownList = dvstaf.Rows(j).FindControl("cmbweekoff1")
                SQLInsert("UPDATE staf SET weekly_off_tp='2' WHERE loc_cd=" & loc_cd & " AND staf_sl=" & lblsl.Text & "")
                SQLInsert("INSERT INTO wov1(staf_sl,loc_cd,effective_date,day1,week_day) VALUES(" & lblsl.Text & "," & loc_cd & ",'" & Format(stringtodate(txtmonth.Text), "dd/MMM/yyyy") & "','" & cmbweekoff1.Text & "'," & cmbweekoff1.SelectedIndex & ")")
            End If
        Next
        close1()
        Me.clr()
        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim lbl As Label = GridView1.Rows(rw).FindControl("lblslno")
            start1()
            'SQLInsert("DELETE FROM outddor_posting WHERE slno=" & lbl.Text & "")
            'SQLInsert("DELETE FROM elog WHERE slno=" & lbl.Text & "")
            close1()
        End If
        Me.dvdisp()
    End Sub


    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        cmbdivsion.Focus()
    End Sub

    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkhdr As CheckBox = dvstaf.HeaderRow.FindControl("chk1")
        If chkhdr.Checked = True Then
            For i As Integer = 0 To dvstaf.Rows.Count - 1
                Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
                chk.Checked = True
            Next
        ElseIf chkhdr.Checked = False Then
            For i As Integer = 0 To dvstaf.Rows.Count - 1
                Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
                chk.Checked = False
            Next
        End If
    End Sub
End Class
