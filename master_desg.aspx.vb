﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class master_desg
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtstate.Text = ""
        txtstatecd.Text = ""
        cmbactive.SelectedIndex = 0
        If txtmode.Text = "E" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Desgination Master (Entry Mode)"
            txtstate.Focus()
        ElseIf txtmode.Text = "M" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Desgination Master (Edit Mode)"
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Desgination Master (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT row_number() over(ORDER BY desg_nm) as 'sl',desg_nm,(case when active='Y' Then 'Yes' WHEN active='N' Then 'No' END)as 'active' FROM desg_mst WHERE loc_cd=" & loc_cd & " ORDER BY desg_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If txtmode.Text = "E" Then
            Dim dscheck As DataSet = get_dataset("SELECT desg_nm FROM desg_mst WHERE desg_nm='" & UCase(Trim(txtstate.Text)) & "' AND loc_cd=" & loc_cd & "")
            If dscheck.Tables(0).Rows.Count <> 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Desgination Name Already Exits');", True)
                txtstate.Focus()
                Exit Sub
            End If
            txtstatecd.Text = "1"
            Dim ds1 As DataSet = get_dataset("SELECT max(desg_sl) FROM desg_mst ")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                txtstatecd.Text = ds1.Tables(0).Rows(0).Item(0) + 1
            End If
            start1()
            SQLInsert("INSERT INTO desg_mst(desg_sl,desg_nm,loc_cd,active) VALUES(" & Val(txtstatecd.Text) & _
            ",'" & Trim(txtstate.Text) & "'," & loc_cd & ",'" & vb.Left(cmbactive.Text, 1) & "')")
            close1()
            Me.clr()
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
        ElseIf txtmode.Text = "M" Then
            Dim ds As DataSet = get_dataset("SELECT desg_nm FROM desg_mst WHERE desg_sl=" & Val(txtstatecd.Text) & "")
            If ds.Tables(0).Rows.Count <> 0 Then
                If Trim(txtstate.Text) <> ds.Tables(0).Rows(0).Item("desg_nm") Then
                    Dim dsd As DataSet = get_dataset("SELECT desg_nm FROM desg_mst WHERE desg_nm='" & UCase(Trim(txtstate.Text)) & "'")
                    If dsd.Tables(0).Rows.Count <> 0 Then
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Desgination Name Already Exits');", True)
                        txtstate.Focus()
                        Exit Sub
                    End If
                End If
                start1()
                SQLInsert("UPDATE desg_mst SET desg_nm='" & Trim(txtstate.Text) & "',active='" & _
                vb.Left(cmbactive.Text, 1) & "' WHERE desg_sl=" & Val(txtstatecd.Text) & "")
                close1()
                txtmode.Text = "V"
                Me.clr()
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Modified Succesffuly');", True)
            End If
        End If
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT desg_sl FROM desg_mst WHERE desg_nm='" & Trim(GridView1.Rows(rw).Cells(1).Text) & "' AND loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtmode.Text = "M"
                Me.clr()
                txtstatecd.Text = Val(ds1.Tables(0).Rows(0).Item("desg_sl"))
                Me.dvsel()
            End If
        End If
    End Sub

    Private Sub dvsel()
        Dim ds1 As DataSet = get_dataset("SELECT * FROM desg_mst WHERE desg_sl=" & Val(txtstatecd.Text) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtstate.Text = ds1.Tables(0).Rows(0).Item("desg_nm")
            If ds1.Tables(0).Rows(0).Item("active") = "Y" Then
                cmbactive.SelectedIndex = 0
            Else
                cmbactive.SelectedIndex = 1
            End If
        End If
    End Sub

    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        txtstate.Focus()
    End Sub
End Class
