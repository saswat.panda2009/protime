﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class payroll_group
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtbasic.Text = "0.00"
        txtchild.Text = "0.00"
        txtkmrate.Text = "0.00"
        txtcity.Text = "0.00"
        txtcompesi.Text = "0.00"
        txtcomppf.Text = "0.00"
        txtda.Text = "0.00"
        txtempesi.Text = "0.00"
        txtemppf.Text = "0.00"
        txtgross.Text = "0.00"
        txtgroupnm.Text = ""
        txtgroupsl.Text = ""
        txthra.Text = "0.00"
        txtpt.Text = "0.00"
        txtspecial.Text = "0.00"
        txttds.Text = "0.00"
        txttransport.Text = "0.00"
        txtwashing.Text = "0.00"
        txtmobile.Text = "0.00"
        txtfood.Text = "0.00"
        txtmedical.Text = "0.00"
        txtinsurance.Text = "0.00"
        cmbbasictp.SelectedIndex = 0
        cmbchildtp.SelectedIndex = 0
        cmbcity.SelectedIndex = 0
        cmbcompesi.SelectedIndex = 0
        cmbcomppf.SelectedIndex = 0
        cmbdatp.SelectedIndex = 0
        cmbempesi.SelectedIndex = 0
        cmbemppf.SelectedIndex = 0
        cmbesiappl.SelectedIndex = 0
        cmbhratp.SelectedIndex = 0
        cmbpfappl.SelectedIndex = 0
        cmbpt.SelectedIndex = 0
        cmbspecial.SelectedIndex = 0
        cmbtds.SelectedIndex = 0
        cmbtransporttp.SelectedIndex = 0
        cmbwashing.SelectedIndex = 0
        cmbmobile.SelectedIndex = 0
        cmbfood.SelectedIndex = 0
        cmbmedical.SelectedIndex = 0
        cmbinsurance.SelectedIndex = 0
        chkcompesicformula.Checked = False
        chkemppfoptional.Checked = False
        chkhra.Checked = False
        chkptoptional.Checked = False
        chkptformula.Checked = False
        chktdsformula.Checked = False
        chktdsoptional.Checked = False
        chkspclformula.Checked = False
        chktafull.Checked = False
        chkmobile.Checked = False
        If txtmode.Text = "E" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Payroll Group Master (Entry Mode)"
            txtgroupnm.Focus()
        ElseIf txtmode.Text = "M" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Payroll Group Master (Edit Mode)"
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Payroll Group Master (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT row_number() over(ORDER BY group_nm) as 'sl',group_nm FROM payroll_group WHERE loc_cd=" & loc_cd & " ORDER BY group_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If txtgroupnm.Text = "" Then
            Exit Sub
        End If
        Dim full_hra As Integer = IIf(chkhra.Checked, 1, 0)
        Dim full_ta As Integer = IIf(chktafull.Checked, 1, 0)
        Dim emp_pf_optional As String = IIf(chkemppfoptional.Checked, "Y", "N")
        Dim emp_pt_optional As String = IIf(chkptoptional.Checked, "Y", "N")
        Dim emp_tds_optional As String = IIf(chktdsoptional.Checked, "Y", "N")
        Dim comp_esic_formula As String = IIf(chkcompesicformula.Checked, "Y", "N")
        Dim pt_formula As String = IIf(chkptformula.Checked, "Y", "N")
        Dim tds_formula As String = IIf(chktdsformula.Checked, "Y", "N")
        Dim spl_formula As String = IIf(chkspclformula.Checked, "Y", "N")
        Dim fix_basic As String = IIf(chkbasic.Checked, "Y", "N")
        Dim fix_mobile As String = IIf(chkmobile.Checked, "Y", "N")

        If txtmode.Text = "E" Then
            Dim dscheck As DataSet = get_dataset("SELECT group_nm FROM payroll_group WHERE group_nm='" & UCase(Trim(txtgroupnm.Text)) & "' AND loc_cd=" & loc_cd & "")
            If dscheck.Tables(0).Rows.Count <> 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Payroll Group Name Already Exits');", True)
                txtcity.Focus()
                Exit Sub
            End If
            txtgroupsl.Text = "1"
            Dim ds1 As DataSet = get_dataset("SELECT max(group_sl) FROM payroll_group")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                txtgroupsl.Text = ds1.Tables(0).Rows(0).Item(0) + 1
            End If
            start1()

            SQLInsert("INSERT INTO payroll_group(group_sl,loc_cd,group_nm,pf_app,esi_app,gross,basic_tp,basic_amt,da_tp,da_amt,hra_tp,hra_amt,hra_app," & _
            "transport_tp,transport_amt,child_tp,child_amt,city_tp,city_amt,spcl_tp,spcl_amt,emp_pf_tp,emp_pf_amt,comp_pf_tp,comp_pf_amt,emp_esi_tp,emp_esi_amt," & _
            "comp_esi_tp,comp_esi_amt,pt_tp,pt_amt,tds_tp,tds_amt,washing_tp,washing_amt,emp_pf_optional,emp_pt_optional,emp_tds_optional,comp_esic_formula," & _
            "pt_formula,tds_formula,spl_formula,fix_basic,ta_app,mobile_tp,mobile_amt,fix_mobile,food_tp,food_amt,medical_tp,medical_amt,insurance_tp,insurance_amt,rate_per_km) VALUES(" & _
            Val(txtgroupsl.Text) & "," & loc_cd & ",'" & Trim(txtgroupnm.Text) & "','" & _
            vb.Left(cmbpfappl.Text, 1) & "','" & vb.Left(cmbesiappl.Text, 1) & "'," & Val(txtgross.Text) & ",'" & cmbbasictp.Text & "'," & Val(txtbasic.Text) & _
            ",'" & cmbdatp.Text & "'," & Val(txtda.Text) & ",'" & cmbhratp.Text & "'," & Val(txthra.Text) & "," & full_hra & ",'" & cmbtransporttp.Text & "'," & _
            Val(txttransport.Text) & ",'" & cmbchildtp.Text & "'," & Val(txtchild.Text) & ",'" & cmbcity.Text & "'," & Val(txtcity.Text) & ",'" & cmbspecial.Text & _
            "'," & Val(txtspecial.Text) & ",'" & cmbemppf.Text & "'," & Val(txtemppf.Text) & ",'" & cmbcomppf.Text & "'," & Val(txtcomppf.Text) & ",'" & cmbempesi.Text & _
            "'," & Val(txtempesi.Text) & ",'" & cmbcompesi.Text & "'," & Val(txtcompesi.Text) & ",'" & cmbpt.Text & "'," & Val(txtpt.Text) & ",'" & cmbtds.Text & "'," & _
            Val(txttds.Text) & ",'" & cmbwashing.Text & "'," & Val(txtwashing.Text) & ",'" & emp_pf_optional & "','" & emp_pt_optional & "','" & emp_tds_optional & _
            "','" & comp_esic_formula & "','" & pt_formula & "','" & tds_formula & "','" & spl_formula & "','" & fix_basic & "'," & full_ta & ",'" & cmbmobile.Text & _
            "'," & Val(txtmobile.Text) & ",'" & fix_mobile & "','" & cmbfood.Text & "'," & Val(txtfood.Text) & ",'" & cmbmedical.Text & "'," & Val(txtmedical.Text) & _
            ",'" & cmbinsurance.Text & "'," & Val(txtinsurance.Text) & "," & Val(txtkmrate.Text) & ")")
            close1()
            Me.clr()
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
        ElseIf txtmode.Text = "M" Then
            Dim ds As DataSet = get_dataset("SELECT group_nm FROM payroll_group WHERE group_sl=" & Val(txtgroupsl.Text) & "")
            If ds.Tables(0).Rows.Count <> 0 Then
                If Trim(txtgroupnm.Text) <> ds.Tables(0).Rows(0).Item("group_nm") Then
                    Dim dscheck As DataSet = get_dataset("SELECT group_nm FROM payroll_group WHERE group_nm='" & UCase(Trim(txtgroupnm.Text)) & "' AND loc_cd=" & loc_cd & "")
                    If dscheck.Tables(0).Rows.Count <> 0 Then
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Payroll Group Name Already Exits');", True)
                        txtcity.Focus()
                        Exit Sub
                    End If
                End If
                start1()
                SQLInsert("UPDATE payroll_group SET group_nm='" & Trim(txtgroupnm.Text) & "',pf_app='" & vb.Left(cmbpfappl.Text, 1) & "',esi_app='" & _
                vb.Left(cmbesiappl.Text, 1) & "',gross=" & Val(txtgross.Text) & ",basic_tp='" & cmbbasictp.Text & "',basic_amt=" & Val(txtbasic.Text) & _
                ",da_tp='" & cmbdatp.Text & "',da_amt=" & Val(txtda.Text) & ",hra_tp='" & cmbhratp.Text & "',hra_amt=" & Val(txthra.Text) & ",hra_app=" & full_hra & _
                ",transport_tp='" & cmbtransporttp.Text & "',transport_amt=" & Val(txttransport.Text) & ",child_tp='" & cmbchildtp.Text & "',child_amt=" & Val(txtchild.Text) & _
                ",city_tp='" & cmbcity.Text & "',city_amt=" & Val(txtcity.Text) & ",spcl_tp='" & cmbspecial.Text & "',spcl_amt=" & Val(txtspecial.Text) & ",emp_pf_tp='" & _
                cmbemppf.Text & "',emp_pf_amt=" & Val(txtemppf.Text) & ",comp_pf_tp='" & cmbcomppf.Text & "',comp_pf_amt=" & Val(txtcomppf.Text) & ",emp_esi_tp='" & _
                cmbempesi.Text & "',emp_esi_amt=" & Val(txtempesi.Text) & ",comp_esi_tp='" & cmbcompesi.Text & "',comp_esi_amt=" & Val(txtcompesi.Text) & ",pt_tp='" & _
                cmbpt.Text & "',pt_amt=" & Val(txtpt.Text) & ",tds_tp='" & cmbtds.Text & "',tds_amt=" & Val(txttds.Text) & ",washing_tp='" & cmbwashing.Text & _
                "',washing_amt=" & Val(txtwashing.Text) & ",emp_pf_optional='" & emp_pf_optional & "',emp_pt_optional='" & emp_pt_optional & "',emp_tds_optional='" & _
                emp_tds_optional & "',comp_esic_formula='" & comp_esic_formula & "',pt_formula='" & pt_formula & "',tds_formula='" & tds_formula & _
                "',spl_formula='" & spl_formula & "',fix_basic='" & fix_basic & "',ta_app=" & full_ta & ",mobile_tp ='" & cmbmobile.Text & "',mobile_amt =" & Val(txtmobile.Text) & _
                ", fix_mobile= '" & fix_mobile & "',food_tp ='" & cmbfood.Text & "',food_amt =" & Val(txtfood.Text) & ",medical_tp ='" & cmbmedical.Text & "',medical_amt=" & txtmedical.Text & _
                ",insurance_tp='" & cmbinsurance.Text & "',insurance_amt =" & Val(txtinsurance.Text) & ",rate_per_km=" & Val(txtkmrate.Text) & " WHERE group_sl=" & Val(txtgroupsl.Text) & "")
                close1()
                Me.clr()
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Modified Succesffuly');", True)
            End If
        End If
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT group_sl FROM payroll_group WHERE group_nm='" & Trim(GridView1.Rows(rw).Cells(1).Text) & "' AND loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtmode.Text = "M"
                Me.clr()
                txtgroupsl.Text = Val(ds1.Tables(0).Rows(0).Item("group_sl"))
                Me.dvsel()
            End If
        End If
    End Sub

    Private Sub dvsel()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT payroll_group.* FROM payroll_group WHERE group_sl=" & Val(txtgroupsl.Text) & " AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtbasic.Text = Format(Val(ds1.Tables(0).Rows(0).Item("basic_amt")), "#######0.00")
            txtchild.Text = Format(Val(ds1.Tables(0).Rows(0).Item("child_amt")), "#######0.00")
            txtcity.Text = Format(Val(ds1.Tables(0).Rows(0).Item("city_amt")), "#######0.00")
            txtcompesi.Text = Format(Val(ds1.Tables(0).Rows(0).Item("comp_esi_amt")), "#######0.00")
            txtcomppf.Text = Format(Val(ds1.Tables(0).Rows(0).Item("comp_pf_amt")), "#######0.00")
            txtda.Text = Format(Val(ds1.Tables(0).Rows(0).Item("da_amt")), "#######0.00")
            txtempesi.Text = Format(Val(ds1.Tables(0).Rows(0).Item("emp_esi_amt")), "#######0.00")
            txtemppf.Text = Format(Val(ds1.Tables(0).Rows(0).Item("emp_pf_amt")), "#######0.00")
            txtgross.Text = Format(Val(ds1.Tables(0).Rows(0).Item("gross")), "#######0.00")
            txtgroupnm.Text = ds1.Tables(0).Rows(0).Item("group_nm")
            txtgroupsl.Text = ds1.Tables(0).Rows(0).Item("group_sl")
            txthra.Text = Format(Val(ds1.Tables(0).Rows(0).Item("hra_amt")), "#######0.00")
            txtpt.Text = Format(Val(ds1.Tables(0).Rows(0).Item("pt_amt")), "#######0.00")
            txtspecial.Text = Format(Val(ds1.Tables(0).Rows(0).Item("spcl_amt")), "#######0.00")
            txttds.Text = Format(Val(ds1.Tables(0).Rows(0).Item("tds_amt")), "#######0.00")
            txttransport.Text = Format(Val(ds1.Tables(0).Rows(0).Item("transport_amt")), "#######0.00")
            txtmobile.Text = Format(Val(ds1.Tables(0).Rows(0).Item("mobile_amt")), "#######0.00")
            txtfood.Text = Format(Val(ds1.Tables(0).Rows(0).Item("food_amt")), "#######0.00")
            txtmedical.Text = Format(Val(ds1.Tables(0).Rows(0).Item("medical_amt")), "#######0.00")
            txtinsurance.Text = Format(Val(ds1.Tables(0).Rows(0).Item("insurance_amt")), "#######0.00")
            txtkmrate.Text = Format(Val(ds1.Tables(0).Rows(0).Item("rate_per_km")), "#######0.00")
            cmbbasictp.Text = ds1.Tables(0).Rows(0).Item("basic_tp")
            cmbchildtp.Text = ds1.Tables(0).Rows(0).Item("child_tp")
            cmbcity.Text = ds1.Tables(0).Rows(0).Item("city_tp")
            cmbcompesi.Text = ds1.Tables(0).Rows(0).Item("comp_esi_tp")
            cmbcomppf.Text = ds1.Tables(0).Rows(0).Item("comp_pf_tp")
            cmbdatp.Text = ds1.Tables(0).Rows(0).Item("da_tp")
            cmbempesi.Text = ds1.Tables(0).Rows(0).Item("emp_esi_tp")
            cmbemppf.Text = ds1.Tables(0).Rows(0).Item("emp_pf_tp")
            cmbinsurance.Text = ds1.Tables(0).Rows(0).Item("insurance_tp")
            cmbmobile.Text = ds1.Tables(0).Rows(0).Item("mobile_tp")
            cmbfood.Text = ds1.Tables(0).Rows(0).Item("food_tp")
            cmbmedical.Text = ds1.Tables(0).Rows(0).Item("medical_tp")
            If ds1.Tables(0).Rows(0).Item("esi_app") = "Y" Then
                cmbesiappl.SelectedIndex = 0
            Else
                cmbesiappl.SelectedIndex = 1
            End If
            If ds1.Tables(0).Rows(0).Item("esi_app") = "Y" Then
                cmbpfappl.SelectedIndex = 0
            Else
                cmbpfappl.SelectedIndex = 1
            End If
            cmbhratp.Text = ds1.Tables(0).Rows(0).Item("hra_tp")
            cmbpt.Text = ds1.Tables(0).Rows(0).Item("pt_tp")
            cmbspecial.Text = ds1.Tables(0).Rows(0).Item("spcl_tp")
            cmbtds.Text = ds1.Tables(0).Rows(0).Item("tds_tp")
            cmbtransporttp.Text = ds1.Tables(0).Rows(0).Item("transport_tp")
            cmbwashing.Text = ds1.Tables(0).Rows(0).Item("washing_tp")
            txtwashing.Text = Format(Val(ds1.Tables(0).Rows(0).Item("washing_amt")), "#######0.00")
            If ds1.Tables(0).Rows(0).Item("emp_pf_optional") = "Y" Then
                chkemppfoptional.Checked = True
            Else
                chkemppfoptional.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("emp_pt_optional") = "Y" Then
                chkptoptional.Checked = True
            Else
                chkptoptional.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("emp_tds_optional") = "Y" Then
                chktdsoptional.Checked = True
            Else
                chktdsoptional.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("comp_esic_formula") = "Y" Then
                chkcompesicformula.Checked = True
            Else
                chkcompesicformula.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("pt_formula") = "Y" Then
                chkptformula.Checked = True
            Else
                chkptformula.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("tds_formula") = "Y" Then
                chktdsformula.Checked = True
            Else
                chktdsformula.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("spl_formula") = "Y" Then
                chkspclformula.Checked = True
            Else
                chkspclformula.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("fix_basic") = "Y" Then
                chkbasic.Checked = True
            Else
                chkbasic.Checked = False
            End If
            If ds1.Tables(0).Rows(0).Item("fix_mobile") = "Y" Then
                chkmobile.Checked = True
            Else
                chkmobile.Checked = False
            End If
            If Val(ds1.Tables(0).Rows(0).Item("ta_app")) = 1 Then
                chktafull.Checked = True
            Else
                chktafull.Checked = False
            End If
        End If
    End Sub

    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        txtcity.Focus()
    End Sub
End Class
