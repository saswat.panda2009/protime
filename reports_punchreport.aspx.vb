﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class reports_punchreport
    Inherits System.Web.UI.Page

   <System.Web.Script.Services.ScriptMethod(), _
 System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            "staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            start1()
            SQLInsert("UPDATE elog SET loc_cd =(SELECT loc_cd FROM Devices WHERE SerialNumber=elog.Serialno),staf_sl=(SELECT staf_sl FROM staf WHERE device_code=elog.device_code), device_no=(SELECT deviceid FROM Devices WHERE SerialNumber=elog.Serialno) WHERE log_tp='P'")
            close1()
            Me.clr()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblmsg.Visible = False
        txtfrmdt.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        txttodt.Text = Format(Now, "dd/MM/yyyy")
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim str As String = ""
        If txtdevicecode.Text <> "" Then
            str = "and device_code='" & txtdevicecode.Text & "'"
        End If
        Dim ds1 As DataSet = get_dataset("SELECT   convert(varchar,log_dt,103) as 'Date',  device_code as 'Device Code',(Select staf_nm FROM staf WHERE staf_sl=elog.staf_sl) as 'Employee Name',  Convert(varchar,log_time,108) as 'Time',DeviceDirection  FROM elog WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & " " & str & " ORDER BY log_dt,log_time")
        If ds1.Tables(0).Rows.Count <> 0 Then
            dvdata.DataSource = ds1.Tables(0)
            dvdata.DataBind()
        Else
            dvdata.DataSource = ds1.Tables(0)
            dvdata.DataBind()
            lblmsg.Text = "No Record Found."
        End If

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim str As String = ""
        If txtdevicecode.Text <> "" Then
            str = "and device_code='" & txtdevicecode.Text & "'"
        End If
        Dim ds1 As DataSet = get_dataset("SELECT   convert(varchar,log_dt,103) as 'Date',  device_code as 'Device Code',(Select staf_nm FROM staf WHERE staf_sl=elog.staf_sl) as 'Employee Name',Convert(varchar,log_time,108) as 'Time',DeviceDirection  FROM elog WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & " " & str & " ORDER BY log_dt,log_time")

        Dim dtcust As New DataTable
        dtcust = ds1.Tables(0)

        'Create a dummy GridView

        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.DataSource = dtcust
        GridView1.DataBind()
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=PunchRegister.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        For i As Integer = 0 To GridView1.Rows.Count - 1
            GridView1.Rows(i).Attributes.Add("class", "textmode")
        Next
        GridView1.RenderControl(hw)
        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()
    End Sub
End Class
