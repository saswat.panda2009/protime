﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class payroll_loanplanner
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtassno.Text = ""
        txtempcode.Text = ""
        txtstafsl.Text = ""
        txtstafnm.Text = ""
        txtdept.Text = ""
        txtdesg.Text = ""
        txtdate.Text = Format(Now, "dd/MM/yyyy")
        txtsdt.Text = "01/" & Format(Now.Month, "00") & "/" & Format(Now.Year, "00")
        txtdetails.Text = ""
        txtapproved.Text = ""
        cmbmode.SelectedIndex = 0
        txtloanamount.Text = "0.00"
        txtinstamt.Text = "0.00"
        txtno.Text = "0"
        txtnewamt.Text = "0.00"
        txtnewdt.Text = "01/" & Format(Now.Month, "00") & "/" & Format(Now.Year, "00")
        dvstaf.DataSource = Nothing
        dvstaf.DataBind()
    End Sub

    Protected Sub cmdsave0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave0.Click
        Dim ds1 As DataSet = get_dataset("SELECT staf.staf_sl,staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm FROM desg_mst RIGHT OUTER JOIN staf ON desg_mst.desg_sl = staf.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl  WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd= " & CType(Session("loc_cd"), Integer) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            Me.clr()
            txtstafsl.Text = ds1.Tables(0).Rows(0).Item("staf_sl")
            txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            txtstafnm.Text = ds1.Tables(0).Rows(0).Item("staf_nm")
            txtdept.Text = ds1.Tables(0).Rows(0).Item("dept_nm")
            txtdesg.Text = ds1.Tables(0).Rows(0).Item("desg_nm")
        End If

        Dim ds As DataSet = get_dataset("SELECT ROW_NUMBER() OVER(ORDER BY loan_planner2.loan_dt) as sl,loan_planner1.*, Convert(varchar,loan_planner2.loan_dt,103) AS Date, str(loan_planner2.loan_amount,12,2) AS Amount, loan_planner2.loan_deduct AS paid FROM loan_planner1 RIGHT OUTER JOIN loan_planner2 ON loan_planner1.loan_no = loan_planner2.loan_no WHERE loan_planner1.staf_sl=" & Val(txtstafsl.Text) & " AND paid='N' ORDER BY loan_planner2.loan_dt ")
        If ds.Tables(0).Rows.Count <> 0 Then
            txtsdt.Text = ds.Tables(0).Rows(0).Item("Date")
            txtdetails.Text = ds.Tables(0).Rows(0).Item("transcation_details")
            cmbmode.SelectedIndex = Val(ds.Tables(0).Rows(0).Item("pay_mode")) - 1
            txtloanamount.Text = Format(Val(ds.Tables(0).Rows(0).Item("loan_amount")), "######0.00")
            txtinstamt.Text = Format(Val(ds.Tables(0).Rows(0).Item("installment_amt")), "######0.00")
            txtno.Text = ds.Tables(0).Rows(0).Item("install_no")
            txtapproved.Text = ds.Tables(0).Rows(0).Item("approved_by")
            dvstaf.DataSource = ds.Tables(0)
            dvstaf.DataBind()
        End If
    End Sub

    Protected Sub cmdsave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave1.Click
        Dim paid As Integer = 0
        For i As Integer = 0 To dvstaf.Rows.Count - 1
            Dim lblpaid As Label = dvstaf.Rows(i).FindControl("lblpaid")
            If lblpaid.Text = "Y" Then
                paid = 1
                Exit For
            End If
        Next
        If Val(paid) = 1 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Sorry You Cannot Make A Planner');", True)
            Exit Sub
        End If
        If Val(txtloanamount.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Loan Amount Should Not be Zero');", True)
            txtloanamount.Focus()
            Exit Sub
        End If
        If Val(txtno.Text) <> 0 Then
            txtinstamt.Text = Format(Val(txtloanamount.Text) / Val(txtno.Text), "####0.00")
        End If

        If Val(txtinstamt.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Installment Amount Should Not be Zero');", True)
            txtinstamt.Focus()
            Exit Sub
        End If
        txtno.Text = Int(Val(txtloanamount.Text) / Val(txtinstamt.Text))
        If (Val(txtloanamount.Text) Mod Val(txtinstamt.Text)) <> 0 Then
            txtno.Text = Val(txtno.Text) + 1
        End If
        Dim dt As New DataTable
        dt.Columns.Add("Sl", GetType(Integer))
        dt.Columns.Add("Date", GetType(String))
        dt.Columns.Add("Amount", GetType(String))
        dt.Columns.Add("paid", GetType(String))
        Dim sdt As Date = stringtodate(txtsdt.Text)
        Dim cnt As Integer = 1
        For i As Integer = 0 To Int(Val(txtloanamount.Text) / Val(txtinstamt.Text)) - 1
            dt.Rows.Add(cnt, Format(sdt, "dd/MM/yyyy"), Format(Val(txtinstamt.Text), "####0.00"), "N")
            sdt = sdt.AddMonths(1)
            cnt = cnt + 1
        Next
        If (Val(txtloanamount.Text) Mod Val(txtinstamt.Text)) <> 0 Then
            dt.Rows.Add(cnt, Format(sdt, "dd/MM/yyyy"), Format(Val(Val(txtloanamount.Text) Mod Val(txtinstamt.Text)), "####0.00"), "N")
        End If
        dvstaf.DataSource = dt
        dvstaf.DataBind()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim tot As Decimal
        For i As Integer = 0 To dvstaf.Rows.Count - 1
            Dim lblamt As Label = dvstaf.Rows(i).FindControl("lblamt")
            tot = tot + Val(lblamt.Text)
        Next
        If Val(txtloanamount.Text) <> Val(tot) Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Loan Amount and Sum of Installment Doesnot Matched');", True)
            Exit Sub
        End If
        start1()
        Dim ds As DataSet = get_dataset("SELECT loan_no from loan_planner1 WHERE staf_sl=" & Val(txtstafsl.Text) & " AND paid='N'")
        If ds.Tables(0).Rows.Count = 0 Then
            txtassno.Text = "1"
            Dim ds1 As DataSet = get_dataset("SELECT max(loan_no) FROM loan_planner1")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                txtassno.Text = ds1.Tables(0).Rows(0).Item(0) + 1
            End If
            SQLInsert("INSERT INTO loan_planner1(loan_no,loan_dt,loc_cd,staf_sl,loan_amount,install_no,approved_by,pay_mode,transcation_details,paid,installment_amt) VALUES(" & _
            Val(txtassno.Text) & ",'" & Format(stringtodate(txtdate.Text), "dd/MMM/yyyy") & "'," & loc_cd & "," & Val(txtstafsl.Text) & _
            "," & Val(txtloanamount.Text) & "," & Val(txtno.Text) & ",'" & txtapproved.Text & "','" & vb.Left(cmbmode.Text, 1) & "','" & txtdetails.Text & "','N'," & Val(txtinstamt.Text) & ")")
        Else
            txtassno.Text = ds.Tables(0).Rows(0).Item("loan_no")
        End If
        SQLInsert("DELETE FROM loan_planner2 WHERE loan_no=" & Val(txtassno.Text) & "")
        For i As Integer = 0 To dvstaf.Rows.Count - 1
            Dim lbldt As Label = dvstaf.Rows(i).FindControl("lbldt")
            Dim lblamt As Label = dvstaf.Rows(i).FindControl("lblamt")
            Dim lblpaid As Label = dvstaf.Rows(i).FindControl("lblpaid")

            Dim max As Integer = 1
            Dim ds11 As DataSet = get_dataset("SELECT max(loan_sl) FROM loan_planner2")
            If Not IsDBNull(ds11.Tables(0).Rows(0).Item(0)) Then
                max = ds11.Tables(0).Rows(0).Item(0) + 1
            End If
            SQLInsert("INSERT INTO loan_planner2(loan_sl,loan_no,loan_dt,loc_cd,staf_sl,loan_month,loan_year,loan_amount,loan_deduct) VALUES(" & max & _
            "," & Val(txtassno.Text) & ",'" & Format(stringtodate(lbldt.Text), "dd/MMM/yyyy") & "'," & loc_cd & "," & Val(txtstafsl.Text) & "," & _
            stringtodate(lbldt.Text).Month & "," & stringtodate(lbldt.Text).Year & "," & Val(lblamt.Text) & ",'" & lblpaid.Text & "')")
        Next
        close1()
        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Loan Planner Completed.');", True)
        Me.clr()
        txtempcode.Focus()
    End Sub

    Protected Sub dvstaf_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dvstaf.RowCommand
        Dim r As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim lblpaid As Label = dvstaf.Rows(r).FindControl("lblpaid")
            If lblpaid.Text = "Y" Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Sorry You Cannot Delete The Installment Which is Paid.');", True)
                Exit Sub
            End If
            Dim dt As New DataTable
            dt.Columns.Add("Sl", GetType(Integer))
            dt.Columns.Add("Date", GetType(String))
            dt.Columns.Add("Amount", GetType(String))
            dt.Columns.Add("paid", GetType(String))
            Dim cnt As Integer = 1
            For i As Integer = 0 To dvstaf.Rows.Count - 1
                Dim lbldt As Label = dvstaf.Rows(i).FindControl("lbldt")
                Dim lblamt As Label = dvstaf.Rows(i).FindControl("lblamt")
                Dim lblpaid1 As Label = dvstaf.Rows(i).FindControl("lblpaid")
                If i <> r Then
                    dt.Rows.Add(cnt, lbldt.Text, Format(Val(lblamt.Text), "######0.00"), lblpaid1.Text)
                    cnt = cnt + 1
                End If
            Next
            dvstaf.DataSource = dt
            dvstaf.DataBind()
        End If
    End Sub

    Protected Sub cmdsave2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave2.Click
        If Val(txtnewamt.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Installment Amount Should Not be Zero');", True)
            txtnewamt.Focus()
            Exit Sub
        End If

        Dim dt As New DataTable
        dt.Columns.Add("Sl", GetType(Integer))
        dt.Columns.Add("Date", GetType(String))
        dt.Columns.Add("Amount", GetType(String))
        dt.Columns.Add("paid", GetType(String))
        Dim cnt As Integer = 1
        For i As Integer = 0 To dvstaf.Rows.Count - 1
            Dim lbldt As Label = dvstaf.Rows(i).FindControl("lbldt")
            Dim lblamt As Label = dvstaf.Rows(i).FindControl("lblamt")
            Dim lblpaid1 As Label = dvstaf.Rows(i).FindControl("lblpaid")
            dt.Rows.Add(cnt, lbldt.Text, Format(Val(lblamt.Text), "######0.00"), lblpaid1.Text)
            cnt = cnt + 1
        Next
        dt.Rows.Add(cnt, txtnewdt.Text, Format(Val(txtnewamt.Text), "#####0.00"), "N")
        dvstaf.DataSource = dt
        dvstaf.DataBind()
    End Sub
End Class
