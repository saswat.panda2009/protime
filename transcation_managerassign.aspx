﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="transcation_managerassign.aspx.vb" Inherits="transcation_managerassign" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                 
                </div>

                     <div class="card-body">
                                     <table style="width:100%;">
                         
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table width="100%">
                                                                                        <tr>
                                                  <td colspan="3">
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="45%">
                                                                  Division</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  Department</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:DropDownList ID="cmbdivsion" runat="server" AutoPostBack="True" 
                                                                      class="form-control">
                                                                  </asp:DropDownList>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:DropDownList ID="cmbdept" runat="server" AutoPostBack="True" 
                                                                      class="form-control">
                                                                  </asp:DropDownList>
                                                              </td>
                                                          </tr>
                                                        
                                                      </table>
                                                  </td>
                                              </tr>
                                             
                                              <tr>
                                                  <td width="45%">
                                                      Employee Code</td>
                                                  <td xml:lang="10%">
                                                      &nbsp;</td>
                                                  <td width="45%">
                                                      &nbsp;</td>
                                              </tr>
                                                                                        <tr>
                                                                                            <td width="45%">
                                                                                                <asp:TextBox ID="txtempcode" runat="server" AutoPostBack="True" 
                                                                                                    class="form-control" MaxLength="100" placeholder="Emp. Code"></asp:TextBox>
                                                                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" 
                                                                                                    CompletionInterval="100" CompletionListCssClass="wordWheel listMain .box" 
                                                                                                    CompletionListHighlightedItemCssClass="wordWheel itemsSelected" 
                                                                                                    CompletionListItemCssClass="wordWheel itemsMain" CompletionSetCount="10" 
                                                                                                    EnableCaching="false" FirstRowSelected="false" MinimumPrefixLength="1" 
                                                                                                    ServiceMethod="SearchEmei" TargetControlID="txtempcode">
                                                                                                </asp:AutoCompleteExtender>
                                                                                                <asp:FilteredTextBoxExtender ID="txtempcode_FilteredTextBoxExtender" 
                                                                                                    runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                                                    TargetControlID="txtempcode">
                                                                                                </asp:FilteredTextBoxExtender>
                                                                                            </td>
                                                                                            <td xml:lang="10%">
                                                                                                &nbsp;</td>
                                                                                            <td width="45%">
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      <div style="width: 100%; height: 300px; overflow: auto;">
                                                          <asp:GridView ID="dvstaf" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                              AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                              PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                              <AlternatingRowStyle CssClass="alt" />
                                                              <Columns>
                                                                  <asp:TemplateField>
                                                                  <HeaderTemplate>
                                                                   <asp:CheckBox  runat="Server" ID="chk1" AutoPostBack="True" OnCheckedChanged="CheckBox1_CheckedChanged"/>
                                                                  </HeaderTemplate>
                                                                  <ItemTemplate>
                                                                  <asp:CheckBox  runat="Server" ID="chk"/>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Employee ID">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblempid" Text='<%#Eval("emp_code") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Device Code">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lbldevice_code" Text='<%#Eval("device_code") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Employee Name">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblstaf_nm" Text='<%#Eval("staf_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField Visible="False">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblstaf_sl" Text='<%#Eval("staf_sl") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                              </Columns>
                                                              <PagerStyle HorizontalAlign="Right" />
                                                          </asp:GridView>
                                                      </div>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      <asp:Button ID="cmdsave" runat="server" class="btn btn-primary" Text="Submit" />
                                                      <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                          class="btn btn-default" Text="Reset" />
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      <asp:TextBox ID="txtmode" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtdeptcd" runat="server" Height="22px" Visible="False" 
                                                          Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtdivsl" runat="server" Height="22px" Visible="False" 
                                                          Width="20px"></asp:TextBox>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table> 
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

