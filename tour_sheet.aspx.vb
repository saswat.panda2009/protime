﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.Net.Mail

Partial Class tour_sheet
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            txtmode.Text = "V"
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtdescription.Text = ""
        txttittle.Text = ""
        txttoursl.Text = ""
        txtfrom.Text = Format(Now, "dd/MM/yyyy")
        txtto.Text = Format(Now, "dd/MM/yyyy")
        If txtmode.Text = "M" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Tour Expense Approval ..."
            txtfrom.Focus()
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Tour Expense Approval ..."
            Me.dvdisp()
        End If
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim staf_sl As Integer = CType(Session("staf_sl"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT  Row_number() OVER(ORDER BY travel_startdt) as Sl,Travel_CreateNew.travel_tittle, CONVERT(varchar, Travel_CreateNew.travel_startdt, 103) AS Sdt, CONVERT(varchar, Travel_CreateNew.travel_enddt, 103) AS edt, Travel_CreateNew.travel_descr, Travel_CreateNew.travel_sl, staf.staf_nm, staf.emp_code, dept_mst.dept_nm, desg_mst.desg_nm FROM desg_mst RIGHT OUTER JOIN staf ON desg_mst.desg_sl = staf.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl RIGHT OUTER JOIN Travel_CreateNew ON staf.staf_sl = Travel_CreateNew.staf_sl WHERE travel_status=8 and Travel_CreateNew.loc_cd=" & loc_cd & " ORDER BY travel_startdt")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim lbl As Label = GridView1.Rows(rw).FindControl("lblslno")
            txtmode.Text = "M"
            Me.clr()
            Dim ds As DataSet = get_dataset("SELECT ROW_NUMBER() OVER(ORDER BY plan_time) as sl,CONVERT(varchar, Travel_planner.plan_dt, 103) AS [date], CONVERT(varchar, Travel_planner.plan_time, 108) AS [time], Travel_planner.plan_descr AS descr, staf.staf_nm, Travel_CreateNew.*, dept_mst.dept_nm, desg_mst.desg_nm FROM staf LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl RIGHT OUTER JOIN Travel_CreateNew ON staf.staf_sl = Travel_CreateNew.staf_sl RIGHT OUTER JOIN Travel_planner ON Travel_CreateNew.travel_sl = Travel_planner.travel_sl WHERE Travel_planner.travel_sl=" & lbl.Text & "  ORDER BY plan_time")
            If ds.Tables(0).Rows.Count <> 0 Then
                txtdescription.Text = ds.Tables(0).Rows(0).Item("travel_descr")
                txttittle.Text = ds.Tables(0).Rows(0).Item("travel_tittle")
                txttoursl.Text = ds.Tables(0).Rows(0).Item("travel_sl")
                txtstafnm.Text = ds.Tables(0).Rows(0).Item("staf_nm")
                txtdept.Text = ds.Tables(0).Rows(0).Item("dept_nm")
                txtdesg.Text = ds.Tables(0).Rows(0).Item("desg_nm")
                txtfrom.Text = Format(ds.Tables(0).Rows(0).Item("travel_startdt"), "dd/MM/yyyy")
                txtto.Text = Format(ds.Tables(0).Rows(0).Item("travel_enddt"), "dd/MM/yyyy")
                Me.dvdispexpense1()
                Me.dvdispexpense2()
                Me.dvdispexpense3()
                Me.dvdispexpense4()
            End If
        End If
    End Sub


    Private Sub dvdispexpense3()
        Dim ds As DataSet = get_dataset("SELECT Row_number() OVER(ORDER BY expense_dt) as slno,sl,Convert(varchar,expense_dt,103) as dt,expense_det,expense_mode,expense_from,expense_to,str(expense_amt,12,2) as amt,expense_amt,expense_bill,(Case WHEN bill_img ='' THEN 'No' WHEN bill_img <>'' THEN 'YES' END) as atch FROM Travel_Expenses_Conveyances2 WHERE travel_sl=" & Val(txttoursl.Text) & " AND expense_status='A' ORDER BY expense_dt")
        dvexpense3.DataSource = ds.Tables(0)
        dvexpense3.DataBind()
        If ds.Tables(0).Rows.Count <> 0 Then
            txttotalexpense3.Text = Format(ds.Tables(0).Compute("SUM(expense_amt)", ""), "######0.00")
        End If
        lbltotexpense.Text = Format(Val(txttotalexpense1.Text) + Val(txttotalexpense3.Text) + Val(txttotalexpense4.Text), "######0.00")
    End Sub

    Private Sub dvdispexpense1()
        Dim ds As DataSet = get_dataset("SELECT Row_number() OVER(ORDER BY expense_dt) as slno,sl,Convert(varchar,expense_dt,103) as dt,expense_det,expense_mode,expense_from,expense_to,str(expense_amt,12,2) as amt,expense_amt,expense_bill,(Case WHEN bill_img ='' THEN 'No' WHEN bill_img <>'' THEN 'YES' END) as atch FROM Travel_Expenses_Conveyances1 WHERE travel_sl=" & Val(txttoursl.Text) & "  AND expense_status='A' ORDER BY expense_dt")
        dvexpense1.DataSource = ds.Tables(0)
        dvexpense1.DataBind()
        If ds.Tables(0).Rows.Count <> 0 Then
            txttotalexpense1.Text = Format(ds.Tables(0).Compute("SUM(expense_amt)", ""), "######0.00")
        End If
        lbltotexpense.Text = Format(Val(txttotalexpense1.Text) + Val(txttotalexpense3.Text) + Val(txttotalexpense4.Text), "######0.00")
    End Sub

    Private Sub dvdispexpense2()
        Dim ds As DataSet = get_dataset("SELECT Row_number() OVER(ORDER BY expense_dt) as slno,sl,Convert(varchar,expense_dt,103) as dt,expense_det,expense_days,expense_from,expense_to,str(expense_amt,12,2) as amt,expense_amt,expense_bill,(Case WHEN bill_img ='' THEN 'No' WHEN bill_img <>'' THEN 'YES' END) as atch FROM travel_expenses_lodging WHERE travel_sl=" & Val(txttoursl.Text) & "  AND expense_status='A'  ORDER BY expense_dt")
        dvexpense2.DataSource = ds.Tables(0)
        dvexpense2.DataBind()
        If ds.Tables(0).Rows.Count <> 0 Then
            txttotalexpense2.Text = Format(ds.Tables(0).Compute("SUM(expense_amt)", ""), "######0.00")
        End If
        lbltotexpense.Text = Format(Val(txttotalexpense1.Text) + Val(txttotalexpense3.Text) + Val(txttotalexpense4.Text), "######0.00")
    End Sub

    Private Sub dvdispexpense4()
        Dim ds As DataSet = get_dataset("SELECT Row_number() OVER(ORDER BY expense_dt) as slno,sl,Convert(varchar,expense_dt,103) as dt,expense_det,str(expense_amt,12,2) as amt,expense_amt,(Case WHEN bill_img ='' THEN 'No' WHEN bill_img <>'' THEN 'YES' END) as atch FROM travel_expenses_food WHERE travel_sl=" & Val(txttoursl.Text) & "  AND expense_status='A' ORDER BY expense_dt")
        GridView2.DataSource = ds.Tables(0)
        GridView2.DataBind()
        If ds.Tables(0).Rows.Count <> 0 Then
            txttotalexpense4.Text = Format(ds.Tables(0).Compute("SUM(expense_amt)", ""), "######0.00")
        End If
        lbltotexpense.Text = Format(Val(txttotalexpense1.Text) + Val(txttotalexpense3.Text) + Val(txttotalexpense4.Text), "######0.00")
    End Sub
End Class
