﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="master_category.aspx.vb" Inherits="master_category" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="A1" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                      aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="master_category.aspx">Add New Employee Category</a>
                      <a class="dropdown-item" href="master_category.aspx?mode=V">View Employee Category List</a>
                    
                    </div>
                  </div>
                </div>

                     <div class="card-body">
                                    <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnlview" runat="server">
                                          <table style="width: 100%;">
                                            <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel2" runat="server">
                                                          <div style="width: 100%; height: 600px">
                                                              <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="15" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <Columns>
                                                                      <asp:BoundField DataField="sl" HeaderText="Sl">
                                                                      <HeaderStyle Width="30px" />
                                                                      <ItemStyle Width="30px" />
                                                                      </asp:BoundField>
                                                                      <asp:BoundField DataField="cat_nm" HeaderText="Category Name" >
                                                                      <HeaderStyle HorizontalAlign="Left" />
                                                                      </asp:BoundField>
                                                                      <asp:BoundField DataField="cat_snm" HeaderText="Short Name">
                                                                      </asp:BoundField>
                                                                      <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                                                          ImageUrl="images/edit.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                                                      <ItemStyle Height="30px" Width="30px" />
                                                                      </asp:ButtonField>
                                                                  </Columns>
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>
                                                          </div>
                                                      </asp:Panel>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table style="width:100%;">
                                              
                                              <tr>
                                                  <td colspan="3">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td width="45%">
                                                      Employee Category</td>
                                                  <td width="10%">
                                                      &nbsp;</td>
                                                  <td width="45%">
                                                      Short Name</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtcategory" runat="server" CssClass="form-control"></asp:TextBox>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <asp:TextBox ID="txtshortname" runat="server" CssClass="form-control"></asp:TextBox>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td>
                                                                  OT Formula</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:DropDownList ID="cmbot" runat="server" CssClass="form-control">
                                                                      <asp:ListItem>1.OT Not Appicable</asp:ListItem>
                                                                      <asp:ListItem>2.Early IN + Lateout</asp:ListItem>
                                                                      <asp:ListItem>3.Working Hour - Shift Hour</asp:ListItem>
                                                                      <asp:ListItem>4.Shift End Time-Out Time</asp:ListItem>
                                                                      <asp:ListItem>Shift Start Time - Early IN Time</asp:ListItem>
                                                                      <asp:ListItem>6.Total Hour - Max Hour</asp:ListItem>
                                                                  </asp:DropDownList>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="50%">
                                                                  MIN OT</td>
                                                              <td width="50%">
                                                                  <asp:CheckBox ID="chkot" runat="server" />
                                                                  MAX OT</td>
                                                          </tr>
                                                      </table>
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="50%">
                                                                  <asp:TextBox ID="txtminot" runat="server" CssClass="form-control"></asp:TextBox>
                                                              </td>
                                                              <td width="50%">
                                                                  <asp:TextBox ID="txtmaxot" runat="server" CssClass="form-control"></asp:TextBox>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      IN Punch Starts Before</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      IN Punch End After</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtinpunstbf" runat="server" CssClass="form-control"></asp:TextBox>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <asp:TextBox ID="txtinpunchendaft" runat="server" CssClass="form-control"></asp:TextBox>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td style="height: 21px">
                                                  </td>
                                                  <td style="height: 21px">
                                                  </td>
                                                  <td style="height: 21px">
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      Out Punch End After</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      Allowable Early In Time</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtoutpunchendaft" runat="server" CssClass="form-control"></asp:TextBox>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <asp:TextBox ID="time06" runat="server" CssClass="form-control"></asp:TextBox>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      Allowable Early Out Time</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      Allowable Late IN Time</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="time07" runat="server" CssClass="form-control"></asp:TextBox>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <asp:TextBox ID="time08" runat="server" CssClass="form-control"></asp:TextBox>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      Maximum Working Hour</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="50%">
                                                                  <asp:DropDownList ID="cmbtime4" runat="server" CssClass="form-control">
                                                                      <asp:ListItem>01</asp:ListItem>
                                                                      <asp:ListItem Value="02"></asp:ListItem>
                                                                      <asp:ListItem>03</asp:ListItem>
                                                                      <asp:ListItem>04</asp:ListItem>
                                                                      <asp:ListItem>05</asp:ListItem>
                                                                      <asp:ListItem>06</asp:ListItem>
                                                                      <asp:ListItem>07</asp:ListItem>
                                                                      <asp:ListItem>08</asp:ListItem>
                                                                      <asp:ListItem>09</asp:ListItem>
                                                                      <asp:ListItem>10</asp:ListItem>
                                                                      <asp:ListItem>11</asp:ListItem>
                                                                      <asp:ListItem>12</asp:ListItem>
                                                                      <asp:ListItem>13</asp:ListItem>
                                                                  </asp:DropDownList>
                                                              </td>
                                                              <td width="50%">
                                                                  <asp:DropDownList ID="cmbmin4" runat="server" CssClass="form-control">
                                                                      <asp:ListItem>00</asp:ListItem>
                                                                      <asp:ListItem>01</asp:ListItem>
                                                                      <asp:ListItem Value="02"></asp:ListItem>
                                                                      <asp:ListItem>03</asp:ListItem>
                                                                      <asp:ListItem>04</asp:ListItem>
                                                                      <asp:ListItem>05</asp:ListItem>
                                                                      <asp:ListItem>06</asp:ListItem>
                                                                      <asp:ListItem>07</asp:ListItem>
                                                                      <asp:ListItem>08</asp:ListItem>
                                                                      <asp:ListItem>09</asp:ListItem>
                                                                      <asp:ListItem>10</asp:ListItem>
                                                                      <asp:ListItem>11</asp:ListItem>
                                                                      <asp:ListItem>12</asp:ListItem>
                                                                      <asp:ListItem>13</asp:ListItem>
                                                                      <asp:ListItem>14</asp:ListItem>
                                                                      <asp:ListItem>15</asp:ListItem>
                                                                      <asp:ListItem>16</asp:ListItem>
                                                                      <asp:ListItem>17</asp:ListItem>
                                                                      <asp:ListItem>18</asp:ListItem>
                                                                      <asp:ListItem>19</asp:ListItem>
                                                                      <asp:ListItem>20</asp:ListItem>
                                                                      <asp:ListItem>21</asp:ListItem>
                                                                      <asp:ListItem>22</asp:ListItem>
                                                                      <asp:ListItem>23</asp:ListItem>
                                                                      <asp:ListItem>24</asp:ListItem>
                                                                      <asp:ListItem>25</asp:ListItem>
                                                                      <asp:ListItem>26</asp:ListItem>
                                                                      <asp:ListItem>27</asp:ListItem>
                                                                      <asp:ListItem>28</asp:ListItem>
                                                                      <asp:ListItem>29</asp:ListItem>
                                                                      <asp:ListItem>30</asp:ListItem>
                                                                      <asp:ListItem>31</asp:ListItem>
                                                                      <asp:ListItem>32</asp:ListItem>
                                                                      <asp:ListItem>33</asp:ListItem>
                                                                      <asp:ListItem>34</asp:ListItem>
                                                                      <asp:ListItem>35</asp:ListItem>
                                                                      <asp:ListItem>36</asp:ListItem>
                                                                      <asp:ListItem>37</asp:ListItem>
                                                                      <asp:ListItem>38</asp:ListItem>
                                                                      <asp:ListItem>39</asp:ListItem>
                                                                      <asp:ListItem>40</asp:ListItem>
                                                                      <asp:ListItem>41</asp:ListItem>
                                                                      <asp:ListItem>42</asp:ListItem>
                                                                      <asp:ListItem>43</asp:ListItem>
                                                                      <asp:ListItem>44</asp:ListItem>
                                                                      <asp:ListItem>45</asp:ListItem>
                                                                      <asp:ListItem>46</asp:ListItem>
                                                                      <asp:ListItem>47</asp:ListItem>
                                                                      <asp:ListItem>48</asp:ListItem>
                                                                      <asp:ListItem>49</asp:ListItem>
                                                                      <asp:ListItem>50</asp:ListItem>
                                                                      <asp:ListItem>51</asp:ListItem>
                                                                      <asp:ListItem>52</asp:ListItem>
                                                                      <asp:ListItem>53</asp:ListItem>
                                                                      <asp:ListItem>54</asp:ListItem>
                                                                      <asp:ListItem>55</asp:ListItem>
                                                                      <asp:ListItem>56</asp:ListItem>
                                                                      <asp:ListItem>57</asp:ListItem>
                                                                      <asp:ListItem>58</asp:ListItem>
                                                                      <asp:ListItem>59</asp:ListItem>
                                                                  </asp:DropDownList>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      Halfday Status</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      Late Arrival Status</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:DropDownList ID="cmbhalf" runat="server" CssClass="form-control">
                                                          <asp:ListItem>No</asp:ListItem>
                                                          <asp:ListItem>Yes</asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <asp:DropDownList ID="cmblate" runat="server" CssClass="form-control">
                                                          <asp:ListItem>No</asp:ListItem>
                                                          <asp:ListItem>Yes</asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      Early Depature Status</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      Allow Overtime</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:DropDownList ID="cmbearly" runat="server" CssClass="form-control">
                                                          <asp:ListItem>No</asp:ListItem>
                                                          <asp:ListItem>Yes</asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <asp:DropDownList ID="cmbovertime" runat="server" CssClass="form-control">
                                                          <asp:ListItem>No</asp:ListItem>
                                                          <asp:ListItem>Yes</asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      Is Present Work On Weekly Off</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      Is Present Work On Holiday</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:DropDownList ID="cmbpresent" runat="server" CssClass="form-control">
                                                          <asp:ListItem>No</asp:ListItem>
                                                          <asp:ListItem>Yes</asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <asp:DropDownList ID="cmbholiday1" runat="server" CssClass="form-control">
                                                          <asp:ListItem>No</asp:ListItem>
                                                          <asp:ListItem>Yes</asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      Weekly Off-1</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <asp:CheckBox ID="chkweekoff" runat="server" />
                                                      Weekly Off-2</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:DropDownList ID="cmbweekoff1" runat="server" CssClass="form-control">
                                                          <asp:ListItem>Sunday</asp:ListItem>
                                                          <asp:ListItem>Monday</asp:ListItem>
                                                          <asp:ListItem>Tuesday</asp:ListItem>
                                                          <asp:ListItem>Wednesday</asp:ListItem>
                                                          <asp:ListItem>Thursday</asp:ListItem>
                                                          <asp:ListItem>Friday</asp:ListItem>
                                                          <asp:ListItem>Saturday</asp:ListItem>
                                                          <asp:ListItem>None</asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <asp:DropDownList ID="cmbweekoff2" runat="server" CssClass="form-control">
                                                          <asp:ListItem>Sunday</asp:ListItem>
                                                          <asp:ListItem>Monday</asp:ListItem>
                                                          <asp:ListItem>Tuesday</asp:ListItem>
                                                          <asp:ListItem>Wednesday</asp:ListItem>
                                                          <asp:ListItem>Thursday</asp:ListItem>
                                                          <asp:ListItem>Friday</asp:ListItem>
                                                          <asp:ListItem>Saturday</asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <asp:CheckBox ID="chkwo1" runat="server" />
                                                      1st<asp:CheckBox ID="chkwo2" runat="server" />
                                                      2nd<asp:CheckBox ID="chkwo3" runat="server" />
                                                      3rd<asp:CheckBox ID="chkwo4" runat="server" />
                                                      4th<asp:CheckBox ID="chkwo5" runat="server" />
                                                      5th</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      <asp:CheckBox ID="chk1" runat="server" />
                                                      Consider Only First And Last Punch In Attendance Calculation</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      <asp:CheckBox ID="chk4" runat="server" />
                                                      &nbsp;Deduct Break Hours From Work Duration</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      <asp:CheckBox ID="chk7" runat="server" />
                                                      &nbsp;Transfer Total Work to OT in Weekly Off</td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      <asp:CheckBox ID="chk8" runat="server" />
                                                      &nbsp;Transfer Total Work to OT in Holiday</td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      <asp:CheckBox ID="chk6" runat="server" />
                                                      &nbsp;Mark WeekOff Or Holiday as Absent When Previous Day is Absent</td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="30">
                                                                  <asp:CheckBox ID="chkcontinuoslate" runat="server" Width="30px" />
                                                              </td>
                                                              <td width="60">
                                                                  Deduct</td>
                                                              <td width="150">
                                                                  <asp:DropDownList ID="cmbcontinuoslate" runat="server" 
                                                                      CssClass="form-control">
                                                                      <asp:ListItem>Half Day</asp:ListItem>
                                                                      <asp:ListItem>Full Day</asp:ListItem>
                                                                  </asp:DropDownList>
                                                              </td>
                                                              <td width="200">
                                                                   &nbsp;&nbsp; if Continously Late for</td>
                                                              <td width="50">
                                                                  <asp:TextBox ID="txtcontinuoslate" runat="server" 
                                                                      CssClass="form-control"></asp:TextBox>
                                                              </td>
                                                              <td>
                                                                  &nbsp;&nbsp; days</td>
                                                          </tr>
                                                          <tr>
                                                              <td width="30">
                                                                  &nbsp;</td>
                                                              <td width="60">
                                                                  &nbsp;</td>
                                                              <td width="150">
                                                                  &nbsp;</td>
                                                              <td width="200">
                                                                  &nbsp;</td>
                                                              <td width="50">
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td width="30">
                                                                  <asp:CheckBox ID="chklatemonthly" runat="server" />
                                                              </td>
                                                              <td width="60">
                                                                  &nbsp;Deduct
                                                              </td>
                                                              <td width="150">
                                                                  <asp:DropDownList ID="cmblatemonthly" runat="server" 
                                                                      CssClass="form-control">
                                                                      <asp:ListItem>Half Day</asp:ListItem>
                                                                      <asp:ListItem>Full Day</asp:ListItem>
                                                                  </asp:DropDownList>
                                                              </td>
                                                              <td width="200">
                                                                  &nbsp;&nbsp; if&nbsp; Late for</td>
                                                              <td width="50">
                                                                  <asp:TextBox ID="txtlatemonthly" runat="server" CssClass="form-control"></asp:TextBox>
                                                              </td>
                                                              <td>
                                                                  &nbsp;&nbsp;days in a Month</td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td width="30">
                                                                  <asp:CheckBox ID="chkabsent" runat="server" />
                                                              </td>
                                                              <td width="200">
                                                                  Mark Next Day Absent If</td>
                                                              <td width="50">
                                                                  <asp:TextBox ID="txtabsent" runat="server" CssClass="form-control"></asp:TextBox>
                                                              </td>
                                                              <td>
                                                                  &nbsp; &nbsp;Continuos Absent
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      Consider Half Day If Work Duration Is Less Than</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      Consider Absent If Work Duration Is Less Than</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txthalfday" runat="server" CssClass="form-control"></asp:TextBox>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <asp:TextBox ID="txtfullday" runat="server" CssClass="form-control"></asp:TextBox>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      OT on WeekOff</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      OT On Holiday</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:DropDownList ID="cmbotweekoff" runat="server" CssClass="form-control">
                                                          <asp:ListItem>No</asp:ListItem>
                                                          <asp:ListItem>Half</asp:ListItem>
                                                          <asp:ListItem>Full</asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <asp:DropDownList ID="cmbholiday" runat="server" CssClass="form-control">
                                                          <asp:ListItem>No</asp:ListItem>
                                                          <asp:ListItem>Half</asp:ListItem>
                                                          <asp:ListItem>Full</asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      When No Shift Assign Then</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      Odd Punch Will Be Treated As</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:DropDownList ID="cmbautoshift" runat="server" CssClass="form-control">
                                                          <asp:ListItem>AutoShift</asp:ListItem>
                                                          <asp:ListItem>Previous Day Shift</asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      <asp:DropDownList ID="cmboddpunch" runat="server" CssClass="form-control">
                                                          <asp:ListItem>1.None</asp:ListItem>
                                                          <asp:ListItem>2.Absent</asp:ListItem>
                                                          <asp:ListItem>3.Half Day</asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtmode" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtcategorysl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:Button ID="Button1" runat="server" class="btn btn-primary" Text="Submit" />
                                                       <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                          class="btn btn-default" Text="Reset" />
                                                  </td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>       
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

