﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class company_division
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        Dim ds1 As DataSet = get_dataset("SELECT * FROM company_settings WHERE set_sl=1 AND loc_cd=" & CType(Session("loc_cd"), Integer) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtemppf.Text = Format(Val(ds1.Tables(0).Rows(0).Item("pf_emp_per")), "0.00")
            txtemplrpf.Text = Format(Val(ds1.Tables(0).Rows(0).Item("pf_emplr_per")), "0.00")
            txtempesic.Text = Format(Val(ds1.Tables(0).Rows(0).Item("esic_emp_per")), "0.00")
            txtemplresic.Text = Format(Val(ds1.Tables(0).Rows(0).Item("esic_emplr_per")), "0.00")
            If ds1.Tables(0).Rows(0).Item("pf_app") = "Y" Then
                cmbpf.SelectedIndex = 0
            Else
                cmbpf.SelectedIndex = 1
            End If
            If ds1.Tables(0).Rows(0).Item("esic_app") = "Y" Then
                cmbesic.SelectedIndex = 0
            Else
                cmbesic.SelectedIndex = 1
            End If
        End If
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        start1()
        SQLInsert("UPDATE company_settings SET pf_app='" & vb.Left(cmbpf.Text, 1) & "',pf_emp_per=" & Val(txtemppf.Text) & _
        ",pf_emplr_per=" & Val(txtemplrpf.Text) & ",esic_app='" & vb.Left(cmbesic.Text, 1) & "',esic_emp_per=" & Val(txtempesic.Text) & _
        ",esic_emplr_per=" & Val(txtemplresic.Text) & " WHERE set_sl=1 AND loc_cd=" & CType(Session("loc_cd"), Integer) & "")
        close1()
        Me.clr()
        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Setting Updated Succesffuly');", True)
    End Sub
End Class
