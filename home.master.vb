﻿Imports System.Data

Partial Class home
    Inherits System.Web.UI.MasterPage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Dim ds As DataSet = get_dataset("SELECT comp_nm FROM company")
            If ds.Tables(0).Rows.Count <> 0 Then
                lblcomp.Text = ds.Tables(0).Rows(0).Item("comp_nm")
            End If
            Dim ds1 As DataSet = get_dataset("SELECT loc_nm FROM location_mst WHERE loc_cd=" & CType(Session("loc_cd"), Integer) & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                lblcomp.Text = lblcomp.Text & "(" & ds1.Tables(0).Rows(0).Item("loc_nm") & ")"
            End If
            Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
            lbluser.Text = CType(Session("usr_name"), String)
            lblno.Visible = False
            Dim ds4 As DataSet = get_dataset("select count(*) from lvoucher1 WHERE leave_status='P' AND loc_cd=" & loc_cd & "")
            If ds4.Tables(0).Rows.Count <> 0 Then
                lblleave.Text = ds4.Tables(0).Rows(0).Item(0)
            End If
            Dim ds5 As DataSet = get_dataset("select count(*) from outddor_posting WHERE status='P' AND loc_cd=" & loc_cd & "")
            If ds5.Tables(0).Rows.Count <> 0 Then
                lbloutdoor.Text = ds5.Tables(0).Rows(0).Item(0)
            End If
            Dim ds6 As DataSet = get_dataset("select count(*) from manual_posting WHERE log_status='P' AND loc_cd=" & loc_cd & "")
            If ds6.Tables(0).Rows.Count <> 0 Then
                lblmanual.Text = ds6.Tables(0).Rows(0).Item(0)
            End If

            lblno.Text = Val(lblleave.Text) + Val(lbloutdoor.Text) + Val(lblmanual.Text)
            If Val(lblno.Text) = 0 Then
                lblno.Visible = False
                divalert.Visible = False
            Else
                lblno.Visible = True
                divalert.Visible = True
            End If
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub
End Class

