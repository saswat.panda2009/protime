﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class payroll_odometerapproval
    Inherits System.Web.UI.Page
     Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        Image1.ImageUrl = "~/img/avatar-1.jpg"
        lbladdress.Text = ""
        txtkm.Text = ""
        txtid.Text = ""
        txtstafsl.Text = ""
        txttime.Text = ""
        txtdt.Text = ""
        Me.dvdispl()
    End Sub

    Private Sub dvdispl()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds As DataSet = get_dataset("SELECT CONVERT(varchar, odometer_entry.log_dt, 103) AS dt,CONVERT(varchar, odometer_entry.log_time, 108) AS time, odometer_entry.staf_sl, odometer_entry.id, odometer_entry.odometre_km AS tm, staf.staf_nm, division_mst.div_nm, dept_mst.dept_nm, desg_mst.desg_nm, staf.device_code FROM dept_mst INNER JOIN staf ON dept_mst.dept_sl = staf.dept_sl INNER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl RIGHT OUTER JOIN odometer_entry ON staf.staf_sl = odometer_entry.staf_sl LEFT OUTER JOIN division_mst ON odometer_entry.div_sl = division_mst.div_sl  WHERE odometer_entry.loc_cd=" & loc_cd & " AND log_status='P'  ORDER BY log_dt,staf_sl,log_time ")
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
    End Sub

    Protected Sub dvstaf_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dvstaf.RowCommand
        Dim rw As Integer = e.CommandArgument
        txtid.Text = ""
        txtstafsl.Text = ""
        txttime.Text = ""
        txtdevicecode.Text = ""
        txtdt.Text = ""
        txtkm.Text = ""
        Dim lbl As Label = dvstaf.Rows(rw).FindControl("lblid")
        Dim lblstafsl As Label = dvstaf.Rows(rw).FindControl("lblstaf_sl")
        Dim lbltime As Label = dvstaf.Rows(rw).FindControl("lbltm")
        Dim lbldevicecode As Label = dvstaf.Rows(rw).FindControl("lbldevicecode")
        Dim lbldt As Label = dvstaf.Rows(rw).FindControl("lbldt")
        If e.CommandName = "view" Then
            Dim ds1 As DataSet = get_dataset("SELECT log_img,log_location,log_longitude,log_lattitude,log_note,odometre_km FROM odometer_entry WHERE id=" & lbl.Text & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                Image1.ImageUrl = "data:image/jpg;base64," & ds1.Tables(0).Rows(0).Item("log_img")
                lbladdress.Text = ds1.Tables(0).Rows(0).Item("log_location")
                txtkm.Text = ds1.Tables(0).Rows(0).Item("odometre_km")
                HyperLink1.NavigateUrl = "https://www.google.com/maps?q=" & ds1.Tables(0).Rows(0).Item("log_longitude") & "," & ds1.Tables(0).Rows(0).Item("log_lattitude")
                txtid.Text = lbl.Text
                txtstafsl.Text = lblstafsl.Text
                txttime.Text = lbltime.Text
                txtdevicecode.Text = lbldevicecode.Text
                txtdt.Text = lbldt.Text
            End If
        ElseIf e.CommandName = "reset_state" Then

        End If
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        start1()
        SQLInsert("Update odometer_entry Set log_status='A',odometre_km=" & Val(txtkm.Text) & " WHERE id=" & txtid.Text & "")
        close1()
        Me.clr()
    End Sub

    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        start1()
        SQLInsert("Update  odometer_entry SET log_status='C',odometre_km=" & Val(txtkm.Text) & " WHERE id=" & txtid.Text & "")
        close1()
        Me.clr()
    End Sub

    Protected Sub cmdsave0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave0.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds As DataSet = get_dataset("SELECT CONVERT(varchar, odometer_entry.log_dt, 103) AS dt, odometer_entry.staf_sl, odometer_entry.id, CONVERT(varchar, odometer_entry.log_time, 108) AS tm, staf.staf_nm, division_mst.div_nm, dept_mst.dept_nm, desg_mst.desg_nm, staf.device_code FROM dept_mst INNER JOIN staf ON dept_mst.dept_sl = staf.dept_sl INNER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl RIGHT OUTER JOIN odometer_entry ON staf.staf_sl = odometer_entry.staf_sl LEFT OUTER JOIN division_mst ON odometer_entry.div_sl = division_mst.div_sl  WHERE odometer_entry.loc_cd=" & loc_cd & " AND log_status='P'  ORDER BY log_dt ")
        If ds.Tables(0).Rows.Count <> 0 Then
            start1()
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                SQLInsert("Update odometer_entry Set log_status='A' WHERE id=" & ds.Tables(0).Rows(i).Item("id") & "")
            Next
            close1()
        End If
        Me.clr()
    End Sub

    Protected Sub cmdclear0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear0.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds As DataSet = get_dataset("SELECT CONVERT(varchar, odometer_entry.log_dt, 103) AS dt, odometer_entry.staf_sl, odometer_entry.id, CONVERT(varchar, odometer_entry.log_time, 108) AS tm, staf.staf_nm, division_mst.div_nm, dept_mst.dept_nm, desg_mst.desg_nm, staf.device_code FROM dept_mst INNER JOIN staf ON dept_mst.dept_sl = staf.dept_sl INNER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl RIGHT OUTER JOIN odometer_entry ON staf.staf_sl = odometer_entry.staf_sl LEFT OUTER JOIN division_mst ON odometer_entry.div_sl = division_mst.div_sl  WHERE odometer_entry.loc_cd=" & loc_cd & " AND log_status='P'  ORDER BY log_dt ")
        If ds.Tables(0).Rows.Count <> 0 Then
            start1()
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                SQLInsert("Update odometer_entry Set log_status='C' WHERE id=" & ds.Tables(0).Rows(i).Item("id") & "")
            Next
            close1()
        End If
        Me.clr()
    End Sub
End Class
