﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="security_changepassword.aspx.vb" Inherits="security_changepassword" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Change Password"></asp:Label></h3>
                
                </div>

                     <div class="card-body">
                                     <table style="width:100%;">
                                         <tr>
                                             <td>
                                                 Old Password</td>
                                         </tr>
                                         <tr>
                                             <td>
                                            <asp:TextBox ID="txtoldpassword" runat="server" class="form-control" MaxLength="50" 
                                                     TextMode="Password"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="txtoldpassword_FilteredTextBoxExtender" 
                                                runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                TargetControlID="txtoldpassword">
                                            </asp:FilteredTextBoxExtender>
                                                    </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 &nbsp;</td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 New Password</td>
                                         </tr>
                                         <tr>
                                             <td>
                                            <asp:TextBox ID="txtnewpswd" runat="server" class="form-control" MaxLength="10" 
                                                     TextMode="Password"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="txtnewpswd_FilteredTextBoxExtender" 
                                                runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                TargetControlID="txtnewpswd">
                                            </asp:FilteredTextBoxExtender>
                                                    </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 &nbsp;</td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 Confirm Password</td>
                                         </tr>
                                         <tr>
                                             <td>
                                            <asp:TextBox ID="txtconfirmpswd" runat="server" class="form-control" MaxLength="10" 
                                                     TextMode="Password"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="txtconfirmpswd_FilteredTextBoxExtender" 
                                                runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                TargetControlID="txtconfirmpswd">
                                            </asp:FilteredTextBoxExtender>
                                                    </td>
                                         </tr>
                                         <tr>
                                             <td>
                                            <asp:TextBox ID="txtpassword" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                            <asp:TextBox ID="txtusrsl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                     ControlToValidate="txtoldpassword" Display="None" 
                                                     ErrorMessage="&lt;b&gt;Required Field&lt;/b&gt;&lt;br/&gt;Please Provide The User Id." 
                                                     SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                 <asp:ValidatorCalloutExtender ID="RequiredFieldValidator1_ValidatorCalloutExtender" 
                                                     runat="server" Enabled="True" PopupPosition="BottomLeft" 
                                                     TargetControlID="RequiredFieldValidator1">
                                                 </asp:ValidatorCalloutExtender>
                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                     ControlToValidate="txtnewpswd" Display="None" 
                                                     ErrorMessage="&lt;b&gt;Required Field&lt;/b&gt;Please Provide The Password." 
                                                     SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                 <asp:ValidatorCalloutExtender ID="RequiredFieldValidator2_ValidatorCalloutExtender" 
                                                     runat="server" Enabled="True" PopupPosition="BottomLeft" 
                                                     TargetControlID="RequiredFieldValidator2">
                                                 </asp:ValidatorCalloutExtender>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                            <asp:Button ID="cmdsave" runat="server" class="btn btn-info" 
                                                Text="Submit" /> &nbsp; &nbsp;
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 &nbsp;</td>
                                         </tr>
                                     </table>       
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

