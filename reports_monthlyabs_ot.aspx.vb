﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class reports_monthlyabs_ot
    Inherits System.Web.UI.Page
    <System.Web.Script.Services.ScriptMethod(), _
System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            "staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblmsg.Visible = False
        txtfrmdt.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        txttodt.Text = Format(Now, "dd/MM/yyyy")
        Me.divisiondisp()
        Me.deptdisp()
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
        txtdept.Text = "All Departments"
    End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub deptdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds As DataSet = get_dataset("select distinct dept_nm from dept_mst WHERE loc_cd=" & loc_cd & " order by dept_nm")
        dvdept.DataSource = ds.Tables(0)
        dvdept.DataBind()
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)

        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = str & "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If

        txtdept.Text = ""
        For i As Integer = 0 To dvdept.Rows.Count - 1
            Dim chk As CheckBox = dvdept.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                Dim lbl As Label = dvdept.Rows(i).FindControl("lbldept")
                txtdept.Text = txtdept.Text & "'" & lbl.Text & "',"
            End If
        Next
        If txtdept.Text <> "" Then
            txtdept.Text = vb.Left(txtdept.Text, txtdept.Text.Length - 1)
            str = str & "AND dept_mst.dept_nm IN (" & txtdept.Text & ")"
        Else
            txtdept.Text = "All Departments"
        End If
        If Trim(txtempcode.Text) <> "" Then
            str = ""
            str = "AND staf.emp_code='" & Trim(txtempcode.Text) & "'"
        End If



        Dim dsstaf As DataSet = get_dataset("SELECT staf_sl,staf.staf_nm, staf.emp_code, staf.device_code, division_mst.div_nm, dept_mst.dept_nm FROM staf LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl WHERE staf.emp_status='I' AND staf.loc_cd= " & loc_cd & "  " & str & "")
        Dim ds1 As DataSet = get_dataset("SELECT atnd.* FROM dept_mst RIGHT OUTER JOIN staf ON dept_mst.dept_sl = staf.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl RIGHT OUTER JOIN atnd ON staf.staf_sl = atnd.staf_sl WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND atnd.loc_cd= " & loc_cd & "  " & str & "  ORDER BY log_dt")


        Dim dt As New DataTable
        dt.Columns.Add("Staf Name", GetType(String))
        dt.Columns.Add("Emp. Code", GetType(String))
        dt.Columns.Add("Device Code", GetType(String))
        dt.Columns.Add("Division", GetType(String))
        dt.Columns.Add("Department", GetType(String))

        Dim current As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
        Dim ending As New DateTime(stringtodate(txttodt.Text).Year, stringtodate(txttodt.Text).Month, stringtodate(txttodt.Text).Day)

        While current <= ending
            dt.Columns.Add(current.Date, GetType(String))
            current = current.AddDays(1)
        End While
        dt.Columns.Add("P", GetType(String))
        dt.Columns.Add("OT", GetType(String))
        dt.Columns.Add("A", GetType(String))
        dt.Columns.Add("W", GetType(String))
        dt.Columns.Add("L", GetType(String))
        dt.Columns.Add("H", GetType(String))
        dt.Columns.Add("Total Hour", GetType(String))

        For i As Integer = 0 To dsstaf.Tables(0).Rows.Count - 1
            Dim tot_p As Integer = 0
            Dim tot_a As Integer = 0
            Dim tot_w As Integer = 0
            Dim tot_l As Integer = 0
            Dim tot_h As Integer = 0
            Dim tot_hr As Integer = 0
            Dim tot_ot_count As Integer = 0

            Dim current1 As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
            Dim ending1 As New DateTime(stringtodate(txttodt.Text).Year, stringtodate(txttodt.Text).Month, stringtodate(txttodt.Text).Day)

            Dim dr As DataRow
            dr = dt.NewRow
            dr(0) = Trim(dsstaf.Tables(0).Rows(i).Item("staf_nm"))
            dr(1) = Trim(dsstaf.Tables(0).Rows(i).Item("emp_code"))
            dr(2) = Trim(dsstaf.Tables(0).Rows(i).Item("device_code"))
            dr(3) = Trim(dsstaf.Tables(0).Rows(i).Item("div_nm"))
            dr(4) = Trim(dsstaf.Tables(0).Rows(i).Item("dept_nm"))

            Dim dr1 As DataRow
            dr1 = dt.NewRow
            dr1(0) = ""
            dr1(1) = ""
            dr1(2) = ""
            dr1(3) = ""
            dr1(4) = "OT"

            Dim column_cnt = 5
            While current1 <= ending1
                Dim day_status As String = ""
                Dim intime As String = ""
                Dim outtime As String = ""
                Dim tot_ot As Integer = 0
                Dim tot_ot_str As String = ""
                Dim dratnd As DataRow()
                dratnd = ds1.Tables(0).Select("log_dt='" & Format(current1, "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                If dratnd.Length <> 0 Then
                    If dratnd(0).Item("day_status") = "PRESENT" Then
                        day_status = "P"
                        intime = Format(dratnd(0).Item("first_punch"), "HH:mm")
                        outtime = Format(dratnd(0).Item("last_punch"), "HH:mm")
                        tot_hr = tot_hr + dratnd(0).Item("tot_hour")
                        tot_p = tot_p + 1
                    ElseIf dratnd(0).Item("day_status") = "ABSENT" Then
                        day_status = "A"
                        tot_a = tot_a + 1
                    ElseIf dratnd(0).Item("day_status") = "LEAVEDAY" Then
                        day_status = dratnd(0).Item("leave_nm")
                        tot_l = tot_l + 1
                    ElseIf dratnd(0).Item("day_status") = "WEEKLYOFF" Then
                        day_status = "W"
                        tot_w = tot_w + 1
                    ElseIf dratnd(0).Item("day_status") = "HOLIDAY" Then
                        day_status = "H"
                        tot_h = tot_h + 1
                    End If
                    tot_ot = dratnd(0).Item("tot_otmin")
                    If tot_ot > 420 Then
                        tot_ot_count = tot_ot_count + 1
                        Dim h1 As Integer = Int(tot_ot / 60)
                        Dim min1 As Integer = tot_ot Mod 60
                        tot_ot_str = h1 & ":" & min1
                    End If
                End If
                dr(column_cnt) = day_status
                dr1(column_cnt) = tot_ot_str
                column_cnt = column_cnt + 1
                current1 = current1.AddDays(1)
            End While
            dr("P") = tot_p
            dr("OT") = tot_ot_count
            dr("A") = tot_a
            dr("W") = tot_w
            dr("L") = tot_l
            dr("H") = tot_h
            dr("Total Hour") = tot_hr

            dt.Rows.Add(dr)
            dt.Rows.Add(dr1)
        Next
        If dt.Rows.Count <> 0 Then
            Dim GridView112 As New GridView()
            GridView112.AllowPaging = False
            GridView112.DataSource = dt
            GridView112.DataBind()
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=MonthlyAbstractAttedanceRegisterOT.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            For i As Integer = 0 To GridView112.Rows.Count - 1
                GridView112.Rows(i).Attributes.Add("class", "textmode")
            Next
            GridView112.RenderControl(hw)
            'style to format numbers to string
            Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()
        End If
    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub

    Protected Sub txtempcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtempcode.TextChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If Trim(txtempcode.Text) <> "" Then
            Dim emp_code As String() = Trim(txtempcode.Text).Split("-")
            txtempcode.Text = emp_code(0)
            Dim ds1 As DataSet = get_dataset("SELECT staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm, location_mst.loc_nm, staf.loc_cd FROM location_mst RIGHT OUTER JOIN staf ON location_mst.loc_cd = staf.loc_cd LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                Me.clr()
                txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            Else
                txtempcode.Text = ""
            End If
        End If
    End Sub

    Private Sub report_displ()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If
        txtdept.Text = ""
        For i As Integer = 0 To dvdept.Rows.Count - 1
            Dim chk As CheckBox = dvdept.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                Dim lbl As Label = dvdept.Rows(i).FindControl("lbldept")
                txtdept.Text = txtdept.Text & "'" & lbl.Text & "',"
            End If
        Next
        If txtdept.Text <> "" Then
            txtdept.Text = vb.Left(txtdept.Text, txtdept.Text.Length - 1)
            str = str & "AND dept_mst.dept_nm IN (" & txtdept.Text & ")"
        Else
            txtdept.Text = "All Departments"
        End If

        Dim dsstaf As DataSet = get_dataset("SELECT staf_sl,staf.staf_nm, staf.emp_code, staf.device_code, division_mst.div_nm, dept_mst.dept_nm FROM staf LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl WHERE staf.emp_status='I' and staf.loc_cd= " & loc_cd & "  " & str & "")
        Dim ds1 As DataSet = get_dataset("SELECT atnd.* FROM dept_mst RIGHT OUTER JOIN staf ON dept_mst.dept_sl = staf.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl RIGHT OUTER JOIN atnd ON staf.staf_sl = atnd.staf_sl WHERE log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND atnd.loc_cd= " & loc_cd & "  " & str & "  ORDER BY log_dt")


        Dim dt As New DataTable
        dt.Columns.Add("Staf Name", GetType(String))
        dt.Columns.Add("Emp. Code", GetType(String))
        dt.Columns.Add("Device Code", GetType(String))
        dt.Columns.Add("Division", GetType(String))
        dt.Columns.Add("Department", GetType(String))


        Dim current As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
        Dim ending As New DateTime(stringtodate(txttodt.Text).Year, stringtodate(txttodt.Text).Month, stringtodate(txttodt.Text).Day)


        While current <= ending
            dt.Columns.Add(current.Date, GetType(String))
            current = current.AddDays(1)
        End While

        dt.Columns.Add("P", GetType(String))
        dt.Columns.Add("A", GetType(String))
        dt.Columns.Add("W", GetType(String))
        dt.Columns.Add("L", GetType(String))
        dt.Columns.Add("H", GetType(String))
        dt.Columns.Add("Total Hour", GetType(String))

        For i As Integer = 0 To dsstaf.Tables(0).Rows.Count - 1
            Dim tot_p As Integer = 0
            Dim tot_a As Integer = 0
            Dim tot_w As Integer = 0
            Dim tot_l As Integer = 0
            Dim tot_h As Integer = 0
            Dim tot_hr As Integer = 0


            Dim current1 As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
            Dim ending1 As New DateTime(stringtodate(txttodt.Text).Year, stringtodate(txttodt.Text).Month, stringtodate(txttodt.Text).Day)

            Dim dr As DataRow
            dr = dt.NewRow
            dr(0) = Trim(dsstaf.Tables(0).Rows(i).Item("staf_nm"))
            dr(1) = Trim(dsstaf.Tables(0).Rows(i).Item("emp_code"))
            dr(2) = Trim(dsstaf.Tables(0).Rows(i).Item("device_code"))
            dr(3) = Trim(dsstaf.Tables(0).Rows(i).Item("div_nm"))
            dr(4) = Trim(dsstaf.Tables(0).Rows(i).Item("dept_nm"))


            Dim dr1 As DataRow
            dr1 = dt.NewRow
            dr1(0) = ""
            dr1(1) = ""
            dr1(2) = ""
            dr1(3) = ""
            dr1(4) = "IN TIME"


            Dim dr2 As DataRow
            dr2 = dt.NewRow
            dr2(0) = ""
            dr2(1) = ""
            dr2(2) = ""
            dr2(3) = ""
            dr2(4) = "OUT TIME"
            Dim column_cnt = 5
            While current1 <= ending1
                Dim day_status As String = ""
                Dim intime As String = ""
                Dim outtime As String = ""

                Dim dratnd As DataRow()
                dratnd = ds1.Tables(0).Select("log_dt='" & Format(current1, "dd/MMM/yyyy") & "' AND staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
                If dratnd.Length <> 0 Then
                    If dratnd(0).Item("day_status") = "PRESENT" Then
                        day_status = "P"
                        intime = Format(dratnd(0).Item("first_punch"), "HH:mm")
                        outtime = Format(dratnd(0).Item("last_punch"), "HH:mm")
                        tot_hr = tot_hr + dratnd(0).Item("tot_hour")
                        tot_p = tot_p + 1
                    ElseIf dratnd(0).Item("day_status") = "ABSENT" Then
                        day_status = "A"
                        tot_a = tot_a + 1
                    ElseIf dratnd(0).Item("day_status") = "LEAVEDAY" Then
                        day_status = "L"
                        tot_l = tot_l + 1
                    ElseIf dratnd(0).Item("day_status") = "WEEKLYOFF" Then
                        day_status = "W"
                        tot_w = tot_w + 1
                    ElseIf dratnd(0).Item("day_status") = "HOLIDAY" Then
                        day_status = "H"
                        tot_h = tot_h + 1
                    End If
                End If
                dr(column_cnt) = day_status
                dr1(column_cnt) = intime
                dr2(column_cnt) = outtime

                column_cnt = column_cnt + 1
                current1 = current1.AddDays(1)
            End While
            dr("P") = tot_p
            dr("A") = tot_a
            dr("W") = tot_w
            dr("L") = tot_l
            dr("H") = tot_h
            dr("Total Hour") = tot_hr

            dt.Rows.Add(dr)
            dt.Rows.Add(dr1)
            dt.Rows.Add(dr2)
        Next
        If dt.Rows.Count <> 0 Then
            'Create a dummy GridView

            Dim GridView112 As New GridView()
            GridView112.AllowPaging = False
            GridView112.DataSource = dt
            GridView112.DataBind()
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=MonthlyAttedanceRegister.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            For i As Integer = 0 To GridView112.Rows.Count - 1
                GridView112.Rows(i).Attributes.Add("class", "textmode")
            Next
            GridView112.RenderControl(hw)
            'style to format numbers to string
            Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()
        End If

    End Sub
End Class
