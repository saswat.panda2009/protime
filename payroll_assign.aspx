﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="payroll_assign.aspx.vb" Inherits="payroll_assign" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Earnings / Deduction Assignment"></asp:Label></h3>
                 
                </div>

                     <div class="card-body">
                                    <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table style="width:100%;">
                                                                                         
                                              <tr>
                                                  <td>
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="45%">
                                                                  Employee Id</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td width="45%">
                                                                  <asp:TextBox ID="txtempcode" runat="server" class="form-control" 
                                                                      MaxLength="100"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtempcode_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtempcode">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td width="10%">
                                                                  &nbsp; &nbsp; <asp:Button ID="cmdsave0" runat="server" class="btn btn-info" Text="Get" />
                                                              </td>
                                                              <td width="45%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                                                                               
                                                          <tr>
                                                              <td width="45%">
                                                                  Name</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <asp:TextBox ID="txtstafnm" runat="server" class="form-control" MaxLength="100" 
                                                                      BackColor="White" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtstafnm_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtstafnm">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Department</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  Designation</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtdept" runat="server" BackColor="White" class="form-control" 
                                                                      MaxLength="100" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdept_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdept">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtdesg" runat="server" BackColor="White" class="form-control" 
                                                                      MaxLength="100" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdesg_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdesg">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  For Month</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                <div class="form-group" id="simple-date1">
                    
                     <asp:TextBox ID="txtfordt" runat="server" CssClass="form-control"></asp:TextBox>
                       
                          <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server" 
                              Enabled="True" TargetControlID="txtfordt" Format="dd/MM/yyyy" 
                              PopupButtonID="txtfordt">
                          </asp:CalendarExtender>
                       
                     
                  </div>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                   
                                                              </td>
                                                          </tr>

                                                          <tr>
                                                              <td>
                                                                  <asp:Button ID="cmdsave1" runat="server" class="btn btn-info" 
                                                                      Text="Get Details" />
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <table style="width:100%;">
                                                                      <tr>
                                                                          <td style="border-width: 1px; border-color: #000000; background-color: #008000; color: #FFFFFF; border-top-style: solid; border-left-style: solid; border-bottom-style: solid;" 
                                                                              width="10%" align="center">
                                                                              Sl</td>
                                                                          <td style="border-width: 1px; border-color: #000000; background-color: #008000; color: #FFFFFF; border-top-style: solid; border-left-style: solid; border-bottom-style: solid;" 
                                                                              width="20%" align="center">
                                                                              Date</td>
                                                                          <td style="border-width: 1px; border-color: #000000; background-color: #008000; color: #FFFFFF; border-top-style: solid; border-left-style: solid; border-bottom-style: solid;" 
                                                                              width="45%" align="center">
                                                                              Earnings / Deduction Head</td>
                                                                          <td style="border-width: 1px; border-color: #000000; background-color: #008000; color: #FFFFFF; border-top-style: solid; border-left-style: solid; border-bottom-style: solid;" 
                                                                              width="15%" align="center">
                                                                              Amount</td>
                                                                          <td style="border: 1px solid #000000; background-color: #008000; color: #FFFFFF" 
                                                                              width="10%">
                                                                              &nbsp;</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td>
                                                                              <asp:TextBox ID="txtsl" runat="server" BackColor="White" 
                                                                                  class="form-control" MaxLength="100" ReadOnly="True"></asp:TextBox>
                                                                              <asp:FilteredTextBoxExtender ID="txtsl_FilteredTextBoxExtender" 
                                                                                  runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                                  TargetControlID="txtsl">
                                                                              </asp:FilteredTextBoxExtender>
                                                                          </td>
                                                                          <td>
                                                                              <asp:TextBox ID="txtdate" runat="server" BackColor="White" 
                                                                                  class="form-control" MaxLength="100"></asp:TextBox>
                                                                              <asp:CalendarExtender ID="txtdept4_CalendarExtender" runat="server" 
                                                                                  Enabled="True" TargetControlID="txtdate" Format="dd/MM/yyyy" 
                                                                                  PopupButtonID="txtdate">
                                                                              </asp:CalendarExtender>
                                                                          </td>
                                                                          <td>
                                                                              <asp:DropDownList ID="cmbearnings" runat="server" CssClass="form-control">
                                                                              </asp:DropDownList>
                                                                          </td>
                                                                          <td>
                                                                              <asp:TextBox ID="txtamount" runat="server" BackColor="White" 
                                                                                  class="form-control" MaxLength="100"  style="text-align:right"></asp:TextBox>
                                                                            
                                                                          </td>
                                                                          <td>
                                                                              <asp:Button ID="cmdsave2" runat="server" class="btn btn-info" Text="Next" 
                                                                                  Width="100%" />
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td style="font-size: 5px">
                                                                              &nbsp;</td>
                                                                          <td style="font-size: 5px">
                                                                              &nbsp;</td>
                                                                          <td style="font-size: 5px">
                                                                              &nbsp;</td>
                                                                          <td style="font-size: 5px">
                                                                              &nbsp;</td>
                                                                          <td style="font-size: 5px">
                                                                              &nbsp;</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td colspan="5">
                                                                              <div style="width: 100%; height: 300px; overflow: auto;">
                                                                                  <asp:GridView ID="dvstaf" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                                                      AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                                                      PagerStyle-CssClass="pgr" PageSize="15" Width="100%" ShowHeader="False">
                                                                                      <AlternatingRowStyle CssClass="alt" />
                                                                                      <Columns>
                                                                                          <asp:TemplateField>
                                                                                              <ItemTemplate>
                                                                                                <asp:Label ID="lblsl" runat="server" Text='<%#Eval("sl") %>'></asp:Label>
                                                                                              </ItemTemplate>
                                                                                              <ItemStyle Width="10%" />
                                                                                          </asp:TemplateField>
                                                                                          <asp:TemplateField>
                                                                                           <ItemTemplate>
                                                                                                <asp:Label ID="lbldt" runat="server" Text='<%#Eval("dt") %>'></asp:Label>
                                                                                              </ItemTemplate>
                                                                                              <HeaderStyle Width="20%" />
                                                                                              <ItemStyle Width="20%" />
                                                                                          </asp:TemplateField>
                                                                                          <asp:TemplateField>
                                                                                           <ItemTemplate>
                                                                                                <asp:Label ID="lblnm" runat="server" Text='<%#Eval("head_name") %>'></asp:Label>
                                                                                              </ItemTemplate>
                                                                                              <HeaderStyle Width="45%" />
                                                                                              <ItemStyle Width="45%" />
                                                                                          </asp:TemplateField>
                                                                                          <asp:TemplateField>
                                                                                           <ItemTemplate>
                                                                                                <asp:Label ID="lblamt" runat="server" Text='<%#Eval("amt") %>'></asp:Label>
                                                                                              </ItemTemplate>
                                                                                              <ItemStyle Width="15%" HorizontalAlign="Right" />
                                                                                          </asp:TemplateField>
                                                                                          <asp:TemplateField Visible="False">
                                                                                           <ItemTemplate>
                                                                                                <asp:Label ID="lblslno" runat="server" Text='<%#Eval("ass_sl") %>'></asp:Label>
                                                                                              </ItemTemplate>
                                                                                          </asp:TemplateField>
                                                                                          <asp:ButtonField ButtonType="Image" Text="Button" CommandName="edit_state" 
                                                                                              ImageUrl="~/images/delete.png">
                                                                                          <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                                                          </asp:ButtonField>
                                                                                      </Columns>
                                                                                      <PagerStyle HorizontalAlign="Right" />
                                                                                  </asp:GridView>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                  </table>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <table style="width:100%;">
                                                                      <tr>
                                                                          <td width="25%" align="right">
                                                                              &nbsp;</td>
                                                                          <td width="25%">
                                                                              &nbsp;</td>
                                                                          <td width="25%" align="right">
                                                                              &nbsp;</td>
                                                                          <td width="25%">
                                                                              &nbsp;</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td align="right" width="25%">
                                                                              Total Earnings :</td>
                                                                          <td width="25%">
                                                                              <asp:TextBox ID="txttotearning" runat="server" BackColor="White" 
                                                                                  class="form-control" MaxLength="100" style="text-align:right"></asp:TextBox>
                                                                          </td>
                                                                          <td align="right" width="25%">
                                                                              Total Deduction :</td>
                                                                          <td width="25%">
                                                                              <asp:TextBox ID="txttotdeduction" runat="server" BackColor="White" 
                                                                                  class="form-control" MaxLength="100" style="text-align:right"></asp:TextBox>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td>
                                                                              &nbsp;</td>
                                                                          <td>
                                                                              &nbsp;</td>
                                                                          <td>
                                                                              &nbsp;</td>
                                                                          <td>
                                                                              &nbsp;</td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td>
                                                                              &nbsp;</td>
                                                                          <td>
                                                                              &nbsp;</td>
                                                                          <td>
                                                                              &nbsp;</td>
                                                                          <td>
                                                                              &nbsp;</td>
                                                                      </tr>
                                                                  </table>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3" style="font-size: 5px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtstafsl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                                 
                                                                  <asp:TextBox ID="txtassno" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                                 
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  &nbsp;</td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>    
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

