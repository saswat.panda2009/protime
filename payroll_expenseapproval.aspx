﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="payroll_expenseapproval.aspx.vb" Inherits="payroll_odometerapproval" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Expense Approval . . ."></asp:Label></h3>
                 
                </div>

                     <div class="card-body">
                                       <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table style="width:100%;">
                                                                                         
                                              <tr>
                                                  <td>
                                                      <table style="width:100%;">
                                                             <tr>
                                                              
                                                              <td width="80%" valign="top" align="center">
                                                                  <asp:Image ID="Image1" runat="server" Height="250px" ImageAlign="Middle" 
                                                                      Width="250px" />

                                                                                                                                      </td>
                                                          </tr>
                                                                                                               </table>
                                               <table style="width:100%;">
                                                             <tr>
                                                              <td>
                                                                   <div style="width: 100%; height: 300px; overflow: auto; font-size: 15px;">
                                                          <asp:GridView ID="dvstaf" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                              AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                              PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                              <AlternatingRowStyle CssClass="alt" />
                                                              <Columns>
                                                               <asp:TemplateField HeaderText="Sl">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblsl" runat="server" Text='<%#Eval("sl") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField HeaderText="Date">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lbldt" runat="server" Text='<%#Eval("dt") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                
                                                                              </asp:TemplateField>
                                                                               <asp:TemplateField HeaderText="Emp. Code">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblempcpde" runat="server" Text='<%#Eval("emp_code") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                
                                                                              </asp:TemplateField>
                                                                               <asp:TemplateField HeaderText="Staf">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblstafnm" runat="server" Text='<%#Eval("staf_nm") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                
                                                                              </asp:TemplateField>
                                                                               <asp:TemplateField HeaderText="Department">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lbldeptnm" runat="server" Text='<%#Eval("dept_nm") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                
                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField HeaderText="Expense">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblnm" runat="server" Text='<%#Eval("expense_name") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                
                                                                              </asp:TemplateField>
                                                                               <asp:TemplateField HeaderText="Remark">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lbldescr" runat="server" Text='<%#Eval("expense_desc") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                               
                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField HeaderText="Amount">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblamt" runat="server" Text='<%#Eval("amt") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                                  <ItemStyle HorizontalAlign="Right" />
                                                                              </asp:TemplateField>
                                                                              <asp:TemplateField Visible="False">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblslno" runat="server" Text='<%#Eval("ent_sl") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>
                                                                               <asp:TemplateField Visible="False">
                                                                                  <ItemTemplate>
                                                                                      <asp:Label ID="lblpaid" runat="server" Text='<%#Eval("paid") %>'></asp:Label>
                                                                                  </ItemTemplate>
                                                                              </asp:TemplateField>

                                                                   <asp:ButtonField ButtonType="Image" Text="Button" ImageUrl="images/view.png" CommandName="view">
                                                                   <HeaderStyle Width="30px" />
                                                                   <ItemStyle Width="30px" />
                                                                   </asp:ButtonField>
                                                                  <asp:ButtonField ButtonType="Button" Text="Approve" CommandName="Approve" ControlStyle-CssClass="btn btn-success">
                                                                   <ControlStyle CssClass="btn btn-success" />
                                                                   <ItemStyle Width="80px" />
                                                                  </asp:ButtonField>
                                                                 <asp:ButtonField ButtonType="Button" Text="Pay" CommandName="pay" >
                                                                   <ControlStyle CssClass="btn btn-info" />
                                                                   <ItemStyle Width="30px" />
                                                                  </asp:ButtonField>
                                                                   <asp:ButtonField ButtonType="Button" Text="Cancel" CommandName="Cancel_expense"  
                                                                       ControlStyle-CssClass="btn btn-danger" >
                                                                
                                                                   <ControlStyle CssClass="btn btn-danger" />
                                                                   <ItemStyle Width="60px" />
                                                                   </asp:ButtonField>
                                                                
                                                              </Columns>
                                                              <PagerStyle HorizontalAlign="Right" />
                                                          </asp:GridView>
                                                      </div></td></tr>
                                                                                                               </table>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>       
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

