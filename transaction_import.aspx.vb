﻿Imports System.IO
Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class transaction_import
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Protected Sub cmdget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdget.Click
        Try
            Dim str1 As String
            Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
            Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
            str1 = FileUpload1.FileName.ToString
            If str1 <> "" Then
                If File.Exists(Server.MapPath(".\Import\") & loc_cd & "_" & FileUpload1.FileName) Then
                    File.Delete(Server.MapPath(".\Import\") & loc_cd & "_" & FileUpload1.FileName)
                End If
                FileUpload1.SaveAs(Server.MapPath(".\Import\") & loc_cd & "_" & FileUpload1.FileName)
            End If
            Dim c_excelconstring As String = String.Empty
            Dim uploadpath As String = "~/Import/"
            Dim filepath As String = Server.MapPath(uploadpath & loc_cd & "_" & FileUpload1.PostedFile.FileName)
            Dim filext As String = Path.GetExtension(loc_cd & "_" & FileUpload1.PostedFile.FileName)
            If filext = ".xls" OrElse filext = "XLS" Then
                c_excelconstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & Trim(filepath) & "'" & ";Extended Properties=""Excel 8.0;HDR=Yes'"
            ElseIf filext = ".xlsx" OrElse filext = "XLSX" Then
                c_excelconstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Trim(filepath) & ";Extended Properties=Excel 12.0;Persist Security Info=False"
            End If

            If c_excelconstring <> "" Then
                Dim dt As New DataTable
                dt.Columns.Add("Sl", GetType(Integer))
                dt.Columns.Add("Division", GetType(String))
                dt.Columns.Add("Employee Name", GetType(String))
                dt.Columns.Add("Employee Code", GetType(String))
                dt.Columns.Add("Device Code", GetType(String))
                dt.Columns.Add("Adhara Card No.", GetType(String))
                dt.Columns.Add("Designation", GetType(String))
                dt.Columns.Add("Department", GetType(String))
                dt.Columns.Add("Employee Category", GetType(String))
                dt.Columns.Add("DOJ(DD/MM/YYYY)", GetType(String))
                dt.Columns.Add("Father Name", GetType(String))
                dt.Columns.Add("Present Address", GetType(String))
                dt.Columns.Add("Permanent Address", GetType(String))
                dt.Columns.Add("Contact No.", GetType(String))
                dt.Columns.Add("DOB(DD/MM/YYYY)", GetType(String))
                dt.Columns.Add("Email", GetType(String))
                dt.Columns.Add("PAN", GetType(String))
                dt.Columns.Add("BANK", GetType(String))
                dt.Columns.Add("Account No", GetType(String))
                dt.Columns.Add("IFSC", GetType(String))
                dt.Columns.Add("ESIC", GetType(String))
                dt.Columns.Add("UAN", GetType(String))
                dt.Columns.Add("Marital Status", GetType(String))
                dt.Columns.Add("Gender", GetType(String))
                dt.Columns.Add("Blood Group", GetType(String))
                dt.Columns.Add("Emergency Contact Name", GetType(String))
                dt.Columns.Add("Emergency Contact Relation", GetType(String))
                dt.Columns.Add("Emergency Contact Number", GetType(String))
                dt.Columns.Add("Highest Education", GetType(String))
                dt.Columns.Add("Remark", GetType(String))
                Dim ds As DataSet = excel_dataset("SELECT * FROM [Sheet1$]", c_excelconstring)
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('2');", True)
                start1()
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim div_sl As Integer = 0
                    Dim emp_nm As String = ""
                    Dim emp_code As String = ""
                    Dim device_code As String = ""
                    Dim adhar_card As String = ""
                    Dim desg_sl As Integer = 0
                    Dim dept_sl As Integer = 0
                    Dim cat_sl As Integer = 0
                    Dim fath_nm As String = ""
                    Dim present_addr As String = ""
                    Dim permanent_addr As String = ""
                    Dim cont As String = ""
                    Dim doj As Date
                    Dim dob As Date
                    Dim err_cnt As Integer = 0
                    Dim pan_no As String = ""
                    Dim bank_nm As String = ""
                    Dim email As String = ""
                    Dim acc_no As String = ""
                    Dim ifsc As String = ""
                    Dim esic As String = ""
                    Dim pf As String = ""
                    Dim uan As String = ""
                    Dim married As String = ""
                    Dim Gender As String = "M"
                    Dim blood As String = ""
                    Dim emergency_name As String = ""
                    Dim emg_cont_name As String = ""
                    Dim emg_cont_relation As String = ""
                    Dim emg_cont As String = ""
                    Dim qualification As String = ""
                    Dim shift As Integer = 0
                    Dim reporting_emp_code As Integer = 0
                    Dim msg As String = "Added Successfully"
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(1)) Then
                        Dim div_nm As String = ds.Tables(0).Rows(i).Item(1)
                        Dim dsdiv As DataSet = get_dataset("SELECT * FROM division_mst WHERE div_nm='" & UCase(Trim(ds.Tables(0).Rows(i).Item(1))) & "' AND loc_cd=" & loc_cd & "")
                        If dsdiv.Tables(0).Rows.Count <> 0 Then
                            div_sl = dsdiv.Tables(0).Rows(0).Item("div_sl")
                        Else
                            div_sl = 0
                            err_cnt = 1
                            msg = "Invalid Division"
                        End If
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(2)) Then
                        emp_nm = ds.Tables(0).Rows(i).Item(2)
                    Else
                        If err_cnt <> 1 Then
                            msg = "Employee Name Blank"
                            err_cnt = 1
                        End If
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(3)) Then
                        emp_code = ds.Tables(0).Rows(i).Item(3)
                        Dim dsempcode As DataSet = get_dataset("SELECT emp_code FROM staf WHERE emp_code='" & UCase(Trim(ds.Tables(0).Rows(i).Item(3))) & "'")
                        If dsempcode.Tables(0).Rows.Count <> 0 Then
                            If err_cnt <> 1 Then
                                msg = "Duplicate Employee Code"
                                err_cnt = 1
                            End If
                        End If
                    Else
                        If err_cnt <> 1 Then
                            msg = "Employee Code Blank"
                            err_cnt = 1
                        End If
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(4)) Then
                        device_code = ds.Tables(0).Rows(i).Item(4)
                        Dim dsdvcode As DataSet = get_dataset("SELECT device_code FROM staf WHERE device_code='" & UCase(Trim(ds.Tables(0).Rows(i).Item(4))) & "'")
                        If dsdvcode.Tables(0).Rows.Count <> 0 Then
                            If err_cnt <> 1 Then
                                msg = "Duplicate Device Code"
                                err_cnt = 1
                            End If
                        End If
                    Else
                        If err_cnt <> 1 Then
                            msg = "Device Code Blank"
                            err_cnt = 1
                        End If
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(5)) Then
                        adhar_card = ds.Tables(0).Rows(i).Item(5)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(6)) Then
                        Dim desg_nm As String = ds.Tables(0).Rows(i).Item(6)
                        Dim dsdesg As DataSet = get_dataset("SELECT desg_sl FROM desg_mst WHERE desg_nm='" & UCase(Trim(desg_nm)) & "' AND loc_cd=" & loc_cd & "")
                        If dsdesg.Tables(0).Rows.Count <> 0 Then
                            desg_sl = dsdesg.Tables(0).Rows(0).Item("desg_sl")
                        Else
                            desg_sl = "1"
                            Dim ds1 As DataSet = get_dataset("SELECT max(desg_sl) FROM desg_mst ")
                            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                                desg_sl = ds1.Tables(0).Rows(0).Item(0) + 1
                            End If
                            SQLInsert("INSERT INTO desg_mst(desg_sl,desg_nm,loc_cd,active) VALUES(" & desg_sl & _
                            ",'" & Trim(ds.Tables(0).Rows(i).Item(6)) & "'," & loc_cd & ",'Y')")
                        End If
                    Else
                        If err_cnt <> 1 Then
                            msg = "Designation Blank"
                            err_cnt = 1
                        End If
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(7)) Then
                        Dim dept_nm As String = ds.Tables(0).Rows(i).Item(7)
                        Dim dsdept As DataSet = get_dataset("SELECT dept_sl FROM dept_mst WHERE dept_nm='" & UCase(Trim(dept_nm)) & "' AND loc_cd=" & loc_cd & "")
                        If dsdept.Tables(0).Rows.Count <> 0 Then
                            dept_sl = dsdept.Tables(0).Rows(0).Item("dept_sl")
                        Else
                            dept_sl = "1"
                            Dim ds1 As DataSet = get_dataset("SELECT max(dept_sl) FROM dept_mst ")
                            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                                dept_sl = ds1.Tables(0).Rows(0).Item(0) + 1
                            End If
                            start1()
                            SQLInsert("INSERT INTO dept_mst(dept_sl,dept_nm,loc_cd,active) VALUES(" & dept_sl & _
                            ",'" & Trim(dept_nm) & "'," & loc_cd & ",'Y')")
                        End If
                    Else
                        If err_cnt <> 1 Then
                            msg = "Department Blank"
                            err_cnt = 1
                        End If
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(8)) Then
                        Dim emp_cat As String = ds.Tables(0).Rows(i).Item(8)
                        Dim dscat As DataSet = get_dataset("SELECT cat_sl FROM emp_category WHERE cat_nm='" & UCase(Trim(emp_cat)) & "' AND loc_cd=" & loc_cd & "")
                        If dscat.Tables(0).Rows.Count <> 0 Then
                            cat_sl = dscat.Tables(0).Rows(0).Item("cat_sl")
                        Else
                            cat_sl = 0
                            err_cnt = 1
                            If err_cnt <> 1 Then
                                msg = "Invalid Category"
                                err_cnt = 1
                            End If
                        End If
                    Else
                        If err_cnt <> 1 Then
                            msg = "Employee Category Blank"
                            err_cnt = 1
                        End If
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(9)) Then
                        Try
                            Dim dt1 As Integer = vb.Left(ds.Tables(0).Rows(i).Item(9), 2)
                            Dim mn As Integer = vb.Mid(ds.Tables(0).Rows(i).Item(9), 4, 2)
                            Dim yr As Integer = vb.Right(ds.Tables(0).Rows(i).Item(9), 4)
                            doj = CDate(Format(mn, "00") & "/" & Format(dt1, "00") & "/" & Format(yr, "0000"))
                        Catch ex As Exception
                            If err_cnt <> 1 Then
                                msg = "Invalid DOJ Format"
                                err_cnt = 1
                            End If
                        End Try
                    Else
                        doj = CDate("01/01/1990")
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(10)) Then
                        fath_nm = ds.Tables(0).Rows(i).Item(10)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(11)) Then
                        present_addr = Replace(ds.Tables(0).Rows(i).Item(11), "'", "")
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(12)) Then
                        permanent_addr = Replace(ds.Tables(0).Rows(i).Item(12), "'", "")
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(13)) Then
                        cont = ds.Tables(0).Rows(i).Item(13)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(14)) Then
                        Try
                            Dim dt2 As Integer = vb.Left(ds.Tables(0).Rows(i).Item(14), 2)
                            Dim mn As Integer = vb.Mid(ds.Tables(0).Rows(i).Item(14), 4, 2)
                            Dim yr As Integer = vb.Right(ds.Tables(0).Rows(i).Item(14), 4)
                            dob = CDate(Format(mn, "00") & "/" & Format(dt2, "00") & "/" & Format(yr, "0000"))
                        Catch ex As Exception
                            If err_cnt <> 1 Then
                                msg = "Invalid DOB Format"
                                err_cnt = 1
                            End If
                        End Try
                    Else
                        dob = CDate("01/01/1990")
                    End If
                    shift = 1
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(15)) Then
                        email = ds.Tables(0).Rows(i).Item(15)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(16)) Then
                        pan_no = ds.Tables(0).Rows(i).Item(16)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(17)) Then
                        bank_nm = ds.Tables(0).Rows(i).Item(17)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(18)) Then
                        acc_no = ds.Tables(0).Rows(i).Item(18)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(19)) Then
                        ifsc = ds.Tables(0).Rows(i).Item(19)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(20)) Then
                        esic = ds.Tables(0).Rows(i).Item(20)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(21)) Then
                        uan = ds.Tables(0).Rows(i).Item(21)
                    End If

                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(22)) Then
                        married = ds.Tables(0).Rows(i).Item(22)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(23)) Then
                        Gender = ds.Tables(0).Rows(i).Item(23)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(24)) Then
                        blood = ds.Tables(0).Rows(i).Item(24)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(25)) Then
                        emg_cont_name = ds.Tables(0).Rows(i).Item(25)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(26)) Then
                        emg_cont_relation = ds.Tables(0).Rows(i).Item(26)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(27)) Then
                        emg_cont = ds.Tables(0).Rows(i).Item(27)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(28)) Then
                        qualification = ds.Tables(0).Rows(i).Item(28)
                    End If
                    If Not IsDBNull(ds.Tables(0).Rows(i).Item(29)) Then
                        reporting_emp_code = ds.Tables(0).Rows(i).Item(29)
                        Dim dsempcode As DataSet = get_dataset("SELECT staf_sl FROM staf WHERE emp_code='" & Trim(ds.Tables(0).Rows(i).Item(29)) & "'")
                        If dsempcode.Tables(0).Rows.Count = 0 Then
                            If err_cnt <> 1 Then
                                msg = "Invalid Employee Code"
                                err_cnt = 1
                            End If
                        End If
                    End If

                    If err_cnt = 0 Then
                        Dim max As Integer = 1
                        Dim dsmax As DataSet = get_dataset("SELECT max(staf_sl) FROM staf")
                        If Not IsDBNull(dsmax.Tables(0).Rows(0).Item(0)) Then
                            max = dsmax.Tables(0).Rows(0).Item(0) + 1
                        End If

                        start1()

                        SQLInsert("INSERT INTO staf(staf_sl,staf_nm,short_nm,emp_code,device_code,emp_status,emp_tp,desg_sl,dept_sl,doj," & _
                          "add1,city_cd1,state1,ph1,ph2,add2,city_cd2,state2,ph3,ph4,email,dob,gndr,married,nation,religion,caste,fgh_nm,quali," & _
                          "staf_image,loc_cd,div_sl,shift_tp,pswd,last_login,cat_sl,shift_sl,cycle_sl,weekly_off_tp,RFID_Cardno," & _
                          "pay_mode,pan_no,bank_name,ac_no,ifsc_code,esi_no,pf_no,uan_no,group_sl,wages_type,wages,moth_nm,spouse_name,children_name," & _
                          "blood_grp,emergency_name,relation,emergency_contact,emergency_addr,driving_license,voter_id,adhar_card,dor,reason,week_coff,holi_coff,r_usr_sl,ent_by,ent_dt) VALUES(" & max & ",'" & emp_nm & "','" & emp_nm & "','" & emp_code & "','" & device_code & _
                        "','I','P'," & desg_sl & "," & dept_sl & ",'" & Format(doj, "dd/MMM/yyyy") & "','" & present_addr & "',0,'','" & cont & "','','" & permanent_addr & "',0,'','','','" & email & "','" & Format(dob, "dd/MMM/yyyy") & "','" & vb.Left(Gender, 1) & _
                        "','" & vb.Left(married, 1) & "','','','','" & fath_nm & "','" & qualification & "',''," & loc_cd & "," & div_sl & ",1,'PASSWORD',''," & cat_sl & ",1,0,'1','','1','" & pan_no & "','" & bank_nm & "','" & acc_no & "','" & ifsc & _
                        "','" & esic & "','" & pf & "','" & uan & "',1,'1',0,'','','','" & blood & "','" & emg_cont_name & "','" & emg_cont_relation & "','" & emg_cont & "','','','','" & adhar_card & "','','',0,0," & reporting_emp_code & "," & usr_sl & ",'" & Now & "')")



                        Try
                            SQLInsert("INSERT INTO Employees(EmployeeName,EmployeeCode,StringCode,NumericCode,Gender,CompanyId,DepartmentId,CategoryId,EmployeeCodeInDevice," & _
                             "EmployeeRFIDNumber,EmployementType,Status)  VALUES('" & Trim(emp_nm) & "','" & Trim(emp_code) & "','',0,'" & vb.Left(Gender, 1) & "'," & loc_cd & _
                             "," & Val(dept_sl) & "," & Val(cat_sl) & ",'" & device_code & "','','Permanent','Working')")
                        Catch ex As Exception

                        End Try
         


                        Dim android_sl As Integer = 1
                        Dim dsmax1 As DataSet = get_dataset("SELECT max(android_sl) FROM android_login")
                        If Not IsDBNull(dsmax1.Tables(0).Rows(0).Item(0)) Then
                            android_sl = dsmax1.Tables(0).Rows(0).Item(0) + 1
                        End If
                        SQLInsert("INSERT INTO android_login(android_sl,usr_id,usr_pwd,name,last_login,block,usr_sl,lvl,loc_cd,div_sl) VALUES(" & android_sl & ",'" & _
                        UCase(Trim(emp_code)) & "','PASSWORD','" & Trim(emp_nm) & "','','N'," & max & ",'E'," & loc_cd & "," & _
                        Val(div_sl) & ")")
                    Else
                        dt.Rows.Add(i, ds.Tables(0).Rows(i).Item(1), ds.Tables(0).Rows(i).Item(2), ds.Tables(0).Rows(i).Item(3), ds.Tables(0).Rows(i).Item(4), ds.Tables(0).Rows(i).Item(5), ds.Tables(0).Rows(i).Item(6), ds.Tables(0).Rows(i).Item(7), ds.Tables(0).Rows(i).Item(8), ds.Tables(0).Rows(i).Item(9), ds.Tables(0).Rows(i).Item(10), ds.Tables(0).Rows(i).Item(11), ds.Tables(0).Rows(i).Item(12), ds.Tables(0).Rows(i).Item(13), ds.Tables(0).Rows(i).Item(14), ds.Tables(0).Rows(i).Item(15), ds.Tables(0).Rows(i).Item(16), ds.Tables(0).Rows(i).Item(17), ds.Tables(0).Rows(i).Item(18), ds.Tables(0).Rows(i).Item(19), ds.Tables(0).Rows(i).Item(20), ds.Tables(0).Rows(i).Item(21), ds.Tables(0).Rows(i).Item(22), ds.Tables(0).Rows(i).Item(23), ds.Tables(0).Rows(i).Item(24), ds.Tables(0).Rows(i).Item(25), ds.Tables(0).Rows(i).Item(26), ds.Tables(0).Rows(i).Item(27), ds.Tables(0).Rows(i).Item(28), msg)
                    End If
                Next
                close1()
                Dim gridview1 As New GridView()
                gridview1.AllowPaging = False
                gridview1.DataSource = dt
                gridview1.DataBind()
                Response.Clear()
                Response.Buffer = True
                Response.AddHeader("content-disposition", "attachment;filename=IMPORTRESULT.xls")
                Response.Charset = ""
                Response.ContentType = "application/vnd.ms-excel"
                Dim sw As New StringWriter()
                Dim hw As New HtmlTextWriter(sw)
                For i As Integer = 0 To dt.Rows.Count - 1
                    gridview1.Rows(i).Attributes.Add("class", "textmode")
                Next
                gridview1.RenderControl(hw)

                Dim style As String = "<style> .textmode { mso-number-format :\@} </style>"
                Response.Write(style)
                Response.Output.Write(sw.ToString())
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('" & ex.Message & "');", True)
        End Try
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim str1 As String
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        str1 = FileUpload1.FileName.ToString
        If str1 <> "" Then
            If File.Exists(Server.MapPath(".\Import\") & loc_cd & "_" & FileUpload1.FileName) Then
                File.Delete(Server.MapPath(".\Import\") & loc_cd & "_" & FileUpload1.FileName)
            End If
            FileUpload1.SaveAs(Server.MapPath(".\Import\") & loc_cd & "_" & FileUpload1.FileName)
        End If
        Dim c_excelconstring As String = String.Empty
        Dim uploadpath As String = "~/Import/"
        Dim filepath As String = Server.MapPath(uploadpath & loc_cd & "_" & FileUpload1.PostedFile.FileName)
        Dim filext As String = Path.GetExtension(loc_cd & "_" & FileUpload1.PostedFile.FileName)
        If filext = ".xls" OrElse filext = "XLS" Then
            c_excelconstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & Trim(filepath) & "'" & ";Extended Properties=""Excel 8.0;HDR=Yes'"
        ElseIf filext = ".xlsx" OrElse filext = "XLSX" Then
            c_excelconstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Trim(filepath) & ";Extended Properties=Excel 12.0;Persist Security Info=False"
        End If
        If c_excelconstring <> "" Then
            Dim dt As New DataTable
            dt.Columns.Add("Sl", GetType(Integer))
            dt.Columns.Add("Division", GetType(String))
            dt.Columns.Add("Employee Name", GetType(String))
            dt.Columns.Add("Employee Code", GetType(String))
            dt.Columns.Add("Device Code", GetType(String))
            dt.Columns.Add("Adhara Card No.", GetType(String))
            dt.Columns.Add("Designation", GetType(String))
            dt.Columns.Add("Department", GetType(String))
            dt.Columns.Add("Employee Category", GetType(String))
            dt.Columns.Add("DOJ(DD/MM/YYYY)", GetType(String))
            dt.Columns.Add("Father Name", GetType(String))
            dt.Columns.Add("Present Address", GetType(String))
            dt.Columns.Add("Permanent Address", GetType(String))
            dt.Columns.Add("Contact No.", GetType(String))
            dt.Columns.Add("DOB(DD/MM/YYYY)", GetType(String))
            dt.Columns.Add("Email", GetType(String))
            dt.Columns.Add("PAN", GetType(String))
            dt.Columns.Add("BANK", GetType(String))
            dt.Columns.Add("Account No", GetType(String))
            dt.Columns.Add("IFSC", GetType(String))
            dt.Columns.Add("ESIC", GetType(String))
            dt.Columns.Add("UAN", GetType(String))
            dt.Columns.Add("Marital Status", GetType(String))
            dt.Columns.Add("Gender", GetType(String))
            dt.Columns.Add("Blood Group", GetType(String))
            dt.Columns.Add("Emergency Contact Name", GetType(String))
            dt.Columns.Add("Emergency Contact Relation", GetType(String))
            dt.Columns.Add("Emergency Contact Number", GetType(String))
            dt.Columns.Add("Highest Education", GetType(String))
            dt.Columns.Add("Reporting Manager", GetType(Integer))
            dt.Columns.Add("Remark", GetType(String))
            Dim ds As DataSet = excel_dataset("SELECT * FROM [Sheet1$]", c_excelconstring)
            start1()
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim div_sl As Integer = 0
                Dim emp_nm As String = ""
                Dim emp_code As String = ""
                Dim device_code As String = ""
                Dim adhar_card As String = ""
                Dim desg_sl As Integer = 0
                Dim dept_sl As Integer = 0
                Dim cat_sl As Integer = 0
                Dim fath_nm As String = ""
                Dim present_addr As String = ""
                Dim permanent_addr As String = ""
                Dim cont As String = ""
                Dim doj As Date
                Dim dob As Date
                Dim err_cnt As Integer = 0
                Dim pan_no As String = ""
                Dim bank_nm As String = ""
                Dim email As String = ""
                Dim acc_no As String = ""
                Dim ifsc As String = ""
                Dim esic As String = ""
                Dim pf As String = ""
                Dim uan As String = ""
                Dim married As String = ""
                Dim Gender As String = "M"
                Dim blood As String = ""
                Dim emergency_name As String = ""
                Dim emg_cont_name As String = ""
                Dim emg_cont_relation As String = ""
                Dim emg_cont As String = ""
                Dim qualification As String = ""
                Dim shift As Integer = 0
                Dim reporting_emp_code As Integer = 0
                Dim msg As String = "Added Successfully"
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(1)) Then
                    Dim div_nm As String = ds.Tables(0).Rows(i).Item(1)
                    Dim dsdiv As DataSet = get_dataset("SELECT * FROM division_mst WHERE div_nm='" & UCase(Trim(ds.Tables(0).Rows(i).Item(1))) & "' AND loc_cd=" & loc_cd & "")
                    If dsdiv.Tables(0).Rows.Count <> 0 Then
                        div_sl = dsdiv.Tables(0).Rows(0).Item("div_sl")
                    Else
                        div_sl = 0
                        err_cnt = 1
                        msg = "Invalid Division"
                    End If
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(2)) Then
                    emp_nm = ds.Tables(0).Rows(i).Item(2)
                Else
                    If err_cnt <> 1 Then
                        msg = "Employee Name Blank"
                        err_cnt = 1
                    End If
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(3)) Then
                    emp_code = ds.Tables(0).Rows(i).Item(3)
                    Dim dsempcode As DataSet = get_dataset("SELECT emp_code FROM staf WHERE emp_code='" & UCase(Trim(ds.Tables(0).Rows(i).Item(3))) & "'")
                    If dsempcode.Tables(0).Rows.Count = 0 Then
                        If err_cnt <> 1 Then
                            msg = "Invalid Employee Code"
                            err_cnt = 1
                        End If
                    End If
                Else
                    If err_cnt <> 1 Then
                        msg = "Employee Code Blank"
                        err_cnt = 1
                    End If
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(4)) Then
                    device_code = ds.Tables(0).Rows(i).Item(4)
                    Dim dsdvcode As DataSet = get_dataset("SELECT device_code FROM staf WHERE device_code='" & UCase(Trim(ds.Tables(0).Rows(i).Item(4))) & "'")
                    If dsdvcode.Tables(0).Rows.Count = 0 Then
                        If err_cnt <> 1 Then
                            msg = "Invalid Device Code"
                            err_cnt = 1
                        End If
                    End If
                Else
                    If err_cnt <> 1 Then
                        msg = "Device Code Blank"
                        err_cnt = 1
                    End If
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(5)) Then
                    adhar_card = ds.Tables(0).Rows(i).Item(5)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(6)) Then
                    Dim desg_nm As String = ds.Tables(0).Rows(i).Item(6)
                    Dim dsdesg As DataSet = get_dataset("SELECT desg_sl FROM desg_mst WHERE desg_nm='" & UCase(Trim(desg_nm)) & "' AND loc_cd=" & loc_cd & "")
                    If dsdesg.Tables(0).Rows.Count <> 0 Then
                        desg_sl = dsdesg.Tables(0).Rows(0).Item("desg_sl")
                    Else
                        desg_sl = "1"
                        Dim ds1 As DataSet = get_dataset("SELECT max(desg_sl) FROM desg_mst ")
                        If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                            desg_sl = ds1.Tables(0).Rows(0).Item(0) + 1
                        End If
                        SQLInsert("INSERT INTO desg_mst(desg_sl,desg_nm,loc_cd,active) VALUES(" & desg_sl & _
                        ",'" & Trim(ds.Tables(0).Rows(i).Item(6)) & "'," & loc_cd & ",'Y')")
                    End If
                Else
                    If err_cnt <> 1 Then
                        msg = "Designation Blank"
                        err_cnt = 1
                    End If
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(7)) Then
                    Dim dept_nm As String = ds.Tables(0).Rows(i).Item(7)
                    Dim dsdept As DataSet = get_dataset("SELECT dept_sl FROM dept_mst WHERE dept_nm='" & UCase(Trim(dept_nm)) & "' AND loc_cd=" & loc_cd & "")
                    If dsdept.Tables(0).Rows.Count <> 0 Then
                        dept_sl = dsdept.Tables(0).Rows(0).Item("dept_sl")
                    Else
                        dept_sl = "1"
                        Dim ds1 As DataSet = get_dataset("SELECT max(dept_sl) FROM dept_mst ")
                        If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                            dept_sl = ds1.Tables(0).Rows(0).Item(0) + 1
                        End If
                        start1()
                        SQLInsert("INSERT INTO dept_mst(dept_sl,dept_nm,loc_cd,active) VALUES(" & dept_sl & _
                        ",'" & Trim(dept_nm) & "'," & loc_cd & ",'Y')")
                    End If
                Else
                    If err_cnt <> 1 Then
                        msg = "Department Blank"
                        err_cnt = 1
                    End If
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(8)) Then
                    Dim emp_cat As String = ds.Tables(0).Rows(i).Item(8)
                    Dim dscat As DataSet = get_dataset("SELECT cat_sl FROM emp_category WHERE cat_nm='" & UCase(Trim(emp_cat)) & "' AND loc_cd=" & loc_cd & "")
                    If dscat.Tables(0).Rows.Count <> 0 Then
                        cat_sl = dscat.Tables(0).Rows(0).Item("cat_sl")
                    Else
                        cat_sl = 0
                        err_cnt = 1
                        If err_cnt <> 1 Then
                            msg = "Invalid Category"
                            err_cnt = 1
                        End If
                    End If
                Else
                    If err_cnt <> 1 Then
                        msg = "Employee Category Blank"
                        err_cnt = 1
                    End If
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(9)) Then
                    Try
                        Dim dt1 As Integer = vb.Left(ds.Tables(0).Rows(i).Item(9), 2)
                        Dim mn As Integer = vb.Mid(ds.Tables(0).Rows(i).Item(9), 4, 2)
                        Dim yr As Integer = vb.Right(ds.Tables(0).Rows(i).Item(9), 4)
                        doj = CDate(Format(mn, "00") & "/" & Format(dt1, "00") & "/" & Format(yr, "0000"))
                    Catch ex As Exception
                        If err_cnt <> 1 Then
                            msg = "Invalid DOJ Format"
                            err_cnt = 1
                        End If
                    End Try
                Else
                    doj = CDate("01/01/1990")
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(10)) Then
                    fath_nm = ds.Tables(0).Rows(i).Item(10)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(11)) Then
                    present_addr = Replace(ds.Tables(0).Rows(i).Item(11), "'", "")
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(12)) Then
                    permanent_addr = Replace(ds.Tables(0).Rows(i).Item(12), "'", "")
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(13)) Then
                    cont = ds.Tables(0).Rows(i).Item(13)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(14)) Then
                    Try
                        Dim dt2 As Integer = vb.Left(ds.Tables(0).Rows(i).Item(14), 2)
                        Dim mn As Integer = vb.Mid(ds.Tables(0).Rows(i).Item(14), 4, 2)
                        Dim yr As Integer = vb.Right(ds.Tables(0).Rows(i).Item(14), 4)
                        dob = CDate(Format(mn, "00") & "/" & Format(dt2, "00") & "/" & Format(yr, "0000"))
                    Catch ex As Exception
                        If err_cnt <> 1 Then
                            msg = "Invalid DOB Format"
                            err_cnt = 1
                        End If
                    End Try
                Else
                    dob = CDate("01/01/1990")
                End If
                shift = 1
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(15)) Then
                    email = ds.Tables(0).Rows(i).Item(15)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(16)) Then
                    pan_no = ds.Tables(0).Rows(i).Item(16)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(17)) Then
                    bank_nm = ds.Tables(0).Rows(i).Item(17)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(18)) Then
                    acc_no = ds.Tables(0).Rows(i).Item(18)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(19)) Then
                    ifsc = ds.Tables(0).Rows(i).Item(19)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(20)) Then
                    esic = ds.Tables(0).Rows(i).Item(20)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(21)) Then
                    uan = ds.Tables(0).Rows(i).Item(21)
                End If

                If Not IsDBNull(ds.Tables(0).Rows(i).Item(22)) Then
                    married = ds.Tables(0).Rows(i).Item(22)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(23)) Then
                    Gender = ds.Tables(0).Rows(i).Item(23)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(24)) Then
                    blood = ds.Tables(0).Rows(i).Item(24)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(25)) Then
                    emg_cont_name = ds.Tables(0).Rows(i).Item(25)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(26)) Then
                    emg_cont_relation = ds.Tables(0).Rows(i).Item(26)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(27)) Then
                    emg_cont = ds.Tables(0).Rows(i).Item(27)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(28)) Then
                    qualification = ds.Tables(0).Rows(i).Item(28)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(29)) Then
                    reporting_emp_code = ds.Tables(0).Rows(i).Item(29)
                    Dim dsempcode As DataSet = get_dataset("SELECT staf_sl FROM staf WHERE emp_code='" & Trim(ds.Tables(0).Rows(i).Item(29)) & "'")
                    If dsempcode.Tables(0).Rows.Count = 0 Then
                        If err_cnt <> 1 Then
                            msg = "Invalid Employee Code"
                            err_cnt = 1
                        End If
                    End If
                End If


                If err_cnt = 0 Then
                    Dim max As Integer = 1
                    Dim dsmax As DataSet = get_dataset("SELECT max(staf_sl) FROM staf")
                    If Not IsDBNull(dsmax.Tables(0).Rows(0).Item(0)) Then
                        max = dsmax.Tables(0).Rows(0).Item(0) + 1
                    End If

                    start1()

                    SQLInsert("UPDATE  staf SET staf_nm='" & emp_nm & "',short_nm='" & emp_nm & "',device_code'" & device_code & "',desg_sl=" & desg_sl & ",dept_sl=" & dept_sl & ",doj='" & Format(doj, "dd/MMM/yyyy") & _
                    "',add1='" & present_addr & "',ph1='" & cont & "',add2='" & permanent_addr & "',email='" & email & "',dob='" & Format(dob, "dd/MMM/yyyy") & "',gndr='" & vb.Left(Gender, 1) & _
                    "',married='" & vb.Left(married, 1) & "',fgh_nm='" & fath_nm & "',quali='" & qualification & "',div_sl=" & div_sl & ",cat_sl=" & cat_sl & ",pan_no='" & pan_no & "',bank_name='" & _
                    bank_nm & "',ac_no='" & acc_no & "',ifsc_code='" & ifsc & "',esi_no='" & esic & "',pf_no='" & pf & "',uan_no='" & uan & "',blood_grp='" & blood & "',emergency_name='" & emg_cont_name & _
                    "',relation='" & emg_cont_relation & "',emergency_contact='" & emg_cont & "',adhar_card='" & adhar_card & "',mod_by=" & usr_sl & ",mod_dt='" & Now & "' WHERE emp_code='" & emp_code & "'")

                    SQLInsert("UPDATE Employees SET EmployeeName='" & Trim(emp_nm) & "',Gender='" & vb.Left(Gender, 1) & "',DepartmentId=" & Val(dept_sl) & ",CategoryId=" & Val(cat_sl) & ",EmployeeCodeInDevice='" & device_code & "' WHERE EmployeeCode='" & Trim(emp_code) & "'")

                    SQLInsert("UPDATE android_login SET name='" & Trim(emp_nm) & "' WHERE usr_id='" & emp_code & "'")
                Else
                    dt.Rows.Add(i, ds.Tables(0).Rows(i).Item(1), ds.Tables(0).Rows(i).Item(2), ds.Tables(0).Rows(i).Item(3), ds.Tables(0).Rows(i).Item(4), ds.Tables(0).Rows(i).Item(5), ds.Tables(0).Rows(i).Item(6), ds.Tables(0).Rows(i).Item(7), ds.Tables(0).Rows(i).Item(8), ds.Tables(0).Rows(i).Item(9), ds.Tables(0).Rows(i).Item(10), ds.Tables(0).Rows(i).Item(11), ds.Tables(0).Rows(i).Item(12), ds.Tables(0).Rows(i).Item(13), ds.Tables(0).Rows(i).Item(14), ds.Tables(0).Rows(i).Item(15), ds.Tables(0).Rows(i).Item(16), ds.Tables(0).Rows(i).Item(17), ds.Tables(0).Rows(i).Item(18), ds.Tables(0).Rows(i).Item(19), ds.Tables(0).Rows(i).Item(20), ds.Tables(0).Rows(i).Item(21), ds.Tables(0).Rows(i).Item(22), ds.Tables(0).Rows(i).Item(23), ds.Tables(0).Rows(i).Item(24), ds.Tables(0).Rows(i).Item(25), ds.Tables(0).Rows(i).Item(26), ds.Tables(0).Rows(i).Item(27), ds.Tables(0).Rows(i).Item(28), msg)
                End If
            Next
            close1()
            Dim gridview1 As New GridView()
            gridview1.AllowPaging = False
            gridview1.DataSource = dt
            gridview1.DataBind()
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=IMPORTRESULT.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            For i As Integer = 0 To dt.Rows.Count - 1
                gridview1.Rows(i).Attributes.Add("class", "textmode")
            Next
            gridview1.RenderControl(hw)

            Dim style As String = "<style> .textmode { mso-number-format :\@} </style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()
        End If
    End Sub
End Class
