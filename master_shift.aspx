﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="master_shift.aspx.vb" Inherits="master_shift" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="A1" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                      aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="master_shift.aspx">Add New Shift</a>
                      <a class="dropdown-item" href="master_shift.aspx?mode=V">View Shift List</a>
                    
                    </div>
                  </div>
                </div>

                     <div class="card-body">
                                     <div runat="server" id="divview" style="width: 100%">

                                <asp:GridView ID="dv" runat="server" AllowPaging="True" 
                                    AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                    AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                    PageSize="15" Width="100%">
                                    <AlternatingRowStyle CssClass="alt" />
                                    <Columns>
                                        <asp:BoundField DataField="sl" HeaderText="Sl">
                                        <HeaderStyle Width="30px" />
                                        <ItemStyle Width="30px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="shift_nm" HeaderText="Timming Name" />
                                        <asp:BoundField DataField="sdt" HeaderText="Start Time" />
                                        <asp:BoundField DataField="edt" HeaderText="End Time" />
                                        <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                            ImageUrl="images/edit.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                                                      <ItemStyle Height="30px" Width="30px" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                </asp:GridView>

                            </div>
                            <div runat="server" id="divadd">
                                                          <table align="center" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            Shift Name</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtname" runat="server" class="form-control" MaxLength="100" required></asp:TextBox>
                                          
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table style="width:100%;">
                                                <tr>
                                                    <td width="45%" style="font-size: 8px">
                                                        &nbsp;</td>
                                                    <td width="10%" style="font-size: 8px">
                                                        &nbsp;</td>
                                                    <td width="45%" style="font-size: 8px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="45%">
                                                        Start Time 
                                                        24 Format</td>
                                                    <td width="10%">
                                                        &nbsp;</td>
                                                    <td width="45%">
                                                        End Time 
                                                        24 Format</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                            <asp:TextBox ID="txtsttm" runat="server" class="form-control" MaxLength="100" required></asp:TextBox>
                                           
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                            <asp:TextBox ID="txtendtm" runat="server" class="form-control" MaxLength="100" required></asp:TextBox>
                                        
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table style="width:100%;">
                                                <tr>
                                                    <td width="45%" style="font-size: 8px">
                                                        &nbsp;</td>
                                                    <td width="10%" style="font-size: 8px">
                                                        &nbsp;</td>
                                                    <td width="45%" style="font-size: 8px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="45%">
                                                        Is Night Shift</td>
                                                    <td width="10%">
                                                        &nbsp;</td>
                                                    <td width="45%">
                                                        Include Break Time</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                            <asp:DropDownList ID="cmbnight" runat="server" class="form-control" CssClass="form-control">
                                                <asp:ListItem Value="Yes"></asp:ListItem>
                                                <asp:ListItem Value="No"></asp:ListItem>
                                            </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                            <asp:DropDownList ID="cmbinclude" runat="server" class="form-control">
                                                <asp:ListItem Value="Yes"></asp:ListItem>
                                                <asp:ListItem Value="No"></asp:ListItem>
                                            </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table style="width:100%;">
                                                <tr>
                                                    <td width="45%" style="font-size: 8px">
                                                        &nbsp;</td>
                                                    <td width="10%" style="font-size: 8px">
                                                        &nbsp;</td>
                                                    <td width="45%" style="font-size: 8px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="45%">
                                                        Break
                                                        Start Time 
                                                        24 Format</td>
                                                    <td width="10%">
                                                        &nbsp;</td>
                                                    <td width="45%">
                                                        Break
                                                        End Time 
                                                        24 Format</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                            <asp:TextBox ID="txtbrksttm" runat="server" class="form-control" MaxLength="100"></asp:TextBox>
                                          
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td>
                                            <asp:TextBox ID="txtbrkendtm" runat="server" class="form-control" MaxLength="100"></asp:TextBox>
                                          
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 8px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Active</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="cmbactive" runat="server" class="form-control" 
                                                Width="100px">
                                                <asp:ListItem Value="Yes"></asp:ListItem>
                                                <asp:ListItem Value="No"></asp:ListItem>
                                            </asp:DropDownList>
                                                    </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtmode" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                            <asp:TextBox ID="txtsl" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="cmdsave" runat="server" class="btn btn-info" 
                                                Text="Submit" />&nbsp;&nbsp;
                                            <asp:Button ID="cmdclr" runat="server" class="btn btn-default" 
                                                Text="Clear" />
                                        </td>
                                    </tr>
                                </table>
                            </div>   
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

