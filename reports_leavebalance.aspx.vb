﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class reports_leavebalance
    Inherits System.Web.UI.Page
    <System.Web.Script.Services.ScriptMethod(), _
   System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            "staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblmsg.Visible = False
        txtfrmdt.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        txttodt.Text = Format(Now, "dd/MM/yyyy")
        Me.divisiondisp()
        Me.deptdisp()
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
        txtdept.Text = "All Departments"
    End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub deptdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds As DataSet = get_dataset("select distinct dept_nm from dept_mst WHERE loc_cd=" & loc_cd & " order by dept_nm")
        dvdept.DataSource = ds.Tables(0)
        dvdept.DataBind()
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If
        txtdept.Text = ""
        For i As Integer = 0 To dvdept.Rows.Count - 1
            Dim chk As CheckBox = dvdept.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                Dim lbl As Label = dvdept.Rows(i).FindControl("lbldept")
                txtdept.Text = txtdept.Text & "'" & lbl.Text & "',"
            End If
        Next
        If txtdept.Text <> "" Then
            txtdept.Text = vb.Left(txtdept.Text, txtdept.Text.Length - 1)
            str = str & "AND dept_nm IN (" & txtdept.Text & ")"
        Else
            txtdept.Text = "All Departments"
        End If
        If Trim(txtempcode.Text) <> "" Then
            str = ""
            str = "AND staf.emp_code='" & Trim(txtempcode.Text) & "'"
        End If
        Dim dt As New DataTable
        dt.Columns.Add("Sl", GetType(Integer))
        dt.Columns.Add("NAME", GetType(String))
        dt.Columns.Add("EMP. CODE", GetType(String))
        dt.Columns.Add("DEPARTMENT", GetType(String))
        dt.Columns.Add("DESIGNATION", GetType(String))
        dt.Columns.Add("LEAVE BALANCE", GetType(String))
        Dim dsleaveassign As DataSet = get_dataset("select staf_sl,(CASE WHEN leave_tp='1' THEN 'PL' WHEN leave_tp='2' THEN 'CL' WHEN leave_tp='3' THEN 'NL' WHEN leave_tp='4' THEN 'OL' WHEN leave_tp='5' THEN 'SL' WHEN leave_tp='6' THEN 'ML' WHEN leave_tp='7' THEN 'AL' END) as tp,sum(no_day) as no_day from leave_assignment WHERE assign_year='" & CType(Session("cur_year"), String) & "' GROUP BY staf_sl,leave_tp")
        Dim dsleaveapp As DataSet = get_dataset("SELECT COUNT(lvoucher2.v_sl) AS Expr1, lvoucher2.staf_sl, (CASE WHEN leave_mst.leave_tp = '1' THEN 'PL' WHEN leave_mst.leave_tp = '2' THEN 'CL' WHEN leave_mst.leave_tp = '3' THEN 'NL' WHEN leave_tp='4' THEN 'OL' WHEN leave_tp='5' THEN 'SL' WHEN leave_tp='6' THEN 'ML' WHEN leave_tp='7' THEN 'AL' END) AS leave_tp FROM lvoucher2 LEFT OUTER JOIN leave_mst ON lvoucher2.leave_sl = leave_mst.leave_sl WHERE approved='Y' AND l_dt >='" & Format(stringtodate(CType(Session("st_dt"), String)), "dd/MMM/yyyy") & "' and l_dt <= '" & Format(stringtodate(CType(Session("en_dt"), String)), "dd/MMM/yyyy") & "'GROUP BY staf_sl,leave_tp")
        Dim ds1 As DataSet = get_dataset("SELECT staf.staf_sl, staf.staf_nm, staf.emp_code, dept_mst.dept_nm, desg_mst.desg_nm FROM staf LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl WHERE emp_status='I' AND staf.loc_cd= " & loc_cd & "  " & str & "  ORDER BY emp_code")
        If ds1.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
                Dim dr As DataRow
                dr = dt.NewRow
                dr(0) = i + 1
                dr(1) = ds1.Tables(0).Rows(i).Item("staf_nm")
                dr(2) = ds1.Tables(0).Rows(i).Item("emp_code")
                dr(3) = ds1.Tables(0).Rows(i).Item("dept_nm")
                dr(4) = ds1.Tables(0).Rows(i).Item("desg_nm")
                Dim strleave As String = ""
                Dim dr2 As DataRow()
                dr2 = dsleaveassign.Tables(0).Select("staf_sl=" & ds1.Tables(0).Rows(i).Item("staf_sl") & "")
                If dr2.Length <> 0 Then
                    For j As Integer = 0 To dr2.Length - 1
                        Dim tot_count As Integer = dr2(j).Item("no_day")
                        If j = 0 Then
                            strleave = dr2(j).Item("tp")
                        Else
                            strleave = strleave & "," & dr2(j).Item("tp")
                        End If

                        Dim dr3 As DataRow()
                        Dim tot_leave = 0
                        dr3 = dsleaveapp.Tables(0).Select("staf_sl=" & ds1.Tables(0).Rows(i).Item("staf_sl") & " AND leave_tp='" & dr2(j).Item("tp") & "'")
                        If dr3.Length <> 0 Then
                            tot_leave = dr3(0).Item(0)
                        End If
                        strleave = strleave & "-" & (tot_count - tot_leave)
                    Next
                End If
                dr(5) = strleave
                dt.Rows.Add(dr)
            Next
            dvdata.DataSource = dt
            dvdata.DataBind()
        Else
            dvdata.DataSource = dt
            dvdata.DataBind()
            lblmsg.Text = "No Record Found."
        End If

    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub

    Protected Sub txtempcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtempcode.TextChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If Trim(txtempcode.Text) <> "" Then
            Dim emp_code As String() = Trim(txtempcode.Text).Split("-")
            txtempcode.Text = emp_code(0)
            Dim ds1 As DataSet = get_dataset("SELECT staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm, location_mst.loc_nm, staf.loc_cd FROM location_mst RIGHT OUTER JOIN staf ON location_mst.loc_cd = staf.loc_cd LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            Else
                txtempcode.Text = ""
            End If
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If
        txtdept.Text = ""
        For i As Integer = 0 To dvdept.Rows.Count - 1
            Dim chk As CheckBox = dvdept.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                Dim lbl As Label = dvdept.Rows(i).FindControl("lbldept")
                txtdept.Text = txtdept.Text & "'" & lbl.Text & "',"
            End If
        Next
        If txtdept.Text <> "" Then
            txtdept.Text = vb.Left(txtdept.Text, txtdept.Text.Length - 1)
            str = str & "AND dept_nm IN (" & txtdept.Text & ")"
        Else
            txtdept.Text = "All Departments"
        End If
        If Trim(txtempcode.Text) <> "" Then
            str = ""
            str = "AND staf.emp_code='" & Trim(txtempcode.Text) & "'"
        End If
        Dim dt As New DataTable
        dt.Columns.Add("Sl", GetType(Integer))
        dt.Columns.Add("NAME", GetType(String))
        dt.Columns.Add("EMP. CODE", GetType(String))
        dt.Columns.Add("DEPARTMENT", GetType(String))
        dt.Columns.Add("DESIGNATION", GetType(String))
        dt.Columns.Add("LEAVE BALANCE", GetType(String))
        Dim dsleaveassign As DataSet = get_dataset("select staf_sl,(CASE WHEN leave_tp='1' THEN 'PL' WHEN leave_tp='2' THEN 'CL' WHEN leave_tp='3' THEN 'NL' WHEN leave_tp='4' THEN 'OL' WHEN leave_tp='5' THEN 'SL' WHEN leave_tp='6' THEN 'ML' END) as tp,sum(no_day) as no_day from leave_assignment WHERE assign_year='" & CType(Session("cur_year"), String) & "' GROUP BY staf_sl,leave_tp")
        Dim dsleaveapp As DataSet = get_dataset("SELECT COUNT(lvoucher2.v_sl) AS Expr1, lvoucher2.staf_sl, (CASE WHEN leave_mst.leave_tp = '1' THEN 'PL' WHEN leave_mst.leave_tp = '2' THEN 'CL' WHEN leave_mst.leave_tp = '3' THEN 'NL' WHEN leave_tp='4' THEN 'OL' WHEN leave_tp='5' THEN 'SL' WHEN leave_tp='6' THEN 'ML' END) AS leave_tp FROM lvoucher2 LEFT OUTER JOIN leave_mst ON lvoucher2.leave_sl = leave_mst.leave_sl WHERE approved='Y' AND l_dt >='" & Format(stringtodate(CType(Session("st_dt"), String)), "dd/MMM/yyyy") & "' and l_dt <= '" & Format(stringtodate(CType(Session("en_dt"), String)), "dd/MMM/yyyy") & "'GROUP BY staf_sl,leave_tp")
        Dim ds1 As DataSet = get_dataset("SELECT staf.staf_sl, staf.staf_nm, staf.emp_code, dept_mst.dept_nm, desg_mst.desg_nm FROM staf LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl WHERE emp_status='I' AND staf.loc_cd= " & loc_cd & "  " & str & "  ORDER BY emp_code")
        For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
            Dim dr As DataRow
            dr = dt.NewRow
            dr(0) = i + 1
            dr(1) = ds1.Tables(0).Rows(i).Item("staf_nm")
            dr(2) = ds1.Tables(0).Rows(i).Item("emp_code")
            dr(3) = ds1.Tables(0).Rows(i).Item("dept_nm")
            dr(4) = ds1.Tables(0).Rows(i).Item("desg_nm")
            Dim strleave As String = ""
            Dim dr2 As DataRow()
            dr2 = dsleaveassign.Tables(0).Select("staf_sl=" & ds1.Tables(0).Rows(i).Item("staf_sl") & "")
            If dr2.Length <> 0 Then
                For j As Integer = 0 To dr2.Length - 1
                    Dim tot_count As Integer = dr2(j).Item("no_day")
                    If j = 0 Then
                        strleave = dr2(j).Item("tp")
                    Else
                        strleave = strleave & "," & dr2(j).Item("tp")
                    End If

                    Dim dr3 As DataRow()
                    Dim tot_leave = 0
                    dr3 = dsleaveapp.Tables(0).Select("staf_sl=" & ds1.Tables(0).Rows(i).Item("staf_sl") & " AND leave_tp='" & dr2(j).Item("tp") & "'")
                    If dr3.Length <> 0 Then
                        tot_leave = dr3(0).Item(0)
                    End If
                    strleave = strleave & "-" & (tot_count - tot_leave)
                Next
            End If
            dr(5) = strleave
            dt.Rows.Add(dr)
        Next

        'Create a dummy GridView

        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.DataSource = dt
        GridView1.DataBind()
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=LeaveBalanceRegister.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        For i As Integer = 0 To GridView1.Rows.Count - 1
            GridView1.Rows(i).Attributes.Add("class", "textmode")
        Next
        GridView1.RenderControl(hw)
        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()
    End Sub
End Class
