﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class transcation_leaveassign
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
        txtcatsl.Text = ""
        chk1.Checked = False
        chk2.Checked = False
        chk3.Checked = False
        chk4.Checked = False
        chk5.Checked = False
        chk6.Checked = False
        chk7.Checked = False
        chk8.Checked = False
        chk9.Checked = False
        chk10.Checked = False
        chk11.Checked = False
        chk12.Checked = False
        cmballowvisit.SelectedIndex = 0
        Me.divisiondisp()
        Me.deptdisp()
        Me.categorydisp()
        Me.staf_disp()
        If txtmode.Text = "E" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Employee Mobile App Settings (Entry Mode)"
            cmbdivsion.Focus()
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Employee Mobile App Settings (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub staf_disp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim str As String = ""
        Dim ds As New DataSet
        If Val(txtdivsl.Text) <> 0 Then
            str = "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If
        If Val(txtdeptcd.Text) <> 0 Then
            str = str & "AND staf.dept_sl = " & Val(txtdeptcd.Text) & ""
        End If
        If Val(txtcatsl.Text) <> 0 Then
            str = str & "AND staf.cat_sl = " & Val(txtcatsl.Text) & ""
        End If
        If txtempcode.Text <> "" Then
            str = "AND staf.emp_code = '" & Trim(txtempcode.Text) & "'"
        End If
        ds = get_dataset("SELECT staf.staf_nm, staf.emp_code, staf.device_code, staf.staf_sl, desg_mst.desg_nm, dept_mst.dept_nm FROM staf LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.loc_cd=" & loc_cd & " AND emp_status='I' " & str & " ORDER BY staf_nm")
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
    End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("All Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub deptdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdept.Items.Clear()
        cmbdept.Items.Add("All Department")
        Dim ds As DataSet = get_dataset("SELECT dept_nm FROM dept_mst WHERE loc_cd=" & loc_cd & " ORDER BY dept_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdept.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub categorydisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbcategory.Items.Clear()
        cmbcategory.Items.Add("All Category")
        Dim ds As DataSet = get_dataset("SELECT cat_nm FROM emp_category WHERE loc_cd=" & loc_cd & " ORDER BY cat_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbcategory.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Protected Sub cmbdept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdept.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdeptcd.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT dept_sl FROM dept_mst WHERE dept_nm='" & Trim(cmbdept.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdeptcd.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
        Me.staf_disp()
    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
        Me.staf_disp()
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT division_mst.div_nm, staf.staf_nm, staf.emp_code, staf.andorid_attendance, staf.andorid_WFH, staf.andorid_Tour, staf.andorid_Outdoor, staf.andorid_attendance_approval, staf.andorid_WFH_approval, staf.andorid_Tour_approval, staf.andorid_Outdoor_approval, staf.andorid_attendance_email, staf.andorid_WFH_email, staf.andorid_Tour_email, staf.andorid_Outdoor_email FROM staf LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)

        Dim andorid_attendance As Integer = IIf(chk1.Checked, 1, 0)
        Dim andorid_WFH As Integer = IIf(chk2.Checked, 1, 0)
        Dim andorid_Tour As Integer = IIf(chk3.Checked, 1, 0)
        Dim andorid_Outdoor As Integer = IIf(chk4.Checked, 1, 0)
        Dim andorid_attendance_approval As Integer = IIf(chk5.Checked, 1, 0)
        Dim andorid_WFH_approval As Integer = IIf(chk6.Checked, 1, 0)
        Dim andorid_Tour_approval As Integer = IIf(chk7.Checked, 1, 0)
        Dim andorid_Outdoor_approval As Integer = IIf(chk8.Checked, 1, 0)
        Dim andorid_attendance_email As Integer = IIf(chk9.Checked, 1, 0)
        Dim andorid_WFH_email As Integer = IIf(chk10.Checked, 1, 0)
        Dim andorid_Tour_email As Integer = IIf(chk11.Checked, 1, 0)
        Dim andorid_Outdoor_email As Integer = IIf(chk12.Checked, 1, 0)

        Dim fnd As Integer = 0
        For i As Integer = 0 To dvstaf.Rows.Count - 1
            Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                fnd = 1
                Exit For
            End If
        Next
        If fnd = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Select Atleast One Employee');", True)
            dvstaf.Focus()
            Exit Sub
        End If

        start1()
        For j As Integer = 0 To dvstaf.Rows.Count - 1
            Dim chk As CheckBox = dvstaf.Rows(j).FindControl("chk")
            If chk.Checked = True Then
                Dim lblsl As Label = dvstaf.Rows(j).FindControl("lblstaf_sl")
                SQLInsert("UPDATE staf SET andorid_attendance=" & andorid_attendance & ",andorid_WFH=" & andorid_WFH & ",andorid_Tour=" & andorid_Tour & ",andorid_Outdoor=" & _
                andorid_Outdoor & ",andorid_attendance_approval=" & andorid_attendance_approval & ",andorid_WFH_approval=" & andorid_WFH_approval & ",andorid_Tour_approval=" & _
                andorid_Tour_approval & ",andorid_Outdoor_approval=" & andorid_Outdoor_approval & ",andorid_attendance_email=" & andorid_attendance_email & ",andorid_WFH_email=" & _
                andorid_WFH_email & ",andorid_Tour_email=" & andorid_Tour_email & ",andorid_Outdoor_email=" & andorid_Outdoor_email & ",allow_visit_attendance=" & cmballowvisit.SelectedValue & " WHERE staf_sl = " & lblsl.Text & "")
            End If
        Next
        close1()
        Me.clr()
        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim lbl As Label = GridView1.Rows(rw).FindControl("lblslno")
            start1()
            SQLInsert("DELETE FROM leave_assignment WHERE sl=" & lbl.Text & " AND loc_cd=" & loc_cd & "")
            close1()
        End If
        Me.dvdisp()
    End Sub


    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        cmbdivsion.Focus()
    End Sub

    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkhdr As CheckBox = dvstaf.HeaderRow.FindControl("chk1")
        If chkhdr.Checked = True Then
            For i As Integer = 0 To dvstaf.Rows.Count - 1
                Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
                chk.Checked = True
            Next
        ElseIf chkhdr.Checked = False Then
            For i As Integer = 0 To dvstaf.Rows.Count - 1
                Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
                chk.Checked = False
            Next
        End If
    End Sub

       Protected Sub txtempcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtempcode.TextChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If Trim(txtempcode.Text) <> "" Then
            Dim emp_code As String() = Trim(txtempcode.Text).Split("-")
            txtempcode.Text = emp_code(0)
            Dim ds1 As DataSet = get_dataset("SELECT staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm, location_mst.loc_nm, staf.loc_cd FROM location_mst RIGHT OUTER JOIN staf ON location_mst.loc_cd = staf.loc_cd LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            Else
                txtempcode.Text = ""
            End If
            Me.staf_disp()
        End If
    End Sub

    Protected Sub cmbcategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbcategory.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtcatsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT cat_sl FROM emp_category WHERE cat_nm='" & Trim(cmbcategory.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtcatsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
        Me.staf_disp()
    End Sub
End Class
