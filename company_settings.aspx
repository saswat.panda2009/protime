﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="company_settings.aspx.vb" Inherits="company_division" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Company Settings"></asp:Label></h3>
                
                </div>

                     <div class="card-body">
                                       <table style="width:100%;">
                            
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table width="100%">
                                      <tr>
                                      <td>
                                      <table style="width:100%;"><tr><td width="45%">PF Applicable</td><td width="10%">&nbsp;</td>
                                          <td width="45%">&nbsp;</td></tr><tr><td>
                                          <asp:DropDownList ID="cmbpf" runat="server" CssClass="form-control">
                                              <asp:ListItem>Yes</asp:ListItem>
                                              <asp:ListItem>No</asp:ListItem>
                                          </asp:DropDownList>
                                          </td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>Employee&#39;s Contribution</td><td>&nbsp;</td><td>
                                          Employer&#39;s Contribution</td></tr>
                                          <tr>
                                              <td>
                                                  <asp:TextBox ID="txtemppf" runat="server" CssClass="form-control"></asp:TextBox>
                                              </td>
                                              <td>
                                                  &nbsp;</td>
                                              <td>
                                                  <asp:TextBox ID="txtemplrpf" runat="server" CssClass="form-control"></asp:TextBox>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  ESIC Applicable</td>
                                              <td>
                                                  &nbsp;</td>
                                              <td>
                                                  &nbsp;</td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  <asp:DropDownList ID="cmbesic" runat="server" CssClass="form-control">
                                                      <asp:ListItem>Yes</asp:ListItem>
                                                      <asp:ListItem>No</asp:ListItem>
                                                  </asp:DropDownList>
                                              </td>
                                              <td>
                                                  &nbsp;</td>
                                              <td>
                                                  &nbsp;</td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Employee&#39;s Contribution</td>
                                              <td>
                                                  &nbsp;</td>
                                              <td>
                                                  Employer&#39;s Contribution</td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  <asp:TextBox ID="txtempesic" runat="server" CssClass="form-control"></asp:TextBox>
                                              </td>
                                              <td>
                                                  &nbsp;</td>
                                              <td>
                                                  <asp:TextBox ID="txtemplresic" runat="server" CssClass="form-control"></asp:TextBox>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  &nbsp;</td>
                                              <td>
                                                  &nbsp;</td>
                                              <td>
                                                  &nbsp;</td>
                                          </tr>
                                          </table>
                                      </td>
                                     </tr>
                                              <tr>
                                                  <td>
                                                      <asp:Button ID="cmdsave" runat="server" class="btn btn-primary" Text="Submit" />
                                                      <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                          class="btn btn-default" Text="Reset" />
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtmode" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtdivsl" runat="server" Height="22px" Visible="False" 
                                                          Width="20px"></asp:TextBox>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>       
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

