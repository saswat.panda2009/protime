﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient
Imports System.Drawing

Partial Class reports_absentee
    Inherits System.Web.UI.Page

    <System.Web.Script.Services.ScriptMethod(), _
System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            "staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblmsg.Visible = False
        txtfrmdt.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        txttodt.Text = Format(Now, "dd/MM/yyyy")
        Me.divisiondisp()
        Me.deptdisp()
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
        txtdept.Text = "All Departments"
    End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub deptdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds As DataSet = get_dataset("select distinct dept_nm from dept_mst WHERE loc_cd=" & loc_cd & " order by dept_nm")
        dvdept.DataSource = ds.Tables(0)
        dvdept.DataBind()
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If
        txtdept.Text = ""
        For i As Integer = 0 To dvdept.Rows.Count - 1
            Dim chk As CheckBox = dvdept.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                Dim lbl As Label = dvdept.Rows(i).FindControl("lbldept")
                txtdept.Text = txtdept.Text & "'" & lbl.Text & "',"
            End If
        Next
        If txtdept.Text <> "" Then
            txtdept.Text = vb.Left(txtdept.Text, txtdept.Text.Length - 1)
            str = str & "AND dept_nm IN (" & txtdept.Text & ")"
        Else
            txtdept.Text = "All Departments"
        End If
        If Trim(txtempcode.Text) <> "" Then
            str = ""
            str = "AND atnd.emp_code='" & Trim(txtempcode.Text) & "'"
        End If
        Dim ds1 As DataSet = get_dataset("SELECT convert(varchar,atnd.log_dt,103) AS Date,division_mst.div_nm as 'Division', atnd.staf_nm AS [Staf Name], atnd.emp_code AS [Emp. Code], atnd.device_code AS [Device Code], atnd.dept_nm AS Dept, atnd.desg_nm AS Desg FROM division_mst RIGHT OUTER JOIN staf ON division_mst.div_sl = staf.div_sl RIGHT OUTER JOIN atnd ON staf.staf_sl = atnd.staf_sl WHERE staf.emp_status='I' AND log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND (day_status='ABSENT') AND atnd.loc_cd= " & loc_cd & "  " & str & "  ORDER BY log_dt,div_nm")
        If ds1.Tables(0).Rows.Count <> 0 Then
            dvdata.DataSource = ds1.Tables(0)
            dvdata.DataBind()
        Else
            dvdata.DataSource = ds1.Tables(0)
            dvdata.DataBind()
            lblmsg.Text = "No Record Found."
        End If

    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub

    Protected Sub txtempcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtempcode.TextChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If Trim(txtempcode.Text) <> "" Then
            Dim emp_code As String() = Trim(txtempcode.Text).Split("-")
            txtempcode.Text = emp_code(0)
            Dim ds1 As DataSet = get_dataset("SELECT staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm, location_mst.loc_nm, staf.loc_cd FROM location_mst RIGHT OUTER JOIN staf ON location_mst.loc_cd = staf.loc_cd LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            Else
                txtempcode.Text = ""
            End If
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If
        txtdept.Text = ""
        For i As Integer = 0 To dvdept.Rows.Count - 1
            Dim chk As CheckBox = dvdept.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                Dim lbl As Label = dvdept.Rows(i).FindControl("lbldept")
                txtdept.Text = txtdept.Text & "'" & lbl.Text & "',"
            End If
        Next
        If txtdept.Text <> "" Then
            txtdept.Text = vb.Left(txtdept.Text, txtdept.Text.Length - 1)
            str = str & "AND dept_nm IN (" & txtdept.Text & ")"
        Else
            txtdept.Text = "All Departments"
        End If
        If Trim(txtempcode.Text) <> "" Then
            str = ""
            str = "AND atnd.emp_code='" & Trim(txtempcode.Text) & "'"
        End If
        Dim ds1 As DataSet = get_dataset("SELECT convert(varchar,atnd.log_dt,103) AS Date,division_mst.div_nm as 'Division', atnd.staf_nm AS [Staf Name], atnd.emp_code AS [Emp. Code], atnd.device_code AS [Device Code], atnd.dept_nm AS Dept, atnd.desg_nm AS Desg FROM division_mst RIGHT OUTER JOIN staf ON division_mst.div_sl = staf.div_sl RIGHT OUTER JOIN atnd ON staf.staf_sl = atnd.staf_sl WHERE staf.emp_status='I' AND log_dt >= '" & Format(stringtodate(txtfrmdt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txttodt.Text), "dd/MMM/yyyy") & "' AND (day_status='ABSENT') AND atnd.loc_cd= " & loc_cd & "  " & str & "  ORDER BY log_dt,div_nm")
        Dim dtcust As New DataTable
        dtcust = ds1.Tables(0)



        'Create a dummy GridView

        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.DataSource = dtcust
        GridView1.DataBind()

        Dim comp_nm As String = ""
        Dim ds As DataSet = get_dataset("SELECT comp_nm FROM company")
        If ds.Tables(0).Rows.Count <> 0 Then
            comp_nm = ds.Tables(0).Rows(0).Item("comp_nm")
        End If

        Dim row As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal)
        Dim cell As New TableHeaderCell()
        cell.Text = comp_nm & "<br/><br/>"
        Dim cnt As Integer = ds1.Tables(0).Columns.Count
        cell.ColumnSpan = cnt
        row.Controls.Add(cell)
        row.Font.Bold = True
        row.Font.Size = 16
        'row.BackColor = ColorTranslator.FromHtml("#3AC0F2")
        row.HorizontalAlign = HorizontalAlign.Center
        GridView1.HeaderRow.Parent.Controls.AddAt(0, row)

        Dim row1 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal)
        Dim cell1 As New TableHeaderCell()
        cell1.Text = "Absentee Register For Date : " & txtfrmdt.Text & " To Date : " & txttodt.Text
        cell1.ColumnSpan = cnt
        row1.Controls.Add(cell1)
        row1.Font.Bold = True
        row1.Font.Size = 16
        'row.BackColor = ColorTranslator.FromHtml("#3AC0F2")
        row1.HorizontalAlign = HorizontalAlign.Center
        GridView1.HeaderRow.Parent.Controls.AddAt(1, row1)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=AbsenteeRegister.xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Using sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)

            GridView1.HeaderRow.BackColor = Color.White
            For Each cell2 As TableCell In GridView1.HeaderRow.Cells
                cell2.BackColor = ColorTranslator.FromHtml("#3AC0F2")
            Next
            GridView1.RenderControl(hw)
            'style to format numbers to string
            Dim style As String = "<style> .textmode { } </style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.[End]()
        End Using
    End Sub
End Class
