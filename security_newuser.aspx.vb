﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class company_division
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Dim lvl As String = CType(Session("lvl"), String)
            If lvl <> "A" Then
                Response.Redirect("invalid.aspx")
            End If
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtpswd.Text = ""
        txtusrnm.Text = ""
        txtusrsl.Text = ""
        txtusername.Text = ""
        cmbactive.SelectedIndex = 1
        cmblvl.SelectedIndex = 0
        If txtmode.Text = "E" Then
            divadd.Visible = True
            divview.Visible = False
            lblhdr.Text = "User Creation (Entry Mode)"
            txtusrnm.Focus()
        ElseIf txtmode.Text = "M" Then
            divadd.Visible = True
            divview.Visible = False
            lblhdr.Text = "User Creation (Edit Mode)"
            txtusrnm.Focus()
        ElseIf txtmode.Text = "V" Then
            divadd.Visible = False
            divview.Visible = True
            lblhdr.Text = "User Creation (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT ROW_NUMBER() OVER(ORDER BY usr_name) as 'sl',usr_name,convert(varchar,last_login,113) as 'Last Login',(CASE WHEN lvl='A' THEN 'ADMINISTATOR' WHEN lvl='M' THEN 'MANAGER' WHEN lvl='O' THEN 'OPERATOR' end) AS 'lvl',(CASE WHEN block='N' THEN 'No' WHEN block='Y' THEN 'Yes' END) as blck FROM usr_login WHERE loc_cd=" & loc_cd & "  ORDER BY usr_name")
        dv.DataSource = ds1.Tables(0)
        dv.DataBind()
    End Sub

    Protected Sub dv_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dv.PageIndexChanging
        dv.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub dv_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dv.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT usr_sl FROM usr_login WHERE usr_name='" & Trim(dv.Rows(rw).Cells(1).Text) & "' AND loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtmode.Text = "E"
                Me.clr()
                txtusrsl.Text = Val(ds1.Tables(0).Rows(0).Item("usr_sl"))
                Me.dvsel()
            End If
        End If
    End Sub

    Private Sub dvsel()
        Dim ds As DataSet = get_dataset("SELECT * FROM usr_login WHERE usr_sl=" & Val(txtusrsl.Text) & "")
        If ds.Tables(0).Rows.Count <> 0 Then
            txtpswd.Text = ds.Tables(0).Rows(0).Item("usr_pwd")
            txtusrnm.Text = ds.Tables(0).Rows(0).Item("usr_name")
            If ds.Tables(0).Rows(0).Item("lvl") = "A" Then
                cmblvl.SelectedIndex = 0
            ElseIf ds.Tables(0).Rows(0).Item("lvl") = "M" Then
                cmblvl.SelectedIndex = 1
            Else
                cmblvl.SelectedIndex = 2
            End If
            txtusername.Text = ds.Tables(0).Rows(0).Item("name")
            If ds.Tables(0).Rows(0).Item("block") = "Y" Then
                cmbactive.SelectedIndex = 0
            Else
                cmbactive.SelectedIndex = 1
            End If
        End If
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If txtmode.Text = "E" Then
            Dim dscheck As DataSet = get_dataset("SELECT usr_name FROM usr_login WHERE usr_name='" & UCase(Trim(txtusrnm.Text)) & "'")
            If dscheck.Tables(0).Rows.Count <> 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('User Name Already Exits');", True)
                txtusrnm.Focus()
                Exit Sub
            End If
            txtusrsl.Text = "1"
            Dim ds1 As DataSet = get_dataset("SELECT max(usr_sl) FROM usr_login")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                txtusrsl.Text = ds1.Tables(0).Rows(0).Item(0) + 1
            End If
            start1()
            SQLInsert("INSERT INTO usr_login(usr_sl,usr_name,usr_pwd,last_login,block,lvl,name,loc_cd) VALUES(" & Val(txtusrsl.Text) & _
            ",'" & UCase(Trim(txtusrnm.Text)) & "','" & Trim(txtpswd.Text) & "','" & Now & "','" & _
            vb.Left(Trim(cmbactive.Text), 1) & "','" & vb.Left(Trim(cmblvl.Text), 1) & "','" & UCase(Trim(txtusername.Text)) & "'," & loc_cd & ")")
            close1()
            Me.clr()
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
        ElseIf txtmode.Text = "M" Then
            Dim ds As DataSet = get_dataset("SELECT usr_name FROM emp WHERE usr_sl=" & Val(txtusrsl.Text) & "")
            If ds.Tables(0).Rows.Count <> 0 Then
                If Trim(txtusrnm.Text) <> ds.Tables(0).Rows(0).Item("usr_name") Then
                    Dim dscheck As DataSet = get_dataset("SELECT usr_name FROM usr_login WHERE usr_name='" & UCase(Trim(txtusrnm.Text)) & "'")
                    If dscheck.Tables(0).Rows.Count <> 0 Then
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('User Name Already Exits');", True)
                        txtusrnm.Focus()
                        Exit Sub
                    End If
                End If
                start1()
                SQLInsert("UPDATE usr_login SET usr_name='" & UCase(Trim(txtusrnm.Text)) & "',usr_pwd='" & Trim(txtpswd.Text) & _
                "',block='" & vb.Left(Trim(cmbactive.Text), 1) & "',lvl='" & vb.Left(Trim(cmblvl.Text), 1) & "',name='" & UCase(Trim(txtusername.Text)) & "' WHERE usr_sl=" & _
                Val(txtusrsl.Text) & "")
                close1()
                Me.clr()
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Modified Succesffuly');", True)
            End If
        End If

    End Sub

    Protected Sub cmdclr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclr.Click
        Me.clr()
    End Sub
End Class
