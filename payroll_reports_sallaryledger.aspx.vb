﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class payroll_reports_monthlyabs
    Inherits System.Web.UI.Page

    <System.Web.Script.Services.ScriptMethod(), _
System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            "staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblmsg.Visible = False
        txtfrmdt.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        Me.divisiondisp()
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
    End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)

        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = str & "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If

        txtfrmdt.Text = "01/" & Format(stringtodate(txtfrmdt.Text).Month, "00") & "/" & Format(stringtodate(txtfrmdt.Text).Year, "0000")
        Dim tot_days As Integer = DateTime.DaysInMonth(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month)
        txtto.Text = Format(tot_days, "00") & "/" & Format(stringtodate(txtfrmdt.Text).Month, "00") & "/" & Format(stringtodate(txtfrmdt.Text).Year, "0000")

        Dim dsstaf As DataSet = get_dataset("SELECT staf_sl,staf.staf_nm, staf.emp_code, staf.device_code, division_mst.div_nm, dept_mst.dept_nm,(CASE WHEN pay_mode=1 THEN 'Bank Transfer' WHEN pay_mode=2 THEN 'Cheque/DD' WHEN pay_mode=3 THEN 'Cash' end) as mode,bank_name,ac_no,ifsc_code FROM staf LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl WHERE staf.emp_status='I'  AND staf.loc_cd= " & loc_cd & "  " & str & "")
        Dim dsmonthly As DataSet = get_dataset("SELECT * FROM monthly_pay WHERE pay_month=" & stringtodate(txtfrmdt.Text).Month & " AND pay_year=" & stringtodate(txtfrmdt.Text).Year & "")
        Dim dt As New DataTable
        dt.Columns.Add("Sl.", GetType(String))
        dt.Columns.Add("Staf Name", GetType(String))
        dt.Columns.Add("Emp. Code", GetType(String))
        dt.Columns.Add("Division", GetType(String))
        dt.Columns.Add("Department", GetType(String))
        dt.Columns.Add("Pay Mode", GetType(String))
        dt.Columns.Add("Bank Name", GetType(String))
        dt.Columns.Add("Account No", GetType(String))
        dt.Columns.Add("IFSC Code", GetType(String))
        dt.Columns.Add("Pay Days", GetType(String))
        dt.Columns.Add("Total Earnings", GetType(String))
        dt.Columns.Add("Total Deduction", GetType(String))
        dt.Columns.Add("Net Total", GetType(String))
        Dim tot_amount As Decimal = 0
        For i As Integer = 0 To dsstaf.Tables(0).Rows.Count - 1
            Dim tot_p As Integer = 0
            Dim tot_a As Integer = 0
            Dim tot_w As Integer = 0
            Dim tot_l As Integer = 0
            Dim tot_h As Integer = 0
            Dim tot_hr As Integer = 0

            Dim current1 As New DateTime(stringtodate(txtfrmdt.Text).Year, stringtodate(txtfrmdt.Text).Month, stringtodate(txtfrmdt.Text).Day)
            Dim ending1 As New DateTime(stringtodate(txtto.Text).Year, stringtodate(txtto.Text).Month, stringtodate(txtto.Text).Day)

            Dim dr As DataRow
            dr = dt.NewRow
            dr(0) = i + 1
            dr(1) = Trim(dsstaf.Tables(0).Rows(i).Item("staf_nm"))
            dr(2) = Trim(dsstaf.Tables(0).Rows(i).Item("emp_code"))
            dr(3) = Trim(dsstaf.Tables(0).Rows(i).Item("div_nm"))
            dr(4) = Trim(dsstaf.Tables(0).Rows(i).Item("dept_nm"))
            dr(5) = Trim(dsstaf.Tables(0).Rows(i).Item("mode"))
            dr(6) = Trim(dsstaf.Tables(0).Rows(i).Item("bank_name"))
            dr(7) = "'" & Trim(dsstaf.Tables(0).Rows(i).Item("ac_no"))
            dr(8) = Trim(dsstaf.Tables(0).Rows(i).Item("ifsc_code"))
            Dim drpay As DataRow()
            drpay = dsmonthly.Tables(0).Select("staf_sl=" & dsstaf.Tables(0).Rows(i).Item("staf_sl") & "")
            If drpay.Length <> 0 Then
                dr("Pay Days") = drpay(0).Item("pay_days")
                dr("Total Earnings") = Format(Val(drpay(0).Item("tot_earnings")), "#####0.00")
                dr("Total Deduction") = Format(Val(drpay(0).Item("tot_deduction")), "#####0.00")
                dr("Net Total") = Format(Val(drpay(0).Item("total")), "#####0.00")
                tot_amount = tot_amount + Val(drpay(0).Item("total"))
            End If
            dt.Rows.Add(dr)
        Next
        dt.Rows.Add("", "", "", "", "", "", "", "", "", "", "", "Total : ", Format(Val(tot_amount), "#####0.00"))
        If dt.Rows.Count <> 0 Then
            Dim GridView112 As New GridView()
            GridView112.AllowPaging = False
            GridView112.DataSource = dt
            GridView112.DataBind()
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=MonthlyPaymentRegister.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            For i As Integer = 0 To GridView112.Rows.Count - 1
                GridView112.Rows(i).Attributes.Add("class", "textmode")
            Next
            GridView112.RenderControl(hw)
            'style to format numbers to string
            Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()
        End If
    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub
End Class
