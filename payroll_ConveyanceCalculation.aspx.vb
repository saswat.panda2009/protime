﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class payroll_ConveyanceCalculation
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtfrom.Text = Format(Now, "dd/MM/yyyy")
        divmsg.Visible = False
        lblhdr.Text = "Processing Conveyance"
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim sdt As Date = CDate(stringtodate(txtfrom.Text).Month & "/01/" & stringtodate(txtfrom.Text).Year)
        Dim edt As Date = CDate(stringtodate(txtfrom.Text).Month & "/" & DateTime.DaysInMonth(stringtodate(txtfrom.Text).Year, stringtodate(txtfrom.Text).Month) & "/" & stringtodate(txtfrom.Text).Year)
        Dim dsstaf As DataSet = get_dataset("SELECT payroll_group.*, staf.* FROM staf LEFT OUTER JOIN payroll_group ON staf.group_sl = payroll_group.group_sl WHERE emp_status='I' AND staf.group_sl <> 0 and staf.loc_cd=" & loc_cd & " AND allow_conveyances=1")
        Dim dsatnd As DataSet = get_dataset("select log_dt,staf_sl,log_tp,log_time from elog WHERE log_dt>='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_dt<='" & Format(edt, "dd/MMM/yyyy") & "' ORDER  BY log_dt,log_time")
        Dim dsconveyance As DataSet = get_dataset("SELECT log_dt AS Date, log_longitude AS longitude, log_lattitude AS lattitude, log_location AS Location,log_time as time,staf_sl, 1 as tp FROM manual_posting WHERE log_status='A' and log_dt>='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_dt<='" & Format(edt, "dd/MMM/yyyy") & "'  UNION select visit_dt  AS Date,visit_longitude AS longitude,visit_lattitude AS lattitude,visit_location AS Location,visit_stime as time,staf_sl,2 as tp from staf_daily_visit WHERE visit_dt>='" & Format(sdt, "dd/MMM/yyyy") & "' AND visit_dt<='" & Format(edt, "dd/MMM/yyyy") & "'    ORDER BY Date,time ")
        Dim dspay As DataSet = get_dataset("select top 1 staf_sl, loc_cd, allow_dt, start_km, stop_km, tot_km, rate, allow_amount, all_month, all_year, type, start_location, end_location FROM fuel_allowance")
        Dim dsdiv As DataSet = get_dataset("SELECT * FROM division_mst WHERE loc_cd=" & loc_cd & "")
        dspay.Tables(0).Rows.Clear()
        start1()
        SQLInsert("DELETE FROM fuel_allowance WHERE all_month=" & stringtodate(txtfrom.Text).Month & " AND all_year=" & stringtodate(txtfrom.Text).Year & " AND loc_cd=" & loc_cd & "")
        close1()
        Dim dt_atnd As DataTable = dspay.Tables(0)
        Dim dr_atnd As DataRow
        For i As Integer = 0 To dsstaf.Tables(0).Rows.Count - 1
            Dim staf_sl As Integer = 0
            Dim div_sl As Integer = 0
            Dim rate_per_km As Decimal = 0
            sdt = CDate(stringtodate(txtfrom.Text).Month & "/01/" & stringtodate(txtfrom.Text).Year)
            edt = CDate(stringtodate(txtfrom.Text).Month & "/" & DateTime.DaysInMonth(stringtodate(txtfrom.Text).Year, stringtodate(txtfrom.Text).Month) & "/" & stringtodate(txtfrom.Text).Year)

            staf_sl = dsstaf.Tables(0).Rows(i).Item("staf_sl")
            div_sl = dsstaf.Tables(0).Rows(i).Item("div_sl")
            rate_per_km = dsstaf.Tables(0).Rows(i).Item("rate_per_km")

            Do While sdt <= edt
                Dim startlong As Double = 0
                Dim startlattitude As Double = 0
                Dim startlocation As String = ""
                Dim started As Integer = 0
                Dim drfx_conveyance_datarow() As DataRow
                drfx_conveyance_datarow = dsconveyance.Tables(0).Select("staf_sl=" & staf_sl & " and Date='" & Format(sdt, "dd/MMM/yyyy") & "'", "time asc")
                If drfx_conveyance_datarow.Length <> 0 Then
                    Dim drfx_elod_datarow() As DataRow
                    drfx_elod_datarow = dsatnd.Tables(0).Select("staf_sl=" & staf_sl & " and log_dt='" & Format(sdt, "dd/MMM/yyyy") & "'", "log_time  ASC")
                    If drfx_elod_datarow.Length <> 0 Then
                        If drfx_elod_datarow(0).Item("log_tp") = "P" Then
                            Dim drfx_div_datarow() As DataRow
                            drfx_div_datarow = dsdiv.Tables(0).Select("div_sl=" & div_sl & " ")
                            If drfx_div_datarow.Length <> 0 Then
                                startlong = drfx_div_datarow(0).Item("longitude")
                                startlattitude = drfx_div_datarow(0).Item("lattitude")
                                startlocation = "Office"
                            End If
                            started = 1
                        End If
                    End If
                    For j As Integer = 0 To drfx_conveyance_datarow.Length - 1
                        Dim longitude As Double
                        Dim lattitude As Double
                        Dim location As String = ""
                        Dim totdistance As Double = 0
                        If started = 1 Then
                            longitude = drfx_conveyance_datarow(j).Item("longitude")
                            lattitude = drfx_conveyance_datarow(j).Item("lattitude")
                            location = drfx_conveyance_datarow(j).Item("Location")
                            totdistance = distance(startlattitude, startlong, lattitude, longitude, "K")
                            If totdistance <> 0 Then
                                dr_atnd = dt_atnd.NewRow
                                dr_atnd("staf_sl") = staf_sl
                                dr_atnd("all_month") = stringtodate(txtfrom.Text).Month
                                dr_atnd("all_year") = stringtodate(txtfrom.Text).Year
                                dr_atnd("loc_cd") = loc_cd
                                dr_atnd("allow_dt") = Format(sdt, "dd/MMM/yyyy")
                                dr_atnd("start_km") = 0
                                dr_atnd("stop_km") = 0
                                dr_atnd("tot_km") = totdistance
                                dr_atnd("rate") = rate_per_km
                                dr_atnd("allow_amount") = rate_per_km * totdistance
                                dr_atnd("type") = "Calculation"
                                dr_atnd("start_location") = startlocation
                                dr_atnd("end_location") = location
                                dt_atnd.Rows.Add(dr_atnd)
                            End If
                            startlong = drfx_conveyance_datarow(j).Item("longitude")
                            startlattitude = drfx_conveyance_datarow(j).Item("lattitude")
                            startlocation = drfx_conveyance_datarow(j).Item("Location")
                        Else
                            started = 1
                            startlong = drfx_conveyance_datarow(j).Item("longitude")
                            startlattitude = drfx_conveyance_datarow(j).Item("lattitude")
                            startlocation = drfx_conveyance_datarow(j).Item("Location")
                        End If

                    Next

                    Dim drfx_elod_datarow1() As DataRow
                    drfx_elod_datarow1 = dsatnd.Tables(0).Select("staf_sl=" & staf_sl & " and log_dt='" & Format(sdt, "dd/MMM/yyyy") & "'", "log_time  DESC")
                    If drfx_elod_datarow.Length <> 0 Then
                        Dim longitude As Double
                        Dim lattitude As Double
                        Dim location As String = ""
                        Dim totdistance As Double = 0
                        If drfx_elod_datarow(0).Item("log_tp") = "P" Then
                            Dim drfx_div_datarow() As DataRow
                            drfx_div_datarow = dsdiv.Tables(0).Select("div_sl=" & div_sl & " ")
                            If drfx_div_datarow.Length <> 0 Then
                                longitude = drfx_div_datarow(0).Item("longitude")
                                lattitude = drfx_div_datarow(0).Item("lattitude")
                                location = "Office"
                            End If
                            totdistance = distance(startlattitude, startlong, lattitude, longitude, "K")
                            If totdistance <> 0 Then
                                dr_atnd = dt_atnd.NewRow
                                dr_atnd("staf_sl") = staf_sl
                                dr_atnd("all_month") = stringtodate(txtfrom.Text).Month
                                dr_atnd("all_year") = stringtodate(txtfrom.Text).Year
                                dr_atnd("loc_cd") = loc_cd
                                dr_atnd("allow_dt") = Format(sdt, "dd/MMM/yyyy")
                                dr_atnd("start_km") = 0
                                dr_atnd("stop_km") = 0
                                dr_atnd("tot_km") = totdistance
                                dr_atnd("rate") = rate_per_km
                                dr_atnd("allow_amount") = rate_per_km * totdistance
                                dr_atnd("type") = "Calculation"
                                dr_atnd("start_location") = startlocation
                                dr_atnd("end_location") = location
                                dt_atnd.Rows.Add(dr_atnd)
                            End If
                        End If
                    End If
                End If
                sdt = sdt.AddDays(1)
            Loop
        Next
        start1()
        For cnt As Integer = 0 To dt_atnd.Rows.Count - 1
            SQLInsert("INSERT INTO fuel_allowance(staf_sl, loc_cd, allow_dt, start_km, stop_km, tot_km, rate, allow_amount, all_month, all_year, type, start_location," & _
            " end_location) VALUES(" & dt_atnd.Rows(cnt).Item("staf_sl") & "," & dt_atnd.Rows(cnt).Item("loc_cd") & ",'" & dt_atnd.Rows(cnt).Item("allow_dt") & "',0,0," & _
            dt_atnd.Rows(cnt).Item("tot_km") & "," & dt_atnd.Rows(cnt).Item("rate") & "," & dt_atnd.Rows(cnt).Item("allow_amount") & "," & dt_atnd.Rows(cnt).Item("all_month") & _
            "," & dt_atnd.Rows(cnt).Item("all_year") & ",'Calculation','" & dt_atnd.Rows(cnt).Item("start_location") & "','" & dt_atnd.Rows(cnt).Item("end_location") & "')")
        Next
        close1()
        divmsg.Visible = True
        divmsg.Attributes("class") = "alert bg-success"
        lblmsg.Text = "Conveyance Processed Successfully"
    End Sub
End Class
