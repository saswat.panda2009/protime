﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="payroll_import.aspx.vb" Inherits="payroll_import" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                 
                </div>

                     <div class="card-body">
                                         <table style="width:100%;">
                                <tr>
                                    <td width="60%">
                                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                                    </td>
                                    <td width="40%">
                                        &nbsp;
                                        <asp:Button ID="cmdget" runat="server" CausesValidation="False" 
                                            class="btn btn-success" Text="Import" />
                                    &nbsp;
                                                  <asp:Button ID="Button2" runat="server" CausesValidation="False" 
                                            class="btn btn-info" Text="Sample File" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="45%">
                                        &nbsp;</td>
                                    <td width="10%">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="45%" colspan="2">
                                        &nbsp;</td>
                                </tr>
                                </table>           
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

