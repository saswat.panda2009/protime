﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="master_devices.aspx.vb" Inherits="master_devices" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="A1" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                      aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="master_devices.aspx">Add New Devices</a>
                      <a class="dropdown-item" href="master_devices.aspx?mode=V">View Devices List</a>
                    
                    </div>
                  </div>
                </div>

                     <div class="card-body">
                                 <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnlview" runat="server">
                                          <table style="width: 100%;">
                                                                                          <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel2" runat="server">
                                                          <div style="width: 100%; height: 600px">
                                                              <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="15" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <Columns>
                                                                      <asp:BoundField DataField="sl" HeaderText="Sl">
                                                                      <HeaderStyle Width="30px" />
                                                                      <ItemStyle Width="30px" />
                                                                      </asp:BoundField>
                                                                      <asp:BoundField DataField="DeviceFName" HeaderText="Device Name" >
                                                                      <HeaderStyle HorizontalAlign="Left" />
                                                                      </asp:BoundField>
                                                                      <asp:BoundField DataField="SerialNumber" HeaderText="Serial No" />
                                                                      <asp:BoundField DataField="DeviceDirection" HeaderText="Device Direction" />
                                                                      <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                                                          ImageUrl="images/edit.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                                                      <ItemStyle Height="30px" Width="30px" />
                                                                      </asp:ButtonField>
                                                                  </Columns>
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>
                                                          </div>
                                                      </asp:Panel>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table width="100%">
                                                                                      <tr>
                                                  <td>
                                                      Device</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtstate" runat="server" class="form-control"></asp:TextBox>
                                                      <asp:FilteredTextBoxExtender ID="txtstate_FilteredTextBoxExtender" 
                                                          runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                          TargetControlID="txtstate">
                                                      </asp:FilteredTextBoxExtender>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td height="8" style="font-size: 8px">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <table style="width: 100%;">
                                                          <tr>
                                                              <td width="45%">
                                                                  Serial No</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  Device IP</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtsrl" runat="server" class="form-control"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtsrl_FilteredTextBoxExtender" runat="server" 
                                                                      Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtsrl">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtip" runat="server" class="form-control"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtip_FilteredTextBoxExtender" runat="server" 
                                                                      Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtip">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Device Direction</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  Active</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                 <asp:DropDownList ID="cmbdevices" runat="server" class="form-control" 
                                                                    >
                                                                      <asp:ListItem Value="IN">IN</asp:ListItem>
                                                                      <asp:ListItem Value="OUT">OUT</asp:ListItem>
                                                                      <asp:ListItem>Altinout</asp:ListItem>
                                                                      <asp:ListItem>Canteen</asp:ListItem>
                                                                  </asp:DropDownList>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:DropDownList ID="cmbactive" runat="server" class="form-control" 
                                                                      Width="100px">
                                                                      <asp:ListItem Value="Yes"></asp:ListItem>
                                                                      <asp:ListItem Value="No"></asp:ListItem>
                                                                  </asp:DropDownList>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td style="font-size: 8px">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:Button ID="cmdsave" runat="server" class="btn btn-primary" Text="Submit" />
                                                      <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                          class="btn btn-default" Text="Reset" />
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtmode" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtstatecd" runat="server" Height="22px" Visible="False" 
                                                          Width="20px"></asp:TextBox>
                                                      <asp:RequiredFieldValidator ID="valid_state" runat="server" 
                                                          ControlToValidate="txtstate" Display="None" 
                                                          ErrorMessage="&lt;b&gt;Required Field&lt;/b&gt;&lt;br/&gt;Please Provide The Device Name." 
                                                          SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                      <asp:ValidatorCalloutExtender ID="valid_state_ValidatorCalloutExtender" 
                                                          runat="server" Enabled="True" PopupPosition="BottomLeft" 
                                                          TargetControlID="valid_state">
                                                      </asp:ValidatorCalloutExtender>
                                                      <asp:RequiredFieldValidator ID="valid_state0" runat="server" 
                                                          ControlToValidate="txtsrl" Display="None" 
                                                          ErrorMessage="&lt;b&gt;Required Field&lt;/b&gt;&lt;br/&gt;Please Provide The Device Serial No." 
                                                          SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                      <asp:ValidatorCalloutExtender ID="valid_state0_ValidatorCalloutExtender" 
                                                          runat="server" Enabled="True" PopupPosition="BottomLeft" 
                                                          TargetControlID="valid_state0">
                                                      </asp:ValidatorCalloutExtender>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>  
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

