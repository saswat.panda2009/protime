﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.Net.Mail

Partial Class transcation_outdoor_approve
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            pnlview.Visible = True
            Me.clr()
            Me.dvdisp()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        Me.dvdisp()
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT  slno,Convert(varchar,outddor_posting.outdoor_dt,103) as dt, division_mst.div_nm, staf.device_code,staf.staf_sl, staf.emp_code, staf.staf_nm, Convert(varchar,outddor_posting.in_time,108) as in_time, Convert(varchar,outddor_posting.out_time,108) as out_time, outddor_posting.reason, post_type as tp FROM outddor_posting LEFT OUTER JOIN staf ON outddor_posting.staf_sl = staf.staf_sl LEFT OUTER JOIN division_mst ON staf.div_sl = division_mst.div_sl WHERE outddor_posting.loc_cd=" & loc_cd & " AND status='P' ORDER BY outdoor_dt,staf_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Private Sub email_send(ByVal mail_to, ByVal mail_sub, ByVal mail_msg)
        Dim SmtpServer As New SmtpClient()
        Dim mail As New MailMessage()
        SmtpServer.Credentials = New Net.NetworkCredential("protime.attendance@gmail.com", "14042016")
        SmtpServer.Port = 587
        SmtpServer.Host = "smtp.gmail.com"
        SmtpServer.EnableSsl = True
        mail = New MailMessage()
        mail.From = New MailAddress("protime.attendance@gmail.com")
        mail.To.Add(mail_to)
        mail.Subject = mail_sub
        mail.IsBodyHtml = True
        mail.Body = mail_msg
        SmtpServer.Send(mail)
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim lbl As Label = GridView1.Rows(rw).FindControl("lblslno")
            Dim lbldevice As Label = GridView1.Rows(rw).FindControl("lbldevicecode")
            Dim lblstafsl As Label = GridView1.Rows(rw).FindControl("lblstafsl")
            Dim lblvdt As Label = GridView1.Rows(rw).FindControl("lblvdt")
            Dim lblintime As Label = GridView1.Rows(rw).FindControl("lblintime")
            Dim lbloutTime As Label = GridView1.Rows(rw).FindControl("lbloutTime")
            Dim lbltype As Label = GridView1.Rows(rw).FindControl("lbltype")
            start1()
            SQLInsert("UPDATE outddor_posting SET status='A' WHERE slno=" & lbl.Text & "")


            If lbltype.Text = "Both" Then
                SQLInsert("INSERT INTO elog(device_code,log_dt,log_time,log_tp,slno,read_mark,device_no,loc_cd,staf_sl,view_report,devicedirection) VALUES('" & Trim(lbldevice.Text) & _
                "','" & Format(stringtodate(lblvdt.Text), "dd/MMM/yyyy") & "','" & lblintime.Text & "','M'," & lbl.Text & ",'N',0," & loc_cd & "," & lblstafsl.Text & ",'Y','IN')")

                SQLInsert("INSERT INTO elog(device_code,log_dt,log_time,log_tp,slno,read_mark,device_no,loc_cd,staf_sl,view_report,devicedirection) VALUES('" & Trim(lbldevice.Text) & _
                "','" & Format(stringtodate(lblvdt.Text), "dd/MMM/yyyy") & "','" & lbloutTime.Text & "','M'," & lbl.Text & ",'N',0," & loc_cd & "," & lblstafsl.Text & ",'Y','OUT')")
            ElseIf lbltype.Text = "In" Then
                SQLInsert("INSERT INTO elog(device_code,log_dt,log_time,log_tp,slno,read_mark,device_no,loc_cd,staf_sl,view_report,devicedirection) VALUES('" & Trim(lbldevice.Text) & _
                "','" & Format(stringtodate(lblvdt.Text), "dd/MMM/yyyy") & "','" & lblintime.Text & "','M'," & lbl.Text & ",'N',0," & loc_cd & "," & lblstafsl.Text & ",'Y','IN')")
            ElseIf lbltype.Text = "Out" Then
                SQLInsert("INSERT INTO elog(device_code,log_dt,log_time,log_tp,slno,read_mark,device_no,loc_cd,staf_sl,view_report,devicedirection) VALUES('" & Trim(lbldevice.Text) & _
                "','" & Format(stringtodate(lblvdt.Text), "dd/MMM/yyyy") & "','" & lbloutTime.Text & "','M'," & lbl.Text & ",'N',0," & loc_cd & "," & lblstafsl.Text & ",'Y','OUT')")
            End If

            SQLInsert("INSERT INTO staf_notification(staf_sl,notice_hdr,notice_details,notification_view,not_dt,not_time) VALUES(" & Val(lblstafsl.Text) & _
          ",'OutDoor Posting','Your Request For Outdoor On Date : " & Trim(lblvdt.Text) & "  Has Been Approved.','N','" & Format(Now, "dd/MMM/yyyy") & "','" & Format(Now, "HH:mm") & "')")
            close1()
        ElseIf e.CommandName = "cancel_state" Then
            Dim lbl As Label = GridView1.Rows(rw).FindControl("lblslno")
            Dim lblstafsl As Label = GridView1.Rows(rw).FindControl("lblstafsl")
            Dim lblvdt As Label = GridView1.Rows(rw).FindControl("lblvdt")
            start1()
            SQLInsert("UPDATE outddor_posting SET status='C' WHERE slno=" & lbl.Text & "")
            SQLInsert("INSERT INTO staf_notification(staf_sl,notice_hdr,notice_details,notification_view,not_dt,not_time) VALUES(" & Val(lblstafsl.Text) & _
          ",'OutDoor Posting','Your Request For Outdoor On Date : " & Trim(lblvdt.Text) & "  Has Been Canceled.','N','" & Format(Now, "dd/MMM/yyyy") & "','" & Format(Now, "HH:mm") & "')")
            close1()
        End If
        Me.dvdisp()
    End Sub
End Class
