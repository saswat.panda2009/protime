﻿Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Public Class crypto
    Private Shared des As New TripleDESCryptoServiceProvider
    Private Shared md5 As New MD5CryptoServiceProvider

    Public Shared Function MD5Hash(ByVal value As String) As Byte()
        Return md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(value))
    End Function

    Public Shared Function Encrpt(ByVal str As String, ByVal key As String) As String
        des.Key = crypto.MD5Hash(key)
        des.Mode = CipherMode.ECB
        Dim buffer As Byte() = ASCIIEncoding.ASCII.GetBytes(str)
        Return Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length))
    End Function

    Public Shared Function decrpt(ByVal str As String, ByVal key As String) As String
        des.Key = crypto.MD5Hash(key)
        des.Mode = CipherMode.ECB
        Dim buffer As Byte() = Convert.FromBase64String(str)
        Return ASCIIEncoding.ASCII.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length))
    End Function
End Class
