﻿Imports System.Data

Partial Class dashboard
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
            Me.Process_attendance()
            Dim ds1 As DataSet = get_dataset("SELECT count(div_sl) FROM division_mst WHERE loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                lbl1.Text = ds1.Tables(0).Rows(0).Item(0)
            End If
            Dim ds2 As DataSet = get_dataset("SELECT count(staf_sl) FROM staf WHERE loc_cd=" & loc_cd & " AND emp_status='I'")
            If ds2.Tables(0).Rows.Count <> 0 Then
                lbl2.Text = ds2.Tables(0).Rows(0).Item(0)
            End If
            Dim ds3 As DataSet = get_dataset("SELECT count(staf_sl) FROM atnd WHERE loc_cd=" & loc_cd & " AND (day_status='PRESENT' OR day_status='HALFDAY' OR Cast(in_out as varchar)<>'') AND log_dt='" & Format(Now, "dd/MMM/yyyy") & "'")
            If ds3.Tables(0).Rows.Count <> 0 Then
                lbl3.Text = ds3.Tables(0).Rows(0).Item(0)
                lbl4.Text = Val(lbl2.Text) - Val(lbl3.Text)
            End If
            Dim device As DataSet = get_dataset("SELECT  DeviceFName, SerialNumber, LastPing,(CASE WHEN datediff(minute, lastPing, GETDATE()) < 5 THEN 'ACTIVE' WHEN datediff(minute, lastPing, GETDATE()) > 5 THEN 'IN ACTIVE' END) AS 'Status' FROM Devices WHERE loc_cd=" & loc_cd & "")
            Repeater1.DataSource = device
            Repeater1.DataBind()
            Dim dsdivision As DataSet = get_dataset("SELECT Row_number() OVER(Order BY div_nm) as Sl,div_nm,(SELECT Count(staf_sl) FROM staf WHERE div_sl=division_mst.div_sl) as 'Total',(SELECT Count(atnd.staf_sl) FROM atnd LEFT OUTER JOIN staf ON atnd.staf_sl = staf.staf_sl WHERE div_sl=division_mst.div_sl AND (day_status='PRESENT' OR day_status='HALFDAY' OR Cast(in_out as varchar)<>'') AND log_dt='" & Format(Now, "dd/MMM/yyyy") & "') as 'Present',((SELECT Count(staf_sl) FROM staf WHERE div_sl=division_mst.div_sl)  - (SELECT Count(atnd.staf_sl) FROM atnd LEFT OUTER JOIN staf ON atnd.staf_sl = staf.staf_sl WHERE div_sl=division_mst.div_sl AND (day_status='PRESENT' OR day_status='HALFDAY' OR Cast(in_out as varchar)<>'') AND log_dt='" & Format(Now, "dd/MMM/yyyy") & "')) as 'Absent' FROM division_mst Order BY div_nm")
            GridView1.DataSource = dsdivision
            GridView1.DataBind()
        End If
        Me.chart_display()
    End Sub

    Private Sub chart_display()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds13 As DataSet = get_dataset("SELECT isnull(count(staf_sl),0) FROM atnd WHERE (day_status='PRESENT' OR day_status='HALFDAY' OR Cast(in_out as varchar)<>'') AND log_dt='" & Format(Now, "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & "")
        Dim ds14 As DataSet = get_dataset("SELECT isnull(count(staf_sl),0) FROM atnd WHERE (day_status='ABSENT') AND log_dt='" & Format(Now, "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & "")
        Dim ds15 As DataSet = get_dataset("SELECT isnull(count(staf_sl),0) FROM atnd WHERE (day_status='LEAVEDAY') AND log_dt='" & Format(Now, "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & "")
        Dim ds16 As DataSet = get_dataset("SELECT isnull(count(staf_sl),0) FROM atnd WHERE (day_status='WEEKLYOFF') AND log_dt='" & Format(Now, "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & "")

        Dim data As String = "[" & ds13.Tables(0).Rows(0).Item(0) & "," & ds14.Tables(0).Rows(0).Item(0) & "," & ds15.Tables(0).Rows(0).Item(0) & "," & ds16.Tables(0).Rows(0).Item(0) & ",10]"
        Dim label As String = "[""Present"", ""Absent"", ""Weeklyoff"", ""Leave""]"
        Dim script As String = "window.onload = function(){ UpdateTime(" & data & "," & label & "); };"
        ClientScript.RegisterStartupScript(Me.GetType(), "UpdateTime", script, True)

    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub Process_attendance()
        Dim sys_dt As Date
        Dim dsdt As Dataset = get_dataset("SELECT DATEADD(MINUTE,330,GETUTCDATE())")
        sys_dt = dsdt.Tables(0).Rows(0).Item(0)
        dsdt.Tables(0).Rows.Clear()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim sdt, edt, logoutdate As Date
        sdt = Today
        edt = Today
        logoutdate = sdt
        start1()
        SQLInsert("UPDATE elog SET loc_cd =(SELECT loc_cd FROM Devices WHERE SerialNumber=elog.Serialno),staf_sl=(SELECT staf_sl FROM staf WHERE device_code=elog.device_code), device_no=(SELECT deviceid FROM Devices WHERE SerialNumber=elog.Serialno) WHERE log_tp='P'")
        SQLInsert("DELETE FROM atnd  WHERE log_dt>='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_dt<='" & Format(edt, "dd/MMM/yyyy") & "' and loc_cd=" & loc_cd & "")
        SQLInsert("UPDATE elog SET read_mark='N' WHERE log_dt>='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_dt<='" & Format(edt.AddDays(1), "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & "")
        Close1()
        Dim ds_hld As DataSet = get_dataset("SELECT holiday1.holi_nm AS Expr1,loc_cd, holiday2.* FROM holiday2 LEFT OUTER JOIN holiday1 ON holiday2.holi_sl = holiday1.holi_sl WHERE holi_dt>='" & Format(sdt, "dd/MMM/yyyy") & "' AND holi_dt<='" & Format(edt, "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & " AND active='Y' ORDER BY holi_nm")
        Dim ds_shift As DataSet = get_dataset("SELECT * FROM shift_mst WHERE loc_cd=" & loc_cd & " AND active='Y' ORDER BY shift_sdt")
        Dim ds_wov As DataSet = get_dataset("SELECT * FROM wov WHERE loc_cd=" & loc_cd & "")
        Dim ds_staf_shift As DataSet = get_dataset("SELECT * FROM staf_shift WHERE shift_date >='" & Format(sdt, "dd/MMM/yyyy") & "' AND shift_date <='" & Format(edt, "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & "")
        Dim ds_shiftcycle As DataSet = get_dataset("SELECT shift_mst.shift_sdt AS start_time, cycle2.shift_sl, cycle1.cycle_sl, cycle1.loc_cd FROM cycle1 RIGHT OUTER JOIN cycle2 ON cycle1.cycle_sl = cycle2.cycle_sl LEFT OUTER JOIN shift_mst ON cycle2.shift_sl = shift_mst.shift_sl WHERE cycle1.loc_cd=" & loc_cd & " ORDER BY shift_mst.shift_sdt")
        Dim ds_elog As DataSet = get_dataset("SELECT * FROM elog WHERE log_dt>='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_dt<='" & Format(edt.AddDays(1), "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & " AND view_report='Y'")
        Dim ds_lvoucher As DataSet = get_dataset("SELECT lvoucher2.*, lvoucher1.reason,lvoucher1.leave_day FROM lvoucher2 LEFT OUTER JOIN lvoucher1 ON lvoucher2.v_no = lvoucher1.v_no WHERE l_dt >='" & Format(sdt, "dd/MMM/yyyy") & "' AND l_dt <='" & Format(edt, "dd/MMM/yyyy") & "' AND lvoucher2.loc_cd=" & loc_cd & " AND approved='Y'")
        Dim ds_lvmst As DataSet = get_dataset("select * from leave_mst WHERE loc_cd=" & loc_cd & "")
        Dim ds_atnd = get_dataset("SELECT * FROM atnd WHERE log_dt>='" & Format(sdt.AddDays(-1), "dd/MMM/yyyy") & "' AND log_dt='" & Format(edt, "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & "")
        Dim ds_atndnight = get_dataset("SELECT * FROM atnd WHERE logout_date ='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_dt='" & Format(sdt.AddDays(-1), "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & "")
        Dim dsdt_atnd As DataSet = get_dataset("SELECT TOP 1 sl_no, staf_sl, staf_nm, emp_code, device_code, dept_nm, desg_nm, log_dt, logout_date,day_status, shift_nm,first_punch,last_punch,log_tp,early_in,late_in,early_out,late_out,tot_hour,tot_otmin,week_off,holiday_nm, leave_nm, leave_reason,in_out,loc_cd,shift_sl,leave_sl FROM atnd WHERE loc_cd=" & loc_cd & "")
        dsdt_atnd.Tables(0).Clear()
        Dim dt_atnd As DataTable = dsdt_atnd.Tables(0)
        Dim dr_atnd As DataRow
        Dim ds_emp As DataSet = get_dataset("SELECT weekly_off_tp,staf.staf_sl,staf.staf_nm,staf.emp_code,staf.div_sl,staf.device_code, desg_mst.desg_nm,dept_mst.dept_nm,staf.emp_status,staf.shift_tp,staf.shift_sl,staf.cycle_sl,emp_category.* FROM staf LEFT OUTER JOIN emp_category ON staf.cat_sl=emp_category.cat_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl=dept_mst.dept_sl LEFT OUTER JOIN desg_mst ON staf.desg_sl=desg_mst.desg_sl WHERE staf.emp_status='I' and staf.loc_cd=" & loc_cd & "  order by staf.staf_sl ")
        For i As Integer = 0 To ds_emp.Tables(0).Rows.Count - 1
            Dim otallow As String = "N"
            Dim otformula As String = "1"
            Dim minimum_otminute As String = 0
            Dim maximum_otapllicable As String = "N"
            Dim maximum_otminute As Integer = 0
            Dim oton_weekoff As String = "N"
            Dim oton_holiday As String = "N"
            Dim week_off1 As String = ""
            Dim week_off2 As String = ""
            Dim day_off1 As String = ""
            Dim day_off2 As String = ""
            Dim day_off3 As String = ""
            Dim day_off4 As String = ""
            Dim day_off5 As String = ""
            Dim latecoming_status As String
            Dim earlygoing_status As String
            Dim earlyin_minute As Integer
            Dim latein_minute As Integer
            Dim earlygo_minute As Integer
            Dim s_workhour As Integer
            Dim is_presentonwo As String = "N"
            Dim is_presentonho As String = "N"
            Dim when_noshift As String = ""
            Dim when_prefixday_absent As String = "0"
            Dim consider_1stin_lastout As String
            Dim halfday_iflessthan_min As Integer
            Dim absent_iflessthan_min As Integer
            Dim inpunch_stbfr As Integer
            Dim inpunch_endafter As Integer
            Dim outpunch_endafter As Integer
            Dim outpunch_before As Integer = 60
            Dim empnm As String = ""
            Dim stafsl As Integer = 0
            Dim div_sl As Integer = 0
            Dim empcd As String = ""
            Dim empdevcd As String = ""
            Dim deptnm As String = ""
            Dim desgnm As String = ""
            Dim shifttp As String = ""
            Dim shiftsl, cyclesl, leave_sl As Integer
            Dim weeklyoff_tp As String = "1"
            sdt = Today
            edt = Today
            empnm = ds_emp.Tables(0).Rows(i).Item("staf_nm")
            stafsl = ds_emp.Tables(0).Rows(i).Item("staf_sl")
            div_sl = ds_emp.Tables(0).Rows(i).Item("div_sl")
            empcd = ds_emp.Tables(0).Rows(i).Item("emp_code")
            empdevcd = ds_emp.Tables(0).Rows(i).Item("device_code")
            deptnm = ds_emp.Tables(0).Rows(i).Item("dept_nm")
            desgnm = ds_emp.Tables(0).Rows(i).Item("desg_nm")
            shifttp = ds_emp.Tables(0).Rows(i).Item("shift_tp")
            shiftsl = ds_emp.Tables(0).Rows(i).Item("shift_sl")
            cyclesl = ds_emp.Tables(0).Rows(i).Item("cycle_sl")
            otallow = ds_emp.Tables(0).Rows(i).Item("overtime")
            latecoming_status = ds_emp.Tables(0).Rows(i).Item("late_arrv")
            earlygoing_status = ds_emp.Tables(0).Rows(i).Item("erly_dprt")
            earlyin_minute = ds_emp.Tables(0).Rows(i).Item("early_time")
            latein_minute = ds_emp.Tables(0).Rows(i).Item("late_time")
            earlygo_minute = ds_emp.Tables(0).Rows(i).Item("leave_time")
            s_workhour = Val(Format(ds_emp.Tables(0).Rows(i).Item("max_work"), "HH"))
            maximum_otminute = ds_emp.Tables(0).Rows(i).Item("max_ot")
            consider_1stin_lastout = ds_emp.Tables(0).Rows(i).Item("consider_firstpunch")
            otformula = ds_emp.Tables(0).Rows(i).Item("ot_formula")
            oton_weekoff = ds_emp.Tables(0).Rows(i).Item("oton_weekoff")
            oton_holiday = ds_emp.Tables(0).Rows(i).Item("oton_holiday")
            week_off1 = ds_emp.Tables(0).Rows(i).Item("week_off1")
            week_off2 = ds_emp.Tables(0).Rows(i).Item("week_off2")
            day_off1 = ds_emp.Tables(0).Rows(i).Item("week_off2_1")
            day_off2 = ds_emp.Tables(0).Rows(i).Item("week_off2_2")
            day_off3 = ds_emp.Tables(0).Rows(i).Item("week_off2_3")
            day_off4 = ds_emp.Tables(0).Rows(i).Item("week_off2_4")
            day_off5 = ds_emp.Tables(0).Rows(i).Item("week_off2_5")
            when_noshift = ds_emp.Tables(0).Rows(i).Item("no_shift_assign")
            when_prefixday_absent = ds_emp.Tables(0).Rows(i).Item("mark_weekoff")
            weeklyoff_tp = ds_emp.Tables(0).Rows(i).Item("weekly_off_tp")
            Dim is_halfday As String = "0"
            Dim is_absent As String = "0"
            If Val(ds_emp.Tables(0).Rows(0).Item("consider_half")) > 0 Then
                is_halfday = 1
                halfday_iflessthan_min = Val(ds_emp.Tables(0).Rows(i).Item("consider_half"))
            End If
            If Val(ds_emp.Tables(0).Rows(0).Item("consider_absent")) > 0 Then
                is_absent = 1
                absent_iflessthan_min = Val(ds_emp.Tables(0).Rows(i).Item("consider_absent"))
            End If
            is_presentonwo = ds_emp.Tables(0).Rows(i).Item("present_wo")
            is_presentonho = ds_emp.Tables(0).Rows(i).Item("present_hl")
            inpunch_stbfr = ds_emp.Tables(0).Rows(i).Item("in_punch_stbfr")
            inpunch_endafter = ds_emp.Tables(0).Rows(i).Item("inpunch_endafter")
            outpunch_endafter = ds_emp.Tables(0).Rows(i).Item("outpunch_endafter")
            Do While sdt <= edt
                Dim shiftnm As String = ""
                Dim sttime, asttime, bsttime, edtime, aedtime, bedtime As Date
                Dim first_in As Date = #1/1/1753#
                Dim last_out As Date = #1/1/1753#
                Dim inoutsrting As String = ""
                Dim iostatus As String = ""
                Dim puncnt As Integer = 0
                Dim totminute As Integer = 0
                Dim split_minute As Integer = 0
                Dim lunchbreak_minute As Integer = 0
                Dim break_start As Date
                Dim break_end As Date
                Dim shift_hour As Date
                Dim Is_nightshift As Boolean = False
                If shifttp = "1" Then
                    Dim drshift_datarow() As DataRow
                    drshift_datarow = ds_shift.Tables(0).Select("shift_sl=" & shiftsl & "")
                    If drshift_datarow.Length <> 0 Then
                        shiftnm = drshift_datarow(0).Item("shift_nm")
                        sttime = drshift_datarow(0).Item("shift_sdt")
                        edtime = drshift_datarow(0).Item("shift_edt")
                        shift_hour = drshift_datarow(0).Item("min_work")
                        asttime = sttime.AddMinutes(inpunch_endafter)
                        bsttime = sttime.AddMinutes(-inpunch_stbfr)
                        aedtime = aedtime.AddMinutes(outpunch_endafter)
                        bedtime = edtime.AddMinutes(-outpunch_before)
                        break_start = drshift_datarow(0).Item("break_start")
                        break_end = drshift_datarow(0).Item("break_end")
                        If drshift_datarow(0).Item("incl_break") = "N" Then
                            lunchbreak_minute = DateDiff(DateInterval.Minute, break_start, break_end)
                        Else
                            lunchbreak_minute = 0
                        End If
                    End If
                    Dim drfx_elod_datarow() As DataRow
                    drfx_elod_datarow = ds_elog.Tables(0).Select("device_code='" & empdevcd & "' and log_dt='" & Format(sdt, "dd/MMM/yyyy") & "'", "log_time  ASC")
                    If drfx_elod_datarow.Length <> 0 Then
                        If consider_1stin_lastout = "1" Then
                            first_in = Format(drfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                            last_out = Format(drfx_elod_datarow(drfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                            Dim fnd_inout As Integer = 1
                            For for_fixshiftcnt As Integer = 0 To drfx_elod_datarow.Length - 1
                                first_in = Format(drfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                last_out = Format(drfx_elod_datarow(drfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                If fnd_inout Mod 2 = 0 Then
                                    iostatus = "OUT"
                                Else
                                    iostatus = "IN"
                                End If
                                inoutsrting = inoutsrting & Format(drfx_elod_datarow(for_fixshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                fnd_inout = fnd_inout + 1
                            Next
                            totminute = DateDiff(DateInterval.Minute, first_in, last_out)
                        Else
                            Dim fnd_inout As Integer = 1
                            Dim itime As Date = #1/1/1753#
                            Dim otime As Date = #1/1/1753#
                            Dim ltime As Date = #1/1/1753#
                            For for_fixshiftcnt As Integer = 0 To drfx_elod_datarow.Length - 1
                                first_in = Format(drfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                last_out = Format(drfx_elod_datarow(drfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                ltime = drfx_elod_datarow(for_fixshiftcnt).Item("log_time")
                                split_minute = 0
                                If fnd_inout Mod 2 = 0 Then
                                    iostatus = "OUT"
                                    otime = ltime
                                    split_minute = DateDiff(DateInterval.Minute, itime, otime)
                                Else
                                    iostatus = "IN"
                                    itime = ltime
                                End If
                                totminute = totminute + split_minute
                                inoutsrting = inoutsrting & Format(drfx_elod_datarow(for_fixshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                fnd_inout = fnd_inout + 1
                            Next
                        End If
                    End If
                    logoutdate = sdt
                ElseIf shifttp = "2" Then
                    Dim s_type As String = "F"
                    Is_nightshift = False
                    Dim nextdate As Date = sdt.AddDays(1)
                    Dim nonfix_shiftsl As Integer = 0
                    Dim stafshit_datarow() As DataRow
                    stafshit_datarow = ds_staf_shift.Tables(0).Select("staf_sl=" & stafsl & " AND shift_date='" & Format(sdt, "dd/MMM/yyyy") & "' AND loc_cd=" & loc_cd & "")
                    If stafshit_datarow.Length <> 0 Then
                        nonfix_shiftsl = stafshit_datarow(0).Item("shift_sl")
                        s_type = "N"
                    Else
                        If when_noshift = "A" Then
                            s_type = "A"
                            Dim dr_readmark() As DataRow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt>='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_dt<='" & Format(edt.AddDays(1), "dd/MMM/yyyy") & "'AND loc_cd=" & loc_cd & "")
                            For rdmcount As Integer = 0 To dr_readmark.Length - 1
                                dr_readmark(rdmcount).BeginEdit()
                                dr_readmark(rdmcount).Item("read_mark") = "N"
                                dr_readmark(rdmcount).EndEdit()
                            Next
                            Dim shiftcd As Integer = 0
                            Dim dr_getshiftloop() As DataRow = ds_shift.Tables(0).Select("loc_cd=" & loc_cd & "", "shift_sdt ASC")
                            For row_count As Integer = 0 To dr_getshiftloop.Length - 1
                                Dim stime, etime As Date
                                shiftcd = dr_getshiftloop(row_count).Item("shift_sl")
                                Dim shiftmst_datarow1() As DataRow
                                shiftmst_datarow1 = ds_shift.Tables(0).Select("shift_sl=" & shiftcd & "")
                                If shiftmst_datarow1.Length <> 0 Then
                                    sttime = shiftmst_datarow1(0).Item("shift_sdt")
                                    stime = sttime.AddMinutes(-inpunch_stbfr)
                                    etime = sttime.AddMinutes(inpunch_endafter)
                                End If
                                Dim dr_findautoshift() As DataRow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " and log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND (log_time >='" & Format(stime, "HH:mm:ss") & "') AND (log_time<='" & Format(etime, "HH:mm:ss") & "') AND read_mark='N'", "log_time ASC")
                                If dr_findautoshift.Length <> 0 Then
                                    nonfix_shiftsl = shiftcd
                                    Exit For
                                End If
                            Next
                        Else
                            nonfix_shiftsl = shiftsl
                            s_type = "F"
                        End If
                    End If

                    Dim shiftmst_datarow() As DataRow
                    shiftmst_datarow = ds_shift.Tables(0).Select("shift_sl=" & nonfix_shiftsl & "")
                    If shiftmst_datarow.Length <> 0 Then
                        shiftsl = nonfix_shiftsl
                        shiftnm = shiftmst_datarow(0).Item("shift_nm")
                        sttime = shiftmst_datarow(0).Item("shift_sdt")
                        edtime = shiftmst_datarow(0).Item("shift_edt")
                        shift_hour = shiftmst_datarow(0).Item("min_work")
                        If shiftmst_datarow(0).Item("is_nightshift") = "Y" Then
                            Is_nightshift = True
                        Else
                            Is_nightshift = False
                        End If
                        asttime = sttime.AddMinutes(inpunch_endafter)
                        bsttime = sttime.AddMinutes(-inpunch_stbfr)
                        aedtime = edtime.AddMinutes(outpunch_endafter)
                        bedtime = edtime.AddMinutes(-outpunch_before)
                        break_start = shiftmst_datarow(0).Item("break_start")
                        break_end = shiftmst_datarow(0).Item("break_end")
                        If shiftmst_datarow(0).Item("incl_break") = "N" Then
                            lunchbreak_minute = DateDiff(DateInterval.Minute, break_start, break_end)
                        Else
                            lunchbreak_minute = 0
                        End If
                    End If
                    If s_type <> "A" Then
                        If Is_nightshift = True Then
                            If consider_1stin_lastout = "1" Then
                                logoutdate = sdt
                                Dim first_in_1 As Date
                                Dim last_out_1 As Date
                                Dim fnd_inout As Integer = 1
                                Dim drnonfx_elod_datarow() As DataRow
                                drnonfx_elod_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_time >='" & bsttime & "'", "log_time ASC")
                                If drnonfx_elod_datarow.Length <> 0 Then
                                    first_in = Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                    last_out = Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                    first_in_1 = CDate(Format(sdt, "MM/dd/yyyy")) & " " & Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                    last_out_1 = CDate(Format(sdt, "MM/dd/yyyy")) & " " & Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                    For for_fixshiftcnt As Integer = 0 To drnonfx_elod_datarow.Length - 1
                                        first_in = Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                        last_out = Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                        If fnd_inout Mod 2 = 0 Then
                                            iostatus = "OUT"
                                        Else
                                            iostatus = "IN"
                                        End If
                                        inoutsrting = inoutsrting & Format(drnonfx_elod_datarow(for_fixshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                        fnd_inout = fnd_inout + 1
                                    Next
                                End If
                                If iostatus = "IN" Then
                                    fnd_inout = 2
                                ElseIf iostatus = "OUT" Then
                                    fnd_inout = 1
                                End If
                                Dim ltime As Date = #1/1/1753#
                                Dim nextdayioelog_datarow() As DataRow
                                nextdayioelog_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(nextdate, "dd/MMM/yyyy") & "' AND log_time<='" & Format(aedtime, "HH:mm:ss") & "'", "log_time ASC")
                                If nextdayioelog_datarow.Length <> 0 Then
                                    For niocnt As Integer = 0 To nextdayioelog_datarow.Length - 1
                                        last_out = Format(nextdayioelog_datarow(nextdayioelog_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                        last_out_1 = CDate(Format(nextdate, "MM/dd/yyyy") & " " & Format(nextdayioelog_datarow(nextdayioelog_datarow.Length - 1).Item("log_time"), "HH:mm:ss"))
                                        ltime = nextdayioelog_datarow(niocnt).Item("log_time")
                                        If fnd_inout Mod 2 = 0 Then
                                            iostatus = "OUT"
                                        Else
                                            iostatus = "IN"
                                        End If
                                        inoutsrting = inoutsrting & Format(nextdayioelog_datarow(niocnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                        fnd_inout = fnd_inout + 1
                                    Next
                                    logoutdate = nextdate
                                End If
                                totminute = DateDiff(DateInterval.Minute, first_in_1, last_out_1)
                            Else
                                Dim fnd_inout As Integer = 1
                                Dim itime As Date = #1/1/1753#
                                Dim otime As Date = #1/1/1753#
                                Dim ltime As Date = #1/1/1753#
                                Dim drnonfx_elod_datarow() As DataRow
                                drnonfx_elod_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_time >='" & bsttime & "'", "log_time ASC")
                                If drnonfx_elod_datarow.Length <> 0 Then
                                    logoutdate = sdt
                                    first_in = Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                    last_out = Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                    For for_fixshiftcnt As Integer = 0 To drnonfx_elod_datarow.Length - 1
                                        first_in = Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                        last_out = Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                        ltime = CDate(Format(sdt, "MM/dd/yyyy") & " " & Format(drnonfx_elod_datarow(for_fixshiftcnt).Item("log_time"), "HH:mm:ss"))
                                        split_minute = 0
                                        If fnd_inout Mod 2 = 0 Then
                                            iostatus = "OUT"
                                            otime = ltime
                                            split_minute = DateDiff(DateInterval.Minute, itime, otime)
                                        Else
                                            iostatus = "IN"
                                            itime = ltime
                                        End If
                                        totminute = totminute + split_minute
                                        inoutsrting = inoutsrting & Format(drnonfx_elod_datarow(for_fixshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                        fnd_inout = fnd_inout + 1
                                    Next
                                End If
                                If iostatus = "IN" Then
                                    fnd_inout = 2
                                Else
                                    fnd_inout = 1
                                End If
                                Dim nextdayioelog_datarow() As DataRow
                                nextdayioelog_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(nextdate, "dd/MMM/yyyy") & "' AND log_time <='" & Format(aedtime, "HH:mm:ss") & "'", "log_time asc")
                                If nextdayioelog_datarow.Length <> 0 Then
                                    For niocnt As Integer = 0 To nextdayioelog_datarow.Length - 1
                                        last_out = Format(nextdayioelog_datarow(nextdayioelog_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                        ltime = CDate(Format(nextdate, "MM/dd/yyyy") & " " & Format(nextdayioelog_datarow(niocnt).Item("log_time"), "HH:mm:ss"))
                                        split_minute = 0
                                        If fnd_inout Mod 2 = 0 Then
                                            iostatus = "OUT"
                                            otime = ltime
                                            split_minute = DateDiff(DateInterval.Minute, itime, otime)
                                        Else
                                            iostatus = "IN"
                                            itime = ltime
                                        End If
                                        totminute = totminute + split_minute
                                        inoutsrting = inoutsrting & Format(nextdayioelog_datarow(niocnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                        fnd_inout = fnd_inout + 1
                                    Next
                                    logoutdate = nextdate
                                End If
                            End If
                        Else
                            logoutdate = sdt
                            Dim drday_elod_datarow() As DataRow
                            drday_elod_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "'", "log_time ASC")
                            If drday_elod_datarow.Length <> 0 Then
                                If consider_1stin_lastout = "1" Then
                                    first_in = Format(drday_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                    last_out = Format(drday_elod_datarow(drday_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                    Dim fnd_inout As Integer = 1
                                    For for_dayshiftcnt As Integer = 0 To drday_elod_datarow.Length - 1
                                        first_in = Format(drday_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                        last_out = Format(drday_elod_datarow(drday_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                        If fnd_inout Mod 2 = 0 Then
                                            iostatus = "OUT"
                                        Else
                                            iostatus = "IN"
                                        End If
                                        inoutsrting = inoutsrting & Format(drday_elod_datarow(for_dayshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                        fnd_inout = fnd_inout + 1
                                    Next
                                    totminute = DateDiff(DateInterval.Minute, first_in, last_out)
                                Else
                                    Dim fnd_inout As Integer = 1
                                    Dim itime As Date = #1/1/1753#
                                    Dim otime As Date = #1/1/1753#
                                    Dim ltime As Date = #1/1/1753#
                                    For for_dayshiftcnt As Integer = 0 To drday_elod_datarow.Length - 1
                                        first_in = Format(drday_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                        last_out = Format(drday_elod_datarow(drday_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                        ltime = drday_elod_datarow(for_dayshiftcnt).Item("log_time")
                                        split_minute = 0
                                        If fnd_inout Mod 2 = 0 Then
                                            iostatus = "OUT"
                                            otime = ltime
                                            split_minute = DateDiff(DateInterval.Minute, itime, otime)
                                        Else
                                            iostatus = "IN"
                                            itime = ltime
                                        End If
                                        inoutsrting = inoutsrting & Format(drday_elod_datarow(for_dayshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & ");) "
                                        fnd_inout = fnd_inout + 1
                                    Next
                                End If
                            End If
                        End If
                    Else
                        If Is_nightshift = True Then
                            If consider_1stin_lastout = "1" Then
                                logoutdate = sdt
                                Dim first_in_1 As Date
                                Dim last_out_1 As Date
                                Dim fnd_inout As Integer = 1
                                Dim drnonfx_elod_datarow() As DataRow
                                drnonfx_elod_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_time>='" & bsttime & "' AND read_mark='N'", "log_time ASC")
                                If drnonfx_elod_datarow.Length <> 0 Then
                                    first_in = Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                    last_out = Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                    first_in_1 = CDate(Format(sdt, "MM/dd/yyyy") & " " & Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss"))
                                    last_out_1 = CDate(Format(sdt, "MM/dd/yyyy") & " " & Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss"))
                                    For for_fixshiftcnt As Integer = 0 To drnonfx_elod_datarow.Length - 1
                                        first_in = Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                        last_out = Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                        Dim sl_no As Integer = drnonfx_elod_datarow(for_fixshiftcnt).Item("sl_no")
                                        If fnd_inout Mod 2 = 0 Then
                                            iostatus = "OUT"
                                        Else
                                            iostatus = "IN"
                                        End If
                                        inoutsrting = inoutsrting & Format(drnonfx_elod_datarow(for_fixshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                        fnd_inout = fnd_inout + 1
                                        drnonfx_elod_datarow(for_fixshiftcnt).BeginEdit()
                                        drnonfx_elod_datarow(for_fixshiftcnt).Item("read_mark") = "Y"
                                        drnonfx_elod_datarow(for_fixshiftcnt).EndEdit()
                                        ds_elog.Tables(0).Rows(for_fixshiftcnt).Item("read_mark") = "Y"
                                        ds_elog.Tables(0).AcceptChanges()

                                    Next
                                End If
                                If iostatus = "IN" Then
                                    fnd_inout = 2
                                ElseIf iostatus = "OUT" Then
                                    fnd_inout = 1
                                End If
                                Dim ltime As Date = #1/1/1753#
                                Dim nextdayioelog_datarow() As DataRow
                                nextdayioelog_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(nextdate, "dd/MMM/yyyy") & "' AND log_time <='" & Format(aedtime, "HH:mm:ss") & "' AND read_mark='N'", "log_time ASC")
                                If nextdayioelog_datarow.Length <> 0 Then
                                    For niocnt As Integer = 0 To nextdayioelog_datarow.Length - 1
                                        last_out = Format(nextdayioelog_datarow(nextdayioelog_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                        last_out_1 = CDate(Format(nextdate, "MM/dd/yyyy") & " " & Format(nextdayioelog_datarow(nextdayioelog_datarow.Length - 1).Item("log_time"), "HH:mm:ss"))
                                        ltime = nextdayioelog_datarow(niocnt).Item("log_time")
                                        Dim sl_no As Integer = nextdayioelog_datarow(niocnt).Item("read_mark")
                                        If fnd_inout Mod 2 = 0 Then
                                            iostatus = "OUT"
                                        Else
                                            iostatus = "IN"
                                        End If
                                        inoutsrting = inoutsrting & Format(nextdayioelog_datarow(niocnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                        fnd_inout = fnd_inout + 1
                                        nextdayioelog_datarow(niocnt).BeginEdit()
                                        nextdayioelog_datarow(niocnt).Item("read_mark") = "Y"
                                        nextdayioelog_datarow(niocnt).EndEdit()
                                        ds_elog.Tables(0).Rows(niocnt).Item("read_mark") = "Y"
                                        ds_elog.Tables(0).AcceptChanges()
                                    Next
                                    logoutdate = nextdate
                                End If
                                totminute = DateDiff(DateInterval.Minute, first_in_1, last_out_1)
                            Else
                                logoutdate = sdt
                                Dim fnd_inout As Integer = 1
                                Dim itime As Date = #1/1/1753#
                                Dim otime As Date = #1/1/1753#
                                Dim ltime As Date = #1/1/1753#
                                Dim drnonfx_elod_datarow() As DataRow
                                drnonfx_elod_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_time >='" & bsttime & "' AND read_mark='N'", "log_time ASC")
                                If drnonfx_elod_datarow.Length <> 0 Then
                                    first_in = Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                    last_out = Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length).Item("log_time"), "HH:mm:ss")
                                    For for_fixshiftcnt As Integer = 0 To drnonfx_elod_datarow.Length - 1
                                        first_in = Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                        last_out = Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length).Item("log_time"), "HH:mm:ss")
                                        ltime = CDate(Format(sdt, "MM/dd/yyyy") & " " & Format(drnonfx_elod_datarow(for_fixshiftcnt).Item("log_time"), "HH:mm:ss"))
                                        Dim sl_no As Integer = drnonfx_elod_datarow(for_fixshiftcnt).Item("sl_no")
                                        split_minute = 0
                                        If fnd_inout Mod 2 = 0 Then
                                            iostatus = "OUT"
                                            otime = ltime
                                            split_minute = DateDiff(DateInterval.Minute, itime, otime)
                                        Else
                                            iostatus = "IN"
                                            itime = ltime
                                        End If
                                        totminute = totminute + split_minute
                                        inoutsrting = inoutsrting & Format(drnonfx_elod_datarow(for_fixshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                        fnd_inout = fnd_inout + 1
                                        drnonfx_elod_datarow(for_fixshiftcnt).BeginEdit()
                                        drnonfx_elod_datarow(for_fixshiftcnt).Item("read_mark") = "Y"
                                        drnonfx_elod_datarow(for_fixshiftcnt).EndEdit()
                                        ds_elog.Tables(0).Rows(for_fixshiftcnt).Item("read_mark") = "Y"
                                        ds_elog.Tables(0).AcceptChanges()
                                    Next
                                End If
                                If iostatus = "IN" Then
                                    fnd_inout = 2
                                ElseIf iostatus = "OUT" Then
                                    fnd_inout = 1
                                End If
                                Dim nextdayioelog_datarow() As DataRow
                                nextdayioelog_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(nextdate, "dd/MMM/yyyy") & "' and log_time <='" & Format(aedtime, "HH:mm:ss") & "' AND read_mark='N'", "log_time ASC")
                                If nextdayioelog_datarow.Length <> 0 Then
                                    For niocnt As Integer = 0 To nextdayioelog_datarow.Length - 1
                                        last_out = Format(nextdayioelog_datarow(nextdayioelog_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                        ltime = CDate(Format(sdt, "MM/dd/yyyy") & " " & Format(nextdayioelog_datarow(niocnt).Item("log_time"), "HH:mm:ss"))
                                        split_minute = 0
                                        If fnd_inout Mod 2 = 0 Then
                                            iostatus = "OUT"
                                            otime = ltime
                                            split_minute = DateDiff(DateInterval.Minute, itime, otime)
                                        Else
                                            iostatus = "IN"
                                            itime = ltime
                                        End If
                                        totminute = totminute + split_minute
                                        inoutsrting = inoutsrting & Format(nextdayioelog_datarow(niocnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                        fnd_inout = fnd_inout + 1
                                        nextdayioelog_datarow(niocnt).BeginEdit()
                                        nextdayioelog_datarow(niocnt).Item("read_mark") = "Y"
                                        nextdayioelog_datarow(niocnt).EndEdit()
                                        ds_elog.Tables(0).Rows(niocnt).Item("read_mark") = "Y"
                                        ds_elog.Tables(0).AcceptChanges()
                                    Next
                                    logoutdate = nextdate
                                End If
                            End If
                        Else
                            logoutdate = sdt
                            Dim drday_elod_datarow() As DataRow
                            drday_elod_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND read_mark='N'", "log_time ASC")
                            If drday_elod_datarow.Length <> 0 Then
                                If consider_1stin_lastout = "1" Then
                                    first_in = Format(drday_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                    last_out = Format(drday_elod_datarow(drday_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                    Dim fnd_inout As Integer = 1
                                    For for_dayshiftcnt As Integer = 0 To drday_elod_datarow.Length - 1
                                        first_in = Format(drday_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                        last_out = Format(drday_elod_datarow(drday_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                        If fnd_inout Mod 2 = 0 Then
                                            iostatus = "OUT"
                                        Else
                                            iostatus = "IN"
                                        End If
                                        inoutsrting = inoutsrting & Format(drday_elod_datarow(for_dayshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                        fnd_inout = fnd_inout + 1
                                        drday_elod_datarow(for_dayshiftcnt).BeginEdit()
                                        drday_elod_datarow(for_dayshiftcnt).Item("read_mark") = "Y"
                                        drday_elod_datarow(for_dayshiftcnt).EndEdit()
                                        start1()
                                        SQLInsert("UPDATE elog SET read_mark='Y' WHERE sl_no=" & drday_elod_datarow(for_dayshiftcnt).Item("sl_no") & "")
                                        close1()
                                    Next
                                    totminute = DateDiff(DateInterval.Minute, first_in, last_out)
                                Else
                                    Dim fnd_inout As Integer = 1
                                    Dim itime As Date = #1/1/1753#
                                    Dim otime As Date = #1/1/1753#
                                    Dim ltime As Date = #1/1/1753#
                                    For for_dayshiftcnt As Integer = 0 To drday_elod_datarow.Length - 1
                                        first_in = Format(drday_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                        last_out = Format(drday_elod_datarow(drday_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                        ltime = drday_elod_datarow(for_dayshiftcnt).Item("log_time")
                                        split_minute = 0
                                        If fnd_inout Mod 2 = 0 Then
                                            iostatus = "OUT"
                                            otime = ltime
                                            split_minute = DateDiff(DateInterval.Minute, itime, otime)
                                        Else
                                            iostatus = "IN"
                                            itime = ltime
                                        End If
                                        totminute = totminute + split_minute
                                        inoutsrting = inoutsrting & Format(drday_elod_datarow(for_dayshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                        fnd_inout = fnd_inout + 1
                                        drday_elod_datarow(for_dayshiftcnt).BeginEdit()
                                        drday_elod_datarow(for_dayshiftcnt).Item("read_mark") = "Y"
                                        drday_elod_datarow(for_dayshiftcnt).EndEdit()
                                        start1()
                                        SQLInsert("UPDATE elog SET read_mark='Y' WHERE sl_no=" & drday_elod_datarow(for_dayshiftcnt).Item("sl_no") & "")
                                        close1()
                                    Next
                                End If
                            End If
                        End If
                    End If
                ElseIf shifttp = "3" Then
                    Dim nextdate As Date = sdt.AddDays(1)
                    Dim autoshift_sl As Integer = 0
                    Dim dr_readmark() As DataRow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt>= '" & Format(sdt, "dd/MMM/yyyy") & "' AND log_dt<='" & Format(edt.AddDays(1), "dd/MMM/yyyy") & "'")
                    For rdmCount As Integer = 0 To dr_readmark.Length - 1
                        dr_readmark(rdmCount).BeginEdit()
                        dr_readmark(rdmCount).Item("read_mark") = "N"
                        dr_readmark(rdmCount).EndEdit()
                    Next
                    Dim shiftcd As Integer = 0
                    Dim cycle_datarow() As DataRow
                    cycle_datarow = ds_shiftcycle.Tables(0).Select("cycle_sl=" & cyclesl & " and loc_cd=" & loc_cd & "", "start_time asc")
                    If cycle_datarow.Length <> 0 Then
                        For clcnt As Integer = 0 To cycle_datarow.Length - 1
                            Dim stime, etime As Date
                            shiftcd = cycle_datarow(clcnt).Item("shift_sl")
                            Dim shiftmst_datarow1() As DataRow
                            shiftmst_datarow1 = ds_shift.Tables(0).Select("shift_sl=" & shiftcd & "")
                            If shiftmst_datarow1.Length <> 0 Then
                                sttime = shiftmst_datarow1(0).Item("shift_sdt")
                                stime = sttime.AddMinutes(-inpunch_stbfr)
                                etime = sttime.AddMinutes(inpunch_endafter)
                            End If
                            Dim dr_findAutoshift() As DataRow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND (log_time >='" & Format(stime, "HH:mm:ss") & "') AND (log_time <='" & Format(etime, "HH:mm:ss") & "') AND read_mark='N'", "log_time asc")
                            Dim drnightshift_check As DataRow()
                            drnightshift_check = ds_atndnight.Tables(0).Select("staf_sl=" & stafsl & " AND logout_date='" & Format(sdt, "dd/MMM/yyyy") & "'")
                            If drnightshift_check.Length <> 0 Then
                                start1()
                                SQLInsert("UPDATE elog SET read_mark='Y' WHERE staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_time <='" & Format(drnightshift_check(0).Item("last_punch"), "HH:mm:ss") & "'")
                                close1()
                            End If
                            Dim ds111 As DataSet = get_dataset("select * from elog WHERE staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND (log_time >='" & Format(stime, "HH:mm:ss") & "') AND (log_time <='" & Format(etime, "HH:mm:ss") & "') AND read_mark='N' AND view_report='Y'")
                            If ds111.Tables(0).Rows.Count <> 0 Then
                                autoshift_sl = shiftcd
                                Exit For
                            End If
                        Next
                    End If
                    Is_nightshift = False
                    Dim shiftmst_datarow() As DataRow
                    shiftmst_datarow = ds_shift.Tables(0).Select("shift_sl=" & autoshift_sl & "")
                    If shiftmst_datarow.Length <> 0 Then
                        shiftsl = autoshift_sl
                        shiftnm = shiftmst_datarow(0).Item("shift_nm")
                        sttime = shiftmst_datarow(0).Item("shift_sdt")
                        edtime = shiftmst_datarow(0).Item("shift_edt")
                        shift_hour = shiftmst_datarow(0).Item("min_work")
                        If shiftmst_datarow(0).Item("is_nightshift") = "Y" Then
                            Is_nightshift = True
                        Else
                            Is_nightshift = False
                        End If
                        asttime = sttime.AddMinutes(inpunch_endafter)
                        bsttime = sttime.AddMinutes(-inpunch_stbfr)
                        aedtime = edtime.AddMinutes(outpunch_endafter)
                        bedtime = edtime.AddMinutes(-outpunch_before)
                        break_start = shiftmst_datarow(0).Item("break_start")
                        break_end = shiftmst_datarow(0).Item("break_end")
                        If shiftmst_datarow(0).Item("incl_break") = "N" Then
                            lunchbreak_minute = DateDiff(DateInterval.Minute, break_start, break_end)
                        Else
                            lunchbreak_minute = 0
                        End If
                    End If
                    If Is_nightshift = True Then
                        logoutdate = sdt
                        If consider_1stin_lastout = "1" Then
                            Dim first_in_1 As Date
                            Dim last_out_1 As Date
                            Dim fnd_inout As Integer = 1
                            Dim drnonfx_elod_datarow() As DataRow
                            drnonfx_elod_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_time >='" & bsttime & "' AND read_mark='N'", "log_time ASC")
                            If drnonfx_elod_datarow.Length <> 0 Then
                                first_in = Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                last_out = Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                first_in_1 = CDate(Format(sdt, "MM/dd/yyyy") & " " & Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss"))
                                last_out_1 = CDate(Format(sdt, "MM/dd/yyyy") & " " & Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss"))
                                For for_fixshiftcnt As Integer = 0 To drnonfx_elod_datarow.Length - 1
                                    first_in = Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                    last_out = Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                    Dim slno As Integer = drnonfx_elod_datarow(for_fixshiftcnt).Item("sl_no")
                                    If fnd_inout Mod 2 = 0 Then
                                        iostatus = "OUT"
                                    Else
                                        iostatus = "IN"
                                    End If
                                    inoutsrting = inoutsrting & Format(drnonfx_elod_datarow(for_fixshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                    fnd_inout = fnd_inout + 1
                                    drnonfx_elod_datarow(for_fixshiftcnt).BeginEdit()
                                    drnonfx_elod_datarow(for_fixshiftcnt).Item("read_mark") = "Y"
                                    drnonfx_elod_datarow(for_fixshiftcnt).EndEdit()
                                    start1()
                                    SQLInsert("UPDATE elog SET read_mark='Y' WHERE sl_no=" & drnonfx_elod_datarow(for_fixshiftcnt).Item("sl_no") & "")
                                    close1()
                                Next
                            End If
                            If iostatus = "IN" Then
                                fnd_inout = 2
                            ElseIf iostatus = "OUT" Then
                                fnd_inout = 1
                            End If
                            Dim ltime As Date = #1/1/1753#
                            Dim nextdayioelog_datarow() As DataRow
                            nextdayioelog_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " and log_dt='" & Format(nextdate, "dd/MMM/yyyy") & "' AND log_time<='" & aedtime & "' AND read_mark='N'", "log_time ASC")
                            If nextdayioelog_datarow.Length <> 0 Then
                                For niocnt As Integer = 0 To nextdayioelog_datarow.Length - 1
                                    last_out = Format(nextdayioelog_datarow(nextdayioelog_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                    last_out_1 = CDate(Format(nextdate, "MM/dd/yyyy") & " " & Format(nextdayioelog_datarow(nextdayioelog_datarow.Length - 1).Item("log_time"), "HH:mm:ss"))
                                    ltime = nextdayioelog_datarow(niocnt).Item("log_time")
                                    Dim sl_no As String = nextdayioelog_datarow(niocnt).Item("read_mark")
                                    If fnd_inout Mod 2 = 0 Then
                                        iostatus = "OUT"
                                    Else
                                        iostatus = "IN"
                                    End If
                                    inoutsrting = inoutsrting & Format(nextdayioelog_datarow(niocnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                    fnd_inout = fnd_inout + 1
                                    nextdayioelog_datarow(niocnt).BeginEdit()
                                    nextdayioelog_datarow(niocnt).Item("read_mark") = "Y"
                                    nextdayioelog_datarow(niocnt).EndEdit()
                                    start1()
                                    SQLInsert("UPDATE elog SET read_mark='Y' WHERE sl_no=" & nextdayioelog_datarow(niocnt).Item("sl_no") & "")
                                    close1()
                                Next
                                logoutdate = nextdate
                            End If
                            totminute = DateDiff(DateInterval.Minute, first_in_1, last_out_1)
                        Else
                            Dim fnd_inout As Integer = 1
                            Dim itime As Date = #1/1/1753#
                            Dim otime As Date = #1/1/1753#
                            Dim ltime As Date = #1/1/1753#
                            Dim drnonfx_elod_datarow() As DataRow
                            drnonfx_elod_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_time>='" & bsttime & "' AND read_mark='N'", "log_time ASC")
                            If drnonfx_elod_datarow.Length <> 0 Then
                                first_in = Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                last_out = Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                For for_fixshiftcnt As Integer = 0 To drnonfx_elod_datarow.Length - 1
                                    first_in = Format(drnonfx_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                    last_out = Format(drnonfx_elod_datarow(drnonfx_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                    ltime = CDate(Format(sdt, "dd/MMM/yyyy") & " " & Format(drnonfx_elod_datarow(for_fixshiftcnt).Item("log_time"), "HH:mm:ss"))
                                    Dim sl_no As Integer = drnonfx_elod_datarow(for_fixshiftcnt).Item("sl_no")
                                    split_minute = 0
                                    If fnd_inout Mod 2 = 0 Then
                                        iostatus = "OUT"
                                        otime = ltime
                                        split_minute = DateDiff(DateInterval.Minute, itime, otime)
                                    Else
                                        iostatus = "IN"
                                        itime = ltime
                                    End If
                                    totminute = totminute + split_minute
                                    inoutsrting = inoutsrting & Format(drnonfx_elod_datarow(for_fixshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                    fnd_inout = fnd_inout + 1
                                    drnonfx_elod_datarow(for_fixshiftcnt).BeginEdit()
                                    drnonfx_elod_datarow(for_fixshiftcnt).Item("read_mark") = "Y"
                                    drnonfx_elod_datarow(for_fixshiftcnt).EndEdit()
                                    start1()
                                    SQLInsert("UPDATE elog SET read_mark='Y' WHERE sl_no=" & drnonfx_elod_datarow(for_fixshiftcnt).Item("sl_no") & "")
                                    close1()
                                Next
                            End If
                            If iostatus = "IN" Then
                                fnd_inout = 2
                            ElseIf iostatus = "OUT" Then
                                fnd_inout = 1
                            End If
                            Dim nextdayioelog_datarow() As DataRow
                            nextdayioelog_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(nextdate, "dd/MMM/yyyy") & "' AND log_time<='" & aedtime & "' AND read_mark='N'", "log_time ASC")
                            If nextdayioelog_datarow.Length <> 0 Then
                                For niocnt As Integer = 0 To nextdayioelog_datarow.Length - 1
                                    last_out = Format(nextdayioelog_datarow(nextdayioelog_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                    ltime = CDate(Format(nextdate, "MM/dd/yyyy") & " " & Format(nextdayioelog_datarow(niocnt).Item("log_time"), "HH:mm:ss"))
                                    split_minute = 0
                                    If fnd_inout Mod 2 = 0 Then
                                        iostatus = "OUT"
                                        otime = ltime
                                        split_minute = DateDiff(DateInterval.Minute, itime, otime)
                                    Else
                                        iostatus = "IN"
                                        itime = ltime
                                    End If
                                    totminute = totminute + split_minute
                                    inoutsrting = inoutsrting & Format(nextdayioelog_datarow(niocnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                    fnd_inout = fnd_inout + 1
                                    nextdayioelog_datarow(niocnt).BeginEdit()
                                    nextdayioelog_datarow(niocnt).Item("read_mark") = "Y"
                                    nextdayioelog_datarow(niocnt).EndEdit()
                                    start1()
                                    SQLInsert("UPDATE elog SET read_mark='Y' WHERE sl_no=" & nextdayioelog_datarow(niocnt).Item("sl_no") & "")
                                    close1()
                                Next
                                logoutdate = nextdate
                            End If
                        End If
                    Else
                        logoutdate = sdt
                        Dim drday_elod_datarow() As DataRow
                        drday_elod_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND read_mark='N' AND log_time >='" & bsttime & "'", "log_time ASC")
                        If drday_elod_datarow.Length <> 0 Then
                            If consider_1stin_lastout = "1" Then
                                Dim first_in_1 As Date
                                Dim last_out_1 As Date
                                first_in = Format(drday_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                last_out = Format(drday_elod_datarow(drday_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                first_in_1 = CDate(Format(sdt, "MM/dd/yyyy") & " " & Format(drday_elod_datarow(0).Item("log_time"), "HH:mm:ss"))
                                last_out_1 = CDate(Format(sdt, "MM/dd/yyyy") & " " & Format(drday_elod_datarow(drday_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss"))
                                Dim fnd_inout As Integer = 1
                                For for_dayshiftcnt As Integer = 0 To drday_elod_datarow.Length - 1
                                    first_in = Format(drday_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                    last_out = Format(drday_elod_datarow(drday_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                    If fnd_inout Mod 2 = 0 Then
                                        iostatus = "OUT"
                                    Else
                                        iostatus = "IN"
                                    End If
                                    inoutsrting = inoutsrting & Format(drday_elod_datarow(for_dayshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                    fnd_inout = fnd_inout + 1
                                    drday_elod_datarow(for_dayshiftcnt).BeginEdit()
                                    drday_elod_datarow(for_dayshiftcnt).Item("read_mark") = "Y"
                                    drday_elod_datarow(for_dayshiftcnt).EndEdit()
                                    start1()
                                    SQLInsert("UPDATE elog SET read_mark='Y' WHERE sl_no=" & drday_elod_datarow(for_dayshiftcnt).Item("sl_no") & "")
                                    close1()
                                Next
                                If iostatus = "IN" Then
                                    fnd_inout = 2
                                ElseIf iostatus = "OUT" Then
                                    fnd_inout = 1
                                End If
                                Dim ltime As Date = #1/1/1753#
                                If Format(aedtime, "dd/MM/yyyy") > "01/01/1900" Then
                                    aedtime = CDate("01/01/1900 " & Format(aedtime, "HH:mm:sss"))
                                    Dim nextdayioelog121_datarow() As DataRow
                                    nextdayioelog121_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " and log_dt='" & Format(nextdate, "dd/MMM/yyyy") & "' AND log_time <='" & aedtime & "' AND read_mark='N'", "log_time ASC")
                                    If nextdayioelog121_datarow.Length <> 0 Then
                                        For niocnt As Integer = 0 To nextdayioelog121_datarow.Length - 1
                                            last_out = Format(nextdayioelog121_datarow(nextdayioelog121_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                            last_out_1 = CDate(Format(nextdate, "MM/dd/yyyy") & " " & Format(nextdayioelog121_datarow(nextdayioelog121_datarow.Length - 1).Item("log_time"), "HH:mm:ss"))
                                            ltime = nextdayioelog121_datarow(niocnt).Item("log_time")
                                            Dim sl_no As String = nextdayioelog121_datarow(niocnt).Item("read_mark")
                                            If fnd_inout Mod 2 = 0 Then
                                                iostatus = "OUT"
                                            Else
                                                iostatus = "IN"
                                            End If
                                            inoutsrting = inoutsrting & Format(nextdayioelog121_datarow(niocnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                            fnd_inout = fnd_inout + 1
                                            nextdayioelog121_datarow(niocnt).BeginEdit()
                                            nextdayioelog121_datarow(niocnt).Item("read_mark") = "Y"
                                            nextdayioelog121_datarow(niocnt).EndEdit()
                                            start1()
                                            SQLInsert("UPDATE elog SET read_mark='Y' WHERE sl_no=" & nextdayioelog121_datarow(niocnt).Item("sl_no") & "")
                                            close1()
                                        Next
                                        logoutdate = nextdate
                                    End If
                                End If
                                totminute = DateDiff(DateInterval.Minute, first_in_1, last_out_1)
                            Else
                                Dim fnd_inout As Integer = 1
                                Dim itime As Date = #1/1/1753#
                                Dim otime As Date = #1/1/1753#
                                Dim ltime As Date = #1/1/1753#
                                For for_dayshiftcnt As Integer = 0 To drday_elod_datarow.Length - 1
                                    first_in = Format(drday_elod_datarow(0).Item("log_time"), "HH:mm:ss")
                                    last_out = Format(drday_elod_datarow(drday_elod_datarow.Length - 1).Item("log_time"), "HH:mm:ss")
                                    ltime = drday_elod_datarow(for_dayshiftcnt).Item("log_time")
                                    split_minute = 0
                                    If fnd_inout Mod 2 = 0 Then
                                        iostatus = "OUT"
                                        otime = ltime
                                        split_minute = DateDiff(DateInterval.Minute, itime, otime)
                                    Else
                                        iostatus = "IN"
                                        itime = ltime
                                    End If
                                    totminute = totminute + split_minute
                                    inoutsrting = inoutsrting & Format(drday_elod_datarow(for_dayshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                    fnd_inout = fnd_inout + 1
                                    drday_elod_datarow(for_dayshiftcnt).BeginEdit()
                                    drday_elod_datarow(for_dayshiftcnt).Item("read_mark") = "Y"
                                    drday_elod_datarow(for_dayshiftcnt).EndEdit()
                                    start1()
                                    SQLInsert("UPDATE elog SET read_mark='Y' WHERE sl_no=" & drday_elod_datarow(for_dayshiftcnt).Item("sl_no") & "")
                                    close1()
                                Next
                            End If
                        End If
                    End If
                    If autoshift_sl = 0 Then
                        inoutsrting = ""
                        first_in = #1/1/1753#
                        last_out = #1/1/1753#
                    End If
                ElseIf shifttp = "4" Then
                    Dim autoshift_sl As Integer = 0
                    Dim stime, new_time, etime, chk_time As Date
                    Dim drnightshift_check As DataRow()
                    Dim last_punch_time As DateTime = "01/01/1900 00:00:00"
                    drnightshift_check = ds_atndnight.Tables(0).Select("staf_sl=" & stafsl & " AND logout_date='" & Format(sdt, "dd/MMM/yyyy") & "'")
                    If drnightshift_check.Length <> 0 Then
                        start1()
                        SQLInsert("UPDATE elog SET read_mark='Y' WHERE staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND log_time <='" & Format(drnightshift_check(0).Item("last_punch"), "HH:mm:ss") & "'")
                        close1()
                        last_punch_time = "01/01/1900 " & Format(drnightshift_check(0).Item("last_punch"), "HH:mm:ss")
                    End If
                    Dim ds111 As DataSet = get_dataset("select  top 1 * from elog WHERE staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND (log_time >'" & Format(last_punch_time, "HH:mm:ss") & "') AND read_mark='N' AND view_report='Y' AND DeviceDirection='IN' ORDER BY log_time")
                    If ds111.Tables(0).Rows.Count <> 0 Then
                        Dim st_sl As Integer = 0
                        Dim end_sl As Integer = 0
                        st_sl = ds111.Tables(0).Rows(0).Item("sl_no")
                        end_sl = st_sl
                        shiftsl = 0
                        shiftnm = "No"
                        Dim dsshiftfind As DataSet = get_dataset("SELECT shift_mst.* FROM cycle1 RIGHT OUTER JOIN cycle2 ON cycle1.cycle_sl = cycle2.cycle_sl LEFT OUTER JOIN shift_mst ON cycle2.shift_sl = shift_mst.shift_sl WHERE dateadd(minute,-" & inpunch_stbfr & ",shift_sdt) <= '" & ds111.Tables(0).Rows(0).Item("log_time") & "' AND dateadd(minute," & inpunch_endafter & ",shift_sdt) >= '" & ds111.Tables(0).Rows(0).Item("log_time") & "'  AND cycle2.cycle_sl=" & cyclesl & "")
                        If dsshiftfind.Tables(0).Rows.Count <> 0 Then
                            shiftsl = dsshiftfind.Tables(0).Rows(0).Item("shift_sl")
                            shiftnm = dsshiftfind.Tables(0).Rows(0).Item("shift_nm")
                        End If
                        stime = ds111.Tables(0).Rows(0).Item("log_time")
                        new_time = CDate(Format(sdt, "MM/dd/yyyy") & " " & Format(stime, "HH:mm:sss"))
                        etime = new_time.AddMinutes(maximum_otminute + (s_workhour * 60))
                        chk_time = "01/01/1900 " & Format(stime.AddMinutes(maximum_otminute + (s_workhour * 60)), "HH:mm:sss")
                        Dim dsSAMEDAY_LASTPUNCH As DataSet = get_dataset("select  top 1 * from elog WHERE staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND sl_no > " & st_sl & " AND read_mark='N' AND view_report='Y' AND DeviceDirection='OUT' ORDER BY log_time")
                        If dsSAMEDAY_LASTPUNCH.Tables(0).Rows.Count <> 0 Then
                            end_sl = dsSAMEDAY_LASTPUNCH.Tables(0).Rows(0).Item("sl_no")
                        End If
                        Dim dslastpunch As DataSet = get_dataset("select  top 1 * from elog WHERE staf_sl=" & stafsl & " AND log_dt='" & Format(etime, "dd/MMM/yyyy") & "' AND log_time <='" & chk_time & "' AND read_mark='N' AND view_report='Y' AND DeviceDirection='OUT' ORDER BY log_time desc")
                        If dslastpunch.Tables(0).Rows.Count <> 0 Then
                            end_sl = dslastpunch.Tables(0).Rows(0).Item("sl_no")
                        End If
                        logoutdate = sdt
                        Dim dspunchdata As DataSet = get_dataset("SELECT * FROM elog WHERE staf_sl=" & stafsl & " AND sl_no>=" & st_sl & " AND sl_no <=" & end_sl & "  ORDER BY log_dt,Log_time")
                        If dspunchdata.Tables(0).Rows.Count <> 0 Then
                            Dim itime As Date = #1/1/1753#
                            Dim otime As Date = #1/1/1753#
                            Dim ltime As Date = #1/1/1753#
                            For for_dayshiftcnt As Integer = 0 To dspunchdata.Tables(0).Rows.Count - 1
                                first_in = Format(dspunchdata.Tables(0).Rows(0).Item("log_time"), "HH:mm:ss")
                                last_out = Format(dspunchdata.Tables(0).Rows(dspunchdata.Tables(0).Rows.Count - 1).Item("log_time"), "HH:mm:ss")
                                logoutdate = dspunchdata.Tables(0).Rows(dspunchdata.Tables(0).Rows.Count - 1).Item("log_dt")
                                ltime = CDate(Format(dspunchdata.Tables(0).Rows(for_dayshiftcnt).Item("log_dt"), "MM/dd/yyyy") & " " & Format(dspunchdata.Tables(0).Rows(for_dayshiftcnt).Item("log_time"), "HH:mm:sss"))
                                split_minute = 0
                                If dspunchdata.Tables(0).Rows(for_dayshiftcnt).Item("DeviceDirection") = "OUT" Then
                                    iostatus = "OUT"
                                    otime = ltime
                                    split_minute = DateDiff(DateInterval.Minute, itime, otime)
                                Else
                                    iostatus = "IN"
                                    itime = ltime
                                End If
                                totminute = totminute + split_minute
                                inoutsrting = inoutsrting & Format(dspunchdata.Tables(0).Rows(for_dayshiftcnt).Item("log_time"), "HH:mm:ss") & "(" & iostatus & "); "
                                start1()
                                SQLInsert("UPDATE elog SET read_mark='Y' WHERE sl_no=" & dspunchdata.Tables(0).Rows(for_dayshiftcnt).Item("sl_no") & "")
                                close1()
                            Next
                        End If
                    End If
                End If
                Dim leavenm As String = ""
                Dim holidaynm As String = ""
                Dim reason As String = ""
                Dim WEEKLYOFF As String = ""
                Dim status As String = "ABSENT"
                Dim logtp As String = "DEVICE"
                Dim final_tot_late As Integer = 0
                Dim final_tot_earlygo As Integer = 0
                Dim final_tot_earlyin As Integer = 0
                Dim final_tot_lateout As Integer = 0
                Dim final_tot_workinghour As Integer = 0
                Dim fnd_holiday As Integer = 0
                Dim hld_datarow() As DataRow
                hld_datarow = ds_hld.Tables(0).Select("holi_dt='" & Format(sdt, "dd/MMM/yyyy") & "' AND (div_sl=0 OR div_sl=" & div_sl & ")")
                If hld_datarow.Length <> 0 Then
                    status = "HOLIDAY"
                    holidaynm = hld_datarow(0).Item("Expr1")
                    fnd_holiday = 1
                End If
                If inoutsrting <> "" Then
                    status = "PRESENT"
                    If is_presentonho = "N" Then
                        If fnd_holiday = 1 Then
                            status = "HOLIDAY"
                        End If
                    End If
                    Dim logtp_datarow() As DataRow
                    logtp_datarow = ds_elog.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(sdt, "dd/MMM/yyyy") & "'")
                    If logtp_datarow.Length <> 0 Then
                        If logtp_datarow(0).Item("log_tp") = "M" Then
                            logtp = "Manual"
                        ElseIf logtp_datarow(0).Item("log_tp") = "A" Then
                            logtp = "Android"
                        Else
                            logtp = "Device"
                        End If
                    End If
                    If latecoming_status = "Y" Then
                        If (sttime.Hour * 60 + sttime.Minute) + latein_minute < (first_in.Hour * 60 + first_in.Minute) Then
                            final_tot_late = (first_in.Hour * 60 + first_in.Minute) - (sttime.Hour * 60 + sttime.Minute) - latein_minute
                        End If
                    End If
                    'early going
                    If earlygoing_status = "Y" Then
                        If (edtime.Hour * 60 + edtime.Minute) - earlygo_minute > (last_out.Hour * 60 + last_out.Minute) Then
                            If (last_out.Hour * 60 + last_out.Minute) <> 0 Then
                                final_tot_earlygo = (edtime.Hour * 60 + edtime.Minute) - (last_out.Hour * 60 + last_out.Minute)
                            End If
                        End If
                    End If
                    'early in
                    If (sttime.Hour * 60 + sttime.Minute) > (first_in.Hour * 60 + first_in.Minute) Then
                        final_tot_earlyin = (sttime.Hour * 60 + sttime.Minute) - (first_in.Hour * 60 + first_in.Minute)
                    End If
                    'lateout
                    If (last_out.Hour * 60 + last_out.Minute) <> 0 Then
                        If Is_nightshift = True Then
                            final_tot_lateout = (1440 - (last_out.Hour * 60 + last_out.Minute)) + (edtime.Hour * 60 + edtime.Minute)
                        Else
                            If (edtime.Hour * 60 + edtime.Minute) < (last_out.Hour * 60 + last_out.Minute) Then
                                final_tot_lateout = (last_out.Hour * 60 + last_out.Minute) - (edtime.Hour * 60 + edtime.Minute)
                            End If
                        End If
                    End If
                    final_tot_workinghour = totminute - lunchbreak_minute
                    If is_halfday = "1" Then
                        If final_tot_workinghour < halfday_iflessthan_min Then
                            status = "HALFDAY"
                        End If
                    End If
                    If is_absent = "1" Then
                        If final_tot_workinghour < absent_iflessthan_min Then
                            status = "ABSENT"
                        End If
                    End If
                    If weeklyoff_tp = "1" Then
                        If week_off1 = "0" Then
                            If sdt.DayOfWeek = DayOfWeek.Sunday Then
                                WEEKLYOFF = "SUNDAY"
                                status = "WEEKLYOFF"
                            End If
                        ElseIf week_off1 = "1" Then
                            If sdt.DayOfWeek = DayOfWeek.Monday Then
                                WEEKLYOFF = "MONDAY"
                                status = "WEEKLYOFF"
                            End If
                        ElseIf week_off1 = "2" Then
                            If sdt.DayOfWeek = DayOfWeek.Tuesday Then
                                WEEKLYOFF = "TUESDAY"
                                status = "WEEKLYOFF"
                            End If
                        ElseIf week_off1 = "3" Then
                            If sdt.DayOfWeek = DayOfWeek.Wednesday Then
                                WEEKLYOFF = "WEDNESDAY"
                                status = "WEEKLYOFF"
                            End If
                        ElseIf week_off1 = "4" Then
                            If sdt.DayOfWeek = DayOfWeek.Thursday Then
                                WEEKLYOFF = "THURSDAY"
                                status = "WEEKLYOFF"
                            End If
                        ElseIf week_off1 = "5" Then
                            If sdt.DayOfWeek = DayOfWeek.Friday Then
                                WEEKLYOFF = "FRIDAY"
                                status = "WEEKLYOFF"
                            End If
                        ElseIf week_off1 = "6" Then
                            If sdt.DayOfWeek = DayOfWeek.Saturday Then
                                WEEKLYOFF = "SATURDAY"
                                status = "WEEKLYOFF"
                            End If
                        End If
                        Dim if_snd_wo As Integer = 0
                        Dim d_of_week As Integer = 0
                        If week_off2 = "0" Then
                            If sdt.DayOfWeek = DayOfWeek.Sunday Then
                                WEEKLYOFF = "SUNDAY"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Sunday
                            End If
                        ElseIf week_off2 = "1" Then
                            If sdt.DayOfWeek = DayOfWeek.Monday Then
                                WEEKLYOFF = "MONDAY"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Monday
                            End If
                        ElseIf week_off2 = "2" Then
                            If sdt.DayOfWeek = DayOfWeek.Tuesday Then
                                WEEKLYOFF = "TUESDAY"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Tuesday
                            End If
                        ElseIf week_off2 = "3" Then
                            If sdt.DayOfWeek = DayOfWeek.Wednesday Then
                                WEEKLYOFF = "WEDNESDAY"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Wednesday
                            End If
                        ElseIf week_off2 = "4" Then
                            If sdt.DayOfWeek = DayOfWeek.Thursday Then
                                WEEKLYOFF = "THURSDAY"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Thursday
                            End If
                        ElseIf week_off2 = "5" Then
                            If sdt.DayOfWeek = DayOfWeek.Friday Then
                                WEEKLYOFF = "FRIDAY"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Friday
                            End If
                        ElseIf week_off2 = "6" Then
                            If sdt.DayOfWeek = DayOfWeek.Saturday Then
                                WEEKLYOFF = "SATURDAY"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Saturday
                            End If
                        End If
                        If if_snd_wo = 1 Then
                            If Get_weekday_position(sdt, d_of_week) = 1 Then
                                If day_off1 = "1" Then
                                    status = "WEEKLYOFF"
                                Else
                                    WEEKLYOFF = ""
                                End If
                            ElseIf Get_weekday_position(sdt, d_of_week) = 2 Then
                                If day_off2 = "1" Then
                                    status = "WEEKLYOFF"
                                Else
                                    WEEKLYOFF = ""
                                End If
                            ElseIf Get_weekday_position(sdt, d_of_week) = 3 Then
                                If day_off3 = "1" Then
                                    status = "WEEKLYOFF"
                                Else
                                    WEEKLYOFF = ""
                                End If
                            ElseIf Get_weekday_position(sdt, d_of_week) = 4 Then
                                If day_off4 = "1" Then
                                    status = "WEEKLYOFF"
                                Else
                                    WEEKLYOFF = ""
                                End If
                            ElseIf Get_weekday_position(sdt, d_of_week) = 5 Then
                                If day_off5 = "1" Then
                                    status = "WEEKLYOFF"
                                Else
                                    WEEKLYOFF = ""
                                End If
                            End If
                        End If
                    Else
                        Dim dsweeklyoff As DataSet = get_dataset("SELECT top 1 day1,week_day FROM wov1 WHERE loc_cd=" & loc_cd & " and staf_sl=" & stafsl & " and effective_date <='" & Format(sdt, "dd/MMM/yyyy") & "'")
                        If dsweeklyoff.Tables(0).Rows.Count <> 0 Then
                            If sdt.DayOfWeek = dsweeklyoff.Tables(0).Rows(0).Item("week_day") Then
                                WEEKLYOFF = dsweeklyoff.Tables(0).Rows(0).Item("day1")
                                status = "WEEKLYOFF"
                            End If
                        End If
                    End If
                    If status = "WEEKLYOFF" Then
                        If is_presentonwo = "Y" Then
                            status = "PRESENT"
                        End If
                    End If
                    Dim shifttime_min As Integer = 0
                    Dim final_tot_otmin As Integer = 0
                    shifttime_min = (shift_hour.Hour * 60) + shift_hour.Minute
                    If otallow = "Y" Then
                        If otformula = 1 Then
                            final_tot_otmin = 0
                        ElseIf otformula = 2 Then
                            final_tot_otmin = final_tot_earlyin + final_tot_lateout
                        ElseIf otformula = 3 Then
                            final_tot_otmin = final_tot_workinghour - shifttime_min
                        ElseIf otformula = 4 Then
                            final_tot_otmin = final_tot_lateout
                        ElseIf otformula = 5 Then
                            final_tot_otmin = final_tot_earlyin
                        ElseIf otformula = 6 Then
                            final_tot_otmin = final_tot_workinghour - Val(s_workhour * 60)
                        End If
                        If final_tot_otmin < 0 Then
                            final_tot_otmin = 0
                        End If
                        If status = "WEEKLYOFF" Then
                            If oton_weekoff = "H" Then
                                final_tot_otmin = final_tot_workinghour / 2
                            ElseIf oton_weekoff = "F" Then
                                final_tot_otmin = final_tot_workinghour
                            End If
                        End If
                        If status = "HOLIDAY" Then
                            If oton_holiday = "H" Then
                                final_tot_otmin = final_tot_workinghour / 2
                            ElseIf oton_holiday = "F" Then
                                final_tot_otmin = final_tot_workinghour
                            End If
                        End If
                    End If
                    'mark weeoff and holiday as absent if prefix day
                    If when_prefixday_absent = "1" Then
                        Dim dratnd_datarow() As DataRow
                        dratnd_datarow = ds_atnd.Tables(0).Select("device_code='" & empdevcd & "' AND log_dt='" & Format(sdt.AddDays(-1)) & "'")
                        If dratnd_datarow.Length <> 0 Then
                            If dratnd_datarow(0).Item("day_status") = "ABSENT" Then
                                status = "ABSENT"
                                final_tot_workinghour = 0
                                final_tot_earlyin = 0
                                final_tot_late = 0
                                final_tot_earlygo = 0
                                final_tot_lateout = 0
                                final_tot_otmin = 0
                            End If
                        End If
                    End If
                    'insert present data into dtatnd datatable
                    dr_atnd = dt_atnd.NewRow
                    dr_atnd(0) = 1
                    dr_atnd(1) = stafsl
                    dr_atnd(2) = empnm
                    dr_atnd(3) = empcd
                    dr_atnd(4) = empdevcd
                    dr_atnd(5) = deptnm
                    dr_atnd(6) = desgnm
                    dr_atnd(7) = Format(sdt, "dd/MMM/yyyy")
                    dr_atnd(8) = Format(logoutdate, "dd/MMM/yyyy")
                    dr_atnd(9) = status
                    dr_atnd(10) = shiftnm
                    dr_atnd(11) = Format(sdt, "dd/MMM/yyyy") & " " & Format(first_in, "HH:mm:ss")
                    dr_atnd(12) = Format(logoutdate, "dd/MMM/yyyy") & " " & Format(last_out, "HH:mm:ss")
                    dr_atnd(13) = logtp
                    dr_atnd(14) = final_tot_earlyin
                    dr_atnd(15) = final_tot_late
                    dr_atnd(16) = final_tot_earlygo
                    dr_atnd(17) = final_tot_lateout
                    dr_atnd(18) = final_tot_workinghour
                    dr_atnd(19) = final_tot_otmin
                    dr_atnd(20) = WEEKLYOFF
                    dr_atnd(21) = holidaynm
                    dr_atnd(22) = leavenm
                    dr_atnd(23) = reason
                    dr_atnd(24) = inoutsrting
                    dr_atnd(25) = loc_cd
                    dr_atnd(26) = shiftsl
                    dr_atnd(27) = 0
                    dt_atnd.Rows.Add(dr_atnd)
                Else 'absent ,holiday,WEEKLYOFF
                    'when no log data found
                    If weeklyoff_tp = "1" Then
                        'If week_off1 = "1" Then
                        '    If sdt.DayOfWeek = DayOfWeek.Sunday Then
                        '        WEEKLYOFF = ""
                        '        status = ""
                        '    End If
                        'Else
                        If week_off1 = "0" Then
                            If sdt.DayOfWeek = DayOfWeek.Sunday Then
                                WEEKLYOFF = "SUNDAY"
                                status = "WEEKLYOFF"
                            End If
                        ElseIf week_off1 = "1" Then
                            If sdt.DayOfWeek = DayOfWeek.Monday Then
                                WEEKLYOFF = "MONDAY"
                                status = "WEEKLYOFF"
                            End If
                        ElseIf week_off1 = "2" Then
                            If sdt.DayOfWeek = DayOfWeek.Tuesday Then
                                WEEKLYOFF = "TUESDAY"
                                status = "WEEKLYOFF"
                            End If
                        ElseIf week_off1 = "3" Then
                            If sdt.DayOfWeek = DayOfWeek.Wednesday Then
                                WEEKLYOFF = "WEDNESDAY"
                                status = "WEEKLYOFF"
                            End If
                        ElseIf week_off1 = "4" Then
                            If sdt.DayOfWeek = DayOfWeek.Thursday Then
                                WEEKLYOFF = "THURDAY"
                                status = "WEEKLYOFF"
                            End If
                        ElseIf week_off1 = "5" Then
                            If sdt.DayOfWeek = DayOfWeek.Friday Then
                                WEEKLYOFF = "FRIDAY"
                                status = "WEEKLYOFF"
                            End If
                        ElseIf week_off1 = "6" Then
                            If sdt.DayOfWeek = DayOfWeek.Saturday Then
                                WEEKLYOFF = "SATURDAY"
                                status = "WEEKLYOFF"
                            End If
                        End If
                        Dim if_snd_wo As Integer = 0
                        Dim d_of_week As Integer = 0
                        'If week_off2 = "1" Then
                        '    If sdt.DayOfWeek = DayOfWeek.Sunday Then
                        '        WEEKLYOFF = ""
                        '        status = ""
                        '    End If
                        'Else
                        If week_off2 = "0" Then
                            If sdt.DayOfWeek = DayOfWeek.Sunday Then
                                WEEKLYOFF = "Sunday"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Sunday
                            End If
                        ElseIf week_off2 = "1" Then
                            If sdt.DayOfWeek = DayOfWeek.Monday Then
                                WEEKLYOFF = "MONDAY"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Monday
                            End If
                        ElseIf week_off2 = "2" Then
                            If sdt.DayOfWeek = DayOfWeek.Tuesday Then
                                WEEKLYOFF = "TUESDAY"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Tuesday
                            End If
                        ElseIf week_off2 = "3" Then
                            If sdt.DayOfWeek = DayOfWeek.Wednesday Then
                                WEEKLYOFF = "WEDNESDAY"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Wednesday
                            End If
                        ElseIf week_off2 = "4" Then
                            If sdt.DayOfWeek = DayOfWeek.Thursday Then
                                WEEKLYOFF = "THURSDAY"
                                status = "WEEKLYOFF"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Thursday
                            End If
                        ElseIf week_off2 = "5" Then
                            If sdt.DayOfWeek = DayOfWeek.Friday Then
                                WEEKLYOFF = "FRIDAY"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Friday
                            End If
                        ElseIf week_off2 = "6" Then
                            If sdt.DayOfWeek = DayOfWeek.Saturday Then
                                WEEKLYOFF = "SATURDAY"
                                if_snd_wo = 1
                                d_of_week = DayOfWeek.Saturday
                            End If
                        End If
                        If if_snd_wo = 1 Then
                            If Get_weekday_position(sdt, d_of_week) = 1 Then
                                If day_off1 = "1" Then
                                    status = "WEEKLYOFF"
                                Else
                                    WEEKLYOFF = ""
                                End If
                            ElseIf Get_weekday_position(sdt, d_of_week) = 2 Then
                                If day_off2 = "1" Then
                                    status = "WEEKLYOFF"
                                Else
                                    WEEKLYOFF = ""
                                End If
                            ElseIf Get_weekday_position(sdt, d_of_week) = 3 Then
                                If day_off3 = "1" Then
                                    status = "WEEKLYOFF"
                                Else
                                    WEEKLYOFF = ""
                                End If
                            ElseIf Get_weekday_position(sdt, d_of_week) = 4 Then
                                If day_off4 = "1" Then
                                    status = "WEEKLYOFF"
                                Else
                                    WEEKLYOFF = ""
                                End If
                            ElseIf Get_weekday_position(sdt, d_of_week) = 5 Then
                                If day_off5 = "1" Then
                                    status = "WEEKLYOFF"
                                Else
                                    WEEKLYOFF = ""
                                End If
                            End If
                        End If
                    Else 'WHEN MANUAL WEEKLYOFF SETTING
                        Dim dsweeklyoff As DataSet = get_dataset("SELECT top 1 day1,week_day FROM wov1 WHERE loc_cd=" & loc_cd & " and staf_sl=" & stafsl & " and effective_date <='" & Format(sdt, "dd/MMM/yyyy") & "'")
                        If dsweeklyoff.Tables(0).Rows.Count <> 0 Then
                            If sdt.DayOfWeek = dsweeklyoff.Tables(0).Rows(0).Item("week_day") Then
                                WEEKLYOFF = dsweeklyoff.Tables(0).Rows(0).Item("day1")
                                status = "WEEKLYOFF"
                            End If
                        End If
                        'Dim wov_datarow() As DataRow
                        'wov_datarow = ds_wov.Tables(0).Select("staf_sl=" & stafsl & "")
                        'If wov_datarow.Length <> 0 Then
                        '    For wovcnt As Integer = 0 To wov_datarow.Length - 1
                        '        If wov_datarow(wovcnt).Item("first_dt") <> "" Then
                        '            If CDate(wov_datarow(wovcnt).Item("first_dt")) = sdt Then
                        '                WEEKLYOFF = wov_datarow(wovcnt).Item("day1")
                        '                status = "WEEKLYOFF"
                        '            End If
                        '        End If
                        '        If wov_datarow(wovcnt).Item("snd_dt") <> "" Then
                        '            If CDate(wov_datarow(wovcnt).Item("snd_dt")) = sdt Then
                        '                WEEKLYOFF = wov_datarow(wovcnt).Item("day2")
                        '                status = "WEEKLYOFF"
                        '            End If
                        '        End If
                        '        If wov_datarow(wovcnt).Item("third_dt") <> "" Then
                        '            If CDate(wov_datarow(wovcnt).Item("third_dt")) = sdt Then
                        '                WEEKLYOFF = wov_datarow(wovcnt).Item("day3")
                        '                status = "WEEKLYOFF"
                        '            End If
                        '        End If
                        '        If wov_datarow(wovcnt).Item("forth_dt") <> "" Then
                        '            If CDate(wov_datarow(wovcnt).Item("forth_dt")) = sdt Then
                        '                WEEKLYOFF = wov_datarow(wovcnt).Item("day4")
                        '                status = "WEEKLYOFF"
                        '            End If
                        '        End If
                        '        If wov_datarow(wovcnt).Item("fifth_dt") <> "" Then
                        '            If CDate(wov_datarow(wovcnt).Item("fifth_dt")) = sdt Then
                        '                WEEKLYOFF = wov_datarow(wovcnt).Item("day5")
                        '                status = "WEEKLYOFF"
                        '            End If
                        '        End If
                        '    Next
                        'End If
                    End If
                    Dim check_weekoff_or_holiday As Integer = 0
                    If status = "WEEKLYOFF" Then
                        check_weekoff_or_holiday = 1
                    ElseIf status = "HOLIDAY" Then
                        check_weekoff_or_holiday = 1
                    End If
                    If check_weekoff_or_holiday = 1 Then
                        If when_prefixday_absent = "1" Then
                            Dim dratnd_datarow() As DataRow
                            dratnd_datarow = ds_atnd.Tables(0).Select("staf_sl=" & stafsl & " AND log_dt='" & Format(sdt.AddDays(-1), "dd/MMM/yyyy") & "'")
                            If dratnd_datarow.Length <> 0 Then
                                If dratnd_datarow(0).Item("day_status") = "ABSENT" Then
                                    status = "ABSENT"
                                End If
                            End If
                        End If
                    End If
                    'FIND LEAVEDAY
                    Dim lvday_datarow() As DataRow
                    lvday_datarow = ds_lvoucher.Tables(0).Select("staf_sl=" & stafsl & " AND l_dt='" & Format(sdt, "dd/MMM/yyyy") & "'")
                    If lvday_datarow.Length <> 0 Then
                        reason = lvday_datarow(0).Item("reason")
                        status = "LEAVEDAY"
                        Dim lvsl As Integer = lvday_datarow(0).Item("leave_sl")
                        Dim lvnm_datarow() As DataRow
                        lvnm_datarow = ds_lvmst.Tables(0).Select("leave_sl=" & lvsl & "")
                        leavenm = lvnm_datarow(0).Item("leave_nm")
                        leave_sl = lvday_datarow(0).Item("leave_sl")
                    End If

                    dr_atnd = dt_atnd.NewRow
                    dr_atnd(0) = 1
                    dr_atnd(1) = stafsl
                    dr_atnd(2) = empnm
                    dr_atnd(3) = empcd
                    dr_atnd(4) = empdevcd
                    dr_atnd(5) = deptnm
                    dr_atnd(6) = desgnm
                    dr_atnd(7) = Format(sdt, "dd/MMM/yyyy")
                    dr_atnd(8) = Format(sdt, "dd/MMM/yyyy")
                    dr_atnd(9) = status
                    dr_atnd(10) = ""
                    dr_atnd(11) = Format(sdt, "dd/MMM/yyyy") & " " & Format(first_in, "HH:mm:ss")
                    dr_atnd(12) = Format(sdt, "dd/MMM/yyyy") & " " & Format(last_out, "HH:mm:ss")
                    dr_atnd(13) = ""
                    dr_atnd(14) = 0
                    dr_atnd(15) = 0
                    dr_atnd(16) = 0
                    dr_atnd(17) = 0
                    dr_atnd(18) = 0
                    dr_atnd(19) = 0
                    dr_atnd(20) = WEEKLYOFF
                    dr_atnd(21) = holidaynm
                    dr_atnd(22) = leavenm
                    dr_atnd(23) = reason
                    dr_atnd(24) = ""
                    dr_atnd(25) = loc_cd
                    dr_atnd(26) = 0
                    dr_atnd(27) = leave_sl
                    dt_atnd.Rows.Add(dr_atnd)
                End If
                sdt = sdt.AddDays(1)
            Loop
        Next
        ds_emp.Tables(0).Rows.Clear()
        start1()
        For i As Integer = 0 To dt_atnd.Rows.Count - 1
            SQLInsert("INSERT INTO atnd(staf_sl,staf_nm,emp_code,device_code,log_dt,shift_nm,dept_nm,first_punch,last_punch," & _
            "log_tp,early_in,late_in,early_out,late_out,tot_hour,leave_nm,holiday_nm,leave_reason,in_out,over_time,week_off," & _
            "day_status,desg_nm,loc_cd,logout_date,totwhour_min,totot_min,tot_otmin,shift_sl,leave_sl) VALUES(" & dt_atnd.Rows(i).Item("staf_sl") & _
            ",'" & dt_atnd.Rows(i).Item("staf_nm") & "','" & dt_atnd.Rows(i).Item("emp_code") & "','" & dt_atnd.Rows(i).Item("device_code") & "','" & _
            dt_atnd.Rows(i).Item("log_dt") & "','" & dt_atnd.Rows(i).Item("shift_nm") & "','" & dt_atnd.Rows(i).Item("dept_nm") & "','" & _
            dt_atnd.Rows(i).Item("first_punch") & "','" & dt_atnd.Rows(i).Item("last_punch") & "','" & dt_atnd.Rows(i).Item("log_tp") & "'," & _
            dt_atnd.Rows(i).Item("early_in") & "," & dt_atnd.Rows(i).Item("late_in") & "," & dt_atnd.Rows(i).Item("early_out") & "," & _
            dt_atnd.Rows(i).Item("late_out") & "," & dt_atnd.Rows(i).Item("tot_hour") & ",'" & dt_atnd.Rows(i).Item("leave_nm") & "','" & _
            dt_atnd.Rows(i).Item("holiday_nm") & "','" & dt_atnd.Rows(i).Item("leave_reason") & "','" & dt_atnd.Rows(i).Item("in_out") & "','','" & _
            dt_atnd.Rows(i).Item("week_off") & "','" & dt_atnd.Rows(i).Item("day_status") & "','" & dt_atnd.Rows(i).Item("desg_nm") & "'," & loc_cd & ",'" & _
            dt_atnd.Rows(i).Item("logout_date") & "',0,0," & dt_atnd.Rows(i).Item("tot_otmin") & "," & dt_atnd.Rows(i).Item("shift_sl") & "," & dt_atnd.Rows(i).Item("leave_sl") & ")")
        Next
        close1()
    End Sub

    Private Function Get_weekday_position(ByVal todt_ofmonth As Date, ByVal dow As Integer) As Integer
        Dim dt1 As New Date(todt_ofmonth.Year, todt_ofmonth.Month, 1)
        Dim dt2 As Date = todt_ofmonth
        Dim dpos As Integer = 0
        Dim days As Integer = (From d As Date In _
        (Enumerable.Range(0, 1 + dt2.Subtract(dt1).Days).Select(Function(offset) dt1.AddDays(offset)).ToArray()) _
        Where d.DayOfWeek = dow Select 1).Sum
        Return days
    End Function

End Class
