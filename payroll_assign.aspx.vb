﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class payroll_assign
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtassno.Text = ""
        txtempcode.Text = ""
        txtstafsl.Text = ""
        txtstafnm.Text = ""
        txtdept.Text = ""
        txtdesg.Text = ""
        txtfordt.Text = Format(Now, "dd/MM/yyyy")
        Me.head_display()
        dvstaf.DataSource = Nothing
        dvstaf.DataBind()
        txtdate.Text = Format(Now, "dd/MM/yyyy")
        txttotdeduction.Text = "0.00"
        txttotearning.Text = "0.00"
        txtamount.Text = "0.00"
        txtsl.Text = "1"
    End Sub

    Private Sub head_display()
        cmbearnings.Items.Clear()
        cmbearnings.Items.Add("Select Head")
        Dim ds As DataSet = get_dataset("SELECT head_name FROM head_mst ORDER BY head_name")
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            cmbearnings.Items.Add(ds.Tables(0).Rows(i).Item(0))
        Next
    End Sub

    Protected Sub cmdsave0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave0.Click
        Dim ds1 As DataSet = get_dataset("SELECT staf.staf_sl,staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm FROM desg_mst RIGHT OUTER JOIN staf ON desg_mst.desg_sl = staf.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl  WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd= " & CType(Session("loc_cd"), Integer) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            Me.clr()
            txtstafsl.Text = ds1.Tables(0).Rows(0).Item("staf_sl")
            txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            txtstafnm.Text = ds1.Tables(0).Rows(0).Item("staf_nm")
            txtdept.Text = ds1.Tables(0).Rows(0).Item("dept_nm")
            txtdesg.Text = ds1.Tables(0).Rows(0).Item("desg_nm")
        End If
    End Sub

    Protected Sub cmdsave2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave2.Click
        If Val(txtstafsl.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide The Valid Employee Code');", True)
            txtempcode.Focus()
            Exit Sub
        End If
        If txtdate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide The Valid Date');", True)
            txtdate.Focus()
            Exit Sub
        End If
        Dim head_sl As Integer = 0
        Dim head_tp As String = ""
        Dim dshead As DataSet = get_dataset("select head_sl,type FROM head_mst WHERE head_name='" & cmbearnings.Text & "'")
        If dshead.Tables(0).Rows.Count <> 0 Then
            head_sl = dshead.Tables(0).Rows(0).Item(0)
            head_tp = dshead.Tables(0).Rows(0).Item(0)
        End If
        If Val(head_sl) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Select The Valid Head');", True)
            cmbearnings.Focus()
            Exit Sub
        End If
        If Val(txtamount.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Amount Should Not Be Zero');", True)
            txtamount.Focus()
            Exit Sub
        End If
        start1()
        Dim ds As DataSet = get_dataset("SELECT ass_no from head_assignment1 WHERE staf_sl=" & Val(txtstafsl.Text) & " AND ass_month=" & stringtodate(txtfordt.Text).Month & " AND ass_year=" & stringtodate(txtfordt.Text).Year & "")
        If ds.Tables(0).Rows.Count = 0 Then
            txtassno.Text = "1"
            Dim ds1 As DataSet = get_dataset("SELECT max(ass_no) FROM head_assignment1")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                txtassno.Text = ds1.Tables(0).Rows(0).Item(0) + 1
            End If
            SQLInsert("INSERT INTO head_assignment1(ass_no,staf_sl,ass_month,ass_year,tot_earning,tot_deduction) VALUES(" & Val(txtassno.Text) & "," & Val(txtstafsl.Text) & _
            "," & stringtodate(txtfordt.Text).Month & "," & stringtodate(txtfordt.Text).Year & ",0,0)")
        End If
        Dim max As Integer = 1
        Dim ds11 As DataSet = get_dataset("SELECT max(ass_sl) FROM head_assignment2")
        If Not IsDBNull(ds11.Tables(0).Rows(0).Item(0)) Then
            max = ds11.Tables(0).Rows(0).Item(0) + 1
        End If
        SQLInsert("INSERT INTO head_assignment2(ass_sl,ass_no,staf_sl,ass_month,ass_year,ass_dt,head_sl,tp,amt) VALUES(" & max & "," & Val(txtassno.Text) & _
        "," & Val(txtstafsl.Text) & "," & stringtodate(txtfordt.Text).Month & "," & stringtodate(txtfordt.Text).Year & ",'" & _
        Format(stringtodate(txtdate.Text), "dd/MMM/yyyy") & "'," & head_sl & ",'" & head_tp & "'," & Val(txtamount.Text) & ")")
        Me.dvdispl()
        SQLInsert("UPDATE head_assignment1 SET tot_earning=" & Val(txttotearning.Text) & ",tot_deduction=" & Val(txttotdeduction.Text) & " WHERE staf_sl=" & Val(txtstafsl.Text) & " AND ass_month=" & stringtodate(txtfordt.Text).Month & " AND ass_year=" & stringtodate(txtfordt.Text).Year & "")
        close1()

        cmbearnings.SelectedIndex = 0
        txtamount.Text = "0.00"
        cmbearnings.Focus()
    End Sub

    Private Sub dvdispl()
        Dim ds As DataSet = get_dataset("SELECT Row_number() OVER(ORDER BY ass_dt) as Sl,head_assignment2.tp,head_assignment2.ass_sl, Convert(varchar,head_assignment2.ass_dt,103) as dt, head_mst.head_name, str(head_assignment2.amt,12,2) as amt FROM head_assignment2 LEFT OUTER JOIN head_mst ON head_assignment2.head_sl = head_mst.head_sl WHERE staf_sl=" & Val(txtstafsl.Text) & " AND ass_month=" & stringtodate(txtfordt.Text).Month & " AND ass_year=" & stringtodate(txtfordt.Text).Year & "")
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
        Dim tot_earning As Decimal = 0
        Dim tot_deduct As Decimal = 0
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            If ds.Tables(0).Rows(i).Item("tp") = "1" Then
                tot_earning = tot_earning + Val(ds.Tables(0).Rows(i).Item("amt"))
            ElseIf ds.Tables(0).Rows(i).Item("tp") = "2" Then
                tot_deduct = tot_deduct + Val(ds.Tables(0).Rows(i).Item("amt"))
            End If
        Next
        txttotearning.Text = Format(Val(tot_earning), "#####0.00")
        txttotdeduction.Text = Format(Val(tot_deduct), "#####0.00")
        txtsl.Text = Val(ds.Tables(0).Rows.Count) + 1
    End Sub

    Protected Sub cmdsave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave1.Click
        If Val(txtstafsl.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide The Valid Employee Code');", True)
            txtempcode.Focus()
            Exit Sub
        End If
        Me.dvdispl()
        cmbearnings.Focus()
    End Sub

    Protected Sub dvstaf_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dvstaf.RowCommand
        Dim r As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim lblslno As Label = dvstaf.Rows(r).FindControl("lblslno")
            start1()
            SQLInsert("DELETE FROM head_assignment2 WHERE ass_sl=" & lblslno.Text & "")
            Me.dvdispl()
            SQLInsert("UPDATE head_assignment1 SET tot_earning=" & Val(txttotearning.Text) & ",tot_deduction=" & Val(txttotdeduction.Text) & " WHERE staf_sl=" & Val(txtstafsl.Text) & " AND ass_month=" & stringtodate(txtfordt.Text).Month & " AND ass_year=" & stringtodate(txtfordt.Text).Year & "")
            close1()
        End If
    End Sub
End Class
