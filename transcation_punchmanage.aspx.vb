﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class transcation_punchmanage
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtempcode.Text = ""
        txtstafsl.Text = ""
        dvstaf.DataSource = Nothing
        dvstaf.DataBind()
        txtstafnm.Text = ""
        txtdept.Text = ""
        txtdesg.Text = ""
        txtsttm.Text = ""
        txtfordt.Text = Format(Now, "dd/MM/yyyy")
        txtto1.Text = Format(Now, "dd/MM/yyyy")
        txtpunchdt.Text = Format(Now, "dd/MM/yyyy")
    End Sub

    Protected Sub cmdsave0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave0.Click
        Dim ds1 As DataSet = get_dataset("SELECT staf.staf_sl,staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm FROM desg_mst RIGHT OUTER JOIN staf ON desg_mst.desg_sl = staf.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl  WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd= " & CType(Session("loc_cd"), Integer) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            Me.clr()
            txtstafsl.Text = ds1.Tables(0).Rows(0).Item("staf_sl")
            txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            txtstafnm.Text = ds1.Tables(0).Rows(0).Item("staf_nm")
            txtdept.Text = ds1.Tables(0).Rows(0).Item("dept_nm")
            txtdesg.Text = ds1.Tables(0).Rows(0).Item("desg_nm")
        End If
    End Sub

    Protected Sub cmdsave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave1.Click
        Me.punch_disp()
        Me.report_display()
    End Sub

    Private Sub report_display()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim str As String = ""
        str = "AND atnd.emp_code='" & Trim(txtempcode.Text) & "'"
        Dim ds1 As DataSet = get_dataset("SELECT convert(varchar,atnd.log_dt,103) AS Date, atnd.staf_nm AS [Staf Name], atnd.emp_code AS [Emp. Code], atnd.device_code AS [Device Code], atnd.dept_nm AS Dept, atnd.desg_nm AS Desg, atnd.day_status AS Status, convert(varchar,atnd.first_punch,108) AS [In], (CASE WHEN convert(varchar,atnd.last_punch,108) <> convert(varchar,atnd.first_punch,108) THEN convert(varchar,atnd.last_punch,108) WHEN convert(varchar,atnd.last_punch,108) = convert(varchar,atnd.first_punch,108) THEN '' END) AS [Out],in_out as [Punches],Cast(tot_hour / 60 as Varchar) + ':'+ Cast(tot_hour % 60 as Varchar) as 'Total(Hour)' FROM atnd LEFT OUTER JOIN staf ON atnd.staf_sl = staf.staf_sl  WHERE staf.emp_status='I' AND log_dt >= '" & Format(stringtodate(txtfordt.Text), "dd/MMM/yyyy") & "' AND log_dt <= '" & Format(stringtodate(txtto1.Text), "dd/MMM/yyyy") & "' AND atnd.loc_cd= " & loc_cd & "  " & str & "  ORDER BY log_dt,atnd.emp_code")
        dvdata.DataSource = ds1.Tables(0)
        dvdata.DataBind()
    End Sub

    Private Sub punch_disp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim staf_sl As Integer = 0
        Dim dsstaf_sl As DataSet = get_dataset("SELECT staf_sl FROM staf WHERE emp_code='" & Trim(txtempcode.Text) & "' AND loc_cd=" & loc_cd & "")
        If dsstaf_sl.Tables(0).Rows.Count <> 0 Then
            staf_sl = dsstaf_sl.Tables(0).Rows(0).Item("staf_sl")
        End If
        If txtfordt.Text = "" Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide A Valid Date.');", True)
            txtfordt.Focus()
            Exit Sub
        End If
        If txtto1.Text = "" Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide A Valid Date.');", True)
            txtto1.Focus()
            Exit Sub
        End If
        Dim ds As DataSet = get_dataset("select Convert(varchar,log_dt,103) as log_dt1,log_time,sl_no,view_report,DeviceDirection,(CASE WHEN log_tp='E' THEN 'Manual' WHEN log_tp='D' THEN 'Device'  WHEN log_tp='M' THEN 'Outdoor' END) as tp from elog WHERE loc_cd=" & loc_cd & " and log_dt>='" & Format(stringtodate(txtfordt.Text)) & "' AND log_dt<='" & Format(stringtodate(txtto1.Text)) & "' AND staf_sl=" & staf_sl & " ORDER BY log_dt,log_time")
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
    End Sub

    Protected Sub cmdsave2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave2.Click
        Dim fnd As Integer = 0
        For i As Integer = 0 To dvstaf.Rows.Count - 1
            Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                fnd = 1
                Exit For
            End If
        Next
        If fnd = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Select Atleast One Punch');", True)
            dvstaf.Focus()
            Exit Sub
        End If

        For j As Integer = 0 To dvstaf.Rows.Count - 1
            Dim chk As CheckBox = dvstaf.Rows(j).FindControl("chk")
            If chk.Checked = True Then
                Dim lblsl As Label = dvstaf.Rows(j).FindControl("lblslno")
                start1()
                SQLInsert("UPDATE elog SET view_report ='N' WHERE sl_no=" & lblsl.Text & "")
                close1()
            End If
        Next
        Me.punch_disp()
    End Sub

    Protected Sub cmdsave3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave3.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim staf_sl As Integer = 0
        Dim device_code As String = ""
        Dim dsstaf_sl As DataSet = get_dataset("SELECT staf_sl,device_code FROM staf WHERE emp_code='" & Trim(txtempcode.Text) & "' AND loc_cd=" & loc_cd & "")
        If dsstaf_sl.Tables(0).Rows.Count <> 0 Then
            staf_sl = dsstaf_sl.Tables(0).Rows(0).Item("staf_sl")
            device_code = dsstaf_sl.Tables(0).Rows(0).Item("device_code")
        End If
        If Trim(txtpunchdt.Text) = "" Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide A Punch date.');", True)
            txtpunchdt.Focus()
            Exit Sub
        End If
        If Trim(txtsttm.Text) = "" Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide A Punch Time.');", True)
            txtsttm.Focus()
            Exit Sub
        End If
        Dim slno_cnt As Integer = 1
        Dim dsmaxslno As DataSet = get_dataset("SELECT isnull(max(sl_no),1) FROM elog")
        If dsmaxslno.Tables(0).Rows(0).Item(0) > 1 Then
            slno_cnt = dsmaxslno.Tables(0).Rows(0).Item(0) + 1
        End If
        Dim stm As Date
        stm = CDate(Trim(txtsttm.Text))
        start1()
        SQLInsert("INSERT INTO elog(sl_no,device_code,log_dt,log_time,log_tp,slno,read_mark,device_no,loc_cd,staf_sl,view_report,DeviceDirection) VALUES(" & slno_cnt & ",'" & Trim(device_code) & _
        "','" & Format(stringtodate(txtpunchdt.Text), "dd/MMM/yyyy") & "','" & Format(stm, "HH:mm") & "','E',0,'N',0," & loc_cd & "," & staf_sl & ",'Y','" & cmbtype.Text & "')")
        close1()
        Me.punch_disp()
    End Sub

    Protected Sub cmdsave5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave5.Click
        Dim fnd As Integer = 0
        For i As Integer = 0 To dvstaf.Rows.Count - 1
            Dim chk As CheckBox = dvstaf.Rows(i).FindControl("chk")
            If chk.Checked = True Then
                fnd = 1
                Exit For
            End If
        Next
        If fnd = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Select Atleast One Punch');", True)
            dvstaf.Focus()
            Exit Sub
        End If

        For j As Integer = 0 To dvstaf.Rows.Count - 1
            Dim chk As CheckBox = dvstaf.Rows(j).FindControl("chk")
            If chk.Checked = True Then
                Dim lblsl As Label = dvstaf.Rows(j).FindControl("lblslno")
                start1()
                SQLInsert("UPDATE elog SET view_report ='Y' WHERE sl_no=" & lblsl.Text & "")
                close1()
            End If
        Next
        Me.punch_disp()
    End Sub

    Private Function Get_weekday_position(ByVal todt_ofmonth As Date, ByVal dow As Integer) As Integer
        Dim dt1 As New Date(todt_ofmonth.Year, todt_ofmonth.Month, 1)
        Dim dt2 As Date = todt_ofmonth
        Dim dpos As Integer = 0
        Dim days As Integer = (From d As Date In _
        (Enumerable.Range(0, 1 + dt2.Subtract(dt1).Days).Select(Function(offset) dt1.AddDays(offset)).ToArray()) _
        Where d.DayOfWeek = dow Select 1).Sum
        Return days
    End Function

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim ds As DataSet = get_dataset("SELECT top 1 * FROM elog WHERE device_code=(select device_code FROM staf where emp_code='" & Trim(txtempcode.Text) & "') ORDER BY Log_dt desc")
        If ds.Tables(0).Rows.Count <> 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Last Punch Found on Dt: " & Format(ds.Tables(0).Rows(0).Item("log_dt"), "dd/MM/yyyy") & "');", True)
        Else
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Invalid Employee Code.');", True)
        End If
    End Sub
End Class
