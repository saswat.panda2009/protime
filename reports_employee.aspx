﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="reports_employee.aspx.vb" Inherits="reports_employee" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Employee Register . . ."></asp:Label></h3>
               
                </div>

                     <div class="card-body">
                                    <table style="width:100%;"><tr><td width="20%" valign="top">
                      <asp:DropDownList ID="cmbdivsion" 
                            runat="server" AutoPostBack="True" 
                                                                      class="form-control" Height="42px">
                                                                  </asp:DropDownList></td><td width="20%" valign="top">
                   <table style="width:100%;"><tr><td width="90%"> <asp:TextBox ID="txtdept" 
                           runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox></td><td width="10%">
                       <asp:ImageButton ID="btnselect" runat="server" ImageUrl="~/images/select.png" 
                           Width="100%"></asp:ImageButton></td></tr></table>
                                        </td>
                   <td width="25%" valign="top">
                                      &nbsp;</td>
                   <td width="35%" valign="top"> 
                   <table style="width:100%;"><tr><td width="90%"> &nbsp;</td><td width="10%">
                       &nbsp;</td></tr></table>
                                     <asp:Label ID="lblmsg" runat="server" Text="Label"></asp:Label>
                                      <asp:TextBox ID="txtdeptcd" runat="server" Height="22px" Visible="False" 
                                          Width="20px"></asp:TextBox>
                                      <asp:TextBox ID="txtdivsl" runat="server" Height="22px" Visible="False" 
                                          Width="20px"></asp:TextBox>
</td></tr>
                                        <tr><td valign="top" colspan="4">
                                           <asp:Panel ID="pnlsearch" runat="server">
                                                      <table style="width: 100%;">
                                                  
                                              <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel2" runat="server">
                                                          <div style="width: 100%; height: 500px; overflow: auto;">
                                                              <asp:GridView ID="dvdept" runat="server" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  Width="100%" PageSize="6">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <Columns>
                                                                  <asp:TemplateField>
                                                                  <ItemTemplate>
                                                                  <asp:CheckBox  runat="Server" ID="chk"/>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Department Name">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lbldept" Text='<%#Eval("dept_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>                                                                   
                                                              </Columns>
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>
                                                          </div>
                                                      </asp:Panel>
                                                  </td>
                                              </tr>
                                          </table>
                                                      </asp:Panel>
                                                      <asp:CollapsiblePanelExtender ID="pnlsearch_CollapsiblePanelExtender" 
                                                          runat="server" Enabled="True" TargetControlID="pnlsearch" CollapsedSize="0"
    ExpandedSize="500"
    Collapsed="True"
    ExpandControlID="btnselect"
    CollapseControlID="btnselect"
    AutoCollapse="False"
    AutoExpand="False"
    TextLabelID="Label1"
    CollapsedText="Show Details..."
    ExpandedText="Hide Details" 
    ImageControlID="Image1"
    ExpandDirection="Vertical">
                                                      </asp:CollapsiblePanelExtender></td></tr>
                                 <tr><td valign="top" colspan="4">
                                           <asp:Panel ID="Panel1" runat="server">
                                                      <table style="width: 100%;">
                                                  
                                              <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel3" runat="server">
                                                          <table style="width:100%;"><tr><td colspan="3"><div style="width: 100%; height: 600px; overflow: auto;">
                                                                <asp:GridView ID="dvstaf" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                              AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                              PagerStyle-CssClass="pgr" Width="100%">
                                                              <AlternatingRowStyle CssClass="alt" />
                                                              <Columns>
                                                                  <asp:TemplateField>
                                                                  <HeaderTemplate>
                                                                   <asp:CheckBox  runat="Server" ID="chk1" AutoPostBack="True" OnCheckedChanged="CheckBox1_CheckedChanged"/>
                                                                  </HeaderTemplate>
                                                                  <ItemTemplate>
                                                                  <asp:CheckBox runat="Server" ID="chkvisible"/>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Column">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblcolumn_name" Text='<%#Eval("column_name") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Column Name">
                                                                  <ItemTemplate>
                                                               <asp:TextBox ID="txtcolumn_alias" runat="server" Text='<%#Eval("column_alias") %>' CssClass="form-control"></asp:TextBox>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Sorting Order">
                                                                  <ItemTemplate>
                                                                  <asp:TextBox ID="txtsort" runat="server" Text='<%#Eval("sort_seq") %>' CssClass="form-control"></asp:TextBox>
                                                               
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  
                                                                   <asp:TemplateField Visible="False">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblvisible" Text='<%#Eval("visible") %>'></asp:Label>
                                                                 <asp:Label runat="server" ID="lblsl" Text='<%#Eval("sl") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                              </Columns>
                                                              <PagerStyle HorizontalAlign="Right" />
                                                          </asp:GridView>
                                                          </div></td></tr><tr><td>
                                                                  &nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                                              <tr>
                                                                  <td>
                                                                      <asp:Button ID="cmdsearch0" runat="server" class="btn btn-success" 
                                                                          Text="Save" />
                                                                      &nbsp;<asp:Button ID="cmdsearch1" runat="server" class="btn btn-danger" 
                                                                          Text="Close" />
                                                                  </td>
                                                                  <td>
                                                                      &nbsp;</td>
                                                                  <td>
                                                                      &nbsp;</td>
                                                              </tr>
                                                              <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>
                                                      </asp:Panel>
                                                  </td>
                                              </tr>
                                          </table>
                                                      </asp:Panel>
                                                      <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" 
                                                          runat="server" Enabled="True" TargetControlID="Panel3" CollapsedSize="0"
    ExpandedSize="800"
    Collapsed="True"
    ExpandControlID="cmdquerybuilder"
    CollapseControlID="cmdquerybuilder"
    AutoCollapse="False"
    AutoExpand="False"
    TextLabelID="Label1"
    CollapsedText="Show Details..."
    ExpandedText="Hide Details" 
    ImageControlID="Image1"
    ExpandDirection="Vertical">
                                                      </asp:CollapsiblePanelExtender></td></tr>
                              <tr>
                              
                              <td>
                                      <asp:Button ID="cmdsearch" runat="server" class="btn btn-primary" 
                                          Text="Generate" />
                                           
                                     &nbsp;<asp:Button ID="cmdquerybuilder" runat="server" class="btn btn-success" 
                                          Text="Query Builder" />
                                           
                                     &nbsp;  
                                  </td><td>&nbsp;</td>
                   <td>
                       &nbsp;</td>
                   <td>&nbsp;</td></tr>
                   <tr>
                       <td colspan="4">
                          <div style="width: 100%; height: 500px; overflow: auto;" class="table-responsive">
                                                              <asp:GridView ID="dvdata" runat="server" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  CssClass="grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="15" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>


                                        </div></td>
                   </tr>
                 </table>                              
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

