﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class master_state
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            If Request.QueryString("mode") = "V" Then
                txtmode.Text = "V"
            Else
                txtmode.Text = "E"
            End If
            Me.clr()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtcycle.Text = ""
        txtstatecd.Text = ""
        cmbactive.SelectedIndex = 0
        txtsl.Text = "1"
        Me.shift_disp()
        Dim dt As New DataTable
        dt.Columns.Add("Sl", GetType(Integer))
        dt.Columns.Add("Shift", GetType(String))
        dt.Columns.Add("Shift_sl", GetType(String))
        dvstaf.DataSource = dt
        dvstaf.DataBind()
        If txtmode.Text = "E" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Shift Cycle Master (Entry Mode)"
            txtcycle.Focus()
        ElseIf txtmode.Text = "M" Then
            pnladd.Visible = True
            pnlview.Visible = False
            lblhdr.Text = "Shift Cycle Master (Edit Mode)"
        ElseIf txtmode.Text = "V" Then
            pnladd.Visible = False
            pnlview.Visible = True
            lblhdr.Text = "Shift Cycle Master (View Mode)"
            Me.dvdisp()
        End If
    End Sub

    Private Sub dvdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim ds1 As DataSet = get_dataset("SELECT row_number() over(ORDER BY cycle_nm) as 'sl',cycle_nm,(case when active='Y' Then 'Yes' WHEN active='N' Then 'No' END)as 'active' FROM cycle1 WHERE loc_cd=" & loc_cd & " ORDER BY cycle_nm")
        GridView1.DataSource = ds1.Tables(0)
        GridView1.DataBind()
    End Sub

    Private Sub shift_disp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbshift.Items.Clear()
        cmbshift.Items.Add("Please Select The Shift")
        Dim ds1 As DataSet = get_dataset("SELECT shift_nm From shift_mst WHERE loc_cd=" & loc_cd & " ORDER BY shift_nm")
        For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
            cmbshift.Items.Add(ds1.Tables(0).Rows(i).Item("shift_nm"))
        Next
    End Sub

    Protected Sub cmdsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave.Click
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If dvstaf.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Select The Shift name');", True)
            cmbshift.Focus()
            Exit Sub
        End If
        If txtmode.Text = "E" Then
            Dim dscheck As DataSet = get_dataset("SELECT cycle_nm FROM cycle1 WHERE cycle_nm='" & UCase(Trim(txtcycle.Text)) & "' AND loc_cd=" & loc_cd & "")
            If dscheck.Tables(0).Rows.Count <> 0 Then
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Shift Cycle Name Already Exits');", True)
                txtcycle.Focus()
                Exit Sub
            End If
            txtstatecd.Text = "1"
            Dim ds1 As DataSet = get_dataset("SELECT max(cycle_sl) FROM cycle1 ")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                txtstatecd.Text = ds1.Tables(0).Rows(0).Item(0) + 1
            End If
            start1()
            SQLInsert("INSERT INTO cycle1(cycle_sl,loc_cd,cycle_nm,active) VALUES(" & Val(txtstatecd.Text) & _
            "," & loc_cd & ",N'" & UCase(Trim(txtcycle.Text)) & "','" & vb.Left(cmbactive.Text, 1) & "')")
            Me.dvsave()
            close1()
            Me.clr()
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Succesffuly');", True)
        ElseIf txtmode.Text = "M" Then
            Dim ds As DataSet = get_dataset("SELECT cycle_nm FROM cycle1 WHERE cycle_sl=" & Val(txtstatecd.Text) & "")
            If ds.Tables(0).Rows.Count <> 0 Then
                If Trim(txtcycle.Text) <> ds.Tables(0).Rows(0).Item("cycle_nm") Then
                    Dim dscheck As DataSet = get_dataset("SELECT cycle_nm FROM cycle1 WHERE cycle_nm='" & UCase(Trim(txtcycle.Text)) & "' AND loc_cd=" & loc_cd & "")
                    If dscheck.Tables(0).Rows.Count <> 0 Then
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Shift Cycle Name Already Exits');", True)
                        txtcycle.Focus()
                        Exit Sub
                    End If
                End If
                start1()
                SQLInsert("UPDATE cycle1 SET cycle_nm='" & UCase(Trim(txtcycle.Text)) & "',active='" & _
                vb.Left(cmbactive.Text, 1) & "' WHERE cycle_sl=" & Val(txtstatecd.Text) & "")
                SQLInsert("DELETE FROM cycle2 WHERE cycle_sl=" & Val(txtstatecd.Text) & "")
                Me.dvsave()
                close1()
                Me.clr()
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Modified Succesffuly');", True)
            End If
        End If
    End Sub

    Private Sub dvsave()
        For i As Integer = 0 To dvstaf.Rows.Count - 1
            Dim lbl As Label = dvstaf.Rows(i).FindControl("lblsl")
            Dim lbl1 As Label = dvstaf.Rows(i).FindControl("lblshift")
            Dim lbl2 As Label = dvstaf.Rows(i).FindControl("lblshift_sl")
            Dim max As Integer = 1
            Dim ds1 As DataSet = get_dataset("SELECT max(sl_no) FROM cycle2")
            If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
                max = ds1.Tables(0).Rows(0).Item(0) + 1
            End If
            SQLInsert("INSERT INTO cycle2(sl_no,cycle_sl,shift_sl) VALUES(" & max & "," & Val(txtstatecd.Text) & "," & Val(lbl2.Text) & ")")
        Next
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Me.dvdisp()
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim ds1 As DataSet = get_dataset("SELECT cycle_sl FROM cycle1 WHERE cycle_nm='" & Trim(GridView1.Rows(rw).Cells(1).Text) & "' AND loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtmode.Text = "M"
                Me.clr()
                txtstatecd.Text = Val(ds1.Tables(0).Rows(0).Item("cycle_sl"))
                Me.dvsel()
            End If
        End If
    End Sub

    Private Sub dvsel()
        Dim ds1 As DataSet = get_dataset("SELECT cycle1.cycle_sl, cycle1.cycle_nm, cycle1.active, cycle2.shift_sl, shift_mst.shift_nm FROM cycle1 RIGHT OUTER JOIN cycle2 ON cycle1.cycle_sl = cycle2.cycle_sl LEFT OUTER JOIN shift_mst ON cycle2.shift_sl = shift_mst.shift_sl WHERE cycle2.cycle_sl=" & Val(txtstatecd.Text) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtcycle.Text = ds1.Tables(0).Rows(0).Item("cycle_nm")
            If ds1.Tables(0).Rows(0).Item("active") = "Y" Then
                cmbactive.SelectedIndex = 0
            Else
                cmbactive.SelectedIndex = 1
            End If
            Dim dt As New DataTable
            dt.Columns.Add("Sl", GetType(Integer))
            dt.Columns.Add("Shift", GetType(String))
            dt.Columns.Add("Shift_sl", GetType(String))
            For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
                dt.Rows.Add(i + 1, ds1.Tables(0).Rows(i).Item("shift_nm"), ds1.Tables(0).Rows(i).Item("shift_sl"))
                txtsl.Text = i + 2
            Next
            dvstaf.DataSource = dt
            dvstaf.DataBind()
        End If
    End Sub

    Protected Sub cmdclear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        Me.clr()
        txtcycle.Focus()
    End Sub

    Protected Sub cmdnext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdnext.Click
        Dim shift_sl As Integer = 0
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim dscheck As DataSet = get_dataset("SELECT shift_sl FROM shift_mst WHERE shift_nm='" & UCase(Trim(cmbshift.Text)) & "' AND loc_cd=" & loc_cd & "")
        If dscheck.Tables(0).Rows.Count <> 0 Then
            shift_sl = dscheck.Tables(0).Rows(0).Item("shift_sl")
        End If
        If Val(shift_sl) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Shift Name Should Not Be Blank');", True)
            cmbshift.Focus()
            Exit Sub
        End If
        Dim dt As New DataTable
        dt.Columns.Add("Sl", GetType(Integer))
        dt.Columns.Add("Shift", GetType(String))
        dt.Columns.Add("Shift_sl", GetType(String))

        If dvstaf.Rows.Count > 0 Then
            For i As Integer = 0 To dvstaf.Rows.Count - 1
                Dim lbl As Label = dvstaf.Rows(i).FindControl("lblsl")
                Dim lbl1 As Label = dvstaf.Rows(i).FindControl("lblshift")
                Dim lbl2 As Label = dvstaf.Rows(i).FindControl("lblshift_sl")
                dt.Rows.Add(lbl.Text, lbl1.Text, lbl2.Text)
                txtsl.Text = Val(txtsl.Text)
            Next
        End If
        dt.Rows.Add(Val(txtsl.Text), cmbshift.Text, shift_sl)
        dvstaf.DataSource = dt
        dvstaf.DataBind()
        txtsl.Text = Val(txtsl.Text) + 1
        cmbshift.SelectedIndex = 0
        cmbshift.Focus()
    End Sub

    Protected Sub dvstaf_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dvstaf.RowCommand
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        Dim rw As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim dt As New DataTable
            dt.Columns.Add("Sl", GetType(Integer))
            dt.Columns.Add("Shift", GetType(String))
            dt.Columns.Add("Shift_sl", GetType(String))
            Dim cnt As Integer = 1
            For i As Integer = 0 To dvstaf.Rows.Count - 1
                If i <> rw Then
                    Dim lbl1 As Label = dvstaf.Rows(i).FindControl("lblshift")
                    Dim lbl2 As Label = dvstaf.Rows(i).FindControl("lblshift_sl")
                    dt.Rows.Add(cnt, lbl1.Text, lbl2.Text)
                    cnt = cnt + 1
                End If
            Next
            txtsl.Text = cnt
            dvstaf.DataSource = dt
            dvstaf.DataBind()
        End If
    End Sub

End Class
