﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="transcation_leaveassign.aspx.vb" Inherits="transcation_leaveassign" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="A1" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                      aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="transcation_leaveassign.aspx">Add New Leave Assignment</a>
                      <a class="dropdown-item" href="transcation_leaveassign.aspx?mode=V">View Leave Assignment List</a>
                    
                    </div>
                  </div>
                </div>

                     <div class="card-body">
                                       <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnlview" runat="server">
                                          <table style="width: 100%;">
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel2" runat="server">
                                                          <div style="width: 100%; height: 600px">
                                                              <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="15" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <Columns>
                                                                  <asp:TemplateField HeaderText="Division">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblDivision" Text='<%#Eval("div_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Emp. Id">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblEmpId" Text='<%#Eval("emp_code") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Employee Name">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblStaffName" Text='<%#Eval("staf_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Year">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblDate" Text='<%#Eval("assign_year") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Type">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblInTime" Text='<%#Eval("tp") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Days">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblInTime" Text='<%#Eval("no_day") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="slno" Visible="False">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblslno" Text='<%#Eval("sl") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                      <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                                                          ImageUrl="images/delete.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                                                      <ItemStyle Height="30px" Width="30px" />
                                                                      </asp:ButtonField>
                                                                  </Columns>
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>
                                                          </div>
                                                      </asp:Panel>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table width="100%">
                                                                                        <tr>
                                                  <td colspan="3">
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="45%">
                                                                  Division</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  Department</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:DropDownList ID="cmbdivsion" runat="server" AutoPostBack="True" 
                                                                      class="form-control">
                                                                  </asp:DropDownList>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:DropDownList ID="cmbdept" runat="server" AutoPostBack="True" 
                                                                      class="form-control">
                                                                  </asp:DropDownList>
                                                              </td>
                                                          </tr>
                                                        
                                                      </table>
                                                  </td>
                                              </tr>
                                             
                                              <tr>
                                                  <td width="45%">
                                                      For Year</td>
                                                  <td xml:lang="10%">
                                                      &nbsp;</td>
                                                  <td width="45%">
                                                      Leave Type</td>
                                              </tr>
                                              <tr>
                                                  <td width="45%">
                                                      <asp:TextBox ID="txtyear" runat="server" CssClass="form-control" 
                                                          BackColor="White" ReadOnly="True"></asp:TextBox>

                                                      <asp:CalendarExtender ID="txtyear_CalendarExtender" runat="server" 
                                                          Enabled="True" Format="YYYY" PopupButtonID="txtyear" TargetControlID="txtyear">
                                                      </asp:CalendarExtender>

                                                  </td>
                                                  <td xml:lang="10%">
                                                      &nbsp;</td>
                                                  <td width="45%">
                                                      <asp:DropDownList ID="cmbleavetp" runat="server" class="form-control" 
                                                          AutoPostBack="True">
                                                         <asp:ListItem>1.Paid Leave</asp:ListItem>
                                                          <asp:ListItem>2.Casual leave</asp:ListItem>
                                                          <asp:ListItem>4.Optional Leave</asp:ListItem>
                                                          <asp:ListItem>5.Sick Leave</asp:ListItem>
                                                          <asp:ListItem>6.Maternity Leave</asp:ListItem>
                                                      </asp:DropDownList>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td width="45%">
                                                      &nbsp;</td>
                                                  <td xml:lang="10%">
                                                      &nbsp;</td>
                                                  <td width="45%">
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="30">
                                                                  &nbsp;</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="60">
                                                                  Days</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:CheckBox ID="chkall" runat="server" Text="All" />
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtdays" runat="server" BackColor="White" 
                                                                      CssClass="form-control" ></asp:TextBox>
                                                                 
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      <div style="width: 100%; height: 300px; overflow: auto;">
                                                          <asp:GridView ID="dvstaf" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                              AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                              PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                              <AlternatingRowStyle CssClass="alt" />
                                                              <Columns>
                                                                  <asp:TemplateField>
                                                                  <HeaderTemplate>
                                                                   <asp:CheckBox  runat="Server" ID="chk1" AutoPostBack="True" OnCheckedChanged="CheckBox1_CheckedChanged"/>
                                                                  </HeaderTemplate>
                                                                  <ItemTemplate>
                                                                  <asp:CheckBox  runat="Server" ID="chk"/>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Employee ID">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblempid" Text='<%#Eval("emp_code") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                               <asp:TemplateField HeaderText="Employee Name">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblstaf_nm" Text='<%#Eval("staf_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Department">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lbldep_nm" Text='<%#Eval("dept_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Designation">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lbldesg_nm" Text='<%#Eval("desg_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField Visible="False">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblstaf_sl" Text='<%#Eval("staf_sl") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="No Of Days">
                                                                  <ItemTemplate>
                                                                  <asp:TextBox runat="server" ID="txtday" Text='<%#Eval("no_day") %>' class="form-control"></asp:TextBox>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                              </Columns>
                                                              <PagerStyle HorizontalAlign="Right" />
                                                          </asp:GridView>
                                                      </div>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      <asp:Button ID="cmdsave" runat="server" class="btn btn-primary" Text="Submit" />
                                                      <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                          class="btn btn-default" Text="Reset" />
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td colspan="3">
                                                      <asp:TextBox ID="txtmode" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtdeptcd" runat="server" Height="22px" Visible="False" 
                                                          Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtdivsl" runat="server" Height="22px" Visible="False" 
                                                          Width="20px"></asp:TextBox>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>      
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

