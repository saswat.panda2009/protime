﻿Imports System.Data
Imports vb = Microsoft.VisualBasic

Partial Class company_division
    Inherits System.Web.UI.Page
   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As DataSet = get_dataset("SELECT usr_sl, staf_sl, month_nm, emp_code, emp_nm, desg, category, uan_no, pay_month, pay_year, loc_cd, gross,medical_amt,mobile_amt,food_amt,insurance_amt, basic_amt, da_amt, hra_amt, transport_amt, child_amt, city_amt, spcl_amt, pf_amt, comp_pf_amt, esic_amt, comp_esic_amt, pt_amt, tds_amt, loan_amt, othr_earnings, othr_deuction, tot_earnings, tot_deduction, wages, tot_reimbursment, total, pay_days, tot_days, tot_present, tot_half, tot_week, tot_absnt, tot_leave, tot_holi, l1, l2, l3, l4, l5, l6, washing_amt, (Case WHEN a1<>'' THEN a1 WHEN a1='' THEN '&nbsp;' END) as a1,(Case WHEN a2<>'' THEN a2  WHEN a2='' THEN '&nbsp;' END) as a2, (Case WHEN a3<>'' THEN a3  WHEN a3='' THEN '&nbsp;' END) as a3,(Case WHEN a4<>'' THEN a4  WHEN a4='' THEN '&nbsp;' END) as a4,(Case WHEN a5<>'' THEN a5  WHEN a5='' THEN '&nbsp;' END) as a5, d1, d2, d3, d4, d5, a1_amt, a2_amt, a3_amt, a4_amt, a5_amt, d1_amt, d2_amt, d3_amt, d4_amt, d5_amt, in_words, comp_name FROM print_payslip where usr_sl=" & CType(Session("usr_sl"), Integer) & "")
        Label1.Text = ""
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            If i > 0 Then
                Label1.Text = Label1.Text & "<br/><br/><br/><br/><br/>"
            End If
            Label1.Text = Label1.Text & "<div style=""border: 2px solid #000000;""> <table style=""width:100%;""><tr><td align=""center"" style=""font-weight: bold"">" & _
            "Silvercity Hospityal & Research Centre</td></tr><tr><td align=""center"" style=""font-weight: bold"">Chauliaganj, Cuttack.</td></tr><tr>" & _
             "<td align=""center"" style=""font-weight: bold"">Pay Slip for the Month of " & ds.Tables(0).Rows(i).Item("month_nm") & " - " & ds.Tables(0).Rows(i).Item("pay_year") & "</td></tr>" & _
            "<tr><td bgcolor=""#A4F997"">&nbsp;</td></tr><tr><td>" & _
            "<table style=""width:100%;""><tr><td width=""50%""><table style=""width:100%;""><tr><td width=""30%"">Employee ID</td><td width=""1%"">:</td><td width=""69%"">" & ds.Tables(0).Rows(i).Item("emp_code") & "</td>" & _
            "</tr><tr><td>Department</td><td>:</td><td>" & ds.Tables(0).Rows(i).Item("desg") & "</td></tr><tr><td>Date of Joining</td><td>:</td><td>" & ds.Tables(0).Rows(i).Item("emp_nm") & "</td></tr>" & _
            "<tr><td>Days Worked</td><td>:</td><td>" & ds.Tables(0).Rows(i).Item("pay_days") & "</td></tr><tr><td>Bank Account</td><td>:</td><td>&nbsp;</td></tr></table></td><td width=""50%"">" & _
            "<table style=""width:100%;""><tr><td width=""30%"">Name</td><td width=""1%"">:</td><td width=""69%"">" & ds.Tables(0).Rows(i).Item("emp_nm") & "</td></tr><tr><td>Designation</td>" & _
            "<td>:</td><td>" & ds.Tables(0).Rows(i).Item("desg") & "</td></tr><tr><td>PF NO.</td><td>:</td><td>&nbsp;</td></tr><tr><td>ESI Account No.</td><td>:</td><td>&nbsp;</td></tr>" & _
            "<tr><td>Father&#39;s Name</td><td>:</td><td>&nbsp;</td></tr></table></td></tr><tr><td colspan=""2"" style=""font-size: 5px; border: 1px solid #000000"">&nbsp;</td></tr>" & _
            "<tr><td><table style=""width:100%;""><tr><td bgcolor=""#A4F997"" style=""border: 1px solid #000000"" width=""30%"">Earnings</td><td align=""right"" bgcolor=""#A4F997"" colspan=""2"" style=""border: 1px solid #000000"" width=""1%"">Amount</td>" & _
            "</tr><tr><td width=""30%"">Basic Pay</td><td width=""1%""></td><td align=""right"" width=""69%"">" & Format(ds.Tables(0).Rows(i).Item("basic_amt"), "#0.00") & "</td></tr>" & _
            "<tr><td>HRA</td><td>&nbsp;</td><td align=""right"">" & Format(ds.Tables(0).Rows(i).Item("hra_amt"), "#0.00") & "</td></tr>" & _
            "<tr><td>City Comp. Allowance</td><td>&nbsp;</td><td align=""right"">" & Format(ds.Tables(0).Rows(i).Item("hra_amt"), "#0.00") & "</td></tr>" & _
            "<tr><td>Conveyance Allowance</td><td>&nbsp;</td><td align=""right"">" & Format(ds.Tables(0).Rows(i).Item("transport_amt"), "#0.00") & "</td></tr>" & _
            "<tr><td>Medical Allowance</td><td>&nbsp;</td><td align=""right"">" & Format(ds.Tables(0).Rows(i).Item("medical_amt"), "#0.00") & "</td></tr>" & _
            "<tr><td>Telephone Allowance</td><td>&nbsp;</td><td align=""right"">" & Format(ds.Tables(0).Rows(i).Item("mobile_amt"), "#0.00") & "</td></tr>" & _
            "<tr><td>Tiffin Allowance</td><td>&nbsp;</td><td align=""right"">" & Format(ds.Tables(0).Rows(i).Item("food_amt"), "#0.00") & "</td></tr>" & _
            "<tr><td>Misc. Allowance</td><td>&nbsp;</td><td align=""right"">&nbsp;</td></tr>" & _
            "<tr><td bgcolor=""#A4F997"" style=""border: 1px solid #000000"">Total Earnings</td><td align=""right"" bgcolor=""#A4F997"" colspan=""2"" style=""border: 1px solid #000000""> " & Format(ds.Tables(0).Rows(i).Item("tot_earnings"), "#0.00") & "</td></tr>" & _
            "<tr><td bgcolor=""#A4F997"" style=""border: 1px solid #000000"">Net Payable</td><td align=""right"" bgcolor=""#A4F997"" colspan=""2"" style=""border: 1px solid #000000"">" & Format(ds.Tables(0).Rows(i).Item("total"), "#0000") & ".00</td></tr>" & _
            "</table></td><td><table style=""width:100%;""><tr><td bgcolor=""#A4F997"" style=""border: 1px solid #000000"" width=""30%"">Deduction</td>" & _
            "<td align=""right"" bgcolor=""#A4F997"" colspan=""2"" style=""border: 1px solid #000000"" width=""1%"">Amount</td></tr>" & _
            "<tr><td width=""30%"">Provident Fund</td><td width=""1%""></td><td align=""right"" width=""69%"">" & Format(ds.Tables(0).Rows(i).Item("pf_amt"), "#0.00") & "</td></tr>" & _
            "<tr><td>ESI</td><td>&nbsp;</td><td align=""right"">" & Format(ds.Tables(0).Rows(i).Item("esic_amt"), "#0.00") & "</td></tr>" & _
            "<tr><td>Insurance</td><td>&nbsp;</td><td align=""right"">" & Format(ds.Tables(0).Rows(i).Item("insurance_amt"), "#0.00") & "</td></tr>" & _
            "<tr><td>Loan / Advanced</td><td></td><td align=""right"">" & Format(ds.Tables(0).Rows(i).Item("loan_amt"), "#0.00") & "</td></tr>" & _
            "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>" & _
            "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>" & _
            "<tr><td bgcolor=""#A4F997"" style=""border: 1px solid #000000"">Total Deductions</td><td align=""right"" bgcolor=""#A4F997"" colspan=""2"" style=""border: 1px solid #000000"">" & Format(ds.Tables(0).Rows(i).Item("tot_deduction"), "#0.00") & "</td>" & _
            "</tr><tr><td>&nbsp;</td><td colspan=""2"">&nbsp;</td></tr></table></td></tr>" & _
            "<tr><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>Employer&#39;s Singnature</td><td align=""right"">Employee's Signature</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td colspan=""2"">This is a computer generated payslip dose not require any signature.</td></tr></table></td></tr></table></div>"
        Next

        '    Label1.Text = "<div style=""border: 2px solid #000000""><table style=""width: 100%;""><tr><td><table style=""width:100%;""><tr><td width=""45%"">" & _
        '        "<strong>Month :</strong></td><td width=""10%""></td><td width=""45%""></td></tr><tr><td><strong>Employee Name :</strong> BRUNDABAN SAHOO</td><td></td>" & _
        '        "<td><strong>Designation :</strong> BRUNDABAN SAHOO</td></tr><tr><td><strong>Employee Id :</strong> BRUNDABAN SAHOO</td><td></td><td><strong>UAN No :</strong>" & _
        '        "BRUNDABAN SAHOO</td></tr><tr><td><strong>Total Days :</strong> 31 </td><td></td><td><strong>Payable Days :</strong> 31</td></tr></table></td></tr><tr><td>" & _
        '        "<table style=""width:100%;"" border=""1""><tr><td align=""center"" width=""30%""><b>Description</b></td><td align=""center"" width=""17.5%""><strong>Amount" & _
        '        "</strong></td><td align=""center""width=""5%""></td><td align=""center"" width=""30%""><strong>Description</strong></td><td width=""17.5%"" align=""center"">" & _
        '        "<strong>Amount</strong></td></tr></table></td></tr><tr><td><table style=""width:100%;""><tr><td width=""30%"">Basic Salary</td><td align=""right"" width=""17.5%"">" & _
        '        "1200.00</td><td align=""center"" width=""5%""></td><td align=""left"" width=""30%"">EPF</td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr>" & _
        '        "<td width=""30%"">HRA</td><td align=""right"" width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%"">ESIC</td><td width=""17.5%"" " & _
        '        "align=""right"">&nbsp;</td></tr><tr><td width=""30%"">Washing Allowance</td><td align=""right"" width=""17.5%"">&nbsp;</td><td width=""5%""></td>" & _
        '        "<td width=""30%"">PT</td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr><td width=""30%"">Special Allowance</td><td align=""right"" " & _
        '        "width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%"">TDS</td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr>" & _
        '        "<td width=""30%"">&nbsp;</td><td align=""right"" width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%"">Loan / Advance</td>" & _
        '        "<td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr><td width=""30%"">&nbsp;</td><td align=""right"" width=""17.5%"">&nbsp;</td><td " & _
        '        "width=""5%""></td><td width=""30%""></td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr><td width=""30%"">&nbsp;</td><td align=""right"" " & _
        '        "width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%""></td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr><td width=""30%"">&nbsp;</td><td align=""right"" width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%""></td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr><td width=""30%"">&nbsp;</td><td align=""right"" width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%""></td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr><td width=""30%"">&nbsp;</td><td align=""right"" width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%""></td><td width=""17.5%"" align=""right"">&nbsp;</td></tr></table></td></tr><tr><td><table style=""width:100%;"" border=""1""><tr><td align=""left"" width=""30%""><strong>TotalEarnings</strong></td><td align=""right"" width=""17.5%"">145000.00</td><td align=""center"" width=""5%""></td><td align=""left"" width=""30%""><strong>Total Deduction</strong></td><td width=""17.5%"" align=""right"">1500.00</td></tr></table></td></tr><tr><td>Net Payable : 41,456.00</td></tr><tr><td>In Words :</td></tr><tr><td></td></tr><tr><td></td></tr><tr><td><table style=""width:100%;""><tr><td width=""45%""><strong>Receiver&#39;s Signature</strong></td><td width=""10%""></td><td align=""right"" width=""45%""><strong>For Evos Buildcon Pvt. Ltd.</strong></td></tr></table></td></tr></table></div><br/>"

        '    Label1.Text = Label1.Text & "<div style=""border: 2px solid #000000; width: 100%""><table style=""width: 100%;""><tr><td><table style=""width:100%;""><tr><td width=""45%"">" & _
        '        "<strong>Month :</strong></td><td width=""10%""></td><td width=""45%""></td></tr><tr><td><strong>Employee Name :</strong> BRUNDABAN SAHOO</td><td></td>" & _
        '        "<td><strong>Designation :</strong> BRUNDABAN SAHOO</td></tr><tr><td><strong>Employee Id :</strong> BRUNDABAN SAHOO</td><td></td><td><strong>UAN No :</strong>" & _
        '        "BRUNDABAN SAHOO</td></tr><tr><td><strong>Total Days :</strong> 31 </td><td></td><td><strong>Payable Days :</strong> 31</td></tr></table></td></tr><tr><td>" & _
        '        "<table style=""width:100%;"" border=""1""><tr><td align=""center"" width=""30%""><b>Description</b></td><td align=""center"" width=""17.5%""><strong>Amount" & _
        '        "</strong></td><td align=""center""width=""5%""></td><td align=""center"" width=""30%""><strong>Description</strong></td><td width=""17.5%"" align=""center"">" & _
        '        "<strong>Amount</strong></td></tr></table></td></tr><tr><td><table style=""width:100%;""><tr><td width=""30%"">Basic Salary</td><td align=""right"" width=""17.5%"">" & _
        '        "1200.00</td><td align=""center"" width=""5%""></td><td align=""left"" width=""30%"">EPF</td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr>" & _
        '        "<td width=""30%"">HRA</td><td align=""right"" width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%"">ESIC</td><td width=""17.5%"" " & _
        '        "align=""right"">&nbsp;</td></tr><tr><td width=""30%"">Washing Allowance</td><td align=""right"" width=""17.5%"">&nbsp;</td><td width=""5%""></td>" & _
        '        "<td width=""30%"">PT</td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr><td width=""30%"">Special Allowance</td><td align=""right"" " & _
        '        "width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%"">TDS</td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr>" & _
        '        "<td width=""30%"">&nbsp;</td><td align=""right"" width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%"">Loan / Advance</td>" & _
        '        "<td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr><td width=""30%"">&nbsp;</td><td align=""right"" width=""17.5%"">&nbsp;</td><td " & _
        '        "width=""5%""></td><td width=""30%""></td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr><td width=""30%"">&nbsp;</td><td align=""right"" " & _
        '        "width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%""></td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr><td width=""30%"">&nbsp;</td><td align=""right"" width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%""></td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr><td width=""30%"">&nbsp;</td><td align=""right"" width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%""></td><td width=""17.5%"" align=""right"">&nbsp;</td></tr><tr><td width=""30%"">&nbsp;</td><td align=""right"" width=""17.5%"">&nbsp;</td><td width=""5%""></td><td width=""30%""></td><td width=""17.5%"" align=""right"">&nbsp;</td></tr></table></td></tr><tr><td><table style=""width:100%;"" border=""1""><tr><td align=""left"" width=""30%""><strong>TotalEarnings</strong></td><td align=""right"" width=""17.5%"">145000.00</td><td align=""center"" width=""5%""></td><td align=""left"" width=""30%""><strong>Total Deduction</strong></td><td width=""17.5%"" align=""right"">1500.00</td></tr></table></td></tr><tr><td>Net Payable : 41,456.00</td></tr><tr><td>In Words :</td></tr><tr><td></td></tr><tr><td></td></tr><tr><td><table style=""width:100%;""><tr><td width=""45%""><strong>Receiver&#39;s Signature</strong></td><td width=""10%""></td><td align=""right"" width=""45%""><strong>For Evos Buildcon Pvt. Ltd.</strong></td></tr></table></td></tr></table></div><br/>"
    End Sub
End Class
