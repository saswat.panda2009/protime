﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO

Partial Class payroll_assign
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub
    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        txtempcode.Text = ""
        txtstafsl.Text = ""
        txtstafnm.Text = ""
        txtdept.Text = ""
        txtdesg.Text = ""
        txtamount.Text = ""
        txtfordt.Text = Format(Now, "dd/MM/yyyy")
        Me.head_display()
        dvstaf.DataSource = Nothing
        dvstaf.DataBind()
    End Sub

    Private Sub head_display()
        cmbearnings.Items.Clear()
        cmbearnings.Items.Add("Select Head")
        Dim ds As DataSet = get_dataset("SELECT expense_name FROM Expense_Master ORDER BY expense_name")
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            cmbearnings.Items.Add(ds.Tables(0).Rows(i).Item(0))
        Next
    End Sub

    Protected Sub cmdsave0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave0.Click
        Dim ds1 As DataSet = get_dataset("SELECT staf.staf_sl,staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm FROM desg_mst RIGHT OUTER JOIN staf ON desg_mst.desg_sl = staf.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl  WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd= " & CType(Session("loc_cd"), Integer) & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            Me.clr()
            txtstafsl.Text = ds1.Tables(0).Rows(0).Item("staf_sl")
            txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            txtstafnm.Text = ds1.Tables(0).Rows(0).Item("staf_nm")
            txtdept.Text = ds1.Tables(0).Rows(0).Item("dept_nm")
            txtdesg.Text = ds1.Tables(0).Rows(0).Item("desg_nm")
            Me.dvdispl()
        End If
    End Sub

    Private Sub dvdispl()
        Dim ds As DataSet = get_dataset("SELECT Row_number() OVER(ORDER BY expense_dt,expense_name) as Sl,CONVERT(varchar, Expense_Entry.expense_dt, 103) AS dt, Expense_Master.expense_name, Expense_Entry.expense_desc, STR(Expense_Entry.amount, 12, 2) AS amt, Expense_Entry.paid,Expense_Entry.ent_sl FROM Expense_Master LEFT OUTER JOIN Expense_Entry ON Expense_Master.expense_cd = Expense_Entry.expense_cd WHERE staf_sl=" & Val(txtstafsl.Text) & " AND month(expense_dt)=" & stringtodate(txtfordt.Text).Month & " AND year(expense_dt)=" & stringtodate(txtfordt.Text).Year & " ORDER BY expense_dt,expense_name")
        dvstaf.DataSource = ds.Tables(0)
        dvstaf.DataBind()
    End Sub

    Protected Sub cmdsave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave1.Click
        If Val(txtstafsl.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide The Valid Employee Code');", True)
            txtempcode.Focus()
            Exit Sub
        End If
        If txtfordt.Text = "" Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide The Valid Date');", True)
            txtfordt.Focus()
            Exit Sub
        End If
        Dim head_sl As Integer = 0
        Dim dshead As DataSet = get_dataset("select expense_cd FROM Expense_Master WHERE expense_name='" & cmbearnings.Text & "'")
        If dshead.Tables(0).Rows.Count <> 0 Then
            head_sl = dshead.Tables(0).Rows(0).Item(0)
        End If
        If Val(head_sl) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Select The Valid Head');", True)
            cmbearnings.Focus()
            Exit Sub
        End If
        If Val(txtamount.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Amount Should Not Be Zero');", True)
            txtamount.Focus()
            Exit Sub
        End If
        'Read the uploaded file using BinaryReader and convert it to Byte Array.
        Dim br As New BinaryReader(FileUpload1.PostedFile.InputStream)
        Dim bytes As Byte() = br.ReadBytes(CInt(FileUpload1.PostedFile.InputStream.Length))

        'Convert the Byte Array to Base64 Encoded string.
        Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)


        start1()
        SQLInsert("INSERT INTO Expense_Entry(staf_sl,expense_dt,expense_cd,expense_desc,amount,exp_img,status,paid,approved_by,approved_status_by,loc_cd) VALUES(" & txtstafsl.Text & _
        ",'" & Format(stringtodate(txtfordt.Text), "dd/MMM/yyyy") & "'," & head_sl & ",'" & txtdescr.Text & "'," & Val(txtamount.Text) & ",'" & base64String & "','A','N'," & _
        CType(Session("usr_sl"), Integer) & ",'U'," & CType(Session("loc_cd"), Integer) & ")")
        close1()
        Me.clr()
        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Successfully');", True)
        txtempcode.Focus()
    End Sub

    Protected Sub dvstaf_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dvstaf.RowCommand
        Dim r As Integer = e.CommandArgument
        If e.CommandName = "edit_state" Then
            Dim lblslno As Label = dvstaf.Rows(r).FindControl("lblslno")
            Dim lblpaid As Label = dvstaf.Rows(r).FindControl("lblpaid")
            If lblpaid.Text = "N" Then
                start1()
                SQLInsert("DELETE FROM  Expense_Entry WHERE ent_sl=" & lblslno.Text & "")
                close1()
                Me.dvdispl()
            Else
                ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Sorry You Cannot Delete Expenses, Which is Already Paid');", True)
            End If
        End If
    End Sub

    Protected Sub cmdsave3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsave3.Click
        If Val(txtstafsl.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide The Valid Employee Code');", True)
            txtempcode.Focus()
            Exit Sub
        End If
        If txtfordt.Text = "" Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Provide The Valid Date');", True)
            txtfordt.Focus()
            Exit Sub
        End If
        Dim head_sl As Integer = 0
        Dim dshead As DataSet = get_dataset("select expense_cd FROM Expense_Master WHERE expense_name='" & cmbearnings.Text & "'")
        If dshead.Tables(0).Rows.Count <> 0 Then
            head_sl = dshead.Tables(0).Rows(0).Item(0)
        End If
        If Val(head_sl) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Select The Valid Head');", True)
            cmbearnings.Focus()
            Exit Sub
        End If
        If Val(txtamount.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Amount Should Not Be Zero');", True)
            txtamount.Focus()
            Exit Sub
        End If
        'Read the uploaded file using BinaryReader and convert it to Byte Array.
        Dim br As New BinaryReader(FileUpload1.PostedFile.InputStream)
        Dim bytes As Byte() = br.ReadBytes(CInt(FileUpload1.PostedFile.InputStream.Length))

        'Convert the Byte Array to Base64 Encoded string.
        Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)


        start1()
        SQLInsert("INSERT INTO Expense_Entry(staf_sl,expense_dt,expense_cd,expense_desc,amount,exp_img,status,paid,approved_by,approved_status_by) VALUES(" & txtstafsl.Text & _
        ",'" & Format(stringtodate(txtfordt.Text), "dd/MMM/yyyy") & "'," & head_sl & ",'" & txtdescr.Text & "'," & Val(txtamount.Text) & ",'" & base64String & "','A','Y'," & _
        CType(Session("loc_cd"), Integer) & ",'U')")
        close1()
        Me.clr()
        ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Record Added Successfully');", True)
        txtempcode.Focus()
    End Sub
End Class
