﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default3.aspx.vb" Inherits="Default3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            height: 26px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table style="width:100%;">
            <tr>
                <td align="center" style="font-weight: bold">
                    Silvercity Hospityal &amp; Research Centre</td>
            </tr>
            <tr>
                <td align="center" style="font-weight: bold">
                    Chauliaganj, Cuttack.</td>
            </tr>
            <tr>
                <td align="center" style="font-weight: bold">
                    Pay Slip for the Month of &quot; &amp; ds.Tables(0).Rows(i).Item(&quot;month_nm&quot;) &amp; &quot;-&quot; &amp; 
                    ds.Tables(0).Rows(i).Item(&quot;pay_year&quot;) &amp; &quot;</td>
            </tr>
            <tr>
                <td bgcolor="#A4F997">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td width="50%">
                                <table style="width:100%;">
                                    <tr>
                                        <td width="30%">
                                            Employee ID</td>
                                        <td width="1%">
                                            :</td>
                                        <td width="69%">
                                            &quot; &amp; ds.Tables(0).Rows(i).Item(&quot;emp_code&quot;) &amp; &quot;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Department</td>
                                        <td>
                                            :</td>
                                        <td>
                                            &quot; &amp; ds.Tables(0).Rows(i).Item(&quot;dept_nm&quot;) &amp; &quot;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Date of Joining</td>
                                        <td>
                                            :</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Days Worked</td>
                                        <td>
                                            :</td>
                                        <td>
                                            &quot; &amp; ds.Tables(0).Rows(i).Item(&quot;pay_days&quot;) &amp; &quot;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bank Account</td>
                                        <td>
                                            :</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td width="50%">
                                <table style="width:100%;">
                                    <tr>
                                        <td width="30%">
                                            Name</td>
                                        <td width="1%">
                                            :</td>
                                        <td width="69%">
                                            &quot; &amp; ds.Tables(0).Rows(i).Item(&quot;emp_nm&quot;) &amp; &quot;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Designation</td>
                                        <td>
                                            :</td>
                                        <td>
                                            &quot; &amp; ds.Tables(0).Rows(i).Item(&quot;desg&quot;) &amp; &quot;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            PF NO.</td>
                                        <td>
                                            :</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            ESI Account No.</td>
                                        <td>
                                            :</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Father&#39;s Name</td>
                                        <td>
                                            :</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="font-size: 5px; border: 1px solid #000000">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width:100%;">
                                    <tr>
                                        <td bgcolor="#A4F997" style="border: 1px solid #000000" width="30%">
                                            Earnings</td>
                                        <td align="right" bgcolor="#A4F997" colspan="2" 
                                            style="border: 1px solid #000000" width="1%">
                                            Amount</td>
                                    </tr>
                                    <tr>
                                        <td class="style1" width="30%">
                                            Basic Pay</td>
                                        <td class="style1" width="1%">
                                        </td>
                                        <td align="right" width="69%">
                                            &quot; &amp; Format(ds.Tables(0).Rows(i).Item(&quot;basic_amt&quot;), &quot;#0.00&quot;) &amp; &quot;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            HRA</td>
                                        <td>
                                            &nbsp;</td>
                                        <td align="right">
                                            &quot; &amp; Format(ds.Tables(0).Rows(i).Item(&quot;hra_amt&quot;), &quot;#0.00&quot;) &amp; &quot;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            City Comp. Allowance</td>
                                        <td>
                                            &nbsp;</td>
                                        <td align="right">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Conveyance Allowance</td>
                                        <td>
                                            &nbsp;</td>
                                        <td align="right">
                                            &quot; &amp; Format(ds.Tables(0).Rows(i).Item(&quot;transport_amt&quot;), &quot;#0.00&quot;) &amp; &quot;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Medical Allowance</td>
                                        <td>
                                            &nbsp;</td>
                                        <td align="right">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Telephone Allowance</td>
                                        <td>
                                            &nbsp;</td>
                                        <td align="right">
                                            &quot; &amp; Format(ds.Tables(0).Rows(i).Item(&quot;mobile_amt&quot;), &quot;#0.00&quot;) &amp; &quot;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Tiffin Allowance</td>
                                        <td>
                                            &nbsp;</td>
                                        <td align="right">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Misc. Allowance</td>
                                        <td>
                                            &nbsp;</td>
                                        <td align="right">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#A4F997" style="border: 1px solid #000000">
                                            Total Earnings</td>
                                        <td align="right" bgcolor="#A4F997" colspan="2" 
                                            style="border: 1px solid #000000">
                                            &quot; &amp; Format(ds.Tables(0).Rows(i).Item(&quot;tot_earnings&quot;), &quot;#0.00&quot;) &amp; &quot;</td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#A4F997" style="border: 1px solid #000000">
                                            Net Payable</td>
                                        <td align="right" bgcolor="#A4F997" colspan="2" 
                                            style="border: 1px solid #000000">
                                            &quot; &amp; Format(ds.Tables(0).Rows(i).Item(&quot;total&quot;), &quot;#0000&quot;) &amp; &quot;.00</td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table style="width:100%;">
                                    <tr>
                                        <td bgcolor="#A4F997" style="border: 1px solid #000000" width="30%">
                                            Deduction</td>
                                        <td align="right" bgcolor="#A4F997" colspan="2" 
                                            style="border: 1px solid #000000" width="1%">
                                            Amount</td>
                                    </tr>
                                    <tr>
                                        <td class="style1" width="30%">
                                            Provident Fund</td>
                                        <td class="style1" width="1%">
                                        </td>
                                        <td align="right" class="style1" width="69%">
                                            &quot; &amp; Format(ds.Tables(0).Rows(i).Item(&quot;pf_amt&quot;), &quot;#0.00&quot;) &amp; &quot;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            ESI</td>
                                        <td>
                                            &nbsp;</td>
                                        <td align="right">
                                            &quot; &amp; Format(ds.Tables(0).Rows(i).Item(&quot;esic_amt&quot;), &quot;#0.00&quot;) &amp; &quot;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Insurance</td>
                                        <td>
                                            &nbsp;</td>
                                        <td align="right">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style1">
                                            Loan / Advanced</td>
                                        <td class="style1">
                                        </td>
                                        <td align="right" class="style1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#A4F997" style="border: 1px solid #000000">
                                            Total Deductions</td>
                                        <td align="right" bgcolor="#A4F997" colspan="2" 
                                            style="border: 1px solid #000000">
                                            &quot; &amp; Format(ds.Tables(0).Rows(i).Item(&quot;tot_deduction&quot;), &quot;#0.00&quot;) &amp; &quot;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td colspan="2">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                Employer&#39;s Singnature</td>
                            <td align="right">
                                Employee&#39;s Signature</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                This is a computer generated payslip dose not require any signature.</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
