﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO
Imports System.Data.SqlClient

Partial Class payroll_reports_expenses
    Inherits System.Web.UI.Page

    <System.Web.Script.Services.ScriptMethod(), _
System.Web.Services.WebMethod()> _
    Public Shared Function SearchEmei(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
             .ConnectionStrings("dbnm").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "select emp_code + '-' +  staf_nm  as 'nm' from staf where " & _
            "staf.emp_status='I' AND staf_nm like @SearchText + '%'"
        cmd.Parameters.AddWithValue("@SearchText", prefixText)
        cmd.Connection = conn
        conn.Open()
        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("nm").ToString)
        End While
        conn.Close()
        Return customers
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
            Me.clr()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub

    Private Sub clr()
        lblmsg.Visible = False
        txtfrmdt.Text = Format(Now.AddDays(-1), "dd/MM/yyyy")
        Me.divisiondisp()
        Me.headdisp()
        txtdivsl.Text = ""
        txtdeptcd.Text = ""
    End Sub

    Private Sub divisiondisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbdivsion.Items.Clear()
        cmbdivsion.Items.Add("Please Select A Division")
        Dim ds As DataSet = get_dataset("SELECT div_nm FROM division_mst  WHERE loc_cd=" & loc_cd & " ORDER BY div_nm")
        If ds.Tables(0).Rows.Count <> 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                cmbdivsion.Items.Add(ds.Tables(0).Rows(i).Item(0))
            Next
        End If
    End Sub

    Private Sub headdisp()
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        cmbhead.Items.Clear()
        Dim ds As DataSet = get_dataset("SELECT expense_cd,expense_name FROM Expense_Master  WHERE  active='Y' ORDER BY expense_name")
        cmbhead.DataSource = ds.Tables(0)
        cmbhead.DataTextField = "expense_name"
        cmbhead.DataValueField = "expense_cd"
        cmbhead.DataBind()
        cmbhead.Items.Insert(0, New ListItem("Select Expenses", "0"))
    End Sub

    Protected Sub cmdsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsearch.Click
        lblmsg.Text = ""
        lblmsg.Attributes("class") = "alert alert-warning"
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)

        Dim str As String = ""
        If Trim(txtdivsl.Text) <> "" Then
            str = str & "AND staf.div_sl=" & Val(txtdivsl.Text) & ""
        End If
        If Trim(txtempcode.Text) <> "" Then
            str = "AND staf.emp_code='" & txtempcode.Text & "'"
        End If
        If Val(cmbhead.SelectedValue) <> 0 Then
            str = str & "AND Expense_Entry.expense_cd=" & Val(cmbhead.SelectedValue) & ""
        End If
        Dim dsmonthy As DataSet = get_dataset("SELECT CAST(ROW_NUMBER() OVER (ORDER BY expense_dt) as varchar) as SL,COnvert(varchar,Expense_Entry.expense_dt,103) AS Date, staf.emp_code AS [Emp. Code], staf.staf_nm AS [Staf Name], Expense_Master.expense_name AS Expenses, Expense_Entry.expense_desc AS Description,Str(Expense_Entry.amount,12,2) AS Amount FROM Expense_Entry INNER JOIN Expense_Master ON Expense_Entry.expense_cd = Expense_Master.expense_cd LEFT OUTER JOIN staf ON Expense_Entry.staf_sl = staf.staf_sl WHERE month(expense_dt)=" & stringtodate(txtfrmdt.Text).Month & " AND year(expense_dt)=" & stringtodate(txtfrmdt.Text).Year & " " & str & " ORDER BY expense_dt")
        Dim dsmonthysum As DataSet = get_dataset("SELECT Str(isnull(sum(Expense_Entry.amount),0),12,2) AS Amount FROM Expense_Entry INNER JOIN Expense_Master ON Expense_Entry.expense_cd = Expense_Master.expense_cd LEFT OUTER JOIN staf ON Expense_Entry.staf_sl = staf.staf_sl WHERE month(expense_dt)=" & stringtodate(txtfrmdt.Text).Month & " AND year(expense_dt)=" & stringtodate(txtfrmdt.Text).Year & " " & str & "")
        Dim dt As New DataTable
        dt = dsmonthy.Tables(0)
        dt.Rows.Add("", "", "", "", "", "Total :", Format(Val(dsmonthysum.Tables(0).Rows(0).Item(0)), "#####0.00"))

        If dt.Rows.Count <> 0 Then
            Dim GridView112 As New GridView()
            GridView112.AllowPaging = False
            GridView112.DataSource = dt
            GridView112.DataBind()
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=MonthlyExpenseRegister.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            For i As Integer = 0 To GridView112.Rows.Count - 1
                GridView112.Rows(i).Attributes.Add("class", "textmode")
            Next
            GridView112.RenderControl(hw)
            'style to format numbers to string
            Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()
        End If
    End Sub

    Protected Sub cmbdivsion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbdivsion.SelectedIndexChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        txtdivsl.Text = ""
        Dim ds1 As DataSet = get_dataset("SELECT div_sl FROM division_mst WHERE div_nm='" & Trim(cmbdivsion.Text) & "' AND loc_cd=" & loc_cd & "")
        If ds1.Tables(0).Rows.Count <> 0 Then
            txtdivsl.Text = ds1.Tables(0).Rows(0).Item(0)
        End If
    End Sub

    Protected Sub txtempcode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtempcode.TextChanged
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        If Trim(txtempcode.Text) <> "" Then
            Dim emp_code As String() = Trim(txtempcode.Text).Split("-")
            txtempcode.Text = emp_code(0)
            Dim ds1 As DataSet = get_dataset("SELECT staf.emp_code, staf.staf_nm, dept_mst.dept_nm, desg_mst.desg_nm, location_mst.loc_nm, staf.loc_cd FROM location_mst RIGHT OUTER JOIN staf ON location_mst.loc_cd = staf.loc_cd LEFT OUTER JOIN desg_mst ON staf.desg_sl = desg_mst.desg_sl LEFT OUTER JOIN dept_mst ON staf.dept_sl = dept_mst.dept_sl WHERE staf.emp_code = '" & Trim(txtempcode.Text) & "' AND staf.loc_cd=" & loc_cd & "")
            If ds1.Tables(0).Rows.Count <> 0 Then
                txtempcode.Text = ds1.Tables(0).Rows(0).Item("emp_code")
            Else
                txtempcode.Text = ""
            End If
        End If
    End Sub
End Class
