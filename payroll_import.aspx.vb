﻿Imports System.Data
Imports vb = Microsoft.VisualBasic
Imports System.IO

Partial Class payroll_import
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.seq()
        End If
    End Sub

    Private Sub seq()
        Dim usr_sl As Integer = CType(Session("usr_sl"), Integer)
        If usr_sl = 0 Then
            Response.Redirect("Default.aspx")
        End If
    End Sub


    ' EVOS CALCULATION *******************************

    ''Protected Sub cmdget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdget.Click
    ''    Dim str1 As String
    ''    Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
    ''    str1 = FileUpload1.FileName.ToString
    ''    If str1 <> "" Then
    ''        If File.Exists(Server.MapPath(".\Import\") & loc_cd & "_" & FileUpload1.FileName) Then
    ''            File.Delete(Server.MapPath(".\Import\") & loc_cd & "_" & FileUpload1.FileName)
    ''        End If
    ''        FileUpload1.SaveAs(Server.MapPath(".\Import\") & loc_cd & "_" & FileUpload1.FileName)
    ''    End If
    ''    Dim c_excelconstring As String = String.Empty
    ''    Dim uploadpath As String = "~/Import/"
    ''    Dim filepath As String = Server.MapPath(uploadpath & loc_cd & "_" & FileUpload1.PostedFile.FileName)
    ''    Dim filext As String = Path.GetExtension(loc_cd & "_" & FileUpload1.PostedFile.FileName)
    ''    If filext = ".xls" OrElse filext = "XLS" Then
    ''        c_excelconstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & Trim(filepath) & "'" & ";Extended Properties=""Excel 8.0;HDR=Yes'"
    ''    ElseIf filext = ".xlsx" OrElse filext = "XLSX" Then
    ''        c_excelconstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Trim(filepath) & ";Extended Properties=Excel 12.0;Persist Security Info=False"
    ''    End If
    ''    If c_excelconstring <> "" Then
    ''        'EVOS ********************************************************************************
    ''        Dim dt As New DataTable
    ''        dt.Columns.Add("Sl", GetType(Integer))
    ''        dt.Columns.Add("E.Code", GetType(String))
    ''        dt.Columns.Add("Name", GetType(String))
    ''        dt.Columns.Add("Designation", GetType(String))
    ''        dt.Columns.Add("Department", GetType(String))
    ''        dt.Columns.Add("Month", GetType(String))
    ''        dt.Columns.Add("Year", GetType(String))
    ''        dt.Columns.Add("CTC", GetType(String))
    ''        dt.Columns.Add("Salary Payable Days", GetType(String))
    ''        dt.Columns.Add("Basic Salary", GetType(String))
    ''        dt.Columns.Add("HRA", GetType(String))
    ''        dt.Columns.Add("Washing All", GetType(String))
    ''        dt.Columns.Add("Special Allowance", GetType(String))
    ''        dt.Columns.Add("GROSS TOTAL", GetType(String))
    ''        dt.Columns.Add("EPF", GetType(String))
    ''        dt.Columns.Add("ESIC", GetType(String))
    ''        dt.Columns.Add("PT", GetType(String))
    ''        dt.Columns.Add("TDS", GetType(String))
    ''        dt.Columns.Add("Advance", GetType(String))
    ''        dt.Columns.Add("Other Deduction", GetType(String))
    ''        dt.Columns.Add("Total Deduction", GetType(String))
    ''        dt.Columns.Add("Arrears", GetType(String))
    ''        dt.Columns.Add("Net Salary", GetType(String))
    ''        dt.Columns.Add("EPF ER", GetType(String))
    ''        dt.Columns.Add("ESIC ER", GetType(String))
    ''        dt.Columns.Add("Remark", GetType(String))


    ''        Dim ds As DataSet = excel_dataset("SELECT * FROM [Sheet1$]", c_excelconstring)
    ''        start1()
    ''        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
    ''            Dim staf_sl As Integer = 0
    ''            Dim E_Code As String = ""
    ''            Dim Name As String = ""
    ''            Dim Designation As String = ""
    ''            Dim Department As String = ""
    ''            Dim Month As Integer = "1"
    ''            Dim Year As String = ""
    ''            Dim CTC As Decimal = "0"
    ''            Dim tot_days As Integer = "0"
    ''            Dim Salary_Payable_Days As Decimal = "0"
    ''            Dim Basic_Salary As Decimal = "0"
    ''            Dim HRA As Decimal = "0"
    ''            Dim Childre_edu As Decimal = "0"
    ''            Dim Special_Allowance As Decimal = "0"
    ''            Dim GROSS_TOTAL As Decimal = "0"
    ''            Dim EPF As Decimal = "0"
    ''            Dim ESIC As Decimal = "0"
    ''            Dim PT As Decimal = "0"
    ''            Dim TDS As Decimal = "0"
    ''            Dim Advance As Decimal = "0"
    ''            Dim Other_Deduction As Decimal = "0"
    ''            Dim Total_Deduction As Decimal = "0"
    ''            Dim Arrears As Decimal = "0"
    ''            Dim Net_Salary As Decimal = "0"
    ''            Dim EPF_ER As Decimal = "0"
    ''            Dim ESIC_ER As Decimal = "0"
    ''            Dim msg As String = "Added Successfully"

    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(5)) Then
    ''                For mnth As Integer = 1 To 12
    ''                    If ds.Tables(0).Rows(i).Item(5) = MonthName(mnth) Then
    ''                        Month = mnth
    ''                        Exit For
    ''                    End If
    ''                Next
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(6)) Then
    ''                Year = ds.Tables(0).Rows(i).Item(6)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(1)) Then
    ''                E_Code = ds.Tables(0).Rows(i).Item(1)
    ''                Dim dsstaf As DataSet = get_dataset("SELECT staf_sl FROM staf WHERE emp_code='" & E_Code & "'")
    ''                If dsstaf.Tables(0).Rows.Count <> 0 Then
    ''                    staf_sl = dsstaf.Tables(0).Rows(0).Item(0)

    ''                    start1()
    ''                    SQLInsert("DELETE FROM monthly_pay WHERE pay_month=" & Month & " AND pay_year=" & Year & " AND loc_cd=" & loc_cd & " AND staf_sl=" & staf_sl & "")
    ''                    SQLInsert("DELETE FROM head_assignment1 WHERE staf_sl=" & staf_sl & " AND ass_month=" & Month & " AND ass_year=" & Year & "")
    ''                    SQLInsert("DELETE FROM head_assignment2 WHERE staf_sl=" & staf_sl & " AND ass_month=" & Month & " AND ass_year=" & Year & "")
    ''                    close1()
    ''                End If
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(2)) Then
    ''                Name = ds.Tables(0).Rows(i).Item(2)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(3)) Then
    ''                Designation = ds.Tables(0).Rows(i).Item(3)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(4)) Then
    ''                Department = ds.Tables(0).Rows(i).Item(4)
    ''            End If

    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(7)) Then
    ''                CTC = ds.Tables(0).Rows(i).Item(7)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(13)) Then
    ''                tot_days = ds.Tables(0).Rows(i).Item(13)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(16)) Then
    ''                Salary_Payable_Days = ds.Tables(0).Rows(i).Item(16)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(17)) Then
    ''                Basic_Salary = ds.Tables(0).Rows(i).Item(17)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(18)) Then
    ''                HRA = ds.Tables(0).Rows(i).Item(18)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(19)) Then
    ''                Childre_edu = ds.Tables(0).Rows(i).Item(19)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(20)) Then
    ''                Special_Allowance = ds.Tables(0).Rows(i).Item(20)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(21)) Then
    ''                GROSS_TOTAL = ds.Tables(0).Rows(i).Item(21)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(22)) Then
    ''                EPF = ds.Tables(0).Rows(i).Item(22)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(23)) Then
    ''                ESIC = ds.Tables(0).Rows(i).Item(23)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(24)) Then
    ''                PT = ds.Tables(0).Rows(i).Item(24)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(25)) Then
    ''                TDS = ds.Tables(0).Rows(i).Item(25)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(26)) Then
    ''                Advance = ds.Tables(0).Rows(i).Item(26)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(27)) Then
    ''                Other_Deduction = ds.Tables(0).Rows(i).Item(27)
    ''                Dim dshead As DataSet = get_dataset("SELECT ass_no from head_assignment1 WHERE staf_sl=" & Val(staf_sl) & " AND ass_month=" & Month & " AND ass_year=" & Year & "")
    ''                start1()
    ''                If dshead.Tables(0).Rows.Count = 0 Then
    ''                    Dim max_head As Integer = 1
    ''                    Dim ds1 As DataSet = get_dataset("SELECT max(ass_no) FROM head_assignment1")
    ''                    If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
    ''                        max_head = ds1.Tables(0).Rows(0).Item(0) + 1
    ''                    End If
    ''                    SQLInsert("INSERT INTO head_assignment1(ass_no,staf_sl,ass_month,ass_year,tot_earning,tot_deduction) VALUES(" & Val(max_head) & "," & Val(staf_sl) & _
    ''                    "," & Month & "," & Year & ",0,0)")

    ''                    Dim max_sl As Integer = 1
    ''                    Dim ds11 As DataSet = get_dataset("SELECT max(ass_sl) FROM head_assignment2")
    ''                    If Not IsDBNull(ds11.Tables(0).Rows(0).Item(0)) Then
    ''                        max_sl = ds11.Tables(0).Rows(0).Item(0) + 1
    ''                    End If
    ''                    SQLInsert("INSERT INTO head_assignment2(ass_sl,ass_no,staf_sl,ass_month,ass_year,ass_dt,head_sl,tp,amt) VALUES(" & max_sl & "," & Val(max_head) & _
    ''                    "," & Val(staf_sl) & "," & Month & "," & Year & ",'" & Year & "-" & Format(Month, 0) & "-01',2,'2'," & Val(Other_Deduction) & ")")
    ''                Else
    ''                    Dim max_sl As Integer = 1
    ''                    Dim ds11 As DataSet = get_dataset("SELECT max(ass_sl) FROM head_assignment2")
    ''                    If Not IsDBNull(ds11.Tables(0).Rows(0).Item(0)) Then
    ''                        max_sl = ds11.Tables(0).Rows(0).Item(0) + 1
    ''                    End If
    ''                    SQLInsert("INSERT INTO head_assignment2(ass_sl,ass_no,staf_sl,ass_month,ass_year,ass_dt,head_sl,tp,amt) VALUES(" & max_sl & "," & Val(dshead.Tables(0).Rows(0).Item("ass_no")) & _
    ''                    "," & Val(staf_sl) & "," & Month & "," & Year & ",'" & Year & "-" & Format(Month, 0) & "-01',2,'2'," & Val(Other_Deduction) & ")")
    ''                End If
    ''                close1()
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(28)) Then
    ''                Total_Deduction = ds.Tables(0).Rows(i).Item(28)
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(29)) Then
    ''                Arrears = ds.Tables(0).Rows(i).Item(29)
    ''                Dim dshead As DataSet = get_dataset("SELECT ass_no from head_assignment1 WHERE staf_sl=" & Val(staf_sl) & " AND ass_month=" & Month & " AND ass_year=" & Year & "")
    ''                start1()
    ''                If dshead.Tables(0).Rows.Count = 0 Then
    ''                    Dim max_head As Integer = 1
    ''                    Dim ds1 As DataSet = get_dataset("SELECT max(ass_no) FROM head_assignment1")
    ''                    If Not IsDBNull(ds1.Tables(0).Rows(0).Item(0)) Then
    ''                        max_head = ds1.Tables(0).Rows(0).Item(0) + 1
    ''                    End If
    ''                    SQLInsert("INSERT INTO head_assignment1(ass_no,staf_sl,ass_month,ass_year,tot_earning,tot_deduction) VALUES(" & Val(max_head) & "," & Val(staf_sl) & _
    ''                    "," & Month & "," & Year & ",0,0)")

    ''                    Dim max_sl As Integer = 1
    ''                    Dim ds11 As DataSet = get_dataset("SELECT max(ass_sl) FROM head_assignment2")
    ''                    If Not IsDBNull(ds11.Tables(0).Rows(0).Item(0)) Then
    ''                        max_sl = ds11.Tables(0).Rows(0).Item(0) + 1
    ''                    End If
    ''                    SQLInsert("INSERT INTO head_assignment2(ass_sl,ass_no,staf_sl,ass_month,ass_year,ass_dt,head_sl,tp,amt) VALUES(" & max_sl & "," & Val(max_head) & _
    ''                    "," & Val(staf_sl) & "," & Month & "," & Year & ",'" & Year & "-" & Format(Month, 0) & "-01',1,'1'," & Val(Arrears) & ")")
    ''                Else
    ''                    Dim max_sl As Integer = 1
    ''                    Dim ds11 As DataSet = get_dataset("SELECT max(ass_sl) FROM head_assignment2")
    ''                    If Not IsDBNull(ds11.Tables(0).Rows(0).Item(0)) Then
    ''                        max_sl = ds11.Tables(0).Rows(0).Item(0) + 1
    ''                    End If
    ''                    SQLInsert("INSERT INTO head_assignment2(ass_sl,ass_no,staf_sl,ass_month,ass_year,ass_dt,head_sl,tp,amt) VALUES(" & max_sl & "," & Val(dshead.Tables(0).Rows(0).Item("ass_no")) & _
    ''                    "," & Val(staf_sl) & "," & Month & "," & Year & ",'" & Year & "-" & Format(Month, 0) & "-01',1,'1'," & Val(Arrears) & ")")
    ''                End If
    ''                close1()
    ''            End If
    ''            If Not IsDBNull(ds.Tables(0).Rows(i).Item(30)) Then
    ''                Net_Salary = ds.Tables(0).Rows(i).Item(30)
    ''            End If
    ''            'If Not IsDBNull(ds.Tables(0).Rows(i).Item(31)) Then
    ''            '    EPF_ER = ds.Tables(0).Rows(i).Item(31)
    ''            'End If
    ''            'If Not IsDBNull(ds.Tables(0).Rows(i).Item(32)) Then
    ''            '    ESIC_ER = ds.Tables(0).Rows(i).Item(32)
    ''            'End If
    ''            If staf_sl <> 0 Then
    ''                start1()
    ''                SQLInsert("INSERT INTO monthly_pay(staf_sl,pay_month,pay_year,loc_cd,gross,basic_amt,da_amt,hra_amt,transport_amt,child_amt,city_amt," & _
    ''                "spcl_amt,pf_amt,comp_pf_amt,esic_amt,comp_esic_amt,pt_amt,tds_amt,loan_amt,othr_earnings,othr_deuction,tot_earnings,tot_deduction,wages," & _
    ''                "tot_reimbursment,total,pay_days,tot_days,tot_present,tot_half,tot_week,tot_absnt,tot_leave,tot_holi,l1,l2,l3,l4,l5,l6,washing_amt) VALUES(" & staf_sl & _
    ''                "," & Month & "," & Year & "," & loc_cd & "," & CTC & "," & Basic_Salary & ",0," & HRA & ",0," & Childre_edu & ",0," & Special_Allowance & "," & EPF & "," & EPF_ER & _
    ''                "," & ESIC & "," & ESIC_ER & "," & PT & "," & TDS & "," & Advance & "," & Arrears & "," & Other_Deduction & "," & Basic_Salary + HRA + Special_Allowance + Arrears + Childre_edu & _
    ''                "," & PT + TDS + EPF + ESIC + Other_Deduction + Advance & ",0,0," & Net_Salary & "," & Salary_Payable_Days & "," & tot_days & "," & Salary_Payable_Days & ",0,0," & tot_days - Salary_Payable_Days & _
    ''                ",0,0,0,0,0,0,0,0,0)")
    ''                SQLInsert("UPDATE head_assignment1 SET tot_earning=" & Val(Arrears) & ",tot_deduction=" & Val(Other_Deduction) & " WHERE staf_sl=" & Val(staf_sl) & " AND ass_month=" & Month & " AND ass_year=" & Year & "")
    ''                dt.Rows.Add(i, E_Code, Name, Designation, Department, Month, Year, CTC, Salary_Payable_Days, Basic_Salary, HRA, Childre_edu, Special_Allowance, GROSS_TOTAL, EPF, ESIC, PT, TDS, Advance, Other_Deduction, Total_Deduction, Arrears, Net_Salary, EPF_ER, ESIC_ER, msg)
    ''                close1()
    ''            Else
    ''                dt.Rows.Add(i, E_Code, Name, Designation, Department, Month, Year, CTC, Salary_Payable_Days, Basic_Salary, HRA, Childre_edu, Special_Allowance, GROSS_TOTAL, EPF, ESIC, PT, TDS, Advance, Other_Deduction, Total_Deduction, Arrears, Net_Salary, EPF_ER, ESIC_ER, "Invalid Employee")
    ''            End If
    ''        Next
    ''        close1()
    ''        Dim gridview1 As New GridView()
    ''        gridview1.AllowPaging = False
    ''        gridview1.DataSource = dt
    ''        gridview1.DataBind()
    ''        Response.Clear()
    ''        Response.Buffer = True
    ''        Response.AddHeader("content-disposition", "attachment;filename=IMPORTRESULT.xls")
    ''        Response.Charset = ""
    ''        Response.ContentType = "application/vnd.ms-excel"
    ''        Dim sw As New StringWriter()
    ''        Dim hw As New HtmlTextWriter(sw)
    ''        For i As Integer = 0 To dt.Rows.Count - 1
    ''            gridview1.Rows(i).Attributes.Add("class", "textmode")
    ''        Next
    ''        gridview1.RenderControl(hw)

    ''        Dim style As String = "<style> .textmode { mso-number-format :\@} </style>"
    ''        Response.Write(style)
    ''        Response.Output.Write(sw.ToString())
    ''        Response.Flush()
    ''        Response.End()
    ''    End If
    ''End Sub

    'MANJARI Calculation ***************************
    Protected Sub cmdget121212_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdget.Click
        Dim str1 As String
        Dim loc_cd As Integer = CType(Session("loc_cd"), Integer)
        str1 = FileUpload1.FileName.ToString
        If str1 <> "" Then
            If File.Exists(Server.MapPath(".\Import\") & loc_cd & "_" & FileUpload1.FileName) Then
                File.Delete(Server.MapPath(".\Import\") & loc_cd & "_" & FileUpload1.FileName)
            End If
            FileUpload1.SaveAs(Server.MapPath(".\Import\") & loc_cd & "_" & FileUpload1.FileName)
        End If
        Dim c_excelconstring As String = String.Empty
        Dim uploadpath As String = "~/Import/"
        Dim filepath As String = Server.MapPath(uploadpath & loc_cd & "_" & FileUpload1.PostedFile.FileName)
        Dim filext As String = Path.GetExtension(loc_cd & "_" & FileUpload1.PostedFile.FileName)
        If filext = ".xls" OrElse filext = "XLS" Then
            c_excelconstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & Trim(filepath) & "'" & ";Extended Properties=""Excel 8.0;HDR=Yes'"
        ElseIf filext = ".xlsx" OrElse filext = "XLSX" Then
            c_excelconstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Trim(filepath) & ";Extended Properties=Excel 12.0;Persist Security Info=False"
        End If
        If c_excelconstring <> "" Then
            ''EVOS ********************************************************************************
            'Dim dt As New DataTable
            'dt.Columns.Add("Sl", GetType(Integer))
            'dt.Columns.Add("E.Code", GetType(String))
            'dt.Columns.Add("Name", GetType(String))
            'dt.Columns.Add("Month", GetType(String))
            'dt.Columns.Add("Year", GetType(String))
            'dt.Columns.Add("CTC", GetType(String))
            'dt.Columns.Add("Salary Payable Days", GetType(String))
            'dt.Columns.Add("Basic Salary", GetType(String))
            'dt.Columns.Add("HRA", GetType(String))
            'dt.Columns.Add("Conveyance Allow", GetType(String))
            'dt.Columns.Add("Mobile", GetType(String))
            'dt.Columns.Add("Other Allowance", GetType(String))
            'dt.Columns.Add("GROSS TOTAL", GetType(String))
            'dt.Columns.Add("EPF", GetType(String))
            'dt.Columns.Add("ESIC", GetType(String))
            'dt.Columns.Add("Advance", GetType(String))
            'dt.Columns.Add("Total Deduction", GetType(String))
            'dt.Columns.Add("Net Salary", GetType(String))
            'dt.Columns.Add("Remark", GetType(String))

            'Dim ds As DataSet = excel_dataset("SELECT * FROM [Sheet1$]", c_excelconstring)
            'start1()
            'For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            '    Dim staf_sl As Integer = 0
            '    Dim E_Code As String = ""
            '    Dim Name As String = ""
            '    Dim Designation As String = ""
            '    Dim Department As String = ""
            '    Dim Month As Integer = "1"
            '    Dim Year As String = ""
            '    Dim CTC As Decimal = "0"
            '    Dim tot_days As Integer = "0"
            '    Dim Salary_Payable_Days As Decimal = "0"
            '    Dim Basic_Salary As Decimal = "0"
            '    Dim HRA As Decimal = "0"
            '    Dim transport As Decimal = "0"
            '    Dim other_Allowance As Decimal = "0"
            '    Dim mobile As Decimal = "0"
            '    Dim GROSS_TOTAL As Decimal = "0"
            '    Dim EPF As Decimal = "0"
            '    Dim ESIC As Decimal = "0"
            '    Dim PT As Decimal = "0"
            '    Dim TDS As Decimal = "0"
            '    Dim Advance As Decimal = "0"
            '    Dim Other_Deduction As Decimal = "0"
            '    Dim Total_Deduction As Decimal = "0"
            '    Dim Arrears As Decimal = "0"
            '    Dim Net_Salary As Decimal = "0"
            '    Dim EPF_ER As Decimal = "0"
            '    Dim ESIC_ER As Decimal = "0"
            '    Dim msg As String = "Added Successfully"

            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(3)) Then
            '        For mnth As Integer = 1 To 12
            '            If ds.Tables(0).Rows(i).Item(3) = MonthName(mnth) Then
            '                Month = mnth
            '                Exit For
            '            End If
            '        Next
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(4)) Then
            '        Year = ds.Tables(0).Rows(i).Item(4)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(1)) Then
            '        E_Code = ds.Tables(0).Rows(i).Item(1)
            '        Dim dsstaf As DataSet = get_dataset("SELECT staf_sl FROM staf WHERE emp_code='" & E_Code & "'")
            '        If dsstaf.Tables(0).Rows.Count <> 0 Then
            '            staf_sl = dsstaf.Tables(0).Rows(0).Item(0)

            '            start1()
            '            SQLInsert("DELETE FROM monthly_pay WHERE pay_month=" & Month & " AND pay_year=" & Year & " AND loc_cd=" & loc_cd & " AND staf_sl=" & staf_sl & "")
            '            SQLInsert("DELETE FROM head_assignment1 WHERE staf_sl=" & staf_sl & " AND ass_month=" & Month & " AND ass_year=" & Year & "")
            '            SQLInsert("DELETE FROM head_assignment2 WHERE staf_sl=" & staf_sl & " AND ass_month=" & Month & " AND ass_year=" & Year & "")
            '            close1()
            '        End If
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(2)) Then
            '        Name = ds.Tables(0).Rows(i).Item(2)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(5)) Then
            '        CTC = ds.Tables(0).Rows(i).Item(5)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(6)) Then
            '        tot_days = ds.Tables(0).Rows(i).Item(6)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(7)) Then
            '        Salary_Payable_Days = ds.Tables(0).Rows(i).Item(7)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(8)) Then
            '        Basic_Salary = ds.Tables(0).Rows(i).Item(8)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(9)) Then
            '        HRA = ds.Tables(0).Rows(i).Item(9)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(10)) Then
            '        transport = ds.Tables(0).Rows(i).Item(10)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(11)) Then
            '        mobile = ds.Tables(0).Rows(i).Item(11)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(12)) Then
            '        other_Allowance = ds.Tables(0).Rows(i).Item(12)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(13)) Then
            '        GROSS_TOTAL = ds.Tables(0).Rows(i).Item(13)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(14)) Then
            '        EPF = ds.Tables(0).Rows(i).Item(14)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(15)) Then
            '        ESIC = ds.Tables(0).Rows(i).Item(15)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(16)) Then
            '        Advance = ds.Tables(0).Rows(i).Item(16)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(17)) Then
            '        PT = ds.Tables(0).Rows(i).Item(17)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(18)) Then
            '        Total_Deduction = ds.Tables(0).Rows(i).Item(18)
            '    End If
            '    If Not IsDBNull(ds.Tables(0).Rows(i).Item(19)) Then
            '        Net_Salary = ds.Tables(0).Rows(i).Item(19)
            '    End If

            '    If staf_sl <> 0 Then
            '        start1()
            '        SQLInsert("INSERT INTO monthly_pay(staf_sl,pay_month,pay_year,loc_cd,gross,basic_amt,da_amt,hra_amt,transport_amt,child_amt,city_amt," & _
            '        "spcl_amt,pf_amt,comp_pf_amt,esic_amt,comp_esic_amt,pt_amt,tds_amt,loan_amt,othr_earnings,othr_deuction,tot_earnings,tot_deduction,wages," & _
            '        "tot_reimbursment,total,pay_days,tot_days,tot_present,tot_half,tot_week,tot_absnt,tot_leave,tot_holi,l1,l2,l3,l4,l5,l6,washing_amt,mobile_amt) VALUES(" & staf_sl & _
            '        "," & Month & "," & Year & "," & loc_cd & "," & CTC & "," & Basic_Salary & ",0," & HRA & "," & transport & ",0,0," & other_Allowance & "," & EPF & "," & EPF_ER & _
            '        "," & ESIC & "," & ESIC_ER & "," & PT & "," & TDS & "," & Advance & "," & Arrears & "," & Other_Deduction & "," & Basic_Salary + HRA + other_Allowance + Arrears + transport + mobile & _
            '        "," & PT + TDS + EPF + ESIC + Other_Deduction + Advance + pt & ",0,0," & Net_Salary & "," & Salary_Payable_Days & "," & tot_days & "," & Salary_Payable_Days & ",0,0," & tot_days - Salary_Payable_Days & _
            '        ",0,0,0,0,0,0,0,0,0," & mobile & ")")
            '        SQLInsert("UPDATE head_assignment1 SET tot_earning=" & Val(Arrears) & ",tot_deduction=" & Val(Other_Deduction) & " WHERE staf_sl=" & Val(staf_sl) & " AND ass_month=" & Month & " AND ass_year=" & Year & "")
            '        dt.Rows.Add(i, E_Code, Name, Month, Year, CTC, Salary_Payable_Days, Basic_Salary, HRA, transport, mobile, other_Allowance, GROSS_TOTAL, EPF, ESIC, Advance, Total_Deduction, Net_Salary, msg)
            '        close1()
            '    Else
            '        dt.Rows.Add(i, E_Code, Name, Month, Year, CTC, Salary_Payable_Days, Basic_Salary, HRA, transport, mobile, other_Allowance, GROSS_TOTAL, EPF, ESIC, Advance, Total_Deduction, Net_Salary, "Invalid Employee")
            '    End If
            'Next
            'close1()


            Dim dt As New DataTable
            dt.Columns.Add("Sl", GetType(Integer))
            dt.Columns.Add("E.Code", GetType(String))
            dt.Columns.Add("Name", GetType(String))
            dt.Columns.Add("Month", GetType(String))
            dt.Columns.Add("Year", GetType(String))
            dt.Columns.Add("Tot Days", GetType(String))
            dt.Columns.Add("Salary Payable Days", GetType(String))
            dt.Columns.Add("CTC", GetType(String))
            dt.Columns.Add("Basic Salary", GetType(String))
            dt.Columns.Add("DA", GetType(String))
            dt.Columns.Add("HRA", GetType(String))
            dt.Columns.Add("Conveyance Allow", GetType(String))
            dt.Columns.Add("Children", GetType(String))
            dt.Columns.Add("City", GetType(String))
            dt.Columns.Add("Mobile", GetType(String))
            dt.Columns.Add("Medical", GetType(String))
            dt.Columns.Add("Food", GetType(String))
            dt.Columns.Add("Special Allowance", GetType(String))
            dt.Columns.Add("Others", GetType(String))
            dt.Columns.Add("GROSS TOTAL", GetType(String))
            dt.Columns.Add("EPF", GetType(String))
            dt.Columns.Add("Comp EPF", GetType(String))
            dt.Columns.Add("ESIC", GetType(String))
            dt.Columns.Add("Comp ESIC", GetType(String))
            dt.Columns.Add("PT", GetType(String))
            dt.Columns.Add("TDS", GetType(String))
            dt.Columns.Add("Insurance", GetType(String))
            dt.Columns.Add("Advance/Loan", GetType(String))
            dt.Columns.Add("Others Deduction", GetType(String))
            dt.Columns.Add("Total Deduction", GetType(String))
            dt.Columns.Add("Net Salary", GetType(String))
            dt.Columns.Add("Remark", GetType(String))

            Dim ds As DataSet = excel_dataset("SELECT * FROM [Sheet1$]", c_excelconstring)
            start1()
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim staf_sl As Integer = 0
                Dim E_Code As String = ""
                Dim Name As String = ""
                Dim Designation As String = ""
                Dim Department As String = ""
                Dim Month As Integer = "1"
                Dim Year As String = ""
                Dim CTC As Decimal = "0"
                Dim tot_days As Integer = "0"
                Dim Salary_Payable_Days As Decimal = "0"
                Dim Basic_Salary As Decimal = "0"
                Dim HRA As Decimal = "0"
                Dim transport As Decimal = "0"
                Dim earned_Allowance As Decimal = "0"
                Dim special_Allowance As Decimal = "0"
                Dim mobile As Decimal = "0"
                Dim da As Decimal = "0"
                Dim city As Decimal = "0"
                Dim children As Decimal = "0"
                Dim food As Decimal = "0"
                Dim medical As Decimal = "0"
                Dim GROSS_TOTAL As Decimal = "0"
                Dim EPF As Decimal = "0"
                Dim ESIC As Decimal = "0"
                Dim Comp_EPF As Decimal = "0"
                Dim Comp_ESIC As Decimal = "0"
                Dim PT As Decimal = "0"
                Dim insurance As Decimal = "0"
                Dim TDS As Decimal = "0"
                Dim Advance As Decimal = "0"
                Dim Other_Deduction As Decimal = "0"
                Dim Total_Deduction As Decimal = "0"
                Dim Arrears As Decimal = "0"
                Dim Net_Salary As Decimal = "0"
                Dim EPF_ER As Decimal = "0"
                Dim ESIC_ER As Decimal = "0"
                Dim msg As String = "Added Successfully"

                If Not IsDBNull(ds.Tables(0).Rows(i).Item(3)) Then
                    For mnth As Integer = 1 To 12
                        If ds.Tables(0).Rows(i).Item(3) = MonthName(mnth) Then
                            Month = mnth
                            Exit For
                        End If
                    Next
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(4)) Then
                    Year = ds.Tables(0).Rows(i).Item(4)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(1)) Then
                    E_Code = ds.Tables(0).Rows(i).Item(1)
                    Dim dsstaf As DataSet = get_dataset("SELECT staf_sl FROM staf WHERE emp_code='" & E_Code & "'")
                    If dsstaf.Tables(0).Rows.Count <> 0 Then
                        staf_sl = dsstaf.Tables(0).Rows(0).Item(0)

                        start1()
                        SQLInsert("DELETE FROM monthly_pay WHERE pay_month=" & Month & " AND pay_year=" & Year & " AND loc_cd=" & loc_cd & " AND staf_sl=" & staf_sl & "")
                        SQLInsert("DELETE FROM head_assignment1 WHERE staf_sl=" & staf_sl & " AND ass_month=" & Month & " AND ass_year=" & Year & "")
                        SQLInsert("DELETE FROM head_assignment2 WHERE staf_sl=" & staf_sl & " AND ass_month=" & Month & " AND ass_year=" & Year & "")
                        close1()
                    End If
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(2)) Then
                    Name = ds.Tables(0).Rows(i).Item(2)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(7)) Then
                    CTC = ds.Tables(0).Rows(i).Item(7)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(5)) Then
                    tot_days = ds.Tables(0).Rows(i).Item(5)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(6)) Then
                    Salary_Payable_Days = ds.Tables(0).Rows(i).Item(6)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(8)) Then
                    Basic_Salary = ds.Tables(0).Rows(i).Item(8)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(9)) Then
                    da = ds.Tables(0).Rows(i).Item(9)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(10)) Then
                    HRA = ds.Tables(0).Rows(i).Item(10)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(11)) Then
                    transport = ds.Tables(0).Rows(i).Item(11)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(12)) Then
                    children = ds.Tables(0).Rows(i).Item(12)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(13)) Then
                    city = ds.Tables(0).Rows(i).Item(13)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(14)) Then
                    mobile = ds.Tables(0).Rows(i).Item(14)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(15)) Then
                    medical = ds.Tables(0).Rows(i).Item(15)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(16)) Then
                    food = ds.Tables(0).Rows(i).Item(16)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(17)) Then
                    special_Allowance = ds.Tables(0).Rows(i).Item(17)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(18)) Then
                    earned_Allowance = ds.Tables(0).Rows(i).Item(18)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(19)) Then
                    GROSS_TOTAL = ds.Tables(0).Rows(i).Item(19)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(20)) Then
                    EPF = ds.Tables(0).Rows(i).Item(20)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(21)) Then
                    Comp_EPF = ds.Tables(0).Rows(i).Item(21)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(22)) Then
                    ESIC = ds.Tables(0).Rows(i).Item(22)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(23)) Then
                    Comp_ESIC = ds.Tables(0).Rows(i).Item(23)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(24)) Then
                    PT = ds.Tables(0).Rows(i).Item(24)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(25)) Then
                    TDS = ds.Tables(0).Rows(i).Item(25)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(26)) Then
                    insurance = ds.Tables(0).Rows(i).Item(26)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(27)) Then
                    Advance = ds.Tables(0).Rows(i).Item(27)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(28)) Then
                    Other_Deduction = ds.Tables(0).Rows(i).Item(28)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(29)) Then
                    Total_Deduction = ds.Tables(0).Rows(i).Item(29)
                End If
                If Not IsDBNull(ds.Tables(0).Rows(i).Item(30)) Then
                    Net_Salary = ds.Tables(0).Rows(i).Item(30)
                End If

                If staf_sl <> 0 Then
                    start1()
                    SQLInsert("INSERT INTO monthly_pay(staf_sl,pay_month,pay_year,loc_cd,gross,basic_amt,da_amt,hra_amt,transport_amt,child_amt,city_amt," & _
                    "spcl_amt,pf_amt,comp_pf_amt,esic_amt,comp_esic_amt,pt_amt,tds_amt,loan_amt,othr_earnings,othr_deuction,tot_earnings,tot_deduction,wages," & _
                    "tot_reimbursment,total,pay_days,tot_days,tot_present,tot_half,tot_week,tot_absnt,tot_leave,tot_holi,l1,l2,l3,l4,l5,l6,washing_amt,mobile_amt,food_amt,medical,insurance_amt) VALUES(" & staf_sl & _
                    "," & Month & "," & Year & "," & loc_cd & "," & CTC & "," & Basic_Salary & "," & da & "," & HRA & "," & transport & "," & children & "," & city & "," & special_Allowance & "," & EPF & "," & Comp_EPF & _
                    "," & ESIC & "," & Comp_ESIC & "," & PT & "," & TDS & "," & Advance & "," & Arrears & "," & Other_Deduction & "," & GROSS_TOTAL & _
                    "," & Total_Deduction & ",0,0," & Net_Salary & "," & Salary_Payable_Days & "," & tot_days & "," & Salary_Payable_Days & ",0,0," & tot_days - Salary_Payable_Days & _
                    ",0,0,0,0,0,0,0,0,0," & mobile & "," & food & "," & medical & "," & insurance & ")")

                    'SQLInsert("UPDATE head_assignment1 SET tot_earning=" & Val(Arrears) & ",tot_deduction=" & Val(Other_Deduction) & " WHERE staf_sl=" & Val(staf_sl) & " AND ass_month=" & Month & " AND ass_year=" & Year & "")
                    dt.Rows.Add(i, E_Code, Name, Month, Year, CTC, tot_days, Salary_Payable_Days, Basic_Salary, da, HRA, transport, children, city, mobile, medical, food, special_Allowance, earned_Allowance, GROSS_TOTAL, EPF, Comp_EPF, ESIC, Comp_ESIC, PT, TDS, insurance, Advance, Other_Deduction, Total_Deduction, Net_Salary, msg)
                    close1()
                Else
                    dt.Rows.Add(i, E_Code, Name, Month, Year, CTC, tot_days, Salary_Payable_Days, Basic_Salary, da, HRA, transport, children, city, mobile, medical, food, special_Allowance, earned_Allowance, GROSS_TOTAL, EPF, Comp_EPF, ESIC, Comp_ESIC, PT, TDS, insurance, Advance, Other_Deduction, Total_Deduction, Net_Salary, "Invalid Employee")
                End If
            Next
            close1()


            Dim gridview1 As New GridView()
            gridview1.AllowPaging = False
            gridview1.DataSource = dt
            gridview1.DataBind()
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=IMPORTRESULT.xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            For i As Integer = 0 To dt.Rows.Count - 1
                gridview1.Rows(i).Attributes.Add("class", "textmode")
            Next
            gridview1.RenderControl(hw)

            Dim style As String = "<style> .textmode { mso-number-format :\@} </style>"
            Response.Write(style)
            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.End()
        End If
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Response.Redirect("Payroll_Import.xlsx")
    End Sub
End Class
