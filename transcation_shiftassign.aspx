﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="transcation_shiftassign.aspx.vb" Inherits="transcation_shiftassign" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="A1" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                      aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="transcation_shiftassign.aspx">Add New Shift Assignment</a>
                      <a class="dropdown-item" href="transcation_shiftassign.aspx?mode=V">View Shift List</a>
                    
                    </div>
                  </div>
                </div>

                     <div class="card-body">
                                       <table style="width:100%;">
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnlview" runat="server">
                                          <table style="width: 100%;">
                                                                                          <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel2" runat="server">
                                                          <div style="width: 100%; height: 600px">
                                                              <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="15" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <Columns>
                                                                  <asp:TemplateField HeaderText="Division">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblDivision" Text='<%#Eval("div_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Emp. Id">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblEmpId" Text='<%#Eval("emp_code") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Employee Name">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblStaffName" Text='<%#Eval("staf_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Date">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblDate" Text='<%#Eval("dt") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Shift Name">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblInTime" Text='<%#Eval("shift_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                 
                                                                   <asp:TemplateField HeaderText="slno" Visible="False">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblslno" Text='<%#Eval("sl") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                      <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                                                          ImageUrl="images/delete.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                                                      <ItemStyle Height="30px" Width="30px" />
                                                                      </asp:ButtonField>
                                                                  </Columns>
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>
                                                          </div>
                                                      </asp:Panel>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table width="100%">
                                         
                                              <tr>
                                                  <td>
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="45%">
                                                                  Division</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  Department</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:DropDownList ID="cmbdivsion" runat="server" AutoPostBack="True" 
                                                                      class="form-control">
                                                                  </asp:DropDownList>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:DropDownList ID="cmbdept" runat="server" AutoPostBack="True" 
                                                                      class="form-control">
                                                                  </asp:DropDownList>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  From Date</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  To Date</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtfrom" runat="server" class="form-control"></asp:TextBox>
                                                                  <asp:CalendarExtender ID="txtfrom_CalendarExtender" runat="server" 
                                                                      Enabled="True" Format="dd/MM/yyyy" PopupButtonID="txtfrom" 
                                                                      TargetControlID="txtfrom">
                                                                  </asp:CalendarExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtto" runat="server" class="form-control"></asp:TextBox>
                                                                  <asp:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" 
                                                                      Format="dd/MM/yyyy" PopupButtonID="txtto" TargetControlID="txtto">
                                                                  </asp:CalendarExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td style="font-size: 8px">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td height="8">
                                                      Shift Name</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:DropDownList ID="cmbshift" runat="server" CssClass="form-control">
                                                      </asp:DropDownList>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <div style="width: 100%; height: 300px; overflow: auto;">
                                                          <asp:GridView ID="dvstaf" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                              AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                              PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                              <AlternatingRowStyle CssClass="alt" />
                                                              <Columns>
                                                                  <asp:TemplateField>
                                                                  <HeaderTemplate>
                                                                   <asp:CheckBox  runat="Server" ID="chk1" AutoPostBack="True" OnCheckedChanged="CheckBox1_CheckedChanged"/>
                                                                  </HeaderTemplate>
                                                                  <ItemTemplate>
                                                                  <asp:CheckBox  runat="Server" ID="chk"/>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Employee ID">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblempid" Text='<%#Eval("emp_code") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Device Code">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lbldevice_code" Text='<%#Eval("device_code") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Employee Name">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblstaf_nm" Text='<%#Eval("staf_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField Visible="False">
                                                                  <ItemTemplate>
                                                                 <asp:Label runat="server" ID="lblstaf_sl" Text='<%#Eval("staf_sl") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                              </Columns>
                                                              <PagerStyle HorizontalAlign="Right" />
                                                          </asp:GridView>
                                                      </div>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:Button ID="cmdsave" runat="server" class="btn btn-primary" Text="Submit" />
                                                      <asp:Button ID="cmdclear" runat="server" CausesValidation="false" 
                                                          class="btn btn-default" Text="Reset" />
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtmode" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtdeptcd" runat="server" Height="22px" Visible="False" 
                                                          Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txtdivsl" runat="server" Height="22px" Visible="False" 
                                                          Width="20px"></asp:TextBox>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>       
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

