﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="transcation_usbdownload.aspx.vb" Inherits="transcation_usbdownload" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
               
                </div>

                     <div class="card-body">
                                     <table style="width:100%;">
                             <tr><div class="alert bg-success" id="divmsg" runat="server"> 
                            <asp:Label ID="lblmsg" runat="server" Text="Label"></asp:Label>
                                                    </div></tr>
                              <tr>
                                  <td>
                                      <table style="width:100%;">
                                          <tr>
                                              <td style="50%" valign="top">
                                                  <div __designer:mapid="d7">
                                                      <asp:GridView ID="dv1" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                          AutGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                          Width="98%">
                                                          <AlternatingRowStyle CssClass="alt" />
                                                          <Columns>
                                                              <asp:TemplateField>
                                                                  <ItemTemplate>
                                                                      <asp:CheckBox ID="chk" runat="server" />
                                                                  </ItemTemplate>
                                                              </asp:TemplateField>
                                                          </Columns>
                                                          <PagerStyle HorizontalAlign="Right" />
                                                      </asp:GridView>
                                                  </div>
                                              </td>
                                              <td width="50%">
                                                  <table style="width:100%;">
                                                      <tr>
                                                          <td>
                                                              <asp:CheckBox ID="chk1" runat="server" Text="All Data" />
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              &nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              From Date</td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                                  <asp:TextBox ID="txtfrom" runat="server" 
                                            class="form-control"></asp:TextBox>
                                                                  <asp:CalendarExtender ID="txtfrom_CalendarExtender" runat="server" 
                                                                      Enabled="True" Format="dd/MM/yyyy" PopupButtonID="txtfrom" 
                                                                      TargetControlID="txtfrom">
                                                                  </asp:CalendarExtender>
                                                              </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              &nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              To Date</td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                                  <asp:TextBox ID="txtto" runat="server" 
                                            class="form-control"></asp:TextBox>
                                                                  <asp:CalendarExtender ID="txtto_CalendarExtender" 
                                            runat="server" Enabled="True" 
                                                                      Format="dd/MM/yyyy" PopupButtonID="txtto" 
                                            TargetControlID="txtto">
                                                                  </asp:CalendarExtender>
                                                              </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              &nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              &nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                        <asp:Button ID="cmdget" runat="server" CausesValidation="False" 
                                            class="btn btn-success" Text="Upload &amp; Get File" Width="100%" />
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              &nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                      <asp:Button ID="cmdsave" runat="server" class="btn btn-primary" 
                                            Text="Import" Width="100%" />
                                                          </td>
                                                      </tr>
                                                  </table>
                                              </td>
                                          </tr>
                                      </table>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                              <tr>
                                  <td>
                                      <div __designer:mapid="d7">
                                          <asp:GridView ID="dv2" runat="server" AlternatingRowStyle-CssClass="alt" 
                                              AutGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                              Width="100%">
                                              <AlternatingRowStyle CssClass="alt" />
                                              <PagerStyle HorizontalAlign="Right" />
                                          </asp:GridView>
                                      </div>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>     
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

