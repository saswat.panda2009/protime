﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="tour_expenseapproval.aspx.vb" Inherits="tour_expenseapproval" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                     <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Basic Form-->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                   <h3 class="h4"><asp:Label ID="lblhdr" runat="server" Text="Label"></asp:Label></h3>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="A1" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                      aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      
                      <a class="dropdown-item" href="transcation_outdoor.aspx?mode=V">View Tour List</a>
                    
                    </div>
                  </div>
                </div>

                     <div class="card-body">
                                             <table style="width:100%;">

                              <tr>
                                  <td>
                                      <asp:Panel ID="pnlview" runat="server">
                                          <table style="width: 100%;">
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:Panel ID="Panel2" runat="server">
                                                          <div style="width: 100%; height: 600px">
                                                              <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                                                                  AlternatingRowStyle-CssClass="alt" AutGenerateColumns="False" 
                                                                  AutoGenerateColumns="False" CssClass="Grid" PagerStyle-CssClass="pgr" 
                                                                  PageSize="15" Width="100%">
                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                  <Columns>
                                                                   <asp:TemplateField HeaderText="Sl">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblsl" Text='<%#Eval("Sl") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Employee Name">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblstafnm" Text='<%#Eval("staf_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Emp. Code">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblempcode" Text='<%#Eval("emp_code") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Department">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lbldept_nm" Text='<%#Eval("dept_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Designation">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lbldesg" Text='<%#Eval("desg_nm") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Tittle">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblDate" Text='<%#Eval("travel_tittle") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="From Date">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblInTime" Text='<%#Eval("Sdt") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="To Date">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblOutTime" Text='<%#Eval("edt") %>'></asp:Label>3
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Description">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lbldescr" Text='<%#Eval("travel_descr") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="slno" Visible="False">
                                                                  <ItemTemplate>
                                                                  <asp:Label runat="server" ID="lblslno" Text='<%#Eval("travel_sl") %>'></asp:Label>
                                                                  </ItemTemplate>
                                                                  </asp:TemplateField>
                                                                  <asp:ButtonField ButtonType="Image" CommandName="edit_state" 
                                                                          ImageUrl="images/edit.png" ItemStyle-Height="30px" ItemStyle-Width="30px">
                                                                      <ItemStyle Height="30px" Width="30px" />
                                                                      </asp:ButtonField>
                                                                      
                                                                  </Columns>
                                                                  <PagerStyle HorizontalAlign="Right" />
                                                              </asp:GridView>
                                                          </div>
                                                      </asp:Panel>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <asp:Panel ID="pnladd" runat="server">
                                          <table width="100%">                                           
                                                                                      <tr>
                                                  <td>
                                                      <table width="100%">
                                                       <tr>
                                                              <td width="45%">
                                                                  Name</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <asp:TextBox ID="txtstafnm" runat="server" class="form-control" MaxLength="100" 
                                                                      BackColor="White" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtstafnm_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtstafnm">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                              <td style="font-size: 8px">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Department</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  Designation</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtdept" runat="server" BackColor="White" class="form-control" 
                                                                      MaxLength="100" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdept_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdept">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtdesg" runat="server" BackColor="White" class="form-control" 
                                                                      MaxLength="100" ReadOnly="True"></asp:TextBox>
                                                                  <asp:FilteredTextBoxExtender ID="txtdesg_FilteredTextBoxExtender" 
                                                                      runat="server" Enabled="True" FilterMode="InvalidChars" InvalidChars="'" 
                                                                      TargetControlID="txtdesg">
                                                                  </asp:FilteredTextBoxExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td width="45%">
                                                                  Tour Tittle</td>
                                                              <td width="10%">
                                                                  &nbsp;</td>
                                                              <td width="45%">
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <asp:TextBox ID="txttittle" runat="server" CssClass="form-control"></asp:TextBox>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  Tour Description</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td colspan="3">
                                                                  <asp:TextBox ID="txtdescription" runat="server" CssClass="form-control"></asp:TextBox>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  From Date</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  To Date</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txtfrom" runat="server" class="form-control"></asp:TextBox>
                                                                  <asp:CalendarExtender ID="txtfrom_CalendarExtender" runat="server" 
                                                                      Enabled="True" Format="dd/MM/yyyy" PopupButtonID="txtfrom" 
                                                                      TargetControlID="txtfrom">
                                                                  </asp:CalendarExtender>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtto" runat="server" class="form-control"></asp:TextBox>
                                                                  <asp:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" 
                                                                      Format="dd/MM/yyyy" PopupButtonID="txtto" TargetControlID="txtto">
                                                                  </asp:CalendarExtender>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                             <tr>
                                                                  <td  style="color: #000000; font-weight: bold; font-size: 25px;" 
                                                                                                          valign="middle">
                                                                                                          Total Expense :
                                                                                                          <asp:Label ID="lbltotexpense" runat="server" Text="0.00"></asp:Label>
                                                                                                      </td>
                                                                 <td>
                                                                     &nbsp;</td>
                                                                 <td>
                                                                    <asp:Image ID="Image1" runat="server" Height="250px" Width="250px" 
                                                                                  /></td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <asp:TextBox ID="txttotalexpense1" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                                  <asp:TextBox ID="txttotalexpense2" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                                  <asp:TextBox ID="txttotalexpense3" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                                  <asp:TextBox ID="txttotalexpense4" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                              </td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                             <tr>
                                                              <td style="color: #000000; font-weight: bold; font-size: 25px;">
                                                                  Long Distance Fare</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                </td>
                                                                </tr>
                                                                  <tr>
                                                                      <td colspan="3" style="color: #000000; font-weight: bold; font-size: 25px;">
                                                                          <div style="font-size: 15px">
                                                                              <asp:GridView ID="dvexpense1" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                                                  AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                                                  PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                                  <Columns>
                                                                                      <asp:TemplateField HeaderText="Sl">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblslno0" runat="server" Text='<%#Eval("slno") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="3%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Date">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lbldt" runat="server" Text='<%#Eval("dt") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="10%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Particulars">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_det" runat="server" Text='<%#Eval("expense_det") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="20%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Mode">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_mode" runat="server" 
                                                                                                  Text='<%#Eval("expense_mode") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="From">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_from" runat="server" 
                                                                                                  Text='<%#Eval("expense_from") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="13.5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="To">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_to" runat="server" Text='<%#Eval("expense_to") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="13.5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Amount">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblamt" runat="server" Text='<%#Eval("amt") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Bill No">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_bill" runat="server" 
                                                                                                  Text='<%#Eval("expense_bill") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Attachment">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblatch" runat="server" Text='<%#Eval("atch") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField Visible="False">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblsl6" runat="server" Text='<%#Eval("sl") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Remarks">
                                                                                      <ItemTemplate>
                                                                                      <asp:TextBox ID="txtremarks" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                      </ItemTemplate>
                                                                                          <ItemStyle Width="15%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:ButtonField ButtonType="Image" CommandName="view_data" 
                                                                                          ImageUrl="~/images/view.png" Text="Button">
                                                                                      <HeaderStyle Width="30px" />
                                                                                      <ItemStyle HorizontalAlign="Right"/>
                                                                                      </asp:ButtonField>
                                                                                       <asp:ButtonField ButtonType="Image" CommandName="delete_data" 
                                                                                          ImageUrl="~/images/reject.png" Text="Button">
                                                                                      <HeaderStyle Width="30px" />
                                                                                      <ItemStyle HorizontalAlign="Right"/>
                                                                                      </asp:ButtonField>
                                                                                       <asp:ButtonField ButtonType="Image" CommandName="approved_data" 
                                                                                          ImageUrl="~/images/approved1.png" Text="Button">
                                                                                      <HeaderStyle Width="30px" />
                                                                                      <ItemStyle HorizontalAlign="Right"/>
                                                                                      </asp:ButtonField>
                                                                                  </Columns>
                                                                                  <PagerStyle HorizontalAlign="Right" />
                                                                              </asp:GridView>
                                                                          </div>
                                                                      </td>
                                                                  </tr>

                                                                     <tr>
                                                              <td style="color: #000000; font-weight: bold; font-size: 25px;">
                                                                  Lodging Expenses</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                </td>
                                                                </tr>
                                                                  <tr>
                                                                      <td colspan="3" style="color: #000000; font-weight: bold; font-size: 25px;">
                                                                          <div style="font-size: 15px">
                                                                              <asp:GridView ID="dvexpense2" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                                                  AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                                                  PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                                  <Columns>
                                                                                      <asp:TemplateField HeaderText="Sl">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblslno1" runat="server" Text='<%#Eval("slno") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="3%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Date">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lbldt5" runat="server" Text='<%#Eval("dt") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="10%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Hotel / Lodge Name">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_det1" runat="server" Text='<%#Eval("expense_det") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="20%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="From">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_from1" runat="server" 
                                                                                                  Text='<%#Eval("expense_from") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="13.5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="To">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_to1" runat="server" Text='<%#Eval("expense_to") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="13.5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="No Of Days">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_days1" runat="server" 
                                                                                                  Text='<%#Eval("expense_days") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Amount">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblamt1" runat="server" Text='<%#Eval("amt") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Bill No">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_bill1" runat="server" 
                                                                                                  Text='<%#Eval("expense_bill") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Attachment">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblatch1" runat="server" Text='<%#Eval("atch") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField Visible="False">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblsl6" runat="server" Text='<%#Eval("sl") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                      </asp:TemplateField>
                                                                                       <asp:TemplateField HeaderText="Remarks">
                                                                                      <ItemTemplate>
                                                                                      <asp:TextBox ID="txtremarks" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                      </ItemTemplate>
                                                                                          <ItemStyle Width="15%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:ButtonField ButtonType="Image" CommandName="view_data" 
                                                                                          ImageUrl="~/images/view.png" Text="Button">
                                                                                      <HeaderStyle Width="30px" />
                                                                                      <ItemStyle HorizontalAlign="Right"/>
                                                                                      </asp:ButtonField>
                                                                                       <asp:ButtonField ButtonType="Image" CommandName="delete_data" 
                                                                                          ImageUrl="~/images/reject.png" Text="Button">
                                                                                      <HeaderStyle Width="30px" />
                                                                                      <ItemStyle HorizontalAlign="Right"/>
                                                                                      </asp:ButtonField>
                                                                                       <asp:ButtonField ButtonType="Image" CommandName="approved_data" 
                                                                                          ImageUrl="~/images/approved1.png" Text="Button">
                                                                                      <HeaderStyle Width="30px" />
                                                                                      <ItemStyle HorizontalAlign="Right"/>
                                                                                      </asp:ButtonField>
                                                                                  </Columns>
                                                                                  <PagerStyle HorizontalAlign="Right" />
                                                                              </asp:GridView>
                                                                          </div>
                                                                      </td>
                                                                  </tr>

                                                                     <tr>
                                                              <td style="color: #000000; font-weight: bold; font-size: 25px;">
                                                                  Conveyance While Tour</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                </td>
                                                                </tr>
                                                                  <tr>
                                                                      <td colspan="3" style="color: #000000; font-weight: bold; font-size: 25px;">
                                                                          <div style="font-size: 15px">
                                                                              <asp:GridView ID="dvexpense3" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                                                  AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                                                  PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                                  <Columns>
                                                                                      <asp:TemplateField HeaderText="Sl">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblslno2" runat="server" Text='<%#Eval("slno") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="3%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Date">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lbldt6" runat="server" Text='<%#Eval("dt") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="10%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Particulars">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_det2" runat="server" Text='<%#Eval("expense_det") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="20%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Mode">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_mode2" runat="server" 
                                                                                                  Text='<%#Eval("expense_mode") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="From">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_from2" runat="server" 
                                                                                                  Text='<%#Eval("expense_from") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="13.5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="To">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_to2" runat="server" Text='<%#Eval("expense_to") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="13.5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Amount">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblamt2" runat="server" Text='<%#Eval("amt") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Bill No">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_bill2" runat="server" 
                                                                                                  Text='<%#Eval("expense_bill") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Attachment">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblatch2" runat="server" Text='<%#Eval("atch") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="5%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField Visible="False">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblsl6" runat="server" Text='<%#Eval("sl") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Remarks">
                                                                                          <ItemTemplate>
                                                                                              <asp:TextBox ID="txtremarks" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="15%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:ButtonField ButtonType="Image" CommandName="view_data" 
                                                                                          ImageUrl="~/images/view.png" Text="Button">
                                                                                      <HeaderStyle Width="30px" />
                                                                                      <ItemStyle HorizontalAlign="Right" />
                                                                                      </asp:ButtonField>
                                                                                      <asp:ButtonField ButtonType="Image" CommandName="delete_data" 
                                                                                          ImageUrl="~/images/reject.png" Text="Button">
                                                                                      <HeaderStyle Width="30px" />
                                                                                      <ItemStyle HorizontalAlign="Right" />
                                                                                      </asp:ButtonField>
                                                                                      <asp:ButtonField ButtonType="Image" CommandName="approved_data" 
                                                                                          ImageUrl="~/images/approved1.png" Text="Button">
                                                                                      <HeaderStyle Width="30px" />
                                                                                      <ItemStyle HorizontalAlign="Right" />
                                                                                      </asp:ButtonField>
                                                                                  </Columns>
                                                                                  <PagerStyle HorizontalAlign="Right" />
                                                                              </asp:GridView>
                                                                          </div>
                                                                      </td>
                                                                  </tr>

                                                                     <tr>
                                                              <td style="color: #000000; font-weight: bold; font-size: 25px;">
                                                                  Fooding Expenses</td>
                                                              <td>
                                                                  &nbsp;</td>
                                                              <td>
                                                                </td>
                                                                </tr>
                                                                  <tr>
                                                                      <td colspan="3" style="color: #000000; font-weight: bold; font-size: 25px;">
                                                                          <div style="font-size: 15px">
                                                                              <asp:GridView ID="GridView2" runat="server" AlternatingRowStyle-CssClass="alt" 
                                                                                  AutGenerateColumns="False" AutoGenerateColumns="False" CssClass="Grid" 
                                                                                  PagerStyle-CssClass="pgr" PageSize="15" Width="100%">
                                                                                  <AlternatingRowStyle CssClass="alt" />
                                                                                  <Columns>
                                                                                      <asp:TemplateField HeaderText="Sl">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblslno4" runat="server" Text='<%#Eval("slno") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="3%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Date">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lbldt7" runat="server" Text='<%#Eval("dt") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="10%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Amount">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblamt4" runat="server" Text='<%#Eval("amt") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="10%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Remarks">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblexpense_det4" runat="server" Text='<%#Eval("expense_det") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="42%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Attachment">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblatch" runat="server" Text='<%#Eval("atch") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                          <HeaderStyle Width="5%" />
                                                                                          <ItemStyle Width="15%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField Visible="False">
                                                                                          <ItemTemplate>
                                                                                              <asp:Label ID="lblsl6" runat="server" Text='<%#Eval("sl") %>'></asp:Label>
                                                                                          </ItemTemplate>
                                                                                      </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Remarks">
                                                                                          <ItemTemplate>
                                                                                              <asp:TextBox ID="txtremarks" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                          </ItemTemplate>
                                                                                          <ItemStyle Width="15%" />
                                                                                      </asp:TemplateField>
                                                                                      <asp:ButtonField ButtonType="Image" CommandName="view_data" 
                                                                                          ImageUrl="~/images/view.png" Text="Button">
                                                                                      <HeaderStyle Width="30px" />
                                                                                      <ItemStyle HorizontalAlign="Right" />
                                                                                      </asp:ButtonField>
                                                                                      <asp:ButtonField ButtonType="Image" CommandName="delete_data" 
                                                                                          ImageUrl="~/images/reject.png" Text="Button">
                                                                                      <HeaderStyle Width="30px" />
                                                                                      <ItemStyle HorizontalAlign="Right" />
                                                                                      </asp:ButtonField>
                                                                                      <asp:ButtonField ButtonType="Image" CommandName="approved_data" 
                                                                                          ImageUrl="~/images/approved1.png" Text="Button">
                                                                                      <HeaderStyle Width="30px" />
                                                                                      <ItemStyle HorizontalAlign="Right" />
                                                                                      </asp:ButtonField>
                                                                                  </Columns>
                                                                                  <PagerStyle HorizontalAlign="Right" />
                                                                              </asp:GridView>
                                                                          </div>
                                                                      </td>
                                                                  </tr>
                                                                
                                                      </table>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:Button ID="cmdsave" runat="server" class="btn btn-primary" 
                                                          Text="Report Employee" />
                                                      &nbsp;<asp:Button ID="cmdsave0" runat="server" class="btn btn-primary" 
                                                          Text="Approve" />
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td>
                                                      <asp:TextBox ID="txtmode" runat="server" Visible="False" Width="20px"></asp:TextBox>
                                                      <asp:TextBox ID="txttoursl" runat="server" Height="22px" Visible="False" 
                                                          Width="20px"></asp:TextBox>
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      &nbsp;</td>
                              </tr>
                          </table>          
                    </div>
                  </div>
                </div>
               
              </div>
            </div>
          </section>
          <br />
</asp:Content>

